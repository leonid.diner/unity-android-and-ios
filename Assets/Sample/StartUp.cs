﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class StartUp : MonoBehaviour {
	public GameObject text;
	private bool tokenSent;

	// Use this for initialization
	void Start () {
		Application.runInBackground = true;
		Screen.orientation = ScreenOrientation.Portrait;
		DontDestroyOnLoad (this);
		AppsFlyer.setIsDebug(true);

        AppsFlyer.setHost("qatest", "whappsflyer.com");
        AppsFlyer.setMinTimeBetweenSessions(10);
        AppsFlyer.trackAppLaunch();


        //		Dictionary<string, string> eventValue = new Dictionary<string,string> ();
        //		eventValue.Add("af_revenue","300");
        //		eventValue.Add("af_content_type","category_a");
        //		eventValue.Add("af_content_id","1234567");
        //		eventValue.Add("af_currency","USD");
        //		Dictionary<string, string> extraData = new Dictionary<string, string>();
        //		extraData.Add("san_gate", "true");
        ////		AppsFlyer.setAdditionalData(extraData);
        //		AppsFlyer.trackRichEvent("af_purchase", eventValue);


        // EXAMPLE 1
        //Dictionary<string, Dictionary<string, string>> pushMessage = new Dictionary<string, Dictionary<string, string>>();
        //Dictionary<string, string> dict = new Dictionary<string, string>();
        //dict.Add("c", "campUnity");
        //dict.Add("is_retargeting", "true");
        //dict.Add("pid", "mediaUnity");
        //pushMessage.Add("hello", dict);

        // EXAMPLE 2
        //Dictionary<string, Dictionary<string, string>> pushMessage = new Dictionary<string, Dictionary<string, string>>();
        //Dictionary<string, string> myDict;
        //string key = "hello";
        //if (!pushMessage.TryGetValue(key, out myDict))
        //{
        //    myDict = new Dictionary<string, string>();
        //    pushMessage.Add(key, myDict);
        //}
        //myDict.Add("tom", "cat");

        // EXAMPLE 3
        //Dictionary<string, string> pushMessage = new Dictionary<string, string>();
        //pushMessage.Add("c", "campaignUnity");
        //pushMessage.Add("pid", "mediaUnity");
        //pushMessage.Add("is_retargeting", "true");
        //AppsFlyer.handlePushNotification(pushMessage);




#if UNITY_IOS

		Dictionary<string, string> extraData1 = new Dictionary<string, string>();
		extraData1.Add("san_gate", "true");
		AppsFlyer.setAdditionalData(extraData1);

		AppsFlyer.setAppsFlyerKey ("ZYkfwosfGsnfNFKyxxBBwN");
		AppsFlyer.setAppID ("3002003271");
		AppsFlyer.setIsDebug (true);
		AppsFlyer.getConversionData ();
        AppsFlyer.setCustomerUserID("iosUserSetter");
        AppsFlyer.getAppsFlyerId();
        print("this is my app id - " + AppsFlyer.getAppsFlyerId());
        Debug.Log("this is my app id - " + AppsFlyer.getAppsFlyerId());

        //Custom event name
        Dictionary<string, string> eventname = new Dictionary<string, string>();
        eventname.Add("customNameios", "customEventios");
        AppsFlyer.trackRichEvent("iosCustomEventName", eventname);


        //AppsFlyer.setHost("support.appsflyer.com");
        AppsFlyer.trackAppLaunch ();
		AppsFlyer.stopTracking(false);
        AppsFlyer.trackCrossPromoteImpression("com.appsflyer.leoniddiner.organicinstall", "campaignCrossPromotionIOS");



        // register to push notifications for iOS uninstall
        UnityEngine.iOS.NotificationServices.RegisterForNotifications (UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound);
		Screen.orientation = ScreenOrientation.Portrait;



#elif UNITY_ANDROID
                AppsFlyer.init("ZYkfwosfGsnfNFKyxxBBwN");

//		Dictionary<string, string> extraData1 = new Dictionary<string, string>();
//		extraData1.Add("san_gate", "true");
//		extraData1.Add("customData1", "1");
//		AppsFlyer.setAdditionalData(extraData1);
        AppsFlyer.setIsDebug(true);
		AppsFlyer.setAppID ("com.appsflyer.unityproject"); 

		// for getting the conversion data
        AppsFlyer.getConversionData ();
		AppsFlyer.loadConversionData("StartUp");

		AppsFlyer.setCollectIMEI(true);
		AppsFlyer.setCollectAndroidID(true);
		AppsFlyer.stopTracking(false);

		// for in app billing validation
		 AppsFlyer.createValidateInAppListener ("AppsFlyerTrackerCallbacks", "onInAppBillingSuccess", "onInAppBillingFailure"); 

		//For Android Uninstall
		//AppsFlyer.setGCMProjectNumber ("YOUR_GCM_PROJECT_NUMBER");

		AppsFlyer.setCustomerUserID ("UnityCustomerID");

        // CrossPromotion & Store Open
       

        AppsFlyer.trackCrossPromoteImpression("com.appsflyer.leoniddiner.organicinstall", "campaignCrossPromotion");
        //click - https://app.appsflyer.com/com.appsflyer.unityproject?pid=unitymedia&c=unitycampaign&advertiser_id=5f9f9f85-93ce-4171-8635-4cfa1c639190

#endif
    }


    // Update is called once per frame
    void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
            //go to background when pressing back button
#if UNITY_ANDROID
			AndroidJavaObject activity = 
				new AndroidJavaClass("com.unity3d.player.UnityPlayer")
					.GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call<bool>("moveTaskToBack", true);

#endif
        }
        
#if UNITY_IOS
		if (!tokenSent) { 
			byte[] token = UnityEngine.iOS.NotificationServices.deviceToken;           
			if (token != null) {     
			//For iOS uninstall
				AppsFlyer.registerUninstall (token);
				tokenSent = true;
			}
		}    
#endif
    }
	//A custom event tracking
	public void Purchase(){
		Dictionary<string, string> eventValue = new Dictionary<string,string> ();
		eventValue.Add("af_revenue","300");
		eventValue.Add("af_content_type","category_a");
		eventValue.Add("af_content_id","1234567");
		//eventValue.Add("af_currency","USD");

		Dictionary<string, string> extraData = new Dictionary<string, string>();
		extraData.Add("san_gate", "true");
		AppsFlyer.setAdditionalData(extraData);
        AppsFlyer.setCurrencyCode("GBP");

        AppsFlyer.setAppInviteOneLinkID("3J2f");
        AppsFlyer.trackRichEvent("af_purchase", eventValue);



        Dictionary<string, string> customDict = new Dictionary<string, string>();
        customDict.Add("af_revenue", "50");
        customDict.Add("af_content_type", "store_open");
        customDict.Add("af_content_id", "storeID");
        //AppsFlyer.trackAndOpenStore("102938777", "storeopencampaignios", customDict);

        //doesn't open - com.ea.game.simcitymobile_row
        //opens - com.noodlecake.ssg2
        //opens - com.appsflyer.analytics
        //com.appsflyer.referrerSender

       




#if UNITY_IOS
        Dictionary<string, string> extraData1 = new Dictionary<string, string>();
		extraData1.Add("anton", "true");
		AppsFlyer.validateReceipt("blabla", "10", "USD", "123456", extraData1);
        Dictionary<string, string> inviteDic = new Dictionary<string, string>();
        inviteDic.Add("channel", "InviteTestChannelUnity");
        inviteDic.Add("customerID", "InviteTestCustomerIDUnity");
        inviteDic.Add("campaign", "AFinviteCampaignUnity");
        inviteDic.Add("af_sub1", "SubParam1");
        inviteDic.Add("af_sub2", "SubParam2");
        AppsFlyer.generateUserInviteLink(inviteDic, "callbackObject", "callbackMethod", "callbackFailedMethod");


        AppsFlyer.setIsSandbox(true);

        Dictionary<string, string> moreParams = new Dictionary<string, string>();
        moreParams.Add("channel", "validateReceipt");
        moreParams.Add("customerID", "customerValidate");
        moreParams.Add("campaign", "validateCampaign");
       

        //AppsFlyer.validateReceipt(AppsFlyer.getAppsFlyerId., "123", "GBP", "tranID", moreParams );


#elif UNITY_ANDROID

        string publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3dkBTr2pD2YSqSK2ewlEWwH9Llu0iA4PkwgVOyNRxOsHfrOlqi0Cm51qdNS0aqh/SMZkuQTAroqH3pAr9gVFOiejKRw+ymTaL5wB9+5n1mAbdeO2tv2FDsbawDvp7u6fIBejYt7Dtmih+kcu707fEO58HZWqgzx9qSHmrkMZr6yvHCtAhdBLwSBBjyhPHy7RAwKA+PE+HYVV2UNb5urqIZ9eI1dAv3RHX/xxHVHRJcjnTyMAqBmfFM+o31tp8/1CxGIazVN6HpVk8Qi2uqSS5HdKUu6VnIK8VuAHQbXQn4bG6GXx5Tp0SX1fKrejo7hupNUCgOlqsYHFYxsRkEOi0QIDAQAB";
        string purchaseData = "{\"orderId\":\"GPA.3385-5761-9579-49494\",\"packageName\":\"com.appsflyer.testapp\",\"productId\":\"consumable\",\"purchaseTime\":1503494211553,\"purchaseState\":0,\"developerPayload\":\"{\\n  \\\"name\\\": \\\"CoinsPack2\\\",\\n  \\\"quantity\\\": 0\\n}\",\"purchaseToken\":\"hljadjjmkfmdandoobebgpci.AO-J1OzmYh0Ws8VrTvN17HdI8Q_qW0gtWLyteQyWhX8iA6ibInC6ipQ7Qy34jYj5eJ5K43BzsSfpTy1EWezkUywNsf-NsaskGJMd5FpVHSUQhrkM4GLJZyhvPSepjj-P3K9vuI9xttUf\"}";
        string signature = "Lg2mc3yma023YBm2KZdxAIofQM2k61MsE5ZaUML/ik/1tc59+Ai5+aHQIWRML3b7h4+UudRa5wafA/WlCXlAtOB2o2Su4YqwCnnWjFeRatDimfISOTiEp/4Wk3tY9MqqRNiy+hvj2GJSdQxd25mbuXY2v1ghTGpDdXoHXqVCCTxA3aYxwjZtBT1aL+2Nu4yj/MbHYtFg/CBfcL2CUlF+QnCkaMTGqpSXpWH8K0HvuQW/bmqBKUmPUt9D3Gzk8RfmU/OdCchHxDTrX2KPafJ2gOvGrCbxKuycVY1XctlqljN9qbJC6nYHSPEmgy1VbhLOy0xipO1EeWHEqheULk3b0Q==";

        Dictionary<string, string> extraData2 = new Dictionary<string, string>();
		extraData2.Add("vaysberg", "true");
		AppsFlyer.validateReceipt(publicKey, purchaseData, signature, "10", "USD", extraData2);

        // FOR INVITE
        Dictionary<string,string> inviteDic = new Dictionary<string,string>();
        inviteDic.Add ("channel","InviteTestChannelUnity");
        inviteDic.Add ("customerID","InviteTestCustomerIDUnity");
        inviteDic.Add ("campaign", "AFinviteCampaignUnity");
        inviteDic.Add ("af_sub1", "SubParam1");
        inviteDic.Add ("af_sub2", "SubParam2");
        AppsFlyer.generateUserInviteLink(inviteDic, "callbackObject", "callbackMethod", "callbackFailedMethod");


#endif

        AF_Sample_BGScript.pressed ();

	}
	//On Android ou can call the conversion data directly from your CS file, or from the default AppsFlyerTrackerCallbacks
	public void didReceiveConversionData(string conversionData) {
		print ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
		if (conversionData.Contains ("Non")) {
			text.GetComponent<Text> ().text = "Non-Organic Install";
		} else {
			text.GetComponent<Text> ().text = "Organic Install";
		}	
	}
	public void didReceiveConversionDataWithError(string error) {
		print ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
	}

	public void onAppOpenAttribution(string validateResult) {
		print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);

	}

	public void onAppOpenAttributionFailure (string error) {
		print ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);

	}

   

}
