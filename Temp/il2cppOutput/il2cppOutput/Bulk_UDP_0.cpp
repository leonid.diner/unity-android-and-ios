﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action
struct Action_t1264377477;
// System.Action[]
struct ActionU5BU5D_t388269512;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Transform`1<System.Single,System.Action,System.Collections.DictionaryEntry>
struct Transform_1_t4128367065;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1694351041;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,UnityEngine.UDP.PurchaseInfo,System.Collections.DictionaryEntry>
struct Transform_1_t628473575;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.Single,System.Action>
struct Dictionary_2_t1212755687;
// System.Collections.Generic.Dictionary`2<System.Single,System.Object>
struct Dictionary_2_t3028484374;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>
struct Dictionary_2_t2815580273;
// System.Collections.Generic.IDictionary`2<System.Object,System.Object>
struct IDictionary_2_t2891363839;
// System.Collections.Generic.IDictionary`2<System.Single,System.Action>
struct IDictionary_2_t3971574374;
// System.Collections.Generic.IDictionary`2<System.Single,System.Object>
struct IDictionary_2_t1492335765;
// System.Collections.Generic.IDictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>
struct IDictionary_2_t1279431664;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t2131661719;
// System.Collections.Generic.IEqualityComparer`1<System.Single>
struct IEqualityComparer_1_t3504598792;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t2736452219;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.UDP.PurchaseInfo>
struct List_1_t207431420;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t386037858;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t32045322;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.AndroidJavaProxy
struct AndroidJavaProxy_t2835824643;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// UnityEngine.UDP.Analytics.Events.AppInstallEvent
struct AppInstallEvent_t505962745;
// UnityEngine.UDP.Analytics.Events.AppRunningEvent
struct AppRunningEvent_t590370228;
// UnityEngine.UDP.Analytics.Events.AppStartEvent
struct AppStartEvent_t147987345;
// UnityEngine.UDP.Analytics.Events.AppStopEvent
struct AppStopEvent_t3879155;
// UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent
struct PurchaseAttemptEvent_t2556690677;
// UnityEngine.UDP.Analytics.SessionInfo
struct SessionInfo_t3964168581;
// UnityEngine.UDP.AppInfo
struct AppInfo_t4053800566;
// UnityEngine.UDP.AppStoreSettings
struct AppStoreSettings_t763118828;
// UnityEngine.UDP.IInitListener
struct IInitListener_t3363933952;
// UnityEngine.UDP.IPurchaseListener
struct IPurchaseListener_t668871317;
// UnityEngine.UDP.InitLoginForwardCallback
struct InitLoginForwardCallback_t1246786093;
// UnityEngine.UDP.Inventory
struct Inventory_t2002574341;
// UnityEngine.UDP.MainThreadDispatcher
struct MainThreadDispatcher_t2227136958;
// UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6
struct U3CWaitAndDoU3Ed__6_t1673170562;
// UnityEngine.UDP.PurchaseForwardCallback
struct PurchaseForwardCallback_t1388311153;
// UnityEngine.UDP.PurchaseInfo
struct PurchaseInfo_t3030323974;
// UnityEngine.UDP.PurchaseInfo[]
struct PurchaseInfoU5BU5D_t1778061859;
// UnityEngine.UDP.UdpGameManager
struct UdpGameManager_t2849128925;
// UnityEngine.UDP.UserInfo
struct UserInfo_t3529881807;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;

extern RuntimeClass* ActionU5BU5D_t388269512_il2cpp_TypeInfo_var;
extern RuntimeClass* AnalyticsClient_t3694105915_il2cpp_TypeInfo_var;
extern RuntimeClass* AnalyticsService_t760113998_il2cpp_TypeInfo_var;
extern RuntimeClass* AndroidJavaClass_t32045322_il2cpp_TypeInfo_var;
extern RuntimeClass* AndroidJavaObject_t4131667876_il2cpp_TypeInfo_var;
extern RuntimeClass* AppInstallEvent_t505962745_il2cpp_TypeInfo_var;
extern RuntimeClass* AppRunningEvent_t590370228_il2cpp_TypeInfo_var;
extern RuntimeClass* AppStartEvent_t147987345_il2cpp_TypeInfo_var;
extern RuntimeClass* AppStopEvent_t3879155_il2cpp_TypeInfo_var;
extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern RuntimeClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t1212755687_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2815580273_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2865362463_il2cpp_TypeInfo_var;
extern RuntimeClass* EventDispatcher_t1969224201_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1113636619_il2cpp_TypeInfo_var;
extern RuntimeClass* Guid_t_il2cpp_TypeInfo_var;
extern RuntimeClass* IInitListener_t3363933952_il2cpp_TypeInfo_var;
extern RuntimeClass* InitLoginForwardCallback_t1246786093_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern RuntimeClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t207431420_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2736452219_il2cpp_TypeInfo_var;
extern RuntimeClass* MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* PurchaseAttemptEvent_t2556690677_il2cpp_TypeInfo_var;
extern RuntimeClass* PurchaseForwardCallback_t1388311153_il2cpp_TypeInfo_var;
extern RuntimeClass* SessionInfo_t3964168581_il2cpp_TypeInfo_var;
extern RuntimeClass* StoreService_t3764094894_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CWaitAndDoU3Ed__6_t1673170562_il2cpp_TypeInfo_var;
extern RuntimeClass* UInt64_t4134040092_il2cpp_TypeInfo_var;
extern RuntimeClass* UdpGameManager_t2849128925_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1191178036;
extern String_t* _stringLiteral1216028220;
extern String_t* _stringLiteral1231854530;
extern String_t* _stringLiteral1318794425;
extern String_t* _stringLiteral1501416449;
extern String_t* _stringLiteral165665077;
extern String_t* _stringLiteral166562518;
extern String_t* _stringLiteral1687773655;
extern String_t* _stringLiteral1718852242;
extern String_t* _stringLiteral180630352;
extern String_t* _stringLiteral2059856196;
extern String_t* _stringLiteral2149247999;
extern String_t* _stringLiteral2158305376;
extern String_t* _stringLiteral2183561488;
extern String_t* _stringLiteral2186659586;
extern String_t* _stringLiteral2319849551;
extern String_t* _stringLiteral2408352323;
extern String_t* _stringLiteral243999125;
extern String_t* _stringLiteral2553217811;
extern String_t* _stringLiteral2577086779;
extern String_t* _stringLiteral2730811082;
extern String_t* _stringLiteral2807426881;
extern String_t* _stringLiteral2953959406;
extern String_t* _stringLiteral3024613587;
extern String_t* _stringLiteral3099827557;
extern String_t* _stringLiteral3111220510;
extern String_t* _stringLiteral3134671380;
extern String_t* _stringLiteral3331568903;
extern String_t* _stringLiteral3383827812;
extern String_t* _stringLiteral3452315504;
extern String_t* _stringLiteral3452614531;
extern String_t* _stringLiteral3455563724;
extern String_t* _stringLiteral3455629258;
extern String_t* _stringLiteral3547887013;
extern String_t* _stringLiteral3614767605;
extern String_t* _stringLiteral3656337555;
extern String_t* _stringLiteral369603133;
extern String_t* _stringLiteral3705078347;
extern String_t* _stringLiteral3712921954;
extern String_t* _stringLiteral3838603512;
extern String_t* _stringLiteral38630993;
extern String_t* _stringLiteral3872221057;
extern String_t* _stringLiteral3883455643;
extern String_t* _stringLiteral3903019942;
extern String_t* _stringLiteral3920973500;
extern String_t* _stringLiteral4010377966;
extern String_t* _stringLiteral401387461;
extern String_t* _stringLiteral4022591287;
extern String_t* _stringLiteral4027139201;
extern String_t* _stringLiteral4142051001;
extern String_t* _stringLiteral4150543084;
extern String_t* _stringLiteral420381065;
extern String_t* _stringLiteral4294193667;
extern String_t* _stringLiteral43791901;
extern String_t* _stringLiteral476708812;
extern String_t* _stringLiteral52894277;
extern String_t* _stringLiteral549362421;
extern String_t* _stringLiteral634977693;
extern String_t* _stringLiteral720731980;
extern String_t* _stringLiteral757602046;
extern String_t* _stringLiteral818291658;
extern String_t* _stringLiteral835031361;
extern String_t* _stringLiteral892155963;
extern const RuntimeMethod* AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4131667876_m1630827594_RuntimeMethod_var;
extern const RuntimeMethod* AndroidJavaObject_CallStatic_TisString_t_m522892673_RuntimeMethod_var;
extern const RuntimeMethod* AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var;
extern const RuntimeMethod* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4131667876_m671986461_RuntimeMethod_var;
extern const RuntimeMethod* AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m64471701_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Clear_m607508611_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m650535580_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_GetEnumerator_m979315400_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2450723815_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m2644321878_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m784414107_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m792470897_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Count_m3991288552_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_set_Item_m4203027293_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m1063885580_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_Dispose_m158248455_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m1849793072_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m2562535175_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m1534630570_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m608108010_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisMainThreadDispatcher_t2227136958_m3710866612_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_AddComponent_TisUdpGameManager_t2849128925_m3960156118_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m2070292874_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m2243092683_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m3459043748_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m437696916_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m610740988_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Clear_m2936844859_RuntimeMethod_var;
extern const RuntimeMethod* List_1_CopyTo_m2569245864_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2543424368_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m502973724_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m2276455407_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m808340314_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m953835688_RuntimeMethod_var;
extern const RuntimeMethod* Resources_Load_TisAppStoreSettings_t763118828_m644194161_RuntimeMethod_var;
extern const RuntimeMethod* StoreService_Initialize_m4032431360_RuntimeMethod_var;
extern const RuntimeMethod* U3CWaitAndDoU3Ed__6_System_Collections_IEnumerator_Reset_m2609284963_RuntimeMethod_var;
extern const uint32_t AnalyticsClient_CloseService_m1469948210_MetadataUsageId;
extern const uint32_t AnalyticsClient_DetermineNextState_m3099811717_MetadataUsageId;
extern const uint32_t AnalyticsClient_GetSessionId_m1082839017_MetadataUsageId;
extern const uint32_t AnalyticsClient_GetSessionInfo_m2557928941_MetadataUsageId;
extern const uint32_t AnalyticsClient_Initialize_m463104264_MetadataUsageId;
extern const uint32_t AnalyticsClient_OnEnterStatePrepared_m61927341_MetadataUsageId;
extern const uint32_t AnalyticsClient_OnEnterStateReady_m4203028284_MetadataUsageId;
extern const uint32_t AnalyticsClient_OnEnterStateStarted_m2269963401_MetadataUsageId;
extern const uint32_t AnalyticsClient_OnEnterStateStopped_m3763008639_MetadataUsageId;
extern const uint32_t AnalyticsClient_OnPlayerSessionStateChanged_m2310188355_MetadataUsageId;
extern const uint32_t AnalyticsClient_RequestStateChange_m2919322120_MetadataUsageId;
extern const uint32_t AnalyticsClient_SavePersistentValue_m796660950_MetadataUsageId;
extern const uint32_t AnalyticsClient_SendAppRunningEvent_m140057793_MetadataUsageId;
extern const uint32_t AnalyticsClient_SetSessionId_m2204159219_MetadataUsageId;
extern const uint32_t AnalyticsClient_SetState_m2621078371_MetadataUsageId;
extern const uint32_t AnalyticsService_GetPlayerSessionElapsedTime_m889664768_MetadataUsageId;
extern const uint32_t AnalyticsService_Initialize_m4044694567_MetadataUsageId;
extern const uint32_t AnalyticsService_onPlayerStateChanged_m3149236035_MetadataUsageId;
extern const uint32_t AppInstallEvent__ctor_m2634031396_MetadataUsageId;
extern const uint32_t AppRunningEvent__ctor_m2641558448_MetadataUsageId;
extern const uint32_t AppStartEvent__ctor_m3550927429_MetadataUsageId;
extern const uint32_t AppStopEvent__ctor_m761874387_MetadataUsageId;
extern const uint32_t AppStoreSettings__ctor_m1155411528_MetadataUsageId;
extern const uint32_t Common_GetCommonParams_m2618682865_MetadataUsageId;
extern const uint32_t EventDispatcher_DispatchEvent_m2849035944_MetadataUsageId;
extern const uint32_t EventDispatcher_init_m1453862928_MetadataUsageId;
extern const uint32_t InitLoginForwardCallback__ctor_m2942706633_MetadataUsageId;
extern const uint32_t Inventory_GetPurchaseDictionary_m2822751547_MetadataUsageId;
extern const uint32_t Inventory_GetPurchaseList_m3615433102_MetadataUsageId;
extern const uint32_t MainThreadDispatcher_DispatchDelayJob_m1553134548_MetadataUsageId;
extern const uint32_t MainThreadDispatcher_RunOnMainThread_m2241368720_MetadataUsageId;
extern const uint32_t MainThreadDispatcher_Start_m3113591846_MetadataUsageId;
extern const uint32_t MainThreadDispatcher_Update_m2390787740_MetadataUsageId;
extern const uint32_t MainThreadDispatcher_WaitAndDo_m930138745_MetadataUsageId;
extern const uint32_t MainThreadDispatcher__cctor_m3750846867_MetadataUsageId;
extern const uint32_t PlatformWrapper_GenerateRandomId_m2000620507_MetadataUsageId;
extern const uint32_t PlatformWrapper_GetAppInstalled_m1450815954_MetadataUsageId;
extern const uint32_t PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502_MetadataUsageId;
extern const uint32_t PlatformWrapper_GetPlayerPrefsString_m2758919265_MetadataUsageId;
extern const uint32_t PlatformWrapper_GetRuntimePlatformString_m2991492228_MetadataUsageId;
extern const uint32_t PlatformWrapper_SetAppInstalled_m48601936_MetadataUsageId;
extern const uint32_t PurchaseAttemptEvent__ctor_m3473196206_MetadataUsageId;
extern const uint32_t PurchaseForwardCallback__ctor_m665942399_MetadataUsageId;
extern const uint32_t StoreService_ConsumePurchase_m2343943502_MetadataUsageId;
extern const uint32_t StoreService_Initialize_m4032431360_MetadataUsageId;
extern const uint32_t StoreService_Purchase_m2177431105_MetadataUsageId;
extern const uint32_t StoreService_QueryInventory_m3086827487_MetadataUsageId;
extern const uint32_t StoreService_get_StoreName_m1656101778_MetadataUsageId;
extern const uint32_t StoreService_javaArrayFromCSList_m2484081227_MetadataUsageId;
extern const uint32_t U3CWaitAndDoU3Ed__6_MoveNext_m4264311566_MetadataUsageId;
extern const uint32_t U3CWaitAndDoU3Ed__6_System_Collections_IEnumerator_Reset_m2609284963_MetadataUsageId;
extern const uint32_t UdpAnalytics_PurchaseAttempt_m745049746_MetadataUsageId;
extern const uint32_t UdpAnalytics_isInitialized_m2641110313_MetadataUsageId;
extern const uint32_t UdpGameManager_Awake_m3051392357_MetadataUsageId;
extern const uint32_t UdpGameManager_OnApplicationPause_m3137436107_MetadataUsageId;
extern const uint32_t UdpGameManager_OnApplicationQuit_m2061624748_MetadataUsageId;
extern const uint32_t UdpGameManager_Start_m3977941006_MetadataUsageId;
extern const uint32_t UdpGameManager__cctor_m1360727243_MetadataUsageId;

struct ActionU5BU5D_t388269512;
struct ObjectU5BU5D_t2843939325;


#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T1212755687_H
#define DICTIONARY_2_T1212755687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.Single,System.Action>
struct  Dictionary_2_t1212755687  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	SingleU5BU5D_t1444911251* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ActionU5BU5D_t388269512* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___keySlots_6)); }
	inline SingleU5BU5D_t1444911251* get_keySlots_6() const { return ___keySlots_6; }
	inline SingleU5BU5D_t1444911251** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(SingleU5BU5D_t1444911251* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___valueSlots_7)); }
	inline ActionU5BU5D_t388269512* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ActionU5BU5D_t388269512** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ActionU5BU5D_t388269512* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1212755687_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t4128367065 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1212755687_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t4128367065 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t4128367065 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t4128367065 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1212755687_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2865362463_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1694351041 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1694351041 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1694351041 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1694351041 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef DICTIONARY_2_T2815580273_H
#define DICTIONARY_2_T2815580273_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>
struct  Dictionary_2_t2815580273  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	PurchaseInfoU5BU5D_t1778061859* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___valueSlots_7)); }
	inline PurchaseInfoU5BU5D_t1778061859* get_valueSlots_7() const { return ___valueSlots_7; }
	inline PurchaseInfoU5BU5D_t1778061859** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(PurchaseInfoU5BU5D_t1778061859* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2815580273_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t628473575 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2815580273_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t628473575 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t628473575 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t628473575 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2815580273_H
#ifndef LIST_1_T2736452219_H
#define LIST_1_T2736452219_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Action>
struct  List_1_t2736452219  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ActionU5BU5D_t388269512* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2736452219, ____items_1)); }
	inline ActionU5BU5D_t388269512* get__items_1() const { return ____items_1; }
	inline ActionU5BU5D_t388269512** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ActionU5BU5D_t388269512* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2736452219, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2736452219, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2736452219_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	ActionU5BU5D_t388269512* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2736452219_StaticFields, ___EmptyArray_4)); }
	inline ActionU5BU5D_t388269512* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline ActionU5BU5D_t388269512** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(ActionU5BU5D_t388269512* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2736452219_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1281789340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1281789340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1281789340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1281789340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef LIST_1_T207431420_H
#define LIST_1_T207431420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UDP.PurchaseInfo>
struct  List_1_t207431420  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PurchaseInfoU5BU5D_t1778061859* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t207431420, ____items_1)); }
	inline PurchaseInfoU5BU5D_t1778061859* get__items_1() const { return ____items_1; }
	inline PurchaseInfoU5BU5D_t1778061859** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PurchaseInfoU5BU5D_t1778061859* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t207431420, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t207431420, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t207431420_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PurchaseInfoU5BU5D_t1778061859* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t207431420_StaticFields, ___EmptyArray_4)); }
	inline PurchaseInfoU5BU5D_t1778061859* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PurchaseInfoU5BU5D_t1778061859** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PurchaseInfoU5BU5D_t1778061859* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T207431420_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANDROIDJAVAOBJECT_T4131667876_H
#define ANDROIDJAVAOBJECT_T4131667876_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaObject
struct  AndroidJavaObject_t4131667876  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAOBJECT_T4131667876_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef COMMON_T4170922834_H
#define COMMON_T4170922834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Common
struct  Common_t4170922834  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMON_T4170922834_H
#ifndef EVENTDISPATCHER_T1969224201_H
#define EVENTDISPATCHER_T1969224201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.EventDispatcher
struct  EventDispatcher_t1969224201  : public RuntimeObject
{
public:

public:
};

struct EventDispatcher_t1969224201_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.UDP.Analytics.EventDispatcher::serviceClass
	AndroidJavaClass_t32045322 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(EventDispatcher_t1969224201_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_t32045322 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_t32045322 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDISPATCHER_T1969224201_H
#ifndef PLATFORMWRAPPER_T955316379_H
#define PLATFORMWRAPPER_T955316379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.PlatformWrapper
struct  PlatformWrapper_t955316379  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMWRAPPER_T955316379_H
#ifndef SESSIONINFO_T3964168581_H
#define SESSIONINFO_T3964168581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.SessionInfo
struct  SessionInfo_t3964168581  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_AppId
	String_t* ___m_AppId_0;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_SessionId
	String_t* ___m_SessionId_1;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_ClientId
	String_t* ___m_ClientId_2;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_DeviceId
	String_t* ___m_DeviceId_3;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_Platform
	String_t* ___m_Platform_4;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_TargetStore
	String_t* ___m_TargetStore_5;
	// System.String UnityEngine.UDP.Analytics.SessionInfo::m_SystemInfo
	String_t* ___m_SystemInfo_6;
	// System.Boolean UnityEngine.UDP.Analytics.SessionInfo::m_Vr
	bool ___m_Vr_7;

public:
	inline static int32_t get_offset_of_m_AppId_0() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_AppId_0)); }
	inline String_t* get_m_AppId_0() const { return ___m_AppId_0; }
	inline String_t** get_address_of_m_AppId_0() { return &___m_AppId_0; }
	inline void set_m_AppId_0(String_t* value)
	{
		___m_AppId_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppId_0), value);
	}

	inline static int32_t get_offset_of_m_SessionId_1() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_SessionId_1)); }
	inline String_t* get_m_SessionId_1() const { return ___m_SessionId_1; }
	inline String_t** get_address_of_m_SessionId_1() { return &___m_SessionId_1; }
	inline void set_m_SessionId_1(String_t* value)
	{
		___m_SessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SessionId_1), value);
	}

	inline static int32_t get_offset_of_m_ClientId_2() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_ClientId_2)); }
	inline String_t* get_m_ClientId_2() const { return ___m_ClientId_2; }
	inline String_t** get_address_of_m_ClientId_2() { return &___m_ClientId_2; }
	inline void set_m_ClientId_2(String_t* value)
	{
		___m_ClientId_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientId_2), value);
	}

	inline static int32_t get_offset_of_m_DeviceId_3() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_DeviceId_3)); }
	inline String_t* get_m_DeviceId_3() const { return ___m_DeviceId_3; }
	inline String_t** get_address_of_m_DeviceId_3() { return &___m_DeviceId_3; }
	inline void set_m_DeviceId_3(String_t* value)
	{
		___m_DeviceId_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_DeviceId_3), value);
	}

	inline static int32_t get_offset_of_m_Platform_4() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_Platform_4)); }
	inline String_t* get_m_Platform_4() const { return ___m_Platform_4; }
	inline String_t** get_address_of_m_Platform_4() { return &___m_Platform_4; }
	inline void set_m_Platform_4(String_t* value)
	{
		___m_Platform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Platform_4), value);
	}

	inline static int32_t get_offset_of_m_TargetStore_5() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_TargetStore_5)); }
	inline String_t* get_m_TargetStore_5() const { return ___m_TargetStore_5; }
	inline String_t** get_address_of_m_TargetStore_5() { return &___m_TargetStore_5; }
	inline void set_m_TargetStore_5(String_t* value)
	{
		___m_TargetStore_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetStore_5), value);
	}

	inline static int32_t get_offset_of_m_SystemInfo_6() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_SystemInfo_6)); }
	inline String_t* get_m_SystemInfo_6() const { return ___m_SystemInfo_6; }
	inline String_t** get_address_of_m_SystemInfo_6() { return &___m_SystemInfo_6; }
	inline void set_m_SystemInfo_6(String_t* value)
	{
		___m_SystemInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_SystemInfo_6), value);
	}

	inline static int32_t get_offset_of_m_Vr_7() { return static_cast<int32_t>(offsetof(SessionInfo_t3964168581, ___m_Vr_7)); }
	inline bool get_m_Vr_7() const { return ___m_Vr_7; }
	inline bool* get_address_of_m_Vr_7() { return &___m_Vr_7; }
	inline void set_m_Vr_7(bool value)
	{
		___m_Vr_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONINFO_T3964168581_H
#ifndef UDPANALYTICS_T2366384688_H
#define UDPANALYTICS_T2366384688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.UdpAnalytics
struct  UdpAnalytics_t2366384688  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPANALYTICS_T2366384688_H
#ifndef APPINFO_T4053800566_H
#define APPINFO_T4053800566_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.AppInfo
struct  AppInfo_t4053800566  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.AppInfo::<ClientId>k__BackingField
	String_t* ___U3CClientIdU3Ek__BackingField_0;
	// System.String UnityEngine.UDP.AppInfo::<AppSlug>k__BackingField
	String_t* ___U3CAppSlugU3Ek__BackingField_1;
	// System.String UnityEngine.UDP.AppInfo::<ClientKey>k__BackingField
	String_t* ___U3CClientKeyU3Ek__BackingField_2;
	// System.String UnityEngine.UDP.AppInfo::<RSAPublicKey>k__BackingField
	String_t* ___U3CRSAPublicKeyU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CClientIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AppInfo_t4053800566, ___U3CClientIdU3Ek__BackingField_0)); }
	inline String_t* get_U3CClientIdU3Ek__BackingField_0() const { return ___U3CClientIdU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CClientIdU3Ek__BackingField_0() { return &___U3CClientIdU3Ek__BackingField_0; }
	inline void set_U3CClientIdU3Ek__BackingField_0(String_t* value)
	{
		___U3CClientIdU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientIdU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAppSlugU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AppInfo_t4053800566, ___U3CAppSlugU3Ek__BackingField_1)); }
	inline String_t* get_U3CAppSlugU3Ek__BackingField_1() const { return ___U3CAppSlugU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAppSlugU3Ek__BackingField_1() { return &___U3CAppSlugU3Ek__BackingField_1; }
	inline void set_U3CAppSlugU3Ek__BackingField_1(String_t* value)
	{
		___U3CAppSlugU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppSlugU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CClientKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AppInfo_t4053800566, ___U3CClientKeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CClientKeyU3Ek__BackingField_2() const { return ___U3CClientKeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CClientKeyU3Ek__BackingField_2() { return &___U3CClientKeyU3Ek__BackingField_2; }
	inline void set_U3CClientKeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CClientKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientKeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CRSAPublicKeyU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AppInfo_t4053800566, ___U3CRSAPublicKeyU3Ek__BackingField_3)); }
	inline String_t* get_U3CRSAPublicKeyU3Ek__BackingField_3() const { return ___U3CRSAPublicKeyU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CRSAPublicKeyU3Ek__BackingField_3() { return &___U3CRSAPublicKeyU3Ek__BackingField_3; }
	inline void set_U3CRSAPublicKeyU3Ek__BackingField_3(String_t* value)
	{
		___U3CRSAPublicKeyU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRSAPublicKeyU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINFO_T4053800566_H
#ifndef INVENTORY_T2002574341_H
#define INVENTORY_T2002574341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Inventory
struct  Inventory_t2002574341  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo> UnityEngine.UDP.Inventory::_purchaseDictionary
	Dictionary_2_t2815580273 * ____purchaseDictionary_0;

public:
	inline static int32_t get_offset_of__purchaseDictionary_0() { return static_cast<int32_t>(offsetof(Inventory_t2002574341, ____purchaseDictionary_0)); }
	inline Dictionary_2_t2815580273 * get__purchaseDictionary_0() const { return ____purchaseDictionary_0; }
	inline Dictionary_2_t2815580273 ** get_address_of__purchaseDictionary_0() { return &____purchaseDictionary_0; }
	inline void set__purchaseDictionary_0(Dictionary_2_t2815580273 * value)
	{
		____purchaseDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&____purchaseDictionary_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVENTORY_T2002574341_H
#ifndef U3CWAITANDDOU3ED__6_T1673170562_H
#define U3CWAITANDDOU3ED__6_T1673170562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6
struct  U3CWaitAndDoU3Ed__6_t1673170562  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.WaitForSeconds UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::<>2__current
	WaitForSeconds_t1699091251 * ___U3CU3E2__current_1;
	// System.Single UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::waitTime
	float ___waitTime_2;
	// System.Action UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::runnable
	Action_t1264377477 * ___runnable_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t1673170562, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t1673170562, ___U3CU3E2__current_1)); }
	inline WaitForSeconds_t1699091251 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline WaitForSeconds_t1699091251 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(WaitForSeconds_t1699091251 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t1673170562, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_runnable_3() { return static_cast<int32_t>(offsetof(U3CWaitAndDoU3Ed__6_t1673170562, ___runnable_3)); }
	inline Action_t1264377477 * get_runnable_3() const { return ___runnable_3; }
	inline Action_t1264377477 ** get_address_of_runnable_3() { return &___runnable_3; }
	inline void set_runnable_3(Action_t1264377477 * value)
	{
		___runnable_3 = value;
		Il2CppCodeGenWriteBarrier((&___runnable_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITANDDOU3ED__6_T1673170562_H
#ifndef PURCHASEINFO_T3030323974_H
#define PURCHASEINFO_T3030323974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.PurchaseInfo
struct  PurchaseInfo_t3030323974  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.PurchaseInfo::<ItemType>k__BackingField
	String_t* ___U3CItemTypeU3Ek__BackingField_0;
	// System.String UnityEngine.UDP.PurchaseInfo::<ProductId>k__BackingField
	String_t* ___U3CProductIdU3Ek__BackingField_1;
	// System.String UnityEngine.UDP.PurchaseInfo::<GameOrderId>k__BackingField
	String_t* ___U3CGameOrderIdU3Ek__BackingField_2;
	// System.String UnityEngine.UDP.PurchaseInfo::<OrderQueryToken>k__BackingField
	String_t* ___U3COrderQueryTokenU3Ek__BackingField_3;
	// System.String UnityEngine.UDP.PurchaseInfo::<DeveloperPayload>k__BackingField
	String_t* ___U3CDeveloperPayloadU3Ek__BackingField_4;
	// System.String UnityEngine.UDP.PurchaseInfo::<StorePurchaseJsonString>k__BackingField
	String_t* ___U3CStorePurchaseJsonStringU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CItemTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseInfo_t3030323974, ___U3CItemTypeU3Ek__BackingField_0)); }
	inline String_t* get_U3CItemTypeU3Ek__BackingField_0() const { return ___U3CItemTypeU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CItemTypeU3Ek__BackingField_0() { return &___U3CItemTypeU3Ek__BackingField_0; }
	inline void set_U3CItemTypeU3Ek__BackingField_0(String_t* value)
	{
		___U3CItemTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProductIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PurchaseInfo_t3030323974, ___U3CProductIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CProductIdU3Ek__BackingField_1() const { return ___U3CProductIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CProductIdU3Ek__BackingField_1() { return &___U3CProductIdU3Ek__BackingField_1; }
	inline void set_U3CProductIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CProductIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProductIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CGameOrderIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PurchaseInfo_t3030323974, ___U3CGameOrderIdU3Ek__BackingField_2)); }
	inline String_t* get_U3CGameOrderIdU3Ek__BackingField_2() const { return ___U3CGameOrderIdU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CGameOrderIdU3Ek__BackingField_2() { return &___U3CGameOrderIdU3Ek__BackingField_2; }
	inline void set_U3CGameOrderIdU3Ek__BackingField_2(String_t* value)
	{
		___U3CGameOrderIdU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameOrderIdU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COrderQueryTokenU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PurchaseInfo_t3030323974, ___U3COrderQueryTokenU3Ek__BackingField_3)); }
	inline String_t* get_U3COrderQueryTokenU3Ek__BackingField_3() const { return ___U3COrderQueryTokenU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3COrderQueryTokenU3Ek__BackingField_3() { return &___U3COrderQueryTokenU3Ek__BackingField_3; }
	inline void set_U3COrderQueryTokenU3Ek__BackingField_3(String_t* value)
	{
		___U3COrderQueryTokenU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COrderQueryTokenU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDeveloperPayloadU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PurchaseInfo_t3030323974, ___U3CDeveloperPayloadU3Ek__BackingField_4)); }
	inline String_t* get_U3CDeveloperPayloadU3Ek__BackingField_4() const { return ___U3CDeveloperPayloadU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CDeveloperPayloadU3Ek__BackingField_4() { return &___U3CDeveloperPayloadU3Ek__BackingField_4; }
	inline void set_U3CDeveloperPayloadU3Ek__BackingField_4(String_t* value)
	{
		___U3CDeveloperPayloadU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeveloperPayloadU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CStorePurchaseJsonStringU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PurchaseInfo_t3030323974, ___U3CStorePurchaseJsonStringU3Ek__BackingField_5)); }
	inline String_t* get_U3CStorePurchaseJsonStringU3Ek__BackingField_5() const { return ___U3CStorePurchaseJsonStringU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CStorePurchaseJsonStringU3Ek__BackingField_5() { return &___U3CStorePurchaseJsonStringU3Ek__BackingField_5; }
	inline void set_U3CStorePurchaseJsonStringU3Ek__BackingField_5(String_t* value)
	{
		___U3CStorePurchaseJsonStringU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStorePurchaseJsonStringU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEINFO_T3030323974_H
#ifndef STORESERVICE_T3764094894_H
#define STORESERVICE_T3764094894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.StoreService
struct  StoreService_t3764094894  : public RuntimeObject
{
public:

public:
};

struct StoreService_t3764094894_StaticFields
{
public:
	// UnityEngine.AndroidJavaClass UnityEngine.UDP.StoreService::serviceClass
	AndroidJavaClass_t32045322 * ___serviceClass_0;

public:
	inline static int32_t get_offset_of_serviceClass_0() { return static_cast<int32_t>(offsetof(StoreService_t3764094894_StaticFields, ___serviceClass_0)); }
	inline AndroidJavaClass_t32045322 * get_serviceClass_0() const { return ___serviceClass_0; }
	inline AndroidJavaClass_t32045322 ** get_address_of_serviceClass_0() { return &___serviceClass_0; }
	inline void set_serviceClass_0(AndroidJavaClass_t32045322 * value)
	{
		___serviceClass_0 = value;
		Il2CppCodeGenWriteBarrier((&___serviceClass_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STORESERVICE_T3764094894_H
#ifndef USERINFO_T3529881807_H
#define USERINFO_T3529881807_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.UserInfo
struct  UserInfo_t3529881807  : public RuntimeObject
{
public:
	// System.String UnityEngine.UDP.UserInfo::<Channel>k__BackingField
	String_t* ___U3CChannelU3Ek__BackingField_0;
	// System.String UnityEngine.UDP.UserInfo::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_1;
	// System.String UnityEngine.UDP.UserInfo::<UserLoginToken>k__BackingField
	String_t* ___U3CUserLoginTokenU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CChannelU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(UserInfo_t3529881807, ___U3CChannelU3Ek__BackingField_0)); }
	inline String_t* get_U3CChannelU3Ek__BackingField_0() const { return ___U3CChannelU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CChannelU3Ek__BackingField_0() { return &___U3CChannelU3Ek__BackingField_0; }
	inline void set_U3CChannelU3Ek__BackingField_0(String_t* value)
	{
		___U3CChannelU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CChannelU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(UserInfo_t3529881807, ___U3CUserIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_1() const { return ___U3CUserIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_1() { return &___U3CUserIdU3Ek__BackingField_1; }
	inline void set_U3CUserIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CUserLoginTokenU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UserInfo_t3529881807, ___U3CUserLoginTokenU3Ek__BackingField_2)); }
	inline String_t* get_U3CUserLoginTokenU3Ek__BackingField_2() const { return ___U3CUserLoginTokenU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CUserLoginTokenU3Ek__BackingField_2() { return &___U3CUserLoginTokenU3Ek__BackingField_2; }
	inline void set_U3CUserLoginTokenU3Ek__BackingField_2(String_t* value)
	{
		___U3CUserLoginTokenU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserLoginTokenU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERINFO_T3529881807_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef KEYVALUEPAIR_2_T2530217319_H
#define KEYVALUEPAIR_2_T2530217319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_t2530217319 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2530217319, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2530217319_H
#ifndef KEYVALUEPAIR_2_T3610427854_H
#define KEYVALUEPAIR_2_T3610427854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Single,System.Action>
struct  KeyValuePair_2_t3610427854 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	float ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	Action_t1264377477 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3610427854, ___key_0)); }
	inline float get_key_0() const { return ___key_0; }
	inline float* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(float value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3610427854, ___value_1)); }
	inline Action_t1264377477 * get_value_1() const { return ___value_1; }
	inline Action_t1264377477 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(Action_t1264377477 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3610427854_H
#ifndef KEYVALUEPAIR_2_T1131189245_H
#define KEYVALUEPAIR_2_T1131189245_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>
struct  KeyValuePair_2_t1131189245 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	float ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1131189245, ___key_0)); }
	inline float get_key_0() const { return ___key_0; }
	inline float* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(float value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t1131189245, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T1131189245_H
#ifndef KEYVALUEPAIR_2_T918285144_H
#define KEYVALUEPAIR_2_T918285144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.UDP.PurchaseInfo>
struct  KeyValuePair_2_t918285144 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	PurchaseInfo_t3030323974 * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t918285144, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t918285144, ___value_1)); }
	inline PurchaseInfo_t3030323974 * get_value_1() const { return ___value_1; }
	inline PurchaseInfo_t3030323974 ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(PurchaseInfo_t3030323974 * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T918285144_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_0;
	// System.Int16 System.Guid::_b
	int16_t ____b_1;
	// System.Int16 System.Guid::_c
	int16_t ____c_2;
	// System.Byte System.Guid::_d
	uint8_t ____d_3;
	// System.Byte System.Guid::_e
	uint8_t ____e_4;
	// System.Byte System.Guid::_f
	uint8_t ____f_5;
	// System.Byte System.Guid::_g
	uint8_t ____g_6;
	// System.Byte System.Guid::_h
	uint8_t ____h_7;
	// System.Byte System.Guid::_i
	uint8_t ____i_8;
	// System.Byte System.Guid::_j
	uint8_t ____j_9;
	// System.Byte System.Guid::_k
	uint8_t ____k_10;

public:
	inline static int32_t get_offset_of__a_0() { return static_cast<int32_t>(offsetof(Guid_t, ____a_0)); }
	inline int32_t get__a_0() const { return ____a_0; }
	inline int32_t* get_address_of__a_0() { return &____a_0; }
	inline void set__a_0(int32_t value)
	{
		____a_0 = value;
	}

	inline static int32_t get_offset_of__b_1() { return static_cast<int32_t>(offsetof(Guid_t, ____b_1)); }
	inline int16_t get__b_1() const { return ____b_1; }
	inline int16_t* get_address_of__b_1() { return &____b_1; }
	inline void set__b_1(int16_t value)
	{
		____b_1 = value;
	}

	inline static int32_t get_offset_of__c_2() { return static_cast<int32_t>(offsetof(Guid_t, ____c_2)); }
	inline int16_t get__c_2() const { return ____c_2; }
	inline int16_t* get_address_of__c_2() { return &____c_2; }
	inline void set__c_2(int16_t value)
	{
		____c_2 = value;
	}

	inline static int32_t get_offset_of__d_3() { return static_cast<int32_t>(offsetof(Guid_t, ____d_3)); }
	inline uint8_t get__d_3() const { return ____d_3; }
	inline uint8_t* get_address_of__d_3() { return &____d_3; }
	inline void set__d_3(uint8_t value)
	{
		____d_3 = value;
	}

	inline static int32_t get_offset_of__e_4() { return static_cast<int32_t>(offsetof(Guid_t, ____e_4)); }
	inline uint8_t get__e_4() const { return ____e_4; }
	inline uint8_t* get_address_of__e_4() { return &____e_4; }
	inline void set__e_4(uint8_t value)
	{
		____e_4 = value;
	}

	inline static int32_t get_offset_of__f_5() { return static_cast<int32_t>(offsetof(Guid_t, ____f_5)); }
	inline uint8_t get__f_5() const { return ____f_5; }
	inline uint8_t* get_address_of__f_5() { return &____f_5; }
	inline void set__f_5(uint8_t value)
	{
		____f_5 = value;
	}

	inline static int32_t get_offset_of__g_6() { return static_cast<int32_t>(offsetof(Guid_t, ____g_6)); }
	inline uint8_t get__g_6() const { return ____g_6; }
	inline uint8_t* get_address_of__g_6() { return &____g_6; }
	inline void set__g_6(uint8_t value)
	{
		____g_6 = value;
	}

	inline static int32_t get_offset_of__h_7() { return static_cast<int32_t>(offsetof(Guid_t, ____h_7)); }
	inline uint8_t get__h_7() const { return ____h_7; }
	inline uint8_t* get_address_of__h_7() { return &____h_7; }
	inline void set__h_7(uint8_t value)
	{
		____h_7 = value;
	}

	inline static int32_t get_offset_of__i_8() { return static_cast<int32_t>(offsetof(Guid_t, ____i_8)); }
	inline uint8_t get__i_8() const { return ____i_8; }
	inline uint8_t* get_address_of__i_8() { return &____i_8; }
	inline void set__i_8(uint8_t value)
	{
		____i_8 = value;
	}

	inline static int32_t get_offset_of__j_9() { return static_cast<int32_t>(offsetof(Guid_t, ____j_9)); }
	inline uint8_t get__j_9() const { return ____j_9; }
	inline uint8_t* get_address_of__j_9() { return &____j_9; }
	inline void set__j_9(uint8_t value)
	{
		____j_9 = value;
	}

	inline static int32_t get_offset_of__k_10() { return static_cast<int32_t>(offsetof(Guid_t, ____k_10)); }
	inline uint8_t get__k_10() const { return ____k_10; }
	inline uint8_t* get_address_of__k_10() { return &____k_10; }
	inline void set__k_10(uint8_t value)
	{
		____k_10 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_11;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t386037858 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t386037858 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_11() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_11)); }
	inline Guid_t  get_Empty_11() const { return ___Empty_11; }
	inline Guid_t * get_address_of_Empty_11() { return &___Empty_11; }
	inline void set_Empty_11(Guid_t  value)
	{
		___Empty_11 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t386037858 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t386037858 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t386037858 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t386037858 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t386037858 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((&____fastRng_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef UINT64_T4134040092_H
#define UINT64_T4134040092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt64
struct  UInt64_t4134040092 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_t4134040092, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT64_T4134040092_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ANDROIDJAVACLASS_T32045322_H
#define ANDROIDJAVACLASS_T32045322_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaClass
struct  AndroidJavaClass_t32045322  : public AndroidJavaObject_t4131667876
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVACLASS_T32045322_H
#ifndef APPINSTALLEVENT_T505962745_H
#define APPINSTALLEVENT_T505962745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppInstallEvent
struct  AppInstallEvent_t505962745  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppInstallEvent::_params
	Dictionary_2_t2865362463 * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppInstallEvent_t505962745, ____params_0)); }
	inline Dictionary_2_t2865362463 * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t2865362463 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINSTALLEVENT_T505962745_H
#ifndef APPRUNNINGEVENT_T590370228_H
#define APPRUNNINGEVENT_T590370228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppRunningEvent
struct  AppRunningEvent_t590370228  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppRunningEvent::_params
	Dictionary_2_t2865362463 * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppRunningEvent_t590370228, ____params_0)); }
	inline Dictionary_2_t2865362463 * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t2865362463 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPRUNNINGEVENT_T590370228_H
#ifndef APPSTARTEVENT_T147987345_H
#define APPSTARTEVENT_T147987345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppStartEvent
struct  AppStartEvent_t147987345  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppStartEvent::_params
	Dictionary_2_t2865362463 * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppStartEvent_t147987345, ____params_0)); }
	inline Dictionary_2_t2865362463 * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t2865362463 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTARTEVENT_T147987345_H
#ifndef APPSTOPEVENT_T3879155_H
#define APPSTOPEVENT_T3879155_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.AppStopEvent
struct  AppStopEvent_t3879155  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.AppStopEvent::_params
	Dictionary_2_t2865362463 * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(AppStopEvent_t3879155, ____params_0)); }
	inline Dictionary_2_t2865362463 * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t2865362463 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTOPEVENT_T3879155_H
#ifndef PURCHASEATTEMPTEVENT_T2556690677_H
#define PURCHASEATTEMPTEVENT_T2556690677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent
struct  PurchaseAttemptEvent_t2556690677  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent::_params
	Dictionary_2_t2865362463 * ____params_0;

public:
	inline static int32_t get_offset_of__params_0() { return static_cast<int32_t>(offsetof(PurchaseAttemptEvent_t2556690677, ____params_0)); }
	inline Dictionary_2_t2865362463 * get__params_0() const { return ____params_0; }
	inline Dictionary_2_t2865362463 ** get_address_of__params_0() { return &____params_0; }
	inline void set__params_0(Dictionary_2_t2865362463 * value)
	{
		____params_0 = value;
		Il2CppCodeGenWriteBarrier((&____params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEATTEMPTEVENT_T2556690677_H
#ifndef INITLOGINFORWARDCALLBACK_T1246786093_H
#define INITLOGINFORWARDCALLBACK_T1246786093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.InitLoginForwardCallback
struct  InitLoginForwardCallback_t1246786093  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.UDP.IInitListener UnityEngine.UDP.InitLoginForwardCallback::_initListener
	RuntimeObject* ____initListener_0;

public:
	inline static int32_t get_offset_of__initListener_0() { return static_cast<int32_t>(offsetof(InitLoginForwardCallback_t1246786093, ____initListener_0)); }
	inline RuntimeObject* get__initListener_0() const { return ____initListener_0; }
	inline RuntimeObject** get_address_of__initListener_0() { return &____initListener_0; }
	inline void set__initListener_0(RuntimeObject* value)
	{
		____initListener_0 = value;
		Il2CppCodeGenWriteBarrier((&____initListener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITLOGINFORWARDCALLBACK_T1246786093_H
#ifndef PURCHASEFORWARDCALLBACK_T1388311153_H
#define PURCHASEFORWARDCALLBACK_T1388311153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.PurchaseForwardCallback
struct  PurchaseForwardCallback_t1388311153  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.UDP.IPurchaseListener UnityEngine.UDP.PurchaseForwardCallback::purchaseListener
	RuntimeObject* ___purchaseListener_0;

public:
	inline static int32_t get_offset_of_purchaseListener_0() { return static_cast<int32_t>(offsetof(PurchaseForwardCallback_t1388311153, ___purchaseListener_0)); }
	inline RuntimeObject* get_purchaseListener_0() const { return ___purchaseListener_0; }
	inline RuntimeObject** get_address_of_purchaseListener_0() { return &___purchaseListener_0; }
	inline void set_purchaseListener_0(RuntimeObject* value)
	{
		___purchaseListener_0 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseListener_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEFORWARDCALLBACK_T1388311153_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef ENUMERATOR_T2086727927_H
#define ENUMERATOR_T2086727927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>
struct  Enumerator_t2086727927 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t132545152 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t2530217319  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___dictionary_0)); }
	inline Dictionary_2_t132545152 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t132545152 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t132545152 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2086727927, ___current_3)); }
	inline KeyValuePair_2_t2530217319  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t2530217319 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t2530217319  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2086727927_H
#ifndef ENUMERATOR_T3166938462_H
#define ENUMERATOR_T3166938462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Action>
struct  Enumerator_t3166938462 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t1212755687 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t3610427854  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t3166938462, ___dictionary_0)); }
	inline Dictionary_2_t1212755687 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t1212755687 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t1212755687 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3166938462, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t3166938462, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3166938462, ___current_3)); }
	inline KeyValuePair_2_t3610427854  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t3610427854 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t3610427854  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3166938462_H
#ifndef ENUMERATOR_T687699853_H
#define ENUMERATOR_T687699853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Object>
struct  Enumerator_t687699853 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t3028484374 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t1131189245  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t687699853, ___dictionary_0)); }
	inline Dictionary_2_t3028484374 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t3028484374 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t3028484374 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t687699853, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t687699853, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t687699853, ___current_3)); }
	inline KeyValuePair_2_t1131189245  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t1131189245 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t1131189245  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T687699853_H
#ifndef ENUMERATOR_T474795752_H
#define ENUMERATOR_T474795752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.UDP.PurchaseInfo>
struct  Enumerator_t474795752 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t2815580273 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::stamp
	int32_t ___stamp_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_t918285144  ___current_3;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t474795752, ___dictionary_0)); }
	inline Dictionary_2_t2815580273 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t2815580273 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t2815580273 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t474795752, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_stamp_2() { return static_cast<int32_t>(offsetof(Enumerator_t474795752, ___stamp_2)); }
	inline int32_t get_stamp_2() const { return ___stamp_2; }
	inline int32_t* get_address_of_stamp_2() { return &___stamp_2; }
	inline void set_stamp_2(int32_t value)
	{
		___stamp_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t474795752, ___current_3)); }
	inline KeyValuePair_2_t918285144  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t918285144 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t918285144  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T474795752_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef INVALIDOPERATIONEXCEPTION_T56020091_H
#define INVALIDOPERATIONEXCEPTION_T56020091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.InvalidOperationException
struct  InvalidOperationException_t56020091  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDOPERATIONEXCEPTION_T56020091_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef HIDEFLAGS_T4250555765_H
#define HIDEFLAGS_T4250555765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t4250555765 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t4250555765, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T4250555765_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef STATE_T3373387826_H
#define STATE_T3373387826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsClient/State
struct  State_t3373387826 
{
public:
	// System.Int32 UnityEngine.UDP.Analytics.AnalyticsClient/State::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(State_t3373387826, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T3373387826_H
#ifndef ANALYTICSRESULT_T2435309524_H
#define ANALYTICSRESULT_T2435309524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsResult
struct  AnalyticsResult_t2435309524 
{
public:
	// System.Int32 UnityEngine.UDP.Analytics.AnalyticsResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsResult_t2435309524, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSRESULT_T2435309524_H
#ifndef SESSIONSTATE_T3526360511_H
#define SESSIONSTATE_T3526360511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsService/SessionState
struct  SessionState_t3526360511 
{
public:
	// System.Int32 UnityEngine.UDP.Analytics.AnalyticsService/SessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SessionState_t3526360511, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATE_T3526360511_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef ANALYTICSCLIENT_T3694105915_H
#define ANALYTICSCLIENT_T3694105915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsClient
struct  AnalyticsClient_t3694105915  : public RuntimeObject
{
public:

public:
};

struct AnalyticsClient_t3694105915_StaticFields
{
public:
	// UnityEngine.UDP.Analytics.SessionInfo UnityEngine.UDP.Analytics.AnalyticsClient::m_sessionInfo
	SessionInfo_t3964168581 * ___m_sessionInfo_0;
	// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::m_IsNewSession
	bool ___m_IsNewSession_1;
	// UnityEngine.UDP.Analytics.AnalyticsClient/State UnityEngine.UDP.Analytics.AnalyticsClient::m_State
	int32_t ___m_State_2;
	// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::m_InStateTransition
	bool ___m_InStateTransition_3;
	// System.String UnityEngine.UDP.Analytics.AnalyticsClient::m_AppId
	String_t* ___m_AppId_4;
	// System.String UnityEngine.UDP.Analytics.AnalyticsClient::m_ClientId
	String_t* ___m_ClientId_5;
	// System.String UnityEngine.UDP.Analytics.AnalyticsClient::m_TargetStore
	String_t* ___m_TargetStore_6;
	// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::m_AppInstalled
	bool ___m_AppInstalled_7;

public:
	inline static int32_t get_offset_of_m_sessionInfo_0() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_sessionInfo_0)); }
	inline SessionInfo_t3964168581 * get_m_sessionInfo_0() const { return ___m_sessionInfo_0; }
	inline SessionInfo_t3964168581 ** get_address_of_m_sessionInfo_0() { return &___m_sessionInfo_0; }
	inline void set_m_sessionInfo_0(SessionInfo_t3964168581 * value)
	{
		___m_sessionInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_sessionInfo_0), value);
	}

	inline static int32_t get_offset_of_m_IsNewSession_1() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_IsNewSession_1)); }
	inline bool get_m_IsNewSession_1() const { return ___m_IsNewSession_1; }
	inline bool* get_address_of_m_IsNewSession_1() { return &___m_IsNewSession_1; }
	inline void set_m_IsNewSession_1(bool value)
	{
		___m_IsNewSession_1 = value;
	}

	inline static int32_t get_offset_of_m_State_2() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_State_2)); }
	inline int32_t get_m_State_2() const { return ___m_State_2; }
	inline int32_t* get_address_of_m_State_2() { return &___m_State_2; }
	inline void set_m_State_2(int32_t value)
	{
		___m_State_2 = value;
	}

	inline static int32_t get_offset_of_m_InStateTransition_3() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_InStateTransition_3)); }
	inline bool get_m_InStateTransition_3() const { return ___m_InStateTransition_3; }
	inline bool* get_address_of_m_InStateTransition_3() { return &___m_InStateTransition_3; }
	inline void set_m_InStateTransition_3(bool value)
	{
		___m_InStateTransition_3 = value;
	}

	inline static int32_t get_offset_of_m_AppId_4() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_AppId_4)); }
	inline String_t* get_m_AppId_4() const { return ___m_AppId_4; }
	inline String_t** get_address_of_m_AppId_4() { return &___m_AppId_4; }
	inline void set_m_AppId_4(String_t* value)
	{
		___m_AppId_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_AppId_4), value);
	}

	inline static int32_t get_offset_of_m_ClientId_5() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_ClientId_5)); }
	inline String_t* get_m_ClientId_5() const { return ___m_ClientId_5; }
	inline String_t** get_address_of_m_ClientId_5() { return &___m_ClientId_5; }
	inline void set_m_ClientId_5(String_t* value)
	{
		___m_ClientId_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClientId_5), value);
	}

	inline static int32_t get_offset_of_m_TargetStore_6() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_TargetStore_6)); }
	inline String_t* get_m_TargetStore_6() const { return ___m_TargetStore_6; }
	inline String_t** get_address_of_m_TargetStore_6() { return &___m_TargetStore_6; }
	inline void set_m_TargetStore_6(String_t* value)
	{
		___m_TargetStore_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetStore_6), value);
	}

	inline static int32_t get_offset_of_m_AppInstalled_7() { return static_cast<int32_t>(offsetof(AnalyticsClient_t3694105915_StaticFields, ___m_AppInstalled_7)); }
	inline bool get_m_AppInstalled_7() const { return ___m_AppInstalled_7; }
	inline bool* get_address_of_m_AppInstalled_7() { return &___m_AppInstalled_7; }
	inline void set_m_AppInstalled_7(bool value)
	{
		___m_AppInstalled_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSCLIENT_T3694105915_H
#ifndef ANALYTICSSERVICE_T760113998_H
#define ANALYTICSSERVICE_T760113998_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.Analytics.AnalyticsService
struct  AnalyticsService_t760113998  : public RuntimeObject
{
public:

public:
};

struct AnalyticsService_t760113998_StaticFields
{
public:
	// UnityEngine.UDP.Analytics.AnalyticsService/SessionState UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionState
	int32_t ___m_PlayerSessionState_0;
	// System.String UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionId
	String_t* ___m_PlayerSessionId_1;
	// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionElapsedTime
	uint64_t ___m_PlayerSessionElapsedTime_2;
	// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionForegroundTime
	uint64_t ___m_PlayerSessionForegroundTime_3;
	// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::m_PlayerSessionBackgroundTime
	uint64_t ___m_PlayerSessionBackgroundTime_4;

public:
	inline static int32_t get_offset_of_m_PlayerSessionState_0() { return static_cast<int32_t>(offsetof(AnalyticsService_t760113998_StaticFields, ___m_PlayerSessionState_0)); }
	inline int32_t get_m_PlayerSessionState_0() const { return ___m_PlayerSessionState_0; }
	inline int32_t* get_address_of_m_PlayerSessionState_0() { return &___m_PlayerSessionState_0; }
	inline void set_m_PlayerSessionState_0(int32_t value)
	{
		___m_PlayerSessionState_0 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSessionId_1() { return static_cast<int32_t>(offsetof(AnalyticsService_t760113998_StaticFields, ___m_PlayerSessionId_1)); }
	inline String_t* get_m_PlayerSessionId_1() const { return ___m_PlayerSessionId_1; }
	inline String_t** get_address_of_m_PlayerSessionId_1() { return &___m_PlayerSessionId_1; }
	inline void set_m_PlayerSessionId_1(String_t* value)
	{
		___m_PlayerSessionId_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PlayerSessionId_1), value);
	}

	inline static int32_t get_offset_of_m_PlayerSessionElapsedTime_2() { return static_cast<int32_t>(offsetof(AnalyticsService_t760113998_StaticFields, ___m_PlayerSessionElapsedTime_2)); }
	inline uint64_t get_m_PlayerSessionElapsedTime_2() const { return ___m_PlayerSessionElapsedTime_2; }
	inline uint64_t* get_address_of_m_PlayerSessionElapsedTime_2() { return &___m_PlayerSessionElapsedTime_2; }
	inline void set_m_PlayerSessionElapsedTime_2(uint64_t value)
	{
		___m_PlayerSessionElapsedTime_2 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSessionForegroundTime_3() { return static_cast<int32_t>(offsetof(AnalyticsService_t760113998_StaticFields, ___m_PlayerSessionForegroundTime_3)); }
	inline uint64_t get_m_PlayerSessionForegroundTime_3() const { return ___m_PlayerSessionForegroundTime_3; }
	inline uint64_t* get_address_of_m_PlayerSessionForegroundTime_3() { return &___m_PlayerSessionForegroundTime_3; }
	inline void set_m_PlayerSessionForegroundTime_3(uint64_t value)
	{
		___m_PlayerSessionForegroundTime_3 = value;
	}

	inline static int32_t get_offset_of_m_PlayerSessionBackgroundTime_4() { return static_cast<int32_t>(offsetof(AnalyticsService_t760113998_StaticFields, ___m_PlayerSessionBackgroundTime_4)); }
	inline uint64_t get_m_PlayerSessionBackgroundTime_4() const { return ___m_PlayerSessionBackgroundTime_4; }
	inline uint64_t* get_address_of_m_PlayerSessionBackgroundTime_4() { return &___m_PlayerSessionBackgroundTime_4; }
	inline void set_m_PlayerSessionBackgroundTime_4(uint64_t value)
	{
		___m_PlayerSessionBackgroundTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSERVICE_T760113998_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef APPSTORESETTINGS_T763118828_H
#define APPSTORESETTINGS_T763118828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.AppStoreSettings
struct  AppStoreSettings_t763118828  : public ScriptableObject_t2528358522
{
public:
	// System.String UnityEngine.UDP.AppStoreSettings::UnityProjectID
	String_t* ___UnityProjectID_4;
	// System.String UnityEngine.UDP.AppStoreSettings::UnityClientID
	String_t* ___UnityClientID_5;
	// System.String UnityEngine.UDP.AppStoreSettings::UnityClientKey
	String_t* ___UnityClientKey_6;
	// System.String UnityEngine.UDP.AppStoreSettings::UnityClientRSAPublicKey
	String_t* ___UnityClientRSAPublicKey_7;
	// System.String UnityEngine.UDP.AppStoreSettings::AppName
	String_t* ___AppName_8;
	// System.String UnityEngine.UDP.AppStoreSettings::AppSlug
	String_t* ___AppSlug_9;
	// System.String UnityEngine.UDP.AppStoreSettings::AppItemId
	String_t* ___AppItemId_10;
	// System.String UnityEngine.UDP.AppStoreSettings::Permission
	String_t* ___Permission_11;

public:
	inline static int32_t get_offset_of_UnityProjectID_4() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___UnityProjectID_4)); }
	inline String_t* get_UnityProjectID_4() const { return ___UnityProjectID_4; }
	inline String_t** get_address_of_UnityProjectID_4() { return &___UnityProjectID_4; }
	inline void set_UnityProjectID_4(String_t* value)
	{
		___UnityProjectID_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnityProjectID_4), value);
	}

	inline static int32_t get_offset_of_UnityClientID_5() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___UnityClientID_5)); }
	inline String_t* get_UnityClientID_5() const { return ___UnityClientID_5; }
	inline String_t** get_address_of_UnityClientID_5() { return &___UnityClientID_5; }
	inline void set_UnityClientID_5(String_t* value)
	{
		___UnityClientID_5 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientID_5), value);
	}

	inline static int32_t get_offset_of_UnityClientKey_6() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___UnityClientKey_6)); }
	inline String_t* get_UnityClientKey_6() const { return ___UnityClientKey_6; }
	inline String_t** get_address_of_UnityClientKey_6() { return &___UnityClientKey_6; }
	inline void set_UnityClientKey_6(String_t* value)
	{
		___UnityClientKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientKey_6), value);
	}

	inline static int32_t get_offset_of_UnityClientRSAPublicKey_7() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___UnityClientRSAPublicKey_7)); }
	inline String_t* get_UnityClientRSAPublicKey_7() const { return ___UnityClientRSAPublicKey_7; }
	inline String_t** get_address_of_UnityClientRSAPublicKey_7() { return &___UnityClientRSAPublicKey_7; }
	inline void set_UnityClientRSAPublicKey_7(String_t* value)
	{
		___UnityClientRSAPublicKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___UnityClientRSAPublicKey_7), value);
	}

	inline static int32_t get_offset_of_AppName_8() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___AppName_8)); }
	inline String_t* get_AppName_8() const { return ___AppName_8; }
	inline String_t** get_address_of_AppName_8() { return &___AppName_8; }
	inline void set_AppName_8(String_t* value)
	{
		___AppName_8 = value;
		Il2CppCodeGenWriteBarrier((&___AppName_8), value);
	}

	inline static int32_t get_offset_of_AppSlug_9() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___AppSlug_9)); }
	inline String_t* get_AppSlug_9() const { return ___AppSlug_9; }
	inline String_t** get_address_of_AppSlug_9() { return &___AppSlug_9; }
	inline void set_AppSlug_9(String_t* value)
	{
		___AppSlug_9 = value;
		Il2CppCodeGenWriteBarrier((&___AppSlug_9), value);
	}

	inline static int32_t get_offset_of_AppItemId_10() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___AppItemId_10)); }
	inline String_t* get_AppItemId_10() const { return ___AppItemId_10; }
	inline String_t** get_address_of_AppItemId_10() { return &___AppItemId_10; }
	inline void set_AppItemId_10(String_t* value)
	{
		___AppItemId_10 = value;
		Il2CppCodeGenWriteBarrier((&___AppItemId_10), value);
	}

	inline static int32_t get_offset_of_Permission_11() { return static_cast<int32_t>(offsetof(AppStoreSettings_t763118828, ___Permission_11)); }
	inline String_t* get_Permission_11() const { return ___Permission_11; }
	inline String_t** get_address_of_Permission_11() { return &___Permission_11; }
	inline void set_Permission_11(String_t* value)
	{
		___Permission_11 = value;
		Il2CppCodeGenWriteBarrier((&___Permission_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORESETTINGS_T763118828_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MAINTHREADDISPATCHER_T2227136958_H
#define MAINTHREADDISPATCHER_T2227136958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.MainThreadDispatcher
struct  MainThreadDispatcher_t2227136958  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct MainThreadDispatcher_t2227136958_StaticFields
{
public:
	// System.String UnityEngine.UDP.MainThreadDispatcher::OBJECT_NAME
	String_t* ___OBJECT_NAME_4;
	// System.Collections.Generic.List`1<System.Action> UnityEngine.UDP.MainThreadDispatcher::s_Callbacks
	List_1_t2736452219 * ___s_Callbacks_5;
	// System.Collections.Generic.Dictionary`2<System.Single,System.Action> UnityEngine.UDP.MainThreadDispatcher::delayAction
	Dictionary_2_t1212755687 * ___delayAction_6;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngine.UDP.MainThreadDispatcher::s_CallbacksPending
	bool ___s_CallbacksPending_7;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_4() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t2227136958_StaticFields, ___OBJECT_NAME_4)); }
	inline String_t* get_OBJECT_NAME_4() const { return ___OBJECT_NAME_4; }
	inline String_t** get_address_of_OBJECT_NAME_4() { return &___OBJECT_NAME_4; }
	inline void set_OBJECT_NAME_4(String_t* value)
	{
		___OBJECT_NAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___OBJECT_NAME_4), value);
	}

	inline static int32_t get_offset_of_s_Callbacks_5() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t2227136958_StaticFields, ___s_Callbacks_5)); }
	inline List_1_t2736452219 * get_s_Callbacks_5() const { return ___s_Callbacks_5; }
	inline List_1_t2736452219 ** get_address_of_s_Callbacks_5() { return &___s_Callbacks_5; }
	inline void set_s_Callbacks_5(List_1_t2736452219 * value)
	{
		___s_Callbacks_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_Callbacks_5), value);
	}

	inline static int32_t get_offset_of_delayAction_6() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t2227136958_StaticFields, ___delayAction_6)); }
	inline Dictionary_2_t1212755687 * get_delayAction_6() const { return ___delayAction_6; }
	inline Dictionary_2_t1212755687 ** get_address_of_delayAction_6() { return &___delayAction_6; }
	inline void set_delayAction_6(Dictionary_2_t1212755687 * value)
	{
		___delayAction_6 = value;
		Il2CppCodeGenWriteBarrier((&___delayAction_6), value);
	}

	inline static int32_t get_offset_of_s_CallbacksPending_7() { return static_cast<int32_t>(offsetof(MainThreadDispatcher_t2227136958_StaticFields, ___s_CallbacksPending_7)); }
	inline bool get_s_CallbacksPending_7() const { return ___s_CallbacksPending_7; }
	inline bool* get_address_of_s_CallbacksPending_7() { return &___s_CallbacksPending_7; }
	inline void set_s_CallbacksPending_7(bool value)
	{
		___s_CallbacksPending_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINTHREADDISPATCHER_T2227136958_H
#ifndef UDPGAMEMANAGER_T2849128925_H
#define UDPGAMEMANAGER_T2849128925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UDP.UdpGameManager
struct  UdpGameManager_t2849128925  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct UdpGameManager_t2849128925_StaticFields
{
public:
	// System.String UnityEngine.UDP.UdpGameManager::OBJECT_NAME
	String_t* ___OBJECT_NAME_4;

public:
	inline static int32_t get_offset_of_OBJECT_NAME_4() { return static_cast<int32_t>(offsetof(UdpGameManager_t2849128925_StaticFields, ___OBJECT_NAME_4)); }
	inline String_t* get_OBJECT_NAME_4() const { return ___OBJECT_NAME_4; }
	inline String_t** get_address_of_OBJECT_NAME_4() { return &___OBJECT_NAME_4; }
	inline void set_OBJECT_NAME_4(String_t* value)
	{
		___OBJECT_NAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___OBJECT_NAME_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UDPGAMEMANAGER_T2849128925_H
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Action[]
struct ActionU5BU5D_t388269512  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Action_t1264377477 * m_Items[1];

public:
	inline Action_t1264377477 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Action_t1264377477 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Action_t1264377477 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Action_t1264377477 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Action_t1264377477 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Action_t1264377477 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<!0,!1>)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m4025164434_gshared (Dictionary_2_t132545152 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t2086727927  Dictionary_2_GetEnumerator_m3278257048_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR KeyValuePair_2_t2530217319  Enumerator_get_Current_m2655181939_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m1107569389_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m3885012575_gshared (Enumerator_t2086727927 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Object>::set_Item(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m4076998412_gshared (Dictionary_2_t3028484374 * __this, float p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Dictionary`2<System.Single,System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Count_m5797853_gshared (Dictionary_2_t3028484374 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(!0[])
extern "C" IL2CPP_METHOD_ATTR void List_1_CopyTo_m133310179_gshared (List_1_t257213610 * __this, ObjectU5BU5D_t2843939325* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<!0,!1>)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m12006321_gshared (Dictionary_2_t3028484374 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Object>::Clear()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Clear_m1086449433_gshared (Dictionary_2_t3028484374 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Single,System.Object>::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR Enumerator_t687699853  Dictionary_2_GetEnumerator_m3901336279_gshared (Dictionary_2_t3028484374 * __this, const RuntimeMethod* method);
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Object>::get_Current()
extern "C" IL2CPP_METHOD_ATTR KeyValuePair_2_t1131189245  Enumerator_get_Current_m1684375543_gshared (Enumerator_t687699853 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::get_Key()
extern "C" IL2CPP_METHOD_ATTR float KeyValuePair_2_get_Key_m2106391979_gshared (KeyValuePair_2_t1131189245 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Single,System.Object>::get_Value()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * KeyValuePair_2_get_Value_m1864612561_gshared (KeyValuePair_2_t1131189245 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Object>::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m895657732_gshared (Enumerator_t687699853 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Object>::Dispose()
extern "C" IL2CPP_METHOD_ATTR void Enumerator_Dispose_m1384066801_gshared (Enumerator_t687699853 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m1754189472_gshared (Dictionary_2_t3028484374 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m147650894_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_GetStatic_TisRuntimeObject_m2411779517_gshared (AndroidJavaObject_t4131667876 * __this, String_t* p0, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m3850306069_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::Set<System.Object>(System.String,!!0)
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaObject_Set_TisRuntimeObject_m2151407540_gshared (AndroidJavaObject_t4131667876 * __this, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::Call<System.Object>(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_Call_TisRuntimeObject_m3672962752_gshared (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.Object>(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_CallStatic_TisRuntimeObject_m615403890_gshared (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);

// System.Void UnityEngine.UDP.Analytics.SessionInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SessionInfo__ctor_m3167671942 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.PlatformWrapper::GetAppInstalled()
extern "C" IL2CPP_METHOD_ATTR bool PlatformWrapper_GetAppInstalled_m1450815954 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::RequestStateChange(UnityEngine.UDP.Analytics.AnalyticsClient/State)
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_RequestStateChange_m2919322120 (RuntimeObject * __this /* static, unused */, int32_t ___state0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SetSessionId(System.String)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SetSessionId_m2204159219 (RuntimeObject * __this /* static, unused */, String_t* ___sessionId0, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::CloseService()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_CloseService_m1469948210 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::PauseSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_PauseSession_m3295926365 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::ResumeSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_ResumeSession_m1952968552 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::StartSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_StartSession_m3461079815 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::StopSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_StopSession_m2399410269 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::DetermineNextState(UnityEngine.UDP.Analytics.AnalyticsClient/State,UnityEngine.UDP.Analytics.AnalyticsClient/State&)
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_DetermineNextState_m3099811717 (RuntimeObject * __this /* static, unused */, int32_t ___requestedState0, int32_t* ___nextState1, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::ProcessState(UnityEngine.UDP.Analytics.AnalyticsClient/State)
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_ProcessState_m3142547468 (RuntimeObject * __this /* static, unused */, int32_t ___nextState0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStateReady()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStateReady_m4203028284 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStatePrepared()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStatePrepared_m61927341 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStateStarted()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStateStarted_m2269963401 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStatePaused()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStatePaused_m3040972833 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStateStopped()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStateStopped_m3763008639 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SetState(UnityEngine.UDP.Analytics.AnalyticsClient/State)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SetState_m2621078371 (RuntimeObject * __this /* static, unused */, int32_t ___state0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MAppId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MAppId_m913923638 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MClientId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MClientId_m1274542372 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MTargetStore(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MTargetStore_m3709987134 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GetRuntimePlatformString()
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GetRuntimePlatformString_m2991492228 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MPlatform(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MPlatform_m4115858467 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GetSystemInfo()
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GetSystemInfo_m4243581290 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MSystemInfo(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MSystemInfo_m303435404 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C" IL2CPP_METHOD_ATTR String_t* SystemInfo_get_deviceUniqueIdentifier_m3439870207 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MDeviceId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MDeviceId_m1445056562 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MVr(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MVr_m3307225907 (SessionInfo_t3964168581 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.Events.AppStartEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR void AppStartEvent__ctor_m3550927429 (AppStartEvent_t147987345 * __this, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.EventDispatcher::DispatchEvent(System.Object)
extern "C" IL2CPP_METHOD_ATTR void EventDispatcher_DispatchEvent_m2849035944 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___e0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.Events.AppInstallEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR void AppInstallEvent__ctor_m2634031396 (AppInstallEvent_t505962745 * __this, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SavePersistentValue()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SavePersistentValue_m796660950 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnteringStatePausedOrStopped()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnteringStatePausedOrStopped_m600887597 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.Events.AppStopEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR void AppStopEvent__ctor_m761874387 (AppStopEvent_t3879155 * __this, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SendAppRunningEvent()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SendAppRunningEvent_m140057793 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsClient::GetPlayerSessionElapsedTime()
extern "C" IL2CPP_METHOD_ATTR uint64_t AnalyticsClient_GetPlayerSessionElapsedTime_m3367269782 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.Events.AppRunningEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo,System.UInt64)
extern "C" IL2CPP_METHOD_ATTR void AppRunningEvent__ctor_m2641558448 (AppRunningEvent_t590370228 * __this, SessionInfo_t3964168581 * ___sessionInfo0, uint64_t ___duration1, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.PlatformWrapper::SetAppInstalled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlatformWrapper_SetAppInstalled_m48601936 (RuntimeObject * __this /* static, unused */, bool ___v0, const RuntimeMethod* method);
// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::GetPlayerSessionElapsedTime()
extern "C" IL2CPP_METHOD_ATTR uint64_t AnalyticsService_GetPlayerSessionElapsedTime_m889664768 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MSessionId()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MSessionId_m4126112815 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MSessionId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MSessionId_m620876705 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::onPlayerStateChanged(UnityEngine.UDP.Analytics.AnalyticsService/SessionState)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_onPlayerStateChanged_m3149236035 (RuntimeObject * __this /* static, unused */, int32_t ___sessionState0, const RuntimeMethod* method);
// System.UInt64 UnityEngine.UDP.Analytics.PlatformWrapper::GetCurrentMillisecondsInUTC()
extern "C" IL2CPP_METHOD_ATTR uint64_t PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GetPlayerPrefsString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GetPlayerPrefsString_m2758919265 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method);
// System.UInt64 UnityEngine.UDP.Analytics.PlatformWrapper::GetPlayerPrefsUInt64(System.String)
extern "C" IL2CPP_METHOD_ATTR uint64_t PlatformWrapper_GetPlayerPrefsUInt64_m3314372928 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GenerateRandomId()
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GenerateRandomId_m2000620507 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.PlatformWrapper::SetPlayerPrefsString(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PlatformWrapper_SetPlayerPrefsString_m3943675500 (RuntimeObject * __this /* static, unused */, String_t* ___name0, String_t* ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.PlatformWrapper::SetPlayerPrefsUInt64(System.String,System.UInt64)
extern "C" IL2CPP_METHOD_ATTR void PlatformWrapper_SetPlayerPrefsUInt64_m1611667148 (RuntimeObject * __this /* static, unused */, String_t* ___name0, uint64_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnPlayerSessionStateChanged(UnityEngine.UDP.Analytics.AnalyticsService/SessionState,System.String,System.UInt64)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnPlayerSessionStateChanged_m2310188355 (RuntimeObject * __this /* static, unused */, int32_t ___sessionState0, String_t* ___sessionId1, uint64_t ___sessionElapsedTime2, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
inline void Dictionary_2__ctor_m2450723815 (Dictionary_2_t2865362463 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2865362463 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MClientId()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MClientId_m922198160 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1)
inline void Dictionary_2_Add_m64471701 (Dictionary_2_t2865362463 * __this, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method);
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MDeviceId()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MDeviceId_m4217116151 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MPlatform()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MPlatform_m1348540618 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MSystemInfo()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MSystemInfo_m1681693923 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MTargetStore()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MTargetStore_m2176927300 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.SessionInfo::get_MVr()
extern "C" IL2CPP_METHOD_ATTR bool SessionInfo_get_MVr_m351961376 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_m366853020 (AndroidJavaClass_t32045322 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.EventDispatcher::init()
extern "C" IL2CPP_METHOD_ATTR void EventDispatcher_init_m1453862928 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::CallStatic(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaObject_CallStatic_m2922144688 (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaProxy::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaProxy__ctor_m545570009 (AndroidJavaProxy_t2835824643 * __this, String_t* p0, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Common::GetCommonParams(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR Dictionary_2_t2865362463 * Common_GetCommonParams_m2618682865 (RuntimeObject * __this /* static, unused */, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method);
// UnityEngine.UDP.Analytics.SessionInfo UnityEngine.UDP.Analytics.AnalyticsClient::GetSessionInfo()
extern "C" IL2CPP_METHOD_ATTR SessionInfo_t3964168581 * AnalyticsClient_GetSessionInfo_m2557928941 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_UtcNow()
extern "C" IL2CPP_METHOD_ATTR DateTime_t3738529785  DateTime_get_UtcNow_m1393945741 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.DateTime::.ctor(System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void DateTime__ctor_m12900168 (DateTime_t3738529785 * __this, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.TimeSpan System.DateTime::Subtract(System.DateTime)
extern "C" IL2CPP_METHOD_ATTR TimeSpan_t881159249  DateTime_Subtract_m77007479 (DateTime_t3738529785 * __this, DateTime_t3738529785  p0, const RuntimeMethod* method);
// System.Double System.TimeSpan::get_TotalMilliseconds()
extern "C" IL2CPP_METHOD_ATTR double TimeSpan_get_TotalMilliseconds_m2429771311 (TimeSpan_t881159249 * __this, const RuntimeMethod* method);
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C" IL2CPP_METHOD_ATTR int32_t Application_get_platform_m2150679437 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.SystemInfo::get_deviceModel()
extern "C" IL2CPP_METHOD_ATTR String_t* SystemInfo_get_deviceModel_m4078951941 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* PlayerPrefs_GetString_m614532710 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_SetString_m2101271233 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m1299643124 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_m2842000469 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Guid System.Guid::NewGuid()
extern "C" IL2CPP_METHOD_ATTR Guid_t  Guid_NewGuid_m923091018 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.Guid::ToString()
extern "C" IL2CPP_METHOD_ATTR String_t* Guid_ToString_m3279186591 (Guid_t * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UDP.Analytics.UdpAnalytics::isInitialized()
extern "C" IL2CPP_METHOD_ATTR bool UdpAnalytics_isInitialized_m2641110313 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PurchaseAttemptEvent__ctor_m3473196206 (PurchaseAttemptEvent_t2556690677 * __this, String_t* ___productId0, String_t* ___uuid1, const RuntimeMethod* method);
// UnityEngine.UDP.Analytics.AnalyticsResult UnityEngine.UDP.Analytics.UdpAnalytics::dispatchEvent(System.Object)
extern "C" IL2CPP_METHOD_ATTR int32_t UdpAnalytics_dispatchEvent_m2425667419 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___e0, const RuntimeMethod* method);
// System.String UnityEngine.UDP.Analytics.AnalyticsClient::GetSessionId()
extern "C" IL2CPP_METHOD_ATTR String_t* AnalyticsClient_GetSessionId_m1082839017 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>::.ctor(System.Collections.Generic.IDictionary`2<!0,!1>)
inline void Dictionary_2__ctor_m2644321878 (Dictionary_2_t2815580273 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2815580273 *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_m4025164434_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.UDP.PurchaseInfo>::.ctor()
inline void List_1__ctor_m502973724 (List_1_t207431420 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t207431420 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.String,UnityEngine.UDP.PurchaseInfo>::GetEnumerator()
inline Enumerator_t474795752  Dictionary_2_GetEnumerator_m979315400 (Dictionary_2_t2815580273 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t474795752  (*) (Dictionary_2_t2815580273 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3278257048_gshared)(__this, method);
}
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.UDP.PurchaseInfo>::get_Current()
inline KeyValuePair_2_t918285144  Enumerator_get_Current_m608108010 (Enumerator_t474795752 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t918285144  (*) (Enumerator_t474795752 *, const RuntimeMethod*))Enumerator_get_Current_m2655181939_gshared)(__this, method);
}
// !1 System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.UDP.PurchaseInfo>::get_Value()
inline PurchaseInfo_t3030323974 * KeyValuePair_2_get_Value_m2243092683 (KeyValuePair_2_t918285144 * __this, const RuntimeMethod* method)
{
	return ((  PurchaseInfo_t3030323974 * (*) (KeyValuePair_2_t918285144 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.UDP.PurchaseInfo>::Add(!0)
inline void List_1_Add_m610740988 (List_1_t207431420 * __this, PurchaseInfo_t3030323974 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t207431420 *, PurchaseInfo_t3030323974 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.UDP.PurchaseInfo>::MoveNext()
inline bool Enumerator_MoveNext_m1849793072 (Enumerator_t474795752 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t474795752 *, const RuntimeMethod*))Enumerator_MoveNext_m1107569389_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.UDP.PurchaseInfo>::Dispose()
inline void Enumerator_Dispose_m158248455 (Enumerator_t474795752 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t474795752 *, const RuntimeMethod*))Enumerator_Dispose_m3885012575_gshared)(__this, method);
}
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Enter_m2249409497 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::Add(!0)
inline void List_1_Add_m437696916 (List_1_t2736452219 * __this, Action_t1264377477 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2736452219 *, Action_t1264377477 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Monitor_Exit_m3585316909 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Action>::set_Item(!0,!1)
inline void Dictionary_2_set_Item_m4203027293 (Dictionary_2_t1212755687 * __this, float p0, Action_t1264377477 * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1212755687 *, float, Action_t1264377477 *, const RuntimeMethod*))Dictionary_2_set_Item_m4076998412_gshared)(__this, p0, p1, method);
}
// System.Void UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CWaitAndDoU3Ed__6__ctor_m3349979845 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count()
inline int32_t List_1_get_Count_m808340314 (List_1_t2736452219 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t2736452219 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.Dictionary`2<System.Single,System.Action>::get_Count()
inline int32_t Dictionary_2_get_Count_m3991288552 (Dictionary_2_t1212755687 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_t1212755687 *, const RuntimeMethod*))Dictionary_2_get_Count_m5797853_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::CopyTo(!0[])
inline void List_1_CopyTo_m2569245864 (List_1_t2736452219 * __this, ActionU5BU5D_t388269512* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2736452219 *, ActionU5BU5D_t388269512*, const RuntimeMethod*))List_1_CopyTo_m133310179_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.Action>::Clear()
inline void List_1_Clear_m2936844859 (List_1_t2736452219 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2736452219 *, const RuntimeMethod*))List_1_Clear_m3697625829_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Action>::.ctor(System.Collections.Generic.IDictionary`2<!0,!1>)
inline void Dictionary_2__ctor_m792470897 (Dictionary_2_t1212755687 * __this, RuntimeObject* p0, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1212755687 *, RuntimeObject*, const RuntimeMethod*))Dictionary_2__ctor_m12006321_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Action>::Clear()
inline void Dictionary_2_Clear_m607508611 (Dictionary_2_t1212755687 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1212755687 *, const RuntimeMethod*))Dictionary_2_Clear_m1086449433_gshared)(__this, method);
}
// System.Void System.Action::Invoke()
extern "C" IL2CPP_METHOD_ATTR void Action_Invoke_m937035532 (Action_t1264377477 * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2/Enumerator<!0,!1> System.Collections.Generic.Dictionary`2<System.Single,System.Action>::GetEnumerator()
inline Enumerator_t3166938462  Dictionary_2_GetEnumerator_m650535580 (Dictionary_2_t1212755687 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t3166938462  (*) (Dictionary_2_t1212755687 *, const RuntimeMethod*))Dictionary_2_GetEnumerator_m3901336279_gshared)(__this, method);
}
// System.Collections.Generic.KeyValuePair`2<!0,!1> System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Action>::get_Current()
inline KeyValuePair_2_t3610427854  Enumerator_get_Current_m1534630570 (Enumerator_t3166938462 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t3610427854  (*) (Enumerator_t3166938462 *, const RuntimeMethod*))Enumerator_get_Current_m1684375543_gshared)(__this, method);
}
// !0 System.Collections.Generic.KeyValuePair`2<System.Single,System.Action>::get_Key()
inline float KeyValuePair_2_get_Key_m2070292874 (KeyValuePair_2_t3610427854 * __this, const RuntimeMethod* method)
{
	return ((  float (*) (KeyValuePair_2_t3610427854 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2106391979_gshared)(__this, method);
}
// !1 System.Collections.Generic.KeyValuePair`2<System.Single,System.Action>::get_Value()
inline Action_t1264377477 * KeyValuePair_2_get_Value_m3459043748 (KeyValuePair_2_t3610427854 * __this, const RuntimeMethod* method)
{
	return ((  Action_t1264377477 * (*) (KeyValuePair_2_t3610427854 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1864612561_gshared)(__this, method);
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> UnityEngine.UDP.MainThreadDispatcher::WaitAndDo(System.Single,System.Action)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MainThreadDispatcher_WaitAndDo_m930138745 (MainThreadDispatcher_t2227136958 * __this, float ___waitTime0, Action_t1264377477 * ___runnable1, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Action>::MoveNext()
inline bool Enumerator_MoveNext_m2562535175 (Enumerator_t3166938462 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3166938462 *, const RuntimeMethod*))Enumerator_MoveNext_m895657732_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Single,System.Action>::Dispose()
inline void Enumerator_Dispose_m1063885580 (Enumerator_t3166938462 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t3166938462 *, const RuntimeMethod*))Enumerator_Dispose_m1384066801_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
inline void List_1__ctor_m2543424368 (List_1_t2736452219 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2736452219 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Single,System.Action>::.ctor()
inline void Dictionary_2__ctor_m784414107 (Dictionary_2_t1212755687 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1212755687 *, const RuntimeMethod*))Dictionary_2__ctor_m1754189472_gshared)(__this, method);
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" IL2CPP_METHOD_ATTR GameObject_t1113636619 * GameObject_Find_m2032535176 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void GameObject__ctor_m2093116449 (GameObject_t1113636619 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" IL2CPP_METHOD_ATTR void Object_set_hideFlags_m1648752846 (Object_t631007953 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UDP.MainThreadDispatcher>()
inline MainThreadDispatcher_t2227136958 * GameObject_AddComponent_TisMainThreadDispatcher_t2227136958_m3710866612 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  MainThreadDispatcher_t2227136958 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m147650894_gshared)(__this, method);
}
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
inline AndroidJavaObject_t4131667876 * AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4131667876_m671986461 (AndroidJavaObject_t4131667876 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t4131667876 * (*) (AndroidJavaObject_t4131667876 *, String_t*, const RuntimeMethod*))AndroidJavaObject_GetStatic_TisRuntimeObject_m2411779517_gshared)(__this, p0, method);
}
// System.Void UnityEngine.UDP.InitLoginForwardCallback::.ctor(UnityEngine.UDP.IInitListener)
extern "C" IL2CPP_METHOD_ATTR void InitLoginForwardCallback__ctor_m2942706633 (InitLoginForwardCallback_t1246786093 * __this, RuntimeObject* ___initListener0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::.ctor(System.String,System.Object[])
extern "C" IL2CPP_METHOD_ATTR void AndroidJavaObject__ctor_m3828648572 (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.UDP.AppStoreSettings>(System.String)
inline AppStoreSettings_t763118828 * Resources_Load_TisAppStoreSettings_t763118828_m644194161 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method)
{
	return ((  AppStoreSettings_t763118828 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m3850306069_gshared)(__this /* static, unused */, p0, method);
}
// System.String UnityEngine.UDP.AppInfo::get_ClientId()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_ClientId_m1683986167 (AppInfo_t4053800566 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.AppInfo::get_ClientKey()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_ClientKey_m326902363 (AppInfo_t4053800566 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.AppInfo::get_AppSlug()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_AppSlug_m967783639 (AppInfo_t4053800566 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.AppInfo::get_RSAPublicKey()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_RSAPublicKey_m2216431782 (AppInfo_t4053800566 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaObject::Set<System.String>(System.String,!!0)
inline void AndroidJavaObject_Set_TisString_t_m2428066008 (AndroidJavaObject_t4131667876 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method)
{
	((  void (*) (AndroidJavaObject_t4131667876 *, String_t*, String_t*, const RuntimeMethod*))AndroidJavaObject_Set_TisRuntimeObject_m2151407540_gshared)(__this, p0, p1, method);
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::Initialize()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_Initialize_m4044694567 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String UnityEngine.UDP.StoreService::get_StoreName()
extern "C" IL2CPP_METHOD_ATTR String_t* StoreService_get_StoreName_m1656101778 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::Initialize(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_Initialize_m463104264 (RuntimeObject * __this /* static, unused */, String_t* ___clientId0, String_t* ___appId1, String_t* ___targetStore2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.UDP.UdpGameManager>()
inline UdpGameManager_t2849128925 * GameObject_AddComponent_TisUdpGameManager_t2849128925_m3960156118 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  UdpGameManager_t2849128925 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m147650894_gshared)(__this, method);
}
// System.String System.String::Replace(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Replace_m1273907647 (String_t* __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// UnityEngine.UDP.Analytics.AnalyticsResult UnityEngine.UDP.Analytics.UdpAnalytics::PurchaseAttempt(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t UdpAnalytics_PurchaseAttempt_m745049746 (RuntimeObject * __this /* static, unused */, String_t* ___productionId0, String_t* ___uuid1, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.PurchaseForwardCallback::.ctor(UnityEngine.UDP.IPurchaseListener)
extern "C" IL2CPP_METHOD_ATTR void PurchaseForwardCallback__ctor_m665942399 (PurchaseForwardCallback_t1388311153 * __this, RuntimeObject* ___purchaseListener0, const RuntimeMethod* method);
// UnityEngine.AndroidJavaObject UnityEngine.UDP.StoreService::javaArrayFromCSList(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR AndroidJavaObject_t4131667876 * StoreService_javaArrayFromCSList_m2484081227 (RuntimeObject * __this /* static, unused */, List_1_t3319525431 * ___values0, const RuntimeMethod* method);
// System.String UnityEngine.UDP.PurchaseInfo::get_ItemType()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_ItemType_m3568345912 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::Call<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
inline AndroidJavaObject_t4131667876 * AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563 (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t4131667876 * (*) (AndroidJavaObject_t4131667876 *, String_t*, ObjectU5BU5D_t2843939325*, const RuntimeMethod*))AndroidJavaObject_Call_TisRuntimeObject_m3672962752_gshared)(__this, p0, p1, method);
}
// System.String UnityEngine.UDP.PurchaseInfo::get_ProductId()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_ProductId_m3051669569 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.PurchaseInfo::get_DeveloperPayload()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_DeveloperPayload_m3489613356 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.PurchaseInfo::get_GameOrderId()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_GameOrderId_m404388006 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.PurchaseInfo::get_OrderQueryToken()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_OrderQueryToken_m3126970672 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method);
// System.String UnityEngine.UDP.PurchaseInfo::get_StorePurchaseJsonString()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_StorePurchaseJsonString_m1065397329 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::CallStatic<System.String>(System.String,System.Object[])
inline String_t* AndroidJavaObject_CallStatic_TisString_t_m522892673 (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method)
{
	return ((  String_t* (*) (AndroidJavaObject_t4131667876 *, String_t*, ObjectU5BU5D_t2843939325*, const RuntimeMethod*))AndroidJavaObject_CallStatic_TisRuntimeObject_m615403890_gshared)(__this, p0, p1, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m2276455407 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1_get_Count_m2934127733_gshared)(__this, method);
}
// !!0 UnityEngine.AndroidJavaObject::CallStatic<UnityEngine.AndroidJavaObject>(System.String,System.Object[])
inline AndroidJavaObject_t4131667876 * AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4131667876_m1630827594 (AndroidJavaObject_t4131667876 * __this, String_t* p0, ObjectU5BU5D_t2843939325* p1, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t4131667876 * (*) (AndroidJavaObject_t4131667876 *, String_t*, ObjectU5BU5D_t2843939325*, const RuntimeMethod*))AndroidJavaObject_CallStatic_TisRuntimeObject_m615403890_gshared)(__this, p0, p1, method);
}
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
inline String_t* List_1_get_Item_m953835688 (List_1_t3319525431 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2287542950_gshared)(__this, p0, method);
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::OnAppAwake()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_OnAppAwake_m1143235686 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::OnPlayerPaused(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_OnPlayerPaused_m1901506767 (RuntimeObject * __this /* static, unused */, bool ___paused0, const RuntimeMethod* method);
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::OnPlayerQuit()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_OnPlayerQuit_m2182618095 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::Initialize(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_Initialize_m463104264 (RuntimeObject * __this /* static, unused */, String_t* ___clientId0, String_t* ___appId1, String_t* ___targetStore2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_Initialize_m463104264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___clientId0;
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_ClientId_5(L_0);
		String_t* L_1 = ___appId1;
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_AppId_4(L_1);
		String_t* L_2 = ___targetStore2;
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_TargetStore_6(L_2);
		SessionInfo_t3964168581 * L_3 = (SessionInfo_t3964168581 *)il2cpp_codegen_object_new(SessionInfo_t3964168581_il2cpp_TypeInfo_var);
		SessionInfo__ctor_m3167671942(L_3, /*hidden argument*/NULL);
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_sessionInfo_0(L_3);
		bool L_4 = PlatformWrapper_GetAppInstalled_m1450815954(NULL /*static, unused*/, /*hidden argument*/NULL);
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_AppInstalled_7(L_4);
		AnalyticsClient_RequestStateChange_m2919322120(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnPlayerSessionStateChanged(UnityEngine.UDP.Analytics.AnalyticsService/SessionState,System.String,System.UInt64)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnPlayerSessionStateChanged_m2310188355 (RuntimeObject * __this /* static, unused */, int32_t ___sessionState0, String_t* ___sessionId1, uint64_t ___sessionElapsedTime2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_OnPlayerSessionStateChanged_m2310188355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___sessionId1;
		AnalyticsClient_SetSessionId_m2204159219(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___sessionState0;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = ___sessionState0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0016;
		}
	}
	{
		goto IL_001d;
	}

IL_000f:
	{
		AnalyticsClient_CloseService_m1469948210(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_0016:
	{
		AnalyticsClient_PauseSession_m3295926365(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		int32_t L_3 = ___sessionState0;
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_002f;
		}
	}
	{
		bool L_4 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_IsNewSession_1();
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		AnalyticsClient_ResumeSession_m1952968552(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_002f:
	{
		AnalyticsClient_StartSession_m3461079815(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::StartSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_StartSession_m3461079815 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		bool L_0 = AnalyticsClient_RequestStateChange_m2919322120(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::ResumeSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_ResumeSession_m1952968552 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		bool L_0 = AnalyticsClient_RequestStateChange_m2919322120(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::PauseSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_PauseSession_m3295926365 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		bool L_0 = AnalyticsClient_RequestStateChange_m2919322120(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::StopSession()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_StopSession_m2399410269 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		bool L_0 = AnalyticsClient_RequestStateChange_m2919322120(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::CloseService()
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_CloseService_m1469948210 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_CloseService_m1469948210_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_State_2();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return (bool)0;
	}

IL_0009:
	{
		AnalyticsClient_StopSession_m2399410269(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::RequestStateChange(UnityEngine.UDP.Analytics.AnalyticsClient/State)
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_RequestStateChange_m2919322120 (RuntimeObject * __this /* static, unused */, int32_t ___state0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_RequestStateChange_m2919322120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		V_0 = (bool)0;
		bool L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_InStateTransition_3();
		if (L_0)
		{
			goto IL_0028;
		}
	}
	{
		V_1 = 0;
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_InStateTransition_3((bool)1);
		int32_t L_1 = ___state0;
		bool L_2 = AnalyticsClient_DetermineNextState_m3099811717(NULL /*static, unused*/, L_1, (int32_t*)(&V_1), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_3 = V_1;
		bool L_4 = AnalyticsClient_ProcessState_m3142547468(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0022:
	{
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_InStateTransition_3((bool)0);
	}

IL_0028:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::DetermineNextState(UnityEngine.UDP.Analytics.AnalyticsClient/State,UnityEngine.UDP.Analytics.AnalyticsClient/State&)
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_DetermineNextState_m3099811717 (RuntimeObject * __this /* static, unused */, int32_t ___requestedState0, int32_t* ___nextState1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_DetermineNextState_m3099811717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t* L_0 = ___nextState1;
		int32_t L_1 = ___requestedState0;
		*((int32_t*)L_0) = (int32_t)L_1;
		int32_t L_2 = ___requestedState0;
		int32_t L_3 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_State_2();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		int32_t L_4 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_State_2();
		V_0 = L_4;
		int32_t L_5 = V_0;
		switch (L_5)
		{
			case 0:
			{
				goto IL_0033;
			}
			case 1:
			{
				goto IL_0039;
			}
			case 2:
			{
				goto IL_0048;
			}
			case 3:
			{
				goto IL_0048;
			}
			case 4:
			{
				goto IL_0048;
			}
			case 5:
			{
				goto IL_0033;
			}
		}
	}
	{
		goto IL_0048;
	}

IL_0033:
	{
		int32_t L_6 = ___requestedState0;
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}

IL_0039:
	{
		int32_t L_7 = ___requestedState0;
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_0042;
		}
	}
	{
		int32_t* L_8 = ___nextState1;
		*((int32_t*)L_8) = (int32_t)2;
		goto IL_0048;
	}

IL_0042:
	{
		int32_t L_9 = ___requestedState0;
		if ((!(((uint32_t)L_9) == ((uint32_t)4))))
		{
			goto IL_0048;
		}
	}
	{
		return (bool)0;
	}

IL_0048:
	{
		return (bool)1;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.AnalyticsClient::ProcessState(UnityEngine.UDP.Analytics.AnalyticsClient/State)
extern "C" IL2CPP_METHOD_ATTR bool AnalyticsClient_ProcessState_m3142547468 (RuntimeObject * __this /* static, unused */, int32_t ___nextState0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___nextState0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)1)))
		{
			case 0:
			{
				goto IL_001e;
			}
			case 1:
			{
				goto IL_002a;
			}
			case 2:
			{
				goto IL_0031;
			}
			case 3:
			{
				goto IL_0038;
			}
			case 4:
			{
				goto IL_003f;
			}
		}
	}
	{
		goto IL_0046;
	}

IL_001e:
	{
		AnalyticsClient_OnEnterStateReady_m4203028284(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnalyticsClient_OnEnterStatePrepared_m61927341(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_002a:
	{
		AnalyticsClient_OnEnterStatePrepared_m61927341(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0031:
	{
		AnalyticsClient_OnEnterStateStarted_m2269963401(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0038:
	{
		AnalyticsClient_OnEnterStatePaused_m3040972833(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_003f:
	{
		AnalyticsClient_OnEnterStateStopped_m3763008639(NULL /*static, unused*/, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_0046:
	{
		return (bool)0;
	}

IL_0048:
	{
		return (bool)1;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStateReady()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStateReady_m4203028284 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_OnEnterStateReady_m4203028284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AnalyticsClient_SetState_m2621078371(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_1 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_AppId_4();
		NullCheck(L_0);
		SessionInfo_set_MAppId_m913923638(L_0, L_1, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_2 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_3 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_ClientId_5();
		NullCheck(L_2);
		SessionInfo_set_MClientId_m1274542372(L_2, L_3, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_4 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_5 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_TargetStore_6();
		NullCheck(L_4);
		SessionInfo_set_MTargetStore_m3709987134(L_4, L_5, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_6 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_7 = PlatformWrapper_GetRuntimePlatformString_m2991492228(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		SessionInfo_set_MPlatform_m4115858467(L_6, L_7, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_8 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_9 = PlatformWrapper_GetSystemInfo_m4243581290(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_8);
		SessionInfo_set_MSystemInfo_m303435404(L_8, L_9, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_10 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_11 = SystemInfo_get_deviceUniqueIdentifier_m3439870207(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		SessionInfo_set_MDeviceId_m1445056562(L_10, L_11, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_12 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		NullCheck(L_12);
		SessionInfo_set_MVr_m3307225907(L_12, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStatePrepared()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStatePrepared_m61927341 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_OnEnterStatePrepared_m61927341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_State_2();
		AnalyticsClient_SetState_m2621078371(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStateStarted()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStateStarted_m2269963401 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_OnEnterStateStarted_m2269963401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AnalyticsClient_SetState_m2621078371(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		bool L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_IsNewSession_1();
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		SessionInfo_t3964168581 * L_1 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		AppStartEvent_t147987345 * L_2 = (AppStartEvent_t147987345 *)il2cpp_codegen_object_new(AppStartEvent_t147987345_il2cpp_TypeInfo_var);
		AppStartEvent__ctor_m3550927429(L_2, L_1, /*hidden argument*/NULL);
		EventDispatcher_DispatchEvent_m2849035944(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_3 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_AppInstalled_7();
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		SessionInfo_t3964168581 * L_4 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		AppInstallEvent_t505962745 * L_5 = (AppInstallEvent_t505962745 *)il2cpp_codegen_object_new(AppInstallEvent_t505962745_il2cpp_TypeInfo_var);
		AppInstallEvent__ctor_m2634031396(L_5, L_4, /*hidden argument*/NULL);
		EventDispatcher_DispatchEvent_m2849035944(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_AppInstalled_7((bool)1);
		AnalyticsClient_SavePersistentValue_m796660950(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_003d:
	{
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_IsNewSession_1((bool)0);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStatePaused()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStatePaused_m3040972833 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		AnalyticsClient_OnEnteringStatePausedOrStopped_m600887597(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnalyticsClient_SetState_m2621078371(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnterStateStopped()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnterStateStopped_m3763008639 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_OnEnterStateStopped_m3763008639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_State_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)3))))
		{
			goto IL_000d;
		}
	}
	{
		AnalyticsClient_OnEnteringStatePausedOrStopped_m600887597(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_000d:
	{
		SessionInfo_t3964168581 * L_1 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		AppStopEvent_t3879155 * L_2 = (AppStopEvent_t3879155 *)il2cpp_codegen_object_new(AppStopEvent_t3879155_il2cpp_TypeInfo_var);
		AppStopEvent__ctor_m761874387(L_2, L_1, /*hidden argument*/NULL);
		EventDispatcher_DispatchEvent_m2849035944(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::OnEnteringStatePausedOrStopped()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_OnEnteringStatePausedOrStopped_m600887597 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		AnalyticsClient_SendAppRunningEvent_m140057793(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnalyticsClient_SavePersistentValue_m796660950(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SendAppRunningEvent()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SendAppRunningEvent_m140057793 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_SendAppRunningEvent_m140057793_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SessionInfo_t3964168581 * L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		uint64_t L_1 = AnalyticsClient_GetPlayerSessionElapsedTime_m3367269782(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppRunningEvent_t590370228 * L_2 = (AppRunningEvent_t590370228 *)il2cpp_codegen_object_new(AppRunningEvent_t590370228_il2cpp_TypeInfo_var);
		AppRunningEvent__ctor_m2641558448(L_2, L_0, L_1, /*hidden argument*/NULL);
		EventDispatcher_DispatchEvent_m2849035944(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SavePersistentValue()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SavePersistentValue_m796660950 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_SavePersistentValue_m796660950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_AppInstalled_7();
		PlatformWrapper_SetAppInstalled_m48601936(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsClient::GetPlayerSessionElapsedTime()
extern "C" IL2CPP_METHOD_ATTR uint64_t AnalyticsClient_GetPlayerSessionElapsedTime_m3367269782 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		uint64_t L_0 = AnalyticsService_GetPlayerSessionElapsedTime_m889664768(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SetSessionId(System.String)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SetSessionId_m2204159219 (RuntimeObject * __this /* static, unused */, String_t* ___sessionId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_SetSessionId_m2204159219_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SessionInfo_t3964168581 * L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		NullCheck(L_0);
		String_t* L_1 = SessionInfo_get_MSessionId_m4126112815(L_0, /*hidden argument*/NULL);
		String_t* L_2 = ___sessionId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_IsNewSession_1(L_3);
		SessionInfo_t3964168581 * L_4 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		String_t* L_5 = ___sessionId0;
		NullCheck(L_4);
		SessionInfo_set_MSessionId_m620876705(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsClient::SetState(UnityEngine.UDP.Analytics.AnalyticsClient/State)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsClient_SetState_m2621078371 (RuntimeObject * __this /* static, unused */, int32_t ___state0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_SetState_m2621078371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___state0;
		((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->set_m_State_2(L_0);
		return;
	}
}
// UnityEngine.UDP.Analytics.SessionInfo UnityEngine.UDP.Analytics.AnalyticsClient::GetSessionInfo()
extern "C" IL2CPP_METHOD_ATTR SessionInfo_t3964168581 * AnalyticsClient_GetSessionInfo_m2557928941 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_GetSessionInfo_m2557928941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SessionInfo_t3964168581 * L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		return L_0;
	}
}
// System.String UnityEngine.UDP.Analytics.AnalyticsClient::GetSessionId()
extern "C" IL2CPP_METHOD_ATTR String_t* AnalyticsClient_GetSessionId_m1082839017 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsClient_GetSessionId_m1082839017_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SessionInfo_t3964168581 * L_0 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		SessionInfo_t3964168581 * L_1 = ((AnalyticsClient_t3694105915_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsClient_t3694105915_il2cpp_TypeInfo_var))->get_m_sessionInfo_0();
		NullCheck(L_1);
		String_t* L_2 = SessionInfo_get_MSessionId_m4126112815(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0012:
	{
		return _stringLiteral757602046;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::Initialize()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_Initialize_m4044694567 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsService_Initialize_m4044694567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionState_0(0);
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionId_1(_stringLiteral757602046);
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionElapsedTime_2((((int64_t)((int64_t)0))));
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionForegroundTime_3((((int64_t)((int64_t)0))));
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionBackgroundTime_4((((int64_t)((int64_t)0))));
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::OnPlayerQuit()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_OnPlayerQuit_m2182618095 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		AnalyticsService_onPlayerStateChanged_m3149236035(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::OnPlayerPaused(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_OnPlayerPaused_m1901506767 (RuntimeObject * __this /* static, unused */, bool ___paused0, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = ___paused0;
		if (L_0)
		{
			goto IL_0006;
		}
	}
	{
		G_B3_0 = 3;
		goto IL_0007;
	}

IL_0006:
	{
		G_B3_0 = 2;
	}

IL_0007:
	{
		AnalyticsService_onPlayerStateChanged_m3149236035(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::OnAppAwake()
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_OnAppAwake_m1143235686 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		AnalyticsService_onPlayerStateChanged_m3149236035(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.AnalyticsService::onPlayerStateChanged(UnityEngine.UDP.Analytics.AnalyticsService/SessionState)
extern "C" IL2CPP_METHOD_ATTR void AnalyticsService_onPlayerStateChanged_m3149236035 (RuntimeObject * __this /* static, unused */, int32_t ___sessionState0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsService_onPlayerStateChanged_m3149236035_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	uint64_t V_1 = 0;
	uint64_t V_2 = 0;
	{
		int32_t L_0 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionState_0();
		int32_t L_1 = ___sessionState0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		int32_t L_2 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionState_0();
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_3 = ___sessionState0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0015;
		}
	}
	{
		return;
	}

IL_0015:
	{
		uint64_t L_4 = PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = ___sessionState0;
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionState_0(L_5);
		int32_t L_6 = ___sessionState0;
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_7 = ___sessionState0;
		if ((!(((uint32_t)L_7) == ((uint32_t)3))))
		{
			goto IL_00c0;
		}
	}

IL_002c:
	{
		int32_t L_8 = ___sessionState0;
		if ((!(((uint32_t)L_8) == ((uint32_t)1))))
		{
			goto IL_005d;
		}
	}
	{
		String_t* L_9 = PlatformWrapper_GetPlayerPrefsString_m2758919265(NULL /*static, unused*/, _stringLiteral3883455643, /*hidden argument*/NULL);
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionId_1(L_9);
		uint64_t L_10 = PlatformWrapper_GetPlayerPrefsUInt64_m3314372928(NULL /*static, unused*/, _stringLiteral1718852242, /*hidden argument*/NULL);
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionElapsedTime_2(L_10);
		uint64_t L_11 = PlatformWrapper_GetPlayerPrefsUInt64_m3314372928(NULL /*static, unused*/, _stringLiteral4027139201, /*hidden argument*/NULL);
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionBackgroundTime_4(L_11);
	}

IL_005d:
	{
		uint64_t L_12 = V_0;
		uint64_t L_13 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionBackgroundTime_4();
		V_1 = ((int64_t)il2cpp_codegen_subtract((int64_t)L_12, (int64_t)L_13));
		uint64_t L_14 = V_0;
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionForegroundTime_3(L_14);
		String_t* L_15 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionId_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_16 = String_op_Equality_m920492651(NULL /*static, unused*/, L_15, _stringLiteral757602046, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_008f;
		}
	}
	{
		uint64_t L_17 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		if (!L_17)
		{
			goto IL_008f;
		}
	}
	{
		uint64_t L_18 = V_1;
		if ((!(((uint64_t)L_18) > ((uint64_t)(((int64_t)((int64_t)((int32_t)1800000))))))))
		{
			goto IL_012e;
		}
	}

IL_008f:
	{
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionElapsedTime_2((((int64_t)((int64_t)0))));
		String_t* L_19 = PlatformWrapper_GenerateRandomId_m2000620507(NULL /*static, unused*/, /*hidden argument*/NULL);
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionId_1(L_19);
		String_t* L_20 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionId_1();
		PlatformWrapper_SetPlayerPrefsString_m3943675500(NULL /*static, unused*/, _stringLiteral3883455643, L_20, /*hidden argument*/NULL);
		uint64_t L_21 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		PlatformWrapper_SetPlayerPrefsUInt64_m1611667148(NULL /*static, unused*/, _stringLiteral1718852242, L_21, /*hidden argument*/NULL);
		goto IL_012e;
	}

IL_00c0:
	{
		int32_t L_22 = ___sessionState0;
		if (L_22)
		{
			goto IL_00ec;
		}
	}
	{
		PlatformWrapper_SetPlayerPrefsString_m3943675500(NULL /*static, unused*/, _stringLiteral3883455643, _stringLiteral757602046, /*hidden argument*/NULL);
		PlatformWrapper_SetPlayerPrefsUInt64_m1611667148(NULL /*static, unused*/, _stringLiteral1718852242, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		PlatformWrapper_SetPlayerPrefsUInt64_m1611667148(NULL /*static, unused*/, _stringLiteral4027139201, (((int64_t)((int64_t)0))), /*hidden argument*/NULL);
		goto IL_012e;
	}

IL_00ec:
	{
		V_2 = (((int64_t)((int64_t)0)));
		uint64_t L_23 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionForegroundTime_3();
		if (!L_23)
		{
			goto IL_00fe;
		}
	}
	{
		uint64_t L_24 = V_0;
		uint64_t L_25 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionForegroundTime_3();
		V_2 = ((int64_t)il2cpp_codegen_subtract((int64_t)L_24, (int64_t)L_25));
	}

IL_00fe:
	{
		uint64_t L_26 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		uint64_t L_27 = V_2;
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionElapsedTime_2(((int64_t)il2cpp_codegen_add((int64_t)L_26, (int64_t)L_27)));
		uint64_t L_28 = V_0;
		((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->set_m_PlayerSessionBackgroundTime_4(L_28);
		uint64_t L_29 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		PlatformWrapper_SetPlayerPrefsUInt64_m1611667148(NULL /*static, unused*/, _stringLiteral1718852242, L_29, /*hidden argument*/NULL);
		uint64_t L_30 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionBackgroundTime_4();
		PlatformWrapper_SetPlayerPrefsUInt64_m1611667148(NULL /*static, unused*/, _stringLiteral4027139201, L_30, /*hidden argument*/NULL);
	}

IL_012e:
	{
		int32_t L_31 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionState_0();
		String_t* L_32 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionId_1();
		uint64_t L_33 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		AnalyticsClient_OnPlayerSessionStateChanged_m2310188355(NULL /*static, unused*/, L_31, L_32, L_33, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt64 UnityEngine.UDP.Analytics.AnalyticsService::GetPlayerSessionElapsedTime()
extern "C" IL2CPP_METHOD_ATTR uint64_t AnalyticsService_GetPlayerSessionElapsedTime_m889664768 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnalyticsService_GetPlayerSessionElapsedTime_m889664768_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	uint64_t V_1 = 0;
	{
		int32_t L_0 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionState_0();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_1 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionState_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0030;
		}
	}

IL_0010:
	{
		uint64_t L_2 = PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (((int64_t)((int64_t)0)));
		uint64_t L_3 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionForegroundTime_3();
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		uint64_t L_4 = V_0;
		uint64_t L_5 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionForegroundTime_3();
		V_1 = ((int64_t)il2cpp_codegen_subtract((int64_t)L_4, (int64_t)L_5));
	}

IL_0028:
	{
		uint64_t L_6 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		uint64_t L_7 = V_1;
		return ((int64_t)il2cpp_codegen_add((int64_t)L_6, (int64_t)L_7));
	}

IL_0030:
	{
		uint64_t L_8 = ((AnalyticsService_t760113998_StaticFields*)il2cpp_codegen_static_fields_for(AnalyticsService_t760113998_il2cpp_TypeInfo_var))->get_m_PlayerSessionElapsedTime_2();
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.UDP.Analytics.Common::GetCommonParams(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR Dictionary_2_t2865362463 * Common_GetCommonParams_m2618682865 (RuntimeObject * __this /* static, unused */, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Common_GetCommonParams_m2618682865_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2865362463 * L_0 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2450723815(L_0, /*hidden argument*/Dictionary_2__ctor_m2450723815_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_1 = L_0;
		SessionInfo_t3964168581 * L_2 = ___sessionInfo0;
		NullCheck(L_2);
		String_t* L_3 = SessionInfo_get_MClientId_m922198160(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Dictionary_2_Add_m64471701(L_1, _stringLiteral4150543084, L_3, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_4 = L_1;
		SessionInfo_t3964168581 * L_5 = ___sessionInfo0;
		NullCheck(L_5);
		String_t* L_6 = SessionInfo_get_MDeviceId_m4217116151(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Dictionary_2_Add_m64471701(L_4, _stringLiteral2577086779, L_6, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_7 = L_4;
		NullCheck(L_7);
		Dictionary_2_Add_m64471701(L_7, _stringLiteral3903019942, _stringLiteral3383827812, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_8 = L_7;
		SessionInfo_t3964168581 * L_9 = ___sessionInfo0;
		NullCheck(L_9);
		String_t* L_10 = SessionInfo_get_MPlatform_m1348540618(L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		Dictionary_2_Add_m64471701(L_8, _stringLiteral38630993, L_10, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_11 = L_8;
		SessionInfo_t3964168581 * L_12 = ___sessionInfo0;
		NullCheck(L_12);
		String_t* L_13 = SessionInfo_get_MSystemInfo_m1681693923(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Dictionary_2_Add_m64471701(L_11, _stringLiteral2319849551, L_13, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_14 = L_11;
		SessionInfo_t3964168581 * L_15 = ___sessionInfo0;
		NullCheck(L_15);
		String_t* L_16 = SessionInfo_get_MTargetStore_m2176927300(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Dictionary_2_Add_m64471701(L_14, _stringLiteral720731980, L_16, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_17 = L_14;
		SessionInfo_t3964168581 * L_18 = ___sessionInfo0;
		NullCheck(L_18);
		bool L_19 = SessionInfo_get_MVr_m351961376(L_18, /*hidden argument*/NULL);
		bool L_20 = L_19;
		RuntimeObject * L_21 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_17);
		Dictionary_2_Add_m64471701(L_17, _stringLiteral3455629258, L_21, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_22 = L_17;
		uint64_t L_23 = PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_24 = L_23;
		RuntimeObject * L_25 = Box(UInt64_t4134040092_il2cpp_TypeInfo_var, &L_24);
		NullCheck(L_22);
		Dictionary_2_Add_m64471701(L_22, _stringLiteral3455563724, L_25, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_26 = L_22;
		NullCheck(L_26);
		Dictionary_2_Add_m64471701(L_26, _stringLiteral4294193667, _stringLiteral180630352, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		return L_26;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.EventDispatcher::init()
extern "C" IL2CPP_METHOD_ATTR void EventDispatcher_init_m1453862928 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventDispatcher_init_m1453862928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaClass_t32045322 * L_0 = (AndroidJavaClass_t32045322 *)il2cpp_codegen_object_new(AndroidJavaClass_t32045322_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m366853020(L_0, _stringLiteral3705078347, /*hidden argument*/NULL);
		((EventDispatcher_t1969224201_StaticFields*)il2cpp_codegen_static_fields_for(EventDispatcher_t1969224201_il2cpp_TypeInfo_var))->set_serviceClass_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.EventDispatcher::DispatchEvent(System.Object)
extern "C" IL2CPP_METHOD_ATTR void EventDispatcher_DispatchEvent_m2849035944 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___e0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (EventDispatcher_DispatchEvent_m2849035944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaClass_t32045322 * L_0 = ((EventDispatcher_t1969224201_StaticFields*)il2cpp_codegen_static_fields_for(EventDispatcher_t1969224201_il2cpp_TypeInfo_var))->get_serviceClass_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		EventDispatcher_init_m1453862928(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}

IL_000d:
	{
		AndroidJavaClass_t32045322 * L_1 = ((EventDispatcher_t1969224201_StaticFields*)il2cpp_codegen_static_fields_for(EventDispatcher_t1969224201_il2cpp_TypeInfo_var))->get_serviceClass_0();
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_3 = L_2;
		RuntimeObject * L_4 = ___e0;
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		NullCheck(L_1);
		AndroidJavaObject_CallStatic_m2922144688(L_1, _stringLiteral835031361, L_3, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.Events.AppInstallEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR void AppInstallEvent__ctor_m2634031396 (AppInstallEvent_t505962745 * __this, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppInstallEvent__ctor_m2634031396_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral2953959406, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_0 = ___sessionInfo0;
		Dictionary_2_t2865362463 * L_1 = Common_GetCommonParams_m2618682865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set__params_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.Events.AppRunningEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo,System.UInt64)
extern "C" IL2CPP_METHOD_ATTR void AppRunningEvent__ctor_m2641558448 (AppRunningEvent_t590370228 * __this, SessionInfo_t3964168581 * ___sessionInfo0, uint64_t ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRunningEvent__ctor_m2641558448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral2953959406, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_0 = ___sessionInfo0;
		Dictionary_2_t2865362463 * L_1 = Common_GetCommonParams_m2618682865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set__params_0(L_1);
		Dictionary_2_t2865362463 * L_2 = __this->get__params_0();
		uint64_t L_3 = ___duration1;
		uint64_t L_4 = L_3;
		RuntimeObject * L_5 = Box(UInt64_t4134040092_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		Dictionary_2_Add_m64471701(L_2, _stringLiteral1501416449, L_5, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.Events.AppStartEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR void AppStartEvent__ctor_m3550927429 (AppStartEvent_t147987345 * __this, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppStartEvent__ctor_m3550927429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral2953959406, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_0 = ___sessionInfo0;
		Dictionary_2_t2865362463 * L_1 = Common_GetCommonParams_m2618682865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set__params_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.Events.AppStopEvent::.ctor(UnityEngine.UDP.Analytics.SessionInfo)
extern "C" IL2CPP_METHOD_ATTR void AppStopEvent__ctor_m761874387 (AppStopEvent_t3879155 * __this, SessionInfo_t3964168581 * ___sessionInfo0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppStopEvent__ctor_m761874387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral2953959406, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_0 = ___sessionInfo0;
		Dictionary_2_t2865362463 * L_1 = Common_GetCommonParams_m2618682865(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set__params_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.Events.PurchaseAttemptEvent::.ctor(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PurchaseAttemptEvent__ctor_m3473196206 (PurchaseAttemptEvent_t2556690677 * __this, String_t* ___productId0, String_t* ___uuid1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PurchaseAttemptEvent__ctor_m3473196206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SessionInfo_t3964168581 * V_0 = NULL;
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral2953959406, /*hidden argument*/NULL);
		SessionInfo_t3964168581 * L_0 = AnalyticsClient_GetSessionInfo_m2557928941(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		SessionInfo_t3964168581 * L_1 = V_0;
		Dictionary_2_t2865362463 * L_2 = Common_GetCommonParams_m2618682865(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set__params_0(L_2);
		Dictionary_2_t2865362463 * L_3 = __this->get__params_0();
		String_t* L_4 = ___productId0;
		NullCheck(L_3);
		Dictionary_2_Add_m64471701(L_3, _stringLiteral2807426881, L_4, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_5 = __this->get__params_0();
		String_t* L_6 = ___uuid1;
		NullCheck(L_5);
		Dictionary_2_Add_m64471701(L_5, _stringLiteral243999125, L_6, /*hidden argument*/Dictionary_2_Add_m64471701_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.UInt64 UnityEngine.UDP.Analytics.PlatformWrapper::GetCurrentMillisecondsInUTC()
extern "C" IL2CPP_METHOD_ATTR uint64_t PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformWrapper_GetCurrentMillisecondsInUTC_m3750367502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t881159249  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_0 = DateTime_get_UtcNow_m1393945741(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		DateTime_t3738529785  L_1;
		memset(&L_1, 0, sizeof(L_1));
		DateTime__ctor_m12900168((&L_1), ((int32_t)1970), 1, 1, /*hidden argument*/NULL);
		TimeSpan_t881159249  L_2 = DateTime_Subtract_m77007479((DateTime_t3738529785 *)(&V_0), L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		double L_3 = TimeSpan_get_TotalMilliseconds_m2429771311((TimeSpan_t881159249 *)(&V_1), /*hidden argument*/NULL);
		return (((int64_t)((uint64_t)L_3)));
	}
}
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GetRuntimePlatformString()
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GetRuntimePlatformString_m2991492228 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformWrapper_GetRuntimePlatformString_m2991492228_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) > ((int32_t)7)))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)7)))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)11))))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)16))))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_002b;
	}

IL_001f:
	{
		return _stringLiteral2059856196;
	}

IL_0025:
	{
		return _stringLiteral757602046;
	}

IL_002b:
	{
		return _stringLiteral757602046;
	}
}
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GetSystemInfo()
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GetSystemInfo_m4243581290 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		String_t* L_0 = SystemInfo_get_deviceModel_m4078951941(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GetPlayerPrefsString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GetPlayerPrefsString_m2758919265 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformWrapper_GetPlayerPrefsString_m2758919265_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = PlayerPrefs_GetString_m614532710(NULL /*static, unused*/, L_0, _stringLiteral757602046, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.UDP.Analytics.PlatformWrapper::SetPlayerPrefsString(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void PlatformWrapper_SetPlayerPrefsString_m3943675500 (RuntimeObject * __this /* static, unused */, String_t* ___name0, String_t* ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		String_t* L_1 = ___value1;
		PlayerPrefs_SetString_m2101271233(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt64 UnityEngine.UDP.Analytics.PlatformWrapper::GetPlayerPrefsUInt64(System.String)
extern "C" IL2CPP_METHOD_ATTR uint64_t PlatformWrapper_GetPlayerPrefsUInt64_m3314372928 (RuntimeObject * __this /* static, unused */, String_t* ___name0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return (((int64_t)((int64_t)L_1)));
	}
}
// System.Void UnityEngine.UDP.Analytics.PlatformWrapper::SetPlayerPrefsUInt64(System.String,System.UInt64)
extern "C" IL2CPP_METHOD_ATTR void PlatformWrapper_SetPlayerPrefsUInt64_m1611667148 (RuntimeObject * __this /* static, unused */, String_t* ___name0, uint64_t ___value1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		uint64_t L_1 = ___value1;
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, L_0, (((int32_t)((int32_t)L_1))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.PlatformWrapper::GetAppInstalled()
extern "C" IL2CPP_METHOD_ATTR bool PlatformWrapper_GetAppInstalled_m1450815954 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformWrapper_GetAppInstalled_m1450815954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m1299643124(NULL /*static, unused*/, _stringLiteral52894277, 0, /*hidden argument*/NULL);
		return (bool)((!(((uint32_t)L_0) <= ((uint32_t)0)))? 1 : 0);
	}
}
// System.Void UnityEngine.UDP.Analytics.PlatformWrapper::SetAppInstalled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void PlatformWrapper_SetAppInstalled_m48601936 (RuntimeObject * __this /* static, unused */, bool ___v0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformWrapper_SetAppInstalled_m48601936_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	{
		bool L_0 = ___v0;
		G_B1_0 = _stringLiteral52894277;
		if (L_0)
		{
			G_B2_0 = _stringLiteral52894277;
			goto IL_000b;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000c;
	}

IL_000b:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000c:
	{
		PlayerPrefs_SetInt_m2842000469(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.PlatformWrapper::GenerateRandomId()
extern "C" IL2CPP_METHOD_ATTR String_t* PlatformWrapper_GenerateRandomId_m2000620507 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PlatformWrapper_GenerateRandomId_m2000620507_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Guid_t  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_0 = Guid_NewGuid_m923091018(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		String_t* L_1 = Guid_ToString_m3279186591((Guid_t *)(&V_0), /*hidden argument*/NULL);
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MAppId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MAppId_m913923638 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_AppId_0(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MSessionId()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MSessionId_m4126112815 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_SessionId_1();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MSessionId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MSessionId_m620876705 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_SessionId_1(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MClientId()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MClientId_m922198160 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_ClientId_2();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MClientId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MClientId_m1274542372 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_ClientId_2(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MDeviceId()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MDeviceId_m4217116151 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_DeviceId_3();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MDeviceId(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MDeviceId_m1445056562 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_DeviceId_3(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MPlatform()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MPlatform_m1348540618 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_Platform_4();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MPlatform(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MPlatform_m4115858467 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_Platform_4(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MTargetStore()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MTargetStore_m2176927300 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_TargetStore_5();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MTargetStore(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MTargetStore_m3709987134 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_TargetStore_5(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.Analytics.SessionInfo::get_MSystemInfo()
extern "C" IL2CPP_METHOD_ATTR String_t* SessionInfo_get_MSystemInfo_m1681693923 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_m_SystemInfo_6();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MSystemInfo(System.String)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MSystemInfo_m303435404 (SessionInfo_t3964168581 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_SystemInfo_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.UDP.Analytics.SessionInfo::get_MVr()
extern "C" IL2CPP_METHOD_ATTR bool SessionInfo_get_MVr_m351961376 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_Vr_7();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::set_MVr(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void SessionInfo_set_MVr_m3307225907 (SessionInfo_t3964168581 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_Vr_7(L_0);
		return;
	}
}
// System.Void UnityEngine.UDP.Analytics.SessionInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void SessionInfo__ctor_m3167671942 (SessionInfo_t3964168581 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.UDP.Analytics.AnalyticsResult UnityEngine.UDP.Analytics.UdpAnalytics::PurchaseAttempt(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t UdpAnalytics_PurchaseAttempt_m745049746 (RuntimeObject * __this /* static, unused */, String_t* ___productionId0, String_t* ___uuid1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpAnalytics_PurchaseAttempt_m745049746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = UdpAnalytics_isInitialized_m2641110313(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return (int32_t)(2);
	}

IL_0009:
	{
		String_t* L_1 = ___productionId0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0013;
		}
	}
	{
		return (int32_t)(3);
	}

IL_0013:
	{
		String_t* L_3 = ___productionId0;
		String_t* L_4 = ___uuid1;
		PurchaseAttemptEvent_t2556690677 * L_5 = (PurchaseAttemptEvent_t2556690677 *)il2cpp_codegen_object_new(PurchaseAttemptEvent_t2556690677_il2cpp_TypeInfo_var);
		PurchaseAttemptEvent__ctor_m3473196206(L_5, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_6 = UdpAnalytics_dispatchEvent_m2425667419(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.UDP.Analytics.AnalyticsResult UnityEngine.UDP.Analytics.UdpAnalytics::dispatchEvent(System.Object)
extern "C" IL2CPP_METHOD_ATTR int32_t UdpAnalytics_dispatchEvent_m2425667419 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___e0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___e0;
		EventDispatcher_DispatchEvent_m2849035944(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return (int32_t)(0);
	}
}
// System.Boolean UnityEngine.UDP.Analytics.UdpAnalytics::isInitialized()
extern "C" IL2CPP_METHOD_ATTR bool UdpAnalytics_isInitialized_m2641110313 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpAnalytics_isInitialized_m2641110313_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = AnalyticsClient_GetSessionId_m1082839017(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		return (bool)1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.UDP.AppInfo::get_ClientId()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_ClientId_m1683986167 (AppInfo_t4053800566 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CClientIdU3Ek__BackingField_0();
		return L_0;
	}
}
// System.String UnityEngine.UDP.AppInfo::get_AppSlug()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_AppSlug_m967783639 (AppInfo_t4053800566 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CAppSlugU3Ek__BackingField_1();
		return L_0;
	}
}
// System.String UnityEngine.UDP.AppInfo::get_ClientKey()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_ClientKey_m326902363 (AppInfo_t4053800566 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CClientKeyU3Ek__BackingField_2();
		return L_0;
	}
}
// System.String UnityEngine.UDP.AppInfo::get_RSAPublicKey()
extern "C" IL2CPP_METHOD_ATTR String_t* AppInfo_get_RSAPublicKey_m2216431782 (AppInfo_t4053800566 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CRSAPublicKeyU3Ek__BackingField_3();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.AppStoreSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AppStoreSettings__ctor_m1155411528 (AppStoreSettings_t763118828 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppStoreSettings__ctor_m1155411528_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_UnityProjectID_4(_stringLiteral757602046);
		__this->set_UnityClientID_5(_stringLiteral757602046);
		__this->set_UnityClientKey_6(_stringLiteral757602046);
		__this->set_UnityClientRSAPublicKey_7(_stringLiteral757602046);
		__this->set_AppName_8(_stringLiteral757602046);
		__this->set_AppSlug_9(_stringLiteral757602046);
		__this->set_AppItemId_10(_stringLiteral757602046);
		__this->set_Permission_11(_stringLiteral757602046);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.InitLoginForwardCallback::.ctor(UnityEngine.UDP.IInitListener)
extern "C" IL2CPP_METHOD_ATTR void InitLoginForwardCallback__ctor_m2942706633 (InitLoginForwardCallback_t1246786093 * __this, RuntimeObject* ___initListener0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InitLoginForwardCallback__ctor_m2942706633_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral892155963, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___initListener0;
		__this->set__initListener_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IDictionary`2<System.String,UnityEngine.UDP.PurchaseInfo> UnityEngine.UDP.Inventory::GetPurchaseDictionary()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Inventory_GetPurchaseDictionary_m2822751547 (Inventory_t2002574341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inventory_GetPurchaseDictionary_m2822751547_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t2815580273 * L_0 = __this->get__purchaseDictionary_0();
		Dictionary_2_t2815580273 * L_1 = (Dictionary_2_t2815580273 *)il2cpp_codegen_object_new(Dictionary_2_t2815580273_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2644321878(L_1, L_0, /*hidden argument*/Dictionary_2__ctor_m2644321878_RuntimeMethod_var);
		return L_1;
	}
}
// System.Collections.Generic.List`1<UnityEngine.UDP.PurchaseInfo> UnityEngine.UDP.Inventory::GetPurchaseList()
extern "C" IL2CPP_METHOD_ATTR List_1_t207431420 * Inventory_GetPurchaseList_m3615433102 (Inventory_t2002574341 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Inventory_GetPurchaseList_m3615433102_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t207431420 * V_0 = NULL;
	Enumerator_t474795752  V_1;
	memset(&V_1, 0, sizeof(V_1));
	KeyValuePair_2_t918285144  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t207431420 * L_0 = (List_1_t207431420 *)il2cpp_codegen_object_new(List_1_t207431420_il2cpp_TypeInfo_var);
		List_1__ctor_m502973724(L_0, /*hidden argument*/List_1__ctor_m502973724_RuntimeMethod_var);
		V_0 = L_0;
		Dictionary_2_t2815580273 * L_1 = __this->get__purchaseDictionary_0();
		NullCheck(L_1);
		Enumerator_t474795752  L_2 = Dictionary_2_GetEnumerator_m979315400(L_1, /*hidden argument*/Dictionary_2_GetEnumerator_m979315400_RuntimeMethod_var);
		V_1 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0014:
		{
			KeyValuePair_2_t918285144  L_3 = Enumerator_get_Current_m608108010((Enumerator_t474795752 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m608108010_RuntimeMethod_var);
			V_2 = L_3;
			List_1_t207431420 * L_4 = V_0;
			PurchaseInfo_t3030323974 * L_5 = KeyValuePair_2_get_Value_m2243092683((KeyValuePair_2_t918285144 *)(&V_2), /*hidden argument*/KeyValuePair_2_get_Value_m2243092683_RuntimeMethod_var);
			NullCheck(L_4);
			List_1_Add_m610740988(L_4, L_5, /*hidden argument*/List_1_Add_m610740988_RuntimeMethod_var);
		}

IL_0029:
		{
			bool L_6 = Enumerator_MoveNext_m1849793072((Enumerator_t474795752 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m1849793072_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_0014;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x42, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m158248455((Enumerator_t474795752 *)(&V_1), /*hidden argument*/Enumerator_Dispose_m158248455_RuntimeMethod_var);
		IL2CPP_END_FINALLY(52)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x42, IL_0042)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0042:
	{
		List_1_t207431420 * L_7 = V_0;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.MainThreadDispatcher::RunOnMainThread(System.Action)
extern "C" IL2CPP_METHOD_ATTR void MainThreadDispatcher_RunOnMainThread_m2241368720 (RuntimeObject * __this /* static, unused */, Action_t1264377477 * ___runnable0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_RunOnMainThread_m2241368720_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2736452219 * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		List_1_t2736452219 * L_0 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
		V_0 = L_0;
		List_1_t2736452219 * L_1 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		List_1_t2736452219 * L_2 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
		Action_t1264377477 * L_3 = ___runnable0;
		NullCheck(L_2);
		List_1_Add_m437696916(L_2, L_3, /*hidden argument*/List_1_Add_m437696916_RuntimeMethod_var);
		il2cpp_codegen_memory_barrier();
		((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->set_s_CallbacksPending_7(1);
		IL2CPP_LEAVE(0x28, FINALLY_0021);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0021;
	}

FINALLY_0021:
	{ // begin finally (depth: 1)
		List_1_t2736452219 * L_4 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(33)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(33)
	{
		IL2CPP_JUMP_TBL(0x28, IL_0028)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0028:
	{
		return;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher::DispatchDelayJob(System.Single,System.Action)
extern "C" IL2CPP_METHOD_ATTR void MainThreadDispatcher_DispatchDelayJob_m1553134548 (RuntimeObject * __this /* static, unused */, float ___waitTime0, Action_t1264377477 * ___runnable1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_DispatchDelayJob_m1553134548_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2736452219 * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		List_1_t2736452219 * L_0 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
		V_0 = L_0;
		List_1_t2736452219 * L_1 = V_0;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		Dictionary_2_t1212755687 * L_2 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_delayAction_6();
		float L_3 = ___waitTime0;
		Action_t1264377477 * L_4 = ___runnable1;
		NullCheck(L_2);
		Dictionary_2_set_Item_m4203027293(L_2, L_3, L_4, /*hidden argument*/Dictionary_2_set_Item_m4203027293_RuntimeMethod_var);
		il2cpp_codegen_memory_barrier();
		((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->set_s_CallbacksPending_7(1);
		IL2CPP_LEAVE(0x29, FINALLY_0022);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0022;
	}

FINALLY_0022:
	{ // begin finally (depth: 1)
		List_1_t2736452219 * L_5 = V_0;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(34)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(34)
	{
		IL2CPP_JUMP_TBL(0x29, IL_0029)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> UnityEngine.UDP.MainThreadDispatcher::WaitAndDo(System.Single,System.Action)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* MainThreadDispatcher_WaitAndDo_m930138745 (MainThreadDispatcher_t2227136958 * __this, float ___waitTime0, Action_t1264377477 * ___runnable1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_WaitAndDo_m930138745_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CWaitAndDoU3Ed__6_t1673170562 * L_0 = (U3CWaitAndDoU3Ed__6_t1673170562 *)il2cpp_codegen_object_new(U3CWaitAndDoU3Ed__6_t1673170562_il2cpp_TypeInfo_var);
		U3CWaitAndDoU3Ed__6__ctor_m3349979845(L_0, 0, /*hidden argument*/NULL);
		U3CWaitAndDoU3Ed__6_t1673170562 * L_1 = L_0;
		float L_2 = ___waitTime0;
		NullCheck(L_1);
		L_1->set_waitTime_2(L_2);
		U3CWaitAndDoU3Ed__6_t1673170562 * L_3 = L_1;
		Action_t1264377477 * L_4 = ___runnable1;
		NullCheck(L_3);
		L_3->set_runnable_3(L_4);
		return L_3;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher::Start()
extern "C" IL2CPP_METHOD_ATTR void MainThreadDispatcher_Start_m3113591846 (MainThreadDispatcher_t2227136958 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Start_m3113591846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher::Update()
extern "C" IL2CPP_METHOD_ATTR void MainThreadDispatcher_Update_m2390787740 (MainThreadDispatcher_t2227136958 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher_Update_m2390787740_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ActionU5BU5D_t388269512* V_0 = NULL;
	Dictionary_2_t1212755687 * V_1 = NULL;
	List_1_t2736452219 * V_2 = NULL;
	ActionU5BU5D_t388269512* V_3 = NULL;
	int32_t V_4 = 0;
	Enumerator_t3166938462  V_5;
	memset(&V_5, 0, sizeof(V_5));
	KeyValuePair_2_t3610427854  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		bool L_0 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_CallbacksPending_7();
		il2cpp_codegen_memory_barrier();
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		List_1_t2736452219 * L_1 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
		V_2 = L_1;
		List_1_t2736452219 * L_2 = V_2;
		Monitor_Enter_m2249409497(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
			List_1_t2736452219 * L_3 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
			NullCheck(L_3);
			int32_t L_4 = List_1_get_Count_m808340314(L_3, /*hidden argument*/List_1_get_Count_m808340314_RuntimeMethod_var);
			if (L_4)
			{
				goto IL_0033;
			}
		}

IL_0022:
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
			Dictionary_2_t1212755687 * L_5 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_delayAction_6();
			NullCheck(L_5);
			int32_t L_6 = Dictionary_2_get_Count_m3991288552(L_5, /*hidden argument*/Dictionary_2_get_Count_m3991288552_RuntimeMethod_var);
			if (L_6)
			{
				goto IL_0033;
			}
		}

IL_002e:
		{
			IL2CPP_LEAVE(0xE2, FINALLY_0077);
		}

IL_0033:
		{
			IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
			List_1_t2736452219 * L_7 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
			NullCheck(L_7);
			int32_t L_8 = List_1_get_Count_m808340314(L_7, /*hidden argument*/List_1_get_Count_m808340314_RuntimeMethod_var);
			ActionU5BU5D_t388269512* L_9 = (ActionU5BU5D_t388269512*)SZArrayNew(ActionU5BU5D_t388269512_il2cpp_TypeInfo_var, (uint32_t)L_8);
			V_0 = L_9;
			List_1_t2736452219 * L_10 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
			ActionU5BU5D_t388269512* L_11 = V_0;
			NullCheck(L_10);
			List_1_CopyTo_m2569245864(L_10, L_11, /*hidden argument*/List_1_CopyTo_m2569245864_RuntimeMethod_var);
			List_1_t2736452219 * L_12 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_s_Callbacks_5();
			NullCheck(L_12);
			List_1_Clear_m2936844859(L_12, /*hidden argument*/List_1_Clear_m2936844859_RuntimeMethod_var);
			Dictionary_2_t1212755687 * L_13 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_delayAction_6();
			Dictionary_2_t1212755687 * L_14 = (Dictionary_2_t1212755687 *)il2cpp_codegen_object_new(Dictionary_2_t1212755687_il2cpp_TypeInfo_var);
			Dictionary_2__ctor_m792470897(L_14, L_13, /*hidden argument*/Dictionary_2__ctor_m792470897_RuntimeMethod_var);
			V_1 = L_14;
			Dictionary_2_t1212755687 * L_15 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_delayAction_6();
			NullCheck(L_15);
			Dictionary_2_Clear_m607508611(L_15, /*hidden argument*/Dictionary_2_Clear_m607508611_RuntimeMethod_var);
			il2cpp_codegen_memory_barrier();
			((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->set_s_CallbacksPending_7(0);
			IL2CPP_LEAVE(0x7E, FINALLY_0077);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0077;
	}

FINALLY_0077:
	{ // begin finally (depth: 1)
		List_1_t2736452219 * L_16 = V_2;
		Monitor_Exit_m3585316909(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(119)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(119)
	{
		IL2CPP_JUMP_TBL(0xE2, IL_00e2)
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_007e:
	{
		ActionU5BU5D_t388269512* L_17 = V_0;
		V_3 = L_17;
		V_4 = 0;
		goto IL_0094;
	}

IL_0085:
	{
		ActionU5BU5D_t388269512* L_18 = V_3;
		int32_t L_19 = V_4;
		NullCheck(L_18);
		int32_t L_20 = L_19;
		Action_t1264377477 * L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		NullCheck(L_21);
		Action_Invoke_m937035532(L_21, /*hidden argument*/NULL);
		int32_t L_22 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0094:
	{
		int32_t L_23 = V_4;
		ActionU5BU5D_t388269512* L_24 = V_3;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length)))))))
		{
			goto IL_0085;
		}
	}
	{
		Dictionary_2_t1212755687 * L_25 = V_1;
		NullCheck(L_25);
		Enumerator_t3166938462  L_26 = Dictionary_2_GetEnumerator_m650535580(L_25, /*hidden argument*/Dictionary_2_GetEnumerator_m650535580_RuntimeMethod_var);
		V_5 = L_26;
	}

IL_00a3:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c9;
		}

IL_00a5:
		{
			KeyValuePair_2_t3610427854  L_27 = Enumerator_get_Current_m1534630570((Enumerator_t3166938462 *)(&V_5), /*hidden argument*/Enumerator_get_Current_m1534630570_RuntimeMethod_var);
			V_6 = L_27;
			float L_28 = KeyValuePair_2_get_Key_m2070292874((KeyValuePair_2_t3610427854 *)(&V_6), /*hidden argument*/KeyValuePair_2_get_Key_m2070292874_RuntimeMethod_var);
			Action_t1264377477 * L_29 = KeyValuePair_2_get_Value_m3459043748((KeyValuePair_2_t3610427854 *)(&V_6), /*hidden argument*/KeyValuePair_2_get_Value_m3459043748_RuntimeMethod_var);
			RuntimeObject* L_30 = MainThreadDispatcher_WaitAndDo_m930138745(__this, L_28, L_29, /*hidden argument*/NULL);
			MonoBehaviour_StartCoroutine_m3411253000(__this, L_30, /*hidden argument*/NULL);
		}

IL_00c9:
		{
			bool L_31 = Enumerator_MoveNext_m2562535175((Enumerator_t3166938462 *)(&V_5), /*hidden argument*/Enumerator_MoveNext_m2562535175_RuntimeMethod_var);
			if (L_31)
			{
				goto IL_00a5;
			}
		}

IL_00d2:
		{
			IL2CPP_LEAVE(0xE2, FINALLY_00d4);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_00d4;
	}

FINALLY_00d4:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1063885580((Enumerator_t3166938462 *)(&V_5), /*hidden argument*/Enumerator_Dispose_m1063885580_RuntimeMethod_var);
		IL2CPP_END_FINALLY(212)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(212)
	{
		IL2CPP_JUMP_TBL(0xE2, IL_00e2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_00e2:
	{
		return;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MainThreadDispatcher__ctor_m3783269618 (MainThreadDispatcher_t2227136958 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher::.cctor()
extern "C" IL2CPP_METHOD_ATTR void MainThreadDispatcher__cctor_m3750846867 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainThreadDispatcher__cctor_m3750846867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->set_OBJECT_NAME_4(_stringLiteral2183561488);
		List_1_t2736452219 * L_0 = (List_1_t2736452219 *)il2cpp_codegen_object_new(List_1_t2736452219_il2cpp_TypeInfo_var);
		List_1__ctor_m2543424368(L_0, /*hidden argument*/List_1__ctor_m2543424368_RuntimeMethod_var);
		((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->set_s_Callbacks_5(L_0);
		Dictionary_2_t1212755687 * L_1 = (Dictionary_2_t1212755687 *)il2cpp_codegen_object_new(Dictionary_2_t1212755687_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m784414107(L_1, /*hidden argument*/Dictionary_2__ctor_m784414107_RuntimeMethod_var);
		((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->set_delayAction_6(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::.ctor(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void U3CWaitAndDoU3Ed__6__ctor_m3349979845 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::System.IDisposable.Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitAndDoU3Ed__6_System_IDisposable_Dispose_m3813191987 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CWaitAndDoU3Ed__6_MoveNext_m4264311566 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitAndDoU3Ed__6_MoveNext_m4264311566_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0031;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->set_U3CU3E1__state_0((-1));
		float L_3 = __this->get_waitTime_2();
		WaitForSeconds_t1699091251 * L_4 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_4);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0031:
	{
		__this->set_U3CU3E1__state_0((-1));
		Action_t1264377477 * L_5 = __this->get_runnable_3();
		NullCheck(L_5);
		Action_Invoke_m937035532(L_5, /*hidden argument*/NULL);
		return (bool)0;
	}
}
// UnityEngine.WaitForSeconds UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::System.Collections.Generic.IEnumerator<UnityEngine.WaitForSeconds>.get_Current()
extern "C" IL2CPP_METHOD_ATTR WaitForSeconds_t1699091251 * U3CWaitAndDoU3Ed__6_System_Collections_Generic_IEnumeratorU3CUnityEngine_WaitForSecondsU3E_get_Current_m757557379 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, const RuntimeMethod* method)
{
	{
		WaitForSeconds_t1699091251 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::System.Collections.IEnumerator.Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CWaitAndDoU3Ed__6_System_Collections_IEnumerator_Reset_m2609284963 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CWaitAndDoU3Ed__6_System_Collections_IEnumerator_Reset_m2609284963_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CWaitAndDoU3Ed__6_System_Collections_IEnumerator_Reset_m2609284963_RuntimeMethod_var);
	}
}
// System.Object UnityEngine.UDP.MainThreadDispatcher/<WaitAndDo>d__6::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CWaitAndDoU3Ed__6_System_Collections_IEnumerator_get_Current_m65415663 (U3CWaitAndDoU3Ed__6_t1673170562 * __this, const RuntimeMethod* method)
{
	{
		WaitForSeconds_t1699091251 * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.PurchaseForwardCallback::.ctor(UnityEngine.UDP.IPurchaseListener)
extern "C" IL2CPP_METHOD_ATTR void PurchaseForwardCallback__ctor_m665942399 (PurchaseForwardCallback_t1388311153 * __this, RuntimeObject* ___purchaseListener0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PurchaseForwardCallback__ctor_m665942399_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaProxy__ctor_m545570009(__this, _stringLiteral634977693, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___purchaseListener0;
		__this->set_purchaseListener_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.UDP.PurchaseInfo::get_ItemType()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_ItemType_m3568345912 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CItemTypeU3Ek__BackingField_0();
		return L_0;
	}
}
// System.String UnityEngine.UDP.PurchaseInfo::get_ProductId()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_ProductId_m3051669569 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CProductIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.String UnityEngine.UDP.PurchaseInfo::get_GameOrderId()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_GameOrderId_m404388006 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CGameOrderIdU3Ek__BackingField_2();
		return L_0;
	}
}
// System.String UnityEngine.UDP.PurchaseInfo::get_OrderQueryToken()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_OrderQueryToken_m3126970672 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3COrderQueryTokenU3Ek__BackingField_3();
		return L_0;
	}
}
// System.String UnityEngine.UDP.PurchaseInfo::get_DeveloperPayload()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_DeveloperPayload_m3489613356 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CDeveloperPayloadU3Ek__BackingField_4();
		return L_0;
	}
}
// System.String UnityEngine.UDP.PurchaseInfo::get_StorePurchaseJsonString()
extern "C" IL2CPP_METHOD_ATTR String_t* PurchaseInfo_get_StorePurchaseJsonString_m1065397329 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CStorePurchaseJsonStringU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.PurchaseInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void PurchaseInfo__ctor_m836497982 (PurchaseInfo_t3030323974 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.StoreService::Initialize(UnityEngine.UDP.IInitListener,UnityEngine.UDP.AppInfo)
extern "C" IL2CPP_METHOD_ATTR void StoreService_Initialize_m4032431360 (RuntimeObject * __this /* static, unused */, RuntimeObject* ___listener0, AppInfo_t4053800566 * ___appInfo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_Initialize_m4032431360_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaObject_t4131667876 * V_0 = NULL;
	InitLoginForwardCallback_t1246786093 * V_1 = NULL;
	AndroidJavaObject_t4131667876 * V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	int32_t V_7 = 0;
	AppStoreSettings_t763118828 * G_B15_0 = NULL;
	AppStoreSettings_t763118828 * G_B14_0 = NULL;
	{
		int32_t L_0 = Application_get_platform_m2150679437(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_0;
		int32_t L_1 = V_7;
		if ((((int32_t)L_1) > ((int32_t)7)))
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_2 = V_7;
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		int32_t L_3 = V_7;
		if ((((int32_t)L_3) == ((int32_t)7)))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_003e;
	}

IL_0017:
	{
		int32_t L_4 = V_7;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)11))))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_5 = V_7;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)16))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_003e;
	}

IL_0025:
	{
		AndroidJavaClass_t32045322 * L_6 = (AndroidJavaClass_t32045322 *)il2cpp_codegen_object_new(AndroidJavaClass_t32045322_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m366853020(L_6, _stringLiteral1318794425, /*hidden argument*/NULL);
		((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->set_serviceClass_0(L_6);
		goto IL_0049;
	}

IL_0036:
	{
		RuntimeObject* L_7 = ___listener0;
		NullCheck(L_7);
		InterfaceActionInvoker1< UserInfo_t3529881807 * >::Invoke(0 /* System.Void UnityEngine.UDP.IInitListener::OnInitialized(UnityEngine.UDP.UserInfo) */, IInitListener_t3363933952_il2cpp_TypeInfo_var, L_7, (UserInfo_t3529881807 *)NULL);
		return;
	}

IL_003e:
	{
		InvalidOperationException_t56020091 * L_8 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_8, _stringLiteral2186659586, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8, NULL, StoreService_Initialize_m4032431360_RuntimeMethod_var);
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		String_t* L_9 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_OBJECT_NAME_4();
		GameObject_t1113636619 * L_10 = GameObject_Find_m2032535176(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_10, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0078;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var);
		String_t* L_12 = ((MainThreadDispatcher_t2227136958_StaticFields*)il2cpp_codegen_static_fields_for(MainThreadDispatcher_t2227136958_il2cpp_TypeInfo_var))->get_OBJECT_NAME_4();
		GameObject_t1113636619 * L_13 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_13, L_12, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_14 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_15 = L_14;
		NullCheck(L_15);
		Object_set_hideFlags_m1648752846(L_15, 3, /*hidden argument*/NULL);
		NullCheck(L_15);
		GameObject_AddComponent_TisMainThreadDispatcher_t2227136958_m3710866612(L_15, /*hidden argument*/GameObject_AddComponent_TisMainThreadDispatcher_t2227136958_m3710866612_RuntimeMethod_var);
	}

IL_0078:
	{
		AndroidJavaClass_t32045322 * L_16 = (AndroidJavaClass_t32045322 *)il2cpp_codegen_object_new(AndroidJavaClass_t32045322_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m366853020(L_16, _stringLiteral2149247999, /*hidden argument*/NULL);
		NullCheck(L_16);
		AndroidJavaObject_t4131667876 * L_17 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4131667876_m671986461(L_16, _stringLiteral3452315504, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t4131667876_m671986461_RuntimeMethod_var);
		V_0 = L_17;
		RuntimeObject* L_18 = ___listener0;
		InitLoginForwardCallback_t1246786093 * L_19 = (InitLoginForwardCallback_t1246786093 *)il2cpp_codegen_object_new(InitLoginForwardCallback_t1246786093_il2cpp_TypeInfo_var);
		InitLoginForwardCallback__ctor_m2942706633(L_19, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		ObjectU5BU5D_t2843939325* L_20 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0);
		AndroidJavaObject_t4131667876 * L_21 = (AndroidJavaObject_t4131667876 *)il2cpp_codegen_object_new(AndroidJavaObject_t4131667876_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m3828648572(L_21, _stringLiteral401387461, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		AppInfo_t4053800566 * L_22 = ___appInfo1;
		if (L_22)
		{
			goto IL_00e6;
		}
	}
	{
		AppStoreSettings_t763118828 * L_23 = Resources_Load_TisAppStoreSettings_t763118828_m644194161(NULL /*static, unused*/, _stringLiteral818291658, /*hidden argument*/Resources_Load_TisAppStoreSettings_t763118828_m644194161_RuntimeMethod_var);
		AppStoreSettings_t763118828 * L_24 = L_23;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_24, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B14_0 = L_24;
		if (!L_25)
		{
			G_B15_0 = L_24;
			goto IL_00c6;
		}
	}
	{
		InvalidOperationException_t56020091 * L_26 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_26, _stringLiteral476708812, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26, NULL, StoreService_Initialize_m4032431360_RuntimeMethod_var);
	}

IL_00c6:
	{
		AppStoreSettings_t763118828 * L_27 = G_B15_0;
		NullCheck(L_27);
		String_t* L_28 = L_27->get_UnityClientID_5();
		V_3 = L_28;
		AppStoreSettings_t763118828 * L_29 = L_27;
		NullCheck(L_29);
		String_t* L_30 = L_29->get_UnityClientKey_6();
		V_4 = L_30;
		AppStoreSettings_t763118828 * L_31 = L_29;
		NullCheck(L_31);
		String_t* L_32 = L_31->get_AppSlug_9();
		V_5 = L_32;
		NullCheck(L_31);
		String_t* L_33 = L_31->get_UnityClientRSAPublicKey_7();
		V_6 = L_33;
		goto IL_0105;
	}

IL_00e6:
	{
		AppInfo_t4053800566 * L_34 = ___appInfo1;
		NullCheck(L_34);
		String_t* L_35 = AppInfo_get_ClientId_m1683986167(L_34, /*hidden argument*/NULL);
		V_3 = L_35;
		AppInfo_t4053800566 * L_36 = ___appInfo1;
		NullCheck(L_36);
		String_t* L_37 = AppInfo_get_ClientKey_m326902363(L_36, /*hidden argument*/NULL);
		V_4 = L_37;
		AppInfo_t4053800566 * L_38 = ___appInfo1;
		NullCheck(L_38);
		String_t* L_39 = AppInfo_get_AppSlug_m967783639(L_38, /*hidden argument*/NULL);
		V_5 = L_39;
		AppInfo_t4053800566 * L_40 = ___appInfo1;
		NullCheck(L_40);
		String_t* L_41 = AppInfo_get_RSAPublicKey_m2216431782(L_40, /*hidden argument*/NULL);
		V_6 = L_41;
	}

IL_0105:
	{
		AndroidJavaObject_t4131667876 * L_42 = V_2;
		String_t* L_43 = V_3;
		NullCheck(L_42);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_42, _stringLiteral165665077, L_43, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_44 = V_2;
		String_t* L_45 = V_4;
		NullCheck(L_44);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_44, _stringLiteral3331568903, L_45, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_46 = V_2;
		String_t* L_47 = V_5;
		NullCheck(L_46);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_46, _stringLiteral1231854530, L_47, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_48 = V_2;
		String_t* L_49 = V_6;
		NullCheck(L_48);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_48, _stringLiteral166562518, L_49, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaClass_t32045322 * L_50 = ((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->get_serviceClass_0();
		ObjectU5BU5D_t2843939325* L_51 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3);
		ObjectU5BU5D_t2843939325* L_52 = L_51;
		AndroidJavaObject_t4131667876 * L_53 = V_0;
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_53);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_53);
		ObjectU5BU5D_t2843939325* L_54 = L_52;
		AndroidJavaObject_t4131667876 * L_55 = V_2;
		NullCheck(L_54);
		ArrayElementTypeCheck (L_54, L_55);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_55);
		ObjectU5BU5D_t2843939325* L_56 = L_54;
		InitLoginForwardCallback_t1246786093 * L_57 = V_1;
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_57);
		NullCheck(L_50);
		AndroidJavaObject_CallStatic_m2922144688(L_50, _stringLiteral3134671380, L_56, /*hidden argument*/NULL);
		AnalyticsService_Initialize_m4044694567(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_58 = V_3;
		String_t* L_59 = V_5;
		String_t* L_60 = StoreService_get_StoreName_m1656101778(NULL /*static, unused*/, /*hidden argument*/NULL);
		AnalyticsClient_Initialize_m463104264(NULL /*static, unused*/, L_58, L_59, L_60, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UdpGameManager_t2849128925_il2cpp_TypeInfo_var);
		String_t* L_61 = ((UdpGameManager_t2849128925_StaticFields*)il2cpp_codegen_static_fields_for(UdpGameManager_t2849128925_il2cpp_TypeInfo_var))->get_OBJECT_NAME_4();
		GameObject_t1113636619 * L_62 = GameObject_Find_m2032535176(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_62, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_019a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UdpGameManager_t2849128925_il2cpp_TypeInfo_var);
		String_t* L_64 = ((UdpGameManager_t2849128925_StaticFields*)il2cpp_codegen_static_fields_for(UdpGameManager_t2849128925_il2cpp_TypeInfo_var))->get_OBJECT_NAME_4();
		GameObject_t1113636619 * L_65 = (GameObject_t1113636619 *)il2cpp_codegen_object_new(GameObject_t1113636619_il2cpp_TypeInfo_var);
		GameObject__ctor_m2093116449(L_65, L_64, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_66 = L_65;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_66, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_67 = L_66;
		NullCheck(L_67);
		Object_set_hideFlags_m1648752846(L_67, 3, /*hidden argument*/NULL);
		NullCheck(L_67);
		GameObject_AddComponent_TisUdpGameManager_t2849128925_m3960156118(L_67, /*hidden argument*/GameObject_AddComponent_TisUdpGameManager_t2849128925_m3960156118_RuntimeMethod_var);
	}

IL_019a:
	{
		return;
	}
}
// System.Void UnityEngine.UDP.StoreService::Purchase(System.String,System.String,UnityEngine.UDP.IPurchaseListener)
extern "C" IL2CPP_METHOD_ATTR void StoreService_Purchase_m2177431105 (RuntimeObject * __this /* static, unused */, String_t* ___productId0, String_t* ___developerPayload1, RuntimeObject* ___listener2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_Purchase_m2177431105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	PurchaseForwardCallback_t1388311153 * V_1 = NULL;
	AndroidJavaObject_t4131667876 * V_2 = NULL;
	Guid_t  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Guid_t_il2cpp_TypeInfo_var);
		Guid_t  L_0 = Guid_NewGuid_m923091018(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_0;
		String_t* L_1 = Guid_ToString_m3279186591((Guid_t *)(&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		NullCheck(L_1);
		String_t* L_3 = String_Replace_m1273907647(L_1, _stringLiteral3452614531, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___productId0;
		String_t* L_5 = V_0;
		UdpAnalytics_PurchaseAttempt_m745049746(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		RuntimeObject* L_6 = ___listener2;
		PurchaseForwardCallback_t1388311153 * L_7 = (PurchaseForwardCallback_t1388311153 *)il2cpp_codegen_object_new(PurchaseForwardCallback_t1388311153_il2cpp_TypeInfo_var);
		PurchaseForwardCallback__ctor_m665942399(L_7, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		ObjectU5BU5D_t2843939325* L_8 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0);
		AndroidJavaObject_t4131667876 * L_9 = (AndroidJavaObject_t4131667876 *)il2cpp_codegen_object_new(AndroidJavaObject_t4131667876_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m3828648572(L_9, _stringLiteral3656337555, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		AndroidJavaObject_t4131667876 * L_10 = V_2;
		String_t* L_11 = ___productId0;
		NullCheck(L_10);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_10, _stringLiteral1191178036, L_11, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_12 = V_2;
		String_t* L_13 = V_0;
		NullCheck(L_12);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_12, _stringLiteral43791901, L_13, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_14 = V_2;
		String_t* L_15 = ___developerPayload1;
		NullCheck(L_14);
		AndroidJavaObject_Set_TisString_t_m2428066008(L_14, _stringLiteral3920973500, L_15, /*hidden argument*/AndroidJavaObject_Set_TisString_t_m2428066008_RuntimeMethod_var);
		AndroidJavaClass_t32045322 * L_16 = ((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->get_serviceClass_0();
		ObjectU5BU5D_t2843939325* L_17 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_18 = L_17;
		AndroidJavaObject_t4131667876 * L_19 = V_2;
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_19);
		ObjectU5BU5D_t2843939325* L_20 = L_18;
		PurchaseForwardCallback_t1388311153 * L_21 = V_1;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_21);
		NullCheck(L_16);
		AndroidJavaObject_CallStatic_m2922144688(L_16, _stringLiteral369603133, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.StoreService::QueryInventory(System.Collections.Generic.List`1<System.String>,UnityEngine.UDP.IPurchaseListener)
extern "C" IL2CPP_METHOD_ATTR void StoreService_QueryInventory_m3086827487 (RuntimeObject * __this /* static, unused */, List_1_t3319525431 * ___productIds0, RuntimeObject* ___listener1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_QueryInventory_m3086827487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PurchaseForwardCallback_t1388311153 * V_0 = NULL;
	{
		RuntimeObject* L_0 = ___listener1;
		PurchaseForwardCallback_t1388311153 * L_1 = (PurchaseForwardCallback_t1388311153 *)il2cpp_codegen_object_new(PurchaseForwardCallback_t1388311153_il2cpp_TypeInfo_var);
		PurchaseForwardCallback__ctor_m665942399(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaClass_t32045322 * L_2 = ((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->get_serviceClass_0();
		ObjectU5BU5D_t2843939325* L_3 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_4 = L_3;
		List_1_t3319525431 * L_5 = ___productIds0;
		AndroidJavaObject_t4131667876 * L_6 = StoreService_javaArrayFromCSList_m2484081227(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_6);
		ObjectU5BU5D_t2843939325* L_7 = L_4;
		PurchaseForwardCallback_t1388311153 * L_8 = V_0;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		NullCheck(L_2);
		AndroidJavaObject_CallStatic_m2922144688(L_2, _stringLiteral4022591287, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.StoreService::ConsumePurchase(UnityEngine.UDP.PurchaseInfo,UnityEngine.UDP.IPurchaseListener)
extern "C" IL2CPP_METHOD_ATTR void StoreService_ConsumePurchase_m2343943502 (RuntimeObject * __this /* static, unused */, PurchaseInfo_t3030323974 * ___purchaseInfo0, RuntimeObject* ___listener1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_ConsumePurchase_m2343943502_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PurchaseForwardCallback_t1388311153 * V_0 = NULL;
	AndroidJavaObject_t4131667876 * V_1 = NULL;
	{
		RuntimeObject* L_0 = ___listener1;
		PurchaseForwardCallback_t1388311153 * L_1 = (PurchaseForwardCallback_t1388311153 *)il2cpp_codegen_object_new(PurchaseForwardCallback_t1388311153_il2cpp_TypeInfo_var);
		PurchaseForwardCallback__ctor_m665942399(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0);
		AndroidJavaObject_t4131667876 * L_3 = (AndroidJavaObject_t4131667876 *)il2cpp_codegen_object_new(AndroidJavaObject_t4131667876_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m3828648572(L_3, _stringLiteral3656337555, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		AndroidJavaObject_t4131667876 * L_4 = V_1;
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_6 = L_5;
		PurchaseInfo_t3030323974 * L_7 = ___purchaseInfo0;
		NullCheck(L_7);
		String_t* L_8 = PurchaseInfo_get_ItemType_m3568345912(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_8);
		NullCheck(L_4);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563(L_4, _stringLiteral3547887013, L_6, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_9 = V_1;
		ObjectU5BU5D_t2843939325* L_10 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_11 = L_10;
		PurchaseInfo_t3030323974 * L_12 = ___purchaseInfo0;
		NullCheck(L_12);
		String_t* L_13 = PurchaseInfo_get_ProductId_m3051669569(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		NullCheck(L_9);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563(L_9, _stringLiteral2158305376, L_11, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_14 = V_1;
		ObjectU5BU5D_t2843939325* L_15 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_16 = L_15;
		PurchaseInfo_t3030323974 * L_17 = ___purchaseInfo0;
		NullCheck(L_17);
		String_t* L_18 = PurchaseInfo_get_DeveloperPayload_m3489613356(L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_18);
		NullCheck(L_14);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563(L_14, _stringLiteral549362421, L_16, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_19 = V_1;
		ObjectU5BU5D_t2843939325* L_20 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_21 = L_20;
		PurchaseInfo_t3030323974 * L_22 = ___purchaseInfo0;
		NullCheck(L_22);
		String_t* L_23 = PurchaseInfo_get_GameOrderId_m404388006(L_22, /*hidden argument*/NULL);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_23);
		NullCheck(L_19);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563(L_19, _stringLiteral3024613587, L_21, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_24 = V_1;
		ObjectU5BU5D_t2843939325* L_25 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_26 = L_25;
		PurchaseInfo_t3030323974 * L_27 = ___purchaseInfo0;
		NullCheck(L_27);
		String_t* L_28 = PurchaseInfo_get_OrderQueryToken_m3126970672(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_28);
		NullCheck(L_24);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563(L_24, _stringLiteral3111220510, L_26, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var);
		AndroidJavaObject_t4131667876 * L_29 = V_1;
		ObjectU5BU5D_t2843939325* L_30 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_31 = L_30;
		PurchaseInfo_t3030323974 * L_32 = ___purchaseInfo0;
		NullCheck(L_32);
		String_t* L_33 = PurchaseInfo_get_StorePurchaseJsonString_m1065397329(L_32, /*hidden argument*/NULL);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_33);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_33);
		NullCheck(L_29);
		AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563(L_29, _stringLiteral3838603512, L_31, /*hidden argument*/AndroidJavaObject_Call_TisAndroidJavaObject_t4131667876_m2554077563_RuntimeMethod_var);
		AndroidJavaClass_t32045322 * L_34 = ((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->get_serviceClass_0();
		ObjectU5BU5D_t2843939325* L_35 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_36 = L_35;
		AndroidJavaObject_t4131667876 * L_37 = V_1;
		NullCheck(L_36);
		ArrayElementTypeCheck (L_36, L_37);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_37);
		ObjectU5BU5D_t2843939325* L_38 = L_36;
		PurchaseForwardCallback_t1388311153 * L_39 = V_0;
		NullCheck(L_38);
		ArrayElementTypeCheck (L_38, L_39);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_39);
		NullCheck(L_34);
		AndroidJavaObject_CallStatic_m2922144688(L_34, _stringLiteral1216028220, L_38, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UDP.StoreService::get_StoreName()
extern "C" IL2CPP_METHOD_ATTR String_t* StoreService_get_StoreName_m1656101778 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_get_StoreName_m1656101778_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AndroidJavaClass_t32045322 * L_0 = ((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->get_serviceClass_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		AndroidJavaClass_t32045322 * L_1 = ((StoreService_t3764094894_StaticFields*)il2cpp_codegen_static_fields_for(StoreService_t3764094894_il2cpp_TypeInfo_var))->get_serviceClass_0();
		ObjectU5BU5D_t2843939325* L_2 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0);
		NullCheck(L_1);
		String_t* L_3 = AndroidJavaObject_CallStatic_TisString_t_m522892673(L_1, _stringLiteral3614767605, L_2, /*hidden argument*/AndroidJavaObject_CallStatic_TisString_t_m522892673_RuntimeMethod_var);
		return L_3;
	}

IL_001d:
	{
		return _stringLiteral3099827557;
	}
}
// UnityEngine.AndroidJavaObject UnityEngine.UDP.StoreService::javaArrayFromCSList(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR AndroidJavaObject_t4131667876 * StoreService_javaArrayFromCSList_m2484081227 (RuntimeObject * __this /* static, unused */, List_1_t3319525431 * ___values0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StoreService_javaArrayFromCSList_m2484081227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t32045322 * V_0 = NULL;
	AndroidJavaObject_t4131667876 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		List_1_t3319525431 * L_0 = ___values0;
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return (AndroidJavaObject_t4131667876 *)NULL;
	}

IL_0005:
	{
		AndroidJavaClass_t32045322 * L_1 = (AndroidJavaClass_t32045322 *)il2cpp_codegen_object_new(AndroidJavaClass_t32045322_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m366853020(L_1, _stringLiteral4010377966, /*hidden argument*/NULL);
		V_0 = L_1;
		AndroidJavaClass_t32045322 * L_2 = V_0;
		ObjectU5BU5D_t2843939325* L_3 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2);
		ObjectU5BU5D_t2843939325* L_4 = L_3;
		AndroidJavaClass_t32045322 * L_5 = (AndroidJavaClass_t32045322 *)il2cpp_codegen_object_new(AndroidJavaClass_t32045322_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_m366853020(L_5, _stringLiteral1687773655, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_4;
		List_1_t3319525431 * L_7 = ___values0;
		NullCheck(L_7);
		int32_t L_8 = List_1_get_Count_m2276455407(L_7, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		int32_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_10);
		NullCheck(L_2);
		AndroidJavaObject_t4131667876 * L_11 = AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4131667876_m1630827594(L_2, _stringLiteral420381065, L_6, /*hidden argument*/AndroidJavaObject_CallStatic_TisAndroidJavaObject_t4131667876_m1630827594_RuntimeMethod_var);
		V_1 = L_11;
		V_2 = 0;
		goto IL_0080;
	}

IL_0041:
	{
		AndroidJavaClass_t32045322 * L_12 = V_0;
		ObjectU5BU5D_t2843939325* L_13 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3);
		ObjectU5BU5D_t2843939325* L_14 = L_13;
		AndroidJavaObject_t4131667876 * L_15 = V_1;
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_15);
		ObjectU5BU5D_t2843939325* L_16 = L_14;
		int32_t L_17 = V_2;
		int32_t L_18 = L_17;
		RuntimeObject * L_19 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_19);
		ObjectU5BU5D_t2843939325* L_20 = L_16;
		ObjectU5BU5D_t2843939325* L_21 = (ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_t2843939325* L_22 = L_21;
		List_1_t3319525431 * L_23 = ___values0;
		int32_t L_24 = V_2;
		NullCheck(L_23);
		String_t* L_25 = List_1_get_Item_m953835688(L_23, L_24, /*hidden argument*/List_1_get_Item_m953835688_RuntimeMethod_var);
		NullCheck(L_22);
		ArrayElementTypeCheck (L_22, L_25);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_25);
		AndroidJavaObject_t4131667876 * L_26 = (AndroidJavaObject_t4131667876 *)il2cpp_codegen_object_new(AndroidJavaObject_t4131667876_il2cpp_TypeInfo_var);
		AndroidJavaObject__ctor_m3828648572(L_26, _stringLiteral1687773655, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_26);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_26);
		NullCheck(L_12);
		AndroidJavaObject_CallStatic_m2922144688(L_12, _stringLiteral2553217811, L_20, /*hidden argument*/NULL);
		int32_t L_27 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_0080:
	{
		int32_t L_28 = V_2;
		List_1_t3319525431 * L_29 = ___values0;
		NullCheck(L_29);
		int32_t L_30 = List_1_get_Count_m2276455407(L_29, /*hidden argument*/List_1_get_Count_m2276455407_RuntimeMethod_var);
		if ((((int32_t)L_28) < ((int32_t)L_30)))
		{
			goto IL_0041;
		}
	}
	{
		AndroidJavaObject_t4131667876 * L_31 = V_1;
		return L_31;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UDP.UdpGameManager::Awake()
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager_Awake_m3051392357 (UdpGameManager_t2849128925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpGameManager_Awake_m3051392357_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral4142051001, /*hidden argument*/NULL);
		AnalyticsService_OnAppAwake_m1143235686(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.UdpGameManager::Start()
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager_Start_m3977941006 (UdpGameManager_t2849128925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpGameManager_Start_m3977941006_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3712921954, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m442555142(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.UdpGameManager::Update()
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager_Update_m2192247910 (UdpGameManager_t2849128925 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.UDP.UdpGameManager::OnApplicationPause(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager_OnApplicationPause_m3137436107 (UdpGameManager_t2849128925 * __this, bool ___pauseStatus0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpGameManager_OnApplicationPause_m3137436107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral2730811082, /*hidden argument*/NULL);
		bool L_0 = ___pauseStatus0;
		AnalyticsService_OnPlayerPaused_m1901506767(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.UdpGameManager::OnApplicationQuit()
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager_OnApplicationQuit_m2061624748 (UdpGameManager_t2849128925 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpGameManager_OnApplicationQuit_m2061624748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3872221057, /*hidden argument*/NULL);
		AnalyticsService_OnPlayerQuit_m2182618095(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.UdpGameManager::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager__ctor_m1213143332 (UdpGameManager_t2849128925 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UDP.UdpGameManager::.cctor()
extern "C" IL2CPP_METHOD_ATTR void UdpGameManager__cctor_m1360727243 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UdpGameManager__cctor_m1360727243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((UdpGameManager_t2849128925_StaticFields*)il2cpp_codegen_static_fields_for(UdpGameManager_t2849128925_il2cpp_TypeInfo_var))->set_OBJECT_NAME_4(_stringLiteral2408352323);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String UnityEngine.UDP.UserInfo::get_Channel()
extern "C" IL2CPP_METHOD_ATTR String_t* UserInfo_get_Channel_m2452301530 (UserInfo_t3529881807 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CChannelU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.UserInfo::set_Channel(System.String)
extern "C" IL2CPP_METHOD_ATTR void UserInfo_set_Channel_m2172175830 (UserInfo_t3529881807 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CChannelU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.UserInfo::get_UserId()
extern "C" IL2CPP_METHOD_ATTR String_t* UserInfo_get_UserId_m533498025 (UserInfo_t3529881807 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUserIdU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.UserInfo::set_UserId(System.String)
extern "C" IL2CPP_METHOD_ATTR void UserInfo_set_UserId_m4114799225 (UserInfo_t3529881807 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUserIdU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.String UnityEngine.UDP.UserInfo::get_UserLoginToken()
extern "C" IL2CPP_METHOD_ATTR String_t* UserInfo_get_UserLoginToken_m2780617215 (UserInfo_t3529881807 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_U3CUserLoginTokenU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void UnityEngine.UDP.UserInfo::set_UserLoginToken(System.String)
extern "C" IL2CPP_METHOD_ATTR void UserInfo_set_UserLoginToken_m3912512598 (UserInfo_t3529881807 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CUserLoginTokenU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void UnityEngine.UDP.UserInfo::.ctor()
extern "C" IL2CPP_METHOD_ATTR void UserInfo__ctor_m3556248149 (UserInfo_t3529881807 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
