﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// AFInAppEvents
struct AFInAppEvents_t2088576796;
// AF_Sample_BGScript
struct AF_Sample_BGScript_t4024703127;
// AF_Sample_BGScript/<PressAnim>c__Iterator0
struct U3CPressAnimU3Ec__Iterator0_t2687738030;
// AfScriptNew
struct AfScriptNew_t3996606863;
// StartUp
struct StartUp_t2202782244;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Object,System.Collections.DictionaryEntry>
struct Transform_1_t1694351041;
// System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.String,System.Collections.DictionaryEntry>
struct Transform_1_t132201056;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product>
struct Dictionary_2_t3029666358;
// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String>
struct Dictionary_2_t2070795836;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product>
struct HashSet_1_t1809359533;
// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition>
struct HashSet_1_t3199643908;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.Link[]
struct LinkU5BU5D_t964245573;
// System.Collections.Generic.List`1<UnityEngine.Purchasing.PayoutDefinition>
struct List_1_t748791510;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Func`2<UnityEngine.Purchasing.Product,System.String>
struct Func_2_t1313482816;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// Uniject.IUtil
struct IUtil_t1069285358;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.ILogger
struct ILogger_t2607134790;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.Purchasing.CloudCatalogImpl
struct CloudCatalogImpl_t1580312503;
// UnityEngine.Purchasing.ConfigurationBuilder
struct ConfigurationBuilder_t1618671084;
// UnityEngine.Purchasing.Extension.IPurchasingBinder
struct IPurchasingBinder_t3569785493;
// UnityEngine.Purchasing.Extension.IPurchasingModule
struct IPurchasingModule_t960499109;
// UnityEngine.Purchasing.Extension.IPurchasingModule[]
struct IPurchasingModuleU5BU5D_t3784316456;
// UnityEngine.Purchasing.IAsyncWebUtil
struct IAsyncWebUtil_t965808653;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t3180538779;
// UnityEngine.Purchasing.INativeStoreProvider
struct INativeStoreProvider_t2882146526;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t2579314702;
// UnityEngine.Purchasing.IStoreListener
struct IStoreListener_t2917505531;
// UnityEngine.Purchasing.Product
struct Product_t3244410059;
// UnityEngine.Purchasing.ProductCollection
struct ProductCollection_t2671956229;
// UnityEngine.Purchasing.ProductDefinition
struct ProductDefinition_t339727138;
// UnityEngine.Purchasing.ProductMetadata
struct ProductMetadata_t3417118930;
// UnityEngine.Purchasing.Product[]
struct ProductU5BU5D_t2942947242;
// UnityEngine.Purchasing.PurchaseEventArgs
struct PurchaseEventArgs_t3033159582;
// UnityEngine.Purchasing.PurchasingFactory
struct PurchasingFactory_t4012818695;
// UnityEngine.Purchasing.StandardPurchasingModule
struct StandardPurchasingModule_t2580735509;
// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance
struct StoreInstance_t2416643455;
// UnityEngine.Purchasing.WinRTStore
struct WinRTStore_t2015085940;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.TextGenerator
struct TextGenerator_t3211863866;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3055525458;
// UnityEngine.UI.FontData
struct FontData_t746620069;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3661388177;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t3474889437;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t2453304189;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251;
// afobjectscript
struct afobjectscript_t980149766;

extern RuntimeClass* AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var;
extern RuntimeClass* AfScriptNew_t3996606863_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t1632706988_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2865362463_il2cpp_TypeInfo_var;
extern RuntimeClass* IPurchasingModuleU5BU5D_t3784316456_il2cpp_TypeInfo_var;
extern RuntimeClass* IStoreController_t2579314702_il2cpp_TypeInfo_var;
extern RuntimeClass* InitializationFailureReason_t2740567704_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* PurchaseFailureReason_t4243987912_il2cpp_TypeInfo_var;
extern RuntimeClass* Regex_t3657309853_il2cpp_TypeInfo_var;
extern RuntimeClass* StandardPurchasingModule_t2580735509_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CPressAnimU3Ec__Iterator0_t2687738030_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForSeconds_t1699091251_il2cpp_TypeInfo_var;
extern RuntimeClass* afobjectscript_t980149766_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1002265957;
extern String_t* _stringLiteral1002553885;
extern String_t* _stringLiteral1008257438;
extern String_t* _stringLiteral1056623483;
extern String_t* _stringLiteral116130005;
extern String_t* _stringLiteral1232190289;
extern String_t* _stringLiteral1315542119;
extern String_t* _stringLiteral1344052996;
extern String_t* _stringLiteral1504355976;
extern String_t* _stringLiteral1632276064;
extern String_t* _stringLiteral1670686406;
extern String_t* _stringLiteral1727939182;
extern String_t* _stringLiteral1731952304;
extern String_t* _stringLiteral1805404713;
extern String_t* _stringLiteral181065742;
extern String_t* _stringLiteral1844033921;
extern String_t* _stringLiteral1849190365;
extern String_t* _stringLiteral1867591191;
extern String_t* _stringLiteral1928966392;
extern String_t* _stringLiteral1929732520;
extern String_t* _stringLiteral209837549;
extern String_t* _stringLiteral21130133;
extern String_t* _stringLiteral232548031;
extern String_t* _stringLiteral23435790;
extern String_t* _stringLiteral2418339336;
extern String_t* _stringLiteral261945041;
extern String_t* _stringLiteral2777653202;
extern String_t* _stringLiteral2795224173;
extern String_t* _stringLiteral279557124;
extern String_t* _stringLiteral2863737031;
extern String_t* _stringLiteral2972012286;
extern String_t* _stringLiteral3050893988;
extern String_t* _stringLiteral3054649102;
extern String_t* _stringLiteral3073017110;
extern String_t* _stringLiteral3074643751;
extern String_t* _stringLiteral3099958611;
extern String_t* _stringLiteral310096932;
extern String_t* _stringLiteral3239286485;
extern String_t* _stringLiteral3329093674;
extern String_t* _stringLiteral3329093677;
extern String_t* _stringLiteral3385962031;
extern String_t* _stringLiteral3451565963;
extern String_t* _stringLiteral3451565967;
extern String_t* _stringLiteral3514087433;
extern String_t* _stringLiteral356456174;
extern String_t* _stringLiteral3655604716;
extern String_t* _stringLiteral3678466557;
extern String_t* _stringLiteral3833001788;
extern String_t* _stringLiteral3836975195;
extern String_t* _stringLiteral3885527639;
extern String_t* _stringLiteral3898871526;
extern String_t* _stringLiteral3924207445;
extern String_t* _stringLiteral4000924501;
extern String_t* _stringLiteral4002445229;
extern String_t* _stringLiteral4051270574;
extern String_t* _stringLiteral4072640822;
extern String_t* _stringLiteral4101302744;
extern String_t* _stringLiteral415387938;
extern String_t* _stringLiteral421963658;
extern String_t* _stringLiteral425026648;
extern String_t* _stringLiteral4289976629;
extern String_t* _stringLiteral457746925;
extern String_t* _stringLiteral470911652;
extern String_t* _stringLiteral508901929;
extern String_t* _stringLiteral531322540;
extern String_t* _stringLiteral533775410;
extern String_t* _stringLiteral552165993;
extern String_t* _stringLiteral623525981;
extern String_t* _stringLiteral748725514;
extern String_t* _stringLiteral83672638;
extern String_t* _stringLiteral923836985;
extern String_t* _stringLiteral940768469;
extern const RuntimeMethod* Dictionary_2_Add_m3097518446_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m3302800229_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_get_Item_m1539906286_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisText_t1901882714_m565625081_RuntimeMethod_var;
extern const RuntimeMethod* U3CPressAnimU3Ec__Iterator0_Reset_m1696407584_RuntimeMethod_var;
extern const uint32_t AF_Sample_BGScript_PressAnim_m1380001828_MetadataUsageId;
extern const uint32_t AF_Sample_BGScript_Update_m2621362297_MetadataUsageId;
extern const uint32_t AF_Sample_BGScript_pressed_m3047931247_MetadataUsageId;
extern const uint32_t AfScriptNew_BuyConsumable_m148322322_MetadataUsageId;
extern const uint32_t AfScriptNew_BuyProductID_m2762636159_MetadataUsageId;
extern const uint32_t AfScriptNew_ConsTaskOnClick_m1294378824_MetadataUsageId;
extern const uint32_t AfScriptNew_InitializePurchasing_m1487909055_MetadataUsageId;
extern const uint32_t AfScriptNew_IsInitialized_m2350561814_MetadataUsageId;
extern const uint32_t AfScriptNew_OnInitializeFailed_m420728356_MetadataUsageId;
extern const uint32_t AfScriptNew_OnInitialized_m2338278659_MetadataUsageId;
extern const uint32_t AfScriptNew_OnPurchaseFailed_m2320970959_MetadataUsageId;
extern const uint32_t AfScriptNew_ProcessPurchase_m1130738387_MetadataUsageId;
extern const uint32_t AfScriptNew_Start_m2271372884_MetadataUsageId;
extern const uint32_t AfScriptNew__cctor_m3116246755_MetadataUsageId;
extern const uint32_t AfScriptNew_afiosValidate_m178811455_MetadataUsageId;
extern const uint32_t AfScriptNew_appsFlyerInit_m713040632_MetadataUsageId;
extern const uint32_t StartUp_Purchase_m2962335323_MetadataUsageId;
extern const uint32_t StartUp_Start_m453063797_MetadataUsageId;
extern const uint32_t StartUp_Update_m423706476_MetadataUsageId;
extern const uint32_t StartUp_didReceiveConversionDataWithError_m2011039941_MetadataUsageId;
extern const uint32_t StartUp_didReceiveConversionData_m2499638998_MetadataUsageId;
extern const uint32_t StartUp_onAppOpenAttributionFailure_m1278686197_MetadataUsageId;
extern const uint32_t StartUp_onAppOpenAttribution_m3708816227_MetadataUsageId;
extern const uint32_t U3CPressAnimU3Ec__Iterator0_MoveNext_m3825884020_MetadataUsageId;
extern const uint32_t U3CPressAnimU3Ec__Iterator0_Reset_m1696407584_MetadataUsageId;
extern const uint32_t afobjectscript_BuyConsumable_m2485974033_MetadataUsageId;
extern const uint32_t afobjectscript_BuyProductID_m652833323_MetadataUsageId;
extern const uint32_t afobjectscript_ConsTaskOnClick_m1203192837_MetadataUsageId;
extern const uint32_t afobjectscript_InitializePurchasing_m2132555723_MetadataUsageId;
extern const uint32_t afobjectscript_IsInitialized_m2000466453_MetadataUsageId;
extern const uint32_t afobjectscript_OnInitializeFailed_m2008076800_MetadataUsageId;
extern const uint32_t afobjectscript_OnInitialized_m3062782771_MetadataUsageId;
extern const uint32_t afobjectscript_OnPurchaseFailed_m3942370016_MetadataUsageId;
extern const uint32_t afobjectscript_ProcessPurchase_m4169781192_MetadataUsageId;
extern const uint32_t afobjectscript_Start_m1357431867_MetadataUsageId;
extern const uint32_t afobjectscript__cctor_m869613066_MetadataUsageId;
extern const uint32_t afobjectscript_afiosValidate_m391050797_MetadataUsageId;
extern const uint32_t afobjectscript_appsFlyerInit_m1783334442_MetadataUsageId;

struct ByteU5BU5D_t4116647657;
struct StringU5BU5D_t1281789340;
struct IPurchasingModuleU5BU5D_t3784316456;


#ifndef U3CMODULEU3E_T692745559_H
#define U3CMODULEU3E_T692745559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745559 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745559_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef AFINAPPEVENTS_T2088576796_H
#define AFINAPPEVENTS_T2088576796_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AFInAppEvents
struct  AFInAppEvents_t2088576796  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFINAPPEVENTS_T2088576796_H
#ifndef U3CPRESSANIMU3EC__ITERATOR0_T2687738030_H
#define U3CPRESSANIMU3EC__ITERATOR0_T2687738030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AF_Sample_BGScript/<PressAnim>c__Iterator0
struct  U3CPressAnimU3Ec__Iterator0_t2687738030  : public RuntimeObject
{
public:
	// AF_Sample_BGScript AF_Sample_BGScript/<PressAnim>c__Iterator0::$this
	AF_Sample_BGScript_t4024703127 * ___U24this_0;
	// System.Object AF_Sample_BGScript/<PressAnim>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean AF_Sample_BGScript/<PressAnim>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 AF_Sample_BGScript/<PressAnim>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CPressAnimU3Ec__Iterator0_t2687738030, ___U24this_0)); }
	inline AF_Sample_BGScript_t4024703127 * get_U24this_0() const { return ___U24this_0; }
	inline AF_Sample_BGScript_t4024703127 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(AF_Sample_BGScript_t4024703127 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CPressAnimU3Ec__Iterator0_t2687738030, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CPressAnimU3Ec__Iterator0_t2687738030, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CPressAnimU3Ec__Iterator0_t2687738030, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRESSANIMU3EC__ITERATOR0_T2687738030_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	ObjectU5BU5D_t2843939325* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___valueSlots_7)); }
	inline ObjectU5BU5D_t2843939325* get_valueSlots_7() const { return ___valueSlots_7; }
	inline ObjectU5BU5D_t2843939325** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(ObjectU5BU5D_t2843939325* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t2865362463_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t1694351041 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t1694351041 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t1694351041 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t1694351041 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef DICTIONARY_2_T1632706988_H
#define DICTIONARY_2_T1632706988_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct  Dictionary_2_t1632706988  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::table
	Int32U5BU5D_t385246372* ___table_4;
	// System.Collections.Generic.Link[] System.Collections.Generic.Dictionary`2::linkSlots
	LinkU5BU5D_t964245573* ___linkSlots_5;
	// TKey[] System.Collections.Generic.Dictionary`2::keySlots
	StringU5BU5D_t1281789340* ___keySlots_6;
	// TValue[] System.Collections.Generic.Dictionary`2::valueSlots
	StringU5BU5D_t1281789340* ___valueSlots_7;
	// System.Int32 System.Collections.Generic.Dictionary`2::touchedSlots
	int32_t ___touchedSlots_8;
	// System.Int32 System.Collections.Generic.Dictionary`2::emptySlot
	int32_t ___emptySlot_9;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_10;
	// System.Int32 System.Collections.Generic.Dictionary`2::threshold
	int32_t ___threshold_11;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::hcp
	RuntimeObject* ___hcp_12;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.Dictionary`2::serialization_info
	SerializationInfo_t950877179 * ___serialization_info_13;
	// System.Int32 System.Collections.Generic.Dictionary`2::generation
	int32_t ___generation_14;

public:
	inline static int32_t get_offset_of_table_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___table_4)); }
	inline Int32U5BU5D_t385246372* get_table_4() const { return ___table_4; }
	inline Int32U5BU5D_t385246372** get_address_of_table_4() { return &___table_4; }
	inline void set_table_4(Int32U5BU5D_t385246372* value)
	{
		___table_4 = value;
		Il2CppCodeGenWriteBarrier((&___table_4), value);
	}

	inline static int32_t get_offset_of_linkSlots_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___linkSlots_5)); }
	inline LinkU5BU5D_t964245573* get_linkSlots_5() const { return ___linkSlots_5; }
	inline LinkU5BU5D_t964245573** get_address_of_linkSlots_5() { return &___linkSlots_5; }
	inline void set_linkSlots_5(LinkU5BU5D_t964245573* value)
	{
		___linkSlots_5 = value;
		Il2CppCodeGenWriteBarrier((&___linkSlots_5), value);
	}

	inline static int32_t get_offset_of_keySlots_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___keySlots_6)); }
	inline StringU5BU5D_t1281789340* get_keySlots_6() const { return ___keySlots_6; }
	inline StringU5BU5D_t1281789340** get_address_of_keySlots_6() { return &___keySlots_6; }
	inline void set_keySlots_6(StringU5BU5D_t1281789340* value)
	{
		___keySlots_6 = value;
		Il2CppCodeGenWriteBarrier((&___keySlots_6), value);
	}

	inline static int32_t get_offset_of_valueSlots_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___valueSlots_7)); }
	inline StringU5BU5D_t1281789340* get_valueSlots_7() const { return ___valueSlots_7; }
	inline StringU5BU5D_t1281789340** get_address_of_valueSlots_7() { return &___valueSlots_7; }
	inline void set_valueSlots_7(StringU5BU5D_t1281789340* value)
	{
		___valueSlots_7 = value;
		Il2CppCodeGenWriteBarrier((&___valueSlots_7), value);
	}

	inline static int32_t get_offset_of_touchedSlots_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___touchedSlots_8)); }
	inline int32_t get_touchedSlots_8() const { return ___touchedSlots_8; }
	inline int32_t* get_address_of_touchedSlots_8() { return &___touchedSlots_8; }
	inline void set_touchedSlots_8(int32_t value)
	{
		___touchedSlots_8 = value;
	}

	inline static int32_t get_offset_of_emptySlot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___emptySlot_9)); }
	inline int32_t get_emptySlot_9() const { return ___emptySlot_9; }
	inline int32_t* get_address_of_emptySlot_9() { return &___emptySlot_9; }
	inline void set_emptySlot_9(int32_t value)
	{
		___emptySlot_9 = value;
	}

	inline static int32_t get_offset_of_count_10() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___count_10)); }
	inline int32_t get_count_10() const { return ___count_10; }
	inline int32_t* get_address_of_count_10() { return &___count_10; }
	inline void set_count_10(int32_t value)
	{
		___count_10 = value;
	}

	inline static int32_t get_offset_of_threshold_11() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___threshold_11)); }
	inline int32_t get_threshold_11() const { return ___threshold_11; }
	inline int32_t* get_address_of_threshold_11() { return &___threshold_11; }
	inline void set_threshold_11(int32_t value)
	{
		___threshold_11 = value;
	}

	inline static int32_t get_offset_of_hcp_12() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___hcp_12)); }
	inline RuntimeObject* get_hcp_12() const { return ___hcp_12; }
	inline RuntimeObject** get_address_of_hcp_12() { return &___hcp_12; }
	inline void set_hcp_12(RuntimeObject* value)
	{
		___hcp_12 = value;
		Il2CppCodeGenWriteBarrier((&___hcp_12), value);
	}

	inline static int32_t get_offset_of_serialization_info_13() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___serialization_info_13)); }
	inline SerializationInfo_t950877179 * get_serialization_info_13() const { return ___serialization_info_13; }
	inline SerializationInfo_t950877179 ** get_address_of_serialization_info_13() { return &___serialization_info_13; }
	inline void set_serialization_info_13(SerializationInfo_t950877179 * value)
	{
		___serialization_info_13 = value;
		Il2CppCodeGenWriteBarrier((&___serialization_info_13), value);
	}

	inline static int32_t get_offset_of_generation_14() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988, ___generation_14)); }
	inline int32_t get_generation_14() const { return ___generation_14; }
	inline int32_t* get_address_of_generation_14() { return &___generation_14; }
	inline void set_generation_14(int32_t value)
	{
		___generation_14 = value;
	}
};

struct Dictionary_2_t1632706988_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2/Transform`1<TKey,TValue,System.Collections.DictionaryEntry> System.Collections.Generic.Dictionary`2::<>f__am$cacheB
	Transform_1_t132201056 * ___U3CU3Ef__amU24cacheB_15;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_15() { return static_cast<int32_t>(offsetof(Dictionary_2_t1632706988_StaticFields, ___U3CU3Ef__amU24cacheB_15)); }
	inline Transform_1_t132201056 * get_U3CU3Ef__amU24cacheB_15() const { return ___U3CU3Ef__amU24cacheB_15; }
	inline Transform_1_t132201056 ** get_address_of_U3CU3Ef__amU24cacheB_15() { return &___U3CU3Ef__amU24cacheB_15; }
	inline void set_U3CU3Ef__amU24cacheB_15(Transform_1_t132201056 * value)
	{
		___U3CU3Ef__amU24cacheB_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheB_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T1632706988_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CONFIGURATIONBUILDER_T1618671084_H
#define CONFIGURATIONBUILDER_T1618671084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ConfigurationBuilder
struct  ConfigurationBuilder_t1618671084  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.PurchasingFactory UnityEngine.Purchasing.ConfigurationBuilder::m_Factory
	PurchasingFactory_t4012818695 * ___m_Factory_0;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.ProductDefinition> UnityEngine.Purchasing.ConfigurationBuilder::m_Products
	HashSet_1_t3199643908 * ___m_Products_1;
	// System.Boolean UnityEngine.Purchasing.ConfigurationBuilder::<useCatalogProvider>k__BackingField
	bool ___U3CuseCatalogProviderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_m_Factory_0() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t1618671084, ___m_Factory_0)); }
	inline PurchasingFactory_t4012818695 * get_m_Factory_0() const { return ___m_Factory_0; }
	inline PurchasingFactory_t4012818695 ** get_address_of_m_Factory_0() { return &___m_Factory_0; }
	inline void set_m_Factory_0(PurchasingFactory_t4012818695 * value)
	{
		___m_Factory_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Factory_0), value);
	}

	inline static int32_t get_offset_of_m_Products_1() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t1618671084, ___m_Products_1)); }
	inline HashSet_1_t3199643908 * get_m_Products_1() const { return ___m_Products_1; }
	inline HashSet_1_t3199643908 ** get_address_of_m_Products_1() { return &___m_Products_1; }
	inline void set_m_Products_1(HashSet_1_t3199643908 * value)
	{
		___m_Products_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Products_1), value);
	}

	inline static int32_t get_offset_of_U3CuseCatalogProviderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConfigurationBuilder_t1618671084, ___U3CuseCatalogProviderU3Ek__BackingField_2)); }
	inline bool get_U3CuseCatalogProviderU3Ek__BackingField_2() const { return ___U3CuseCatalogProviderU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CuseCatalogProviderU3Ek__BackingField_2() { return &___U3CuseCatalogProviderU3Ek__BackingField_2; }
	inline void set_U3CuseCatalogProviderU3Ek__BackingField_2(bool value)
	{
		___U3CuseCatalogProviderU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGURATIONBUILDER_T1618671084_H
#ifndef ABSTRACTPURCHASINGMODULE_T2882497868_H
#define ABSTRACTPURCHASINGMODULE_T2882497868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Extension.AbstractPurchasingModule
struct  AbstractPurchasingModule_t2882497868  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Extension.IPurchasingBinder UnityEngine.Purchasing.Extension.AbstractPurchasingModule::m_Binder
	RuntimeObject* ___m_Binder_0;

public:
	inline static int32_t get_offset_of_m_Binder_0() { return static_cast<int32_t>(offsetof(AbstractPurchasingModule_t2882497868, ___m_Binder_0)); }
	inline RuntimeObject* get_m_Binder_0() const { return ___m_Binder_0; }
	inline RuntimeObject** get_address_of_m_Binder_0() { return &___m_Binder_0; }
	inline void set_m_Binder_0(RuntimeObject* value)
	{
		___m_Binder_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Binder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTPURCHASINGMODULE_T2882497868_H
#ifndef PRODUCT_T3244410059_H
#define PRODUCT_T3244410059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Product
struct  Product_t3244410059  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.Product::<definition>k__BackingField
	ProductDefinition_t339727138 * ___U3CdefinitionU3Ek__BackingField_0;
	// UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.Product::<metadata>k__BackingField
	ProductMetadata_t3417118930 * ___U3CmetadataU3Ek__BackingField_1;
	// System.Boolean UnityEngine.Purchasing.Product::<availableToPurchase>k__BackingField
	bool ___U3CavailableToPurchaseU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Product::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Product::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CdefinitionU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Product_t3244410059, ___U3CdefinitionU3Ek__BackingField_0)); }
	inline ProductDefinition_t339727138 * get_U3CdefinitionU3Ek__BackingField_0() const { return ___U3CdefinitionU3Ek__BackingField_0; }
	inline ProductDefinition_t339727138 ** get_address_of_U3CdefinitionU3Ek__BackingField_0() { return &___U3CdefinitionU3Ek__BackingField_0; }
	inline void set_U3CdefinitionU3Ek__BackingField_0(ProductDefinition_t339727138 * value)
	{
		___U3CdefinitionU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdefinitionU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CmetadataU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Product_t3244410059, ___U3CmetadataU3Ek__BackingField_1)); }
	inline ProductMetadata_t3417118930 * get_U3CmetadataU3Ek__BackingField_1() const { return ___U3CmetadataU3Ek__BackingField_1; }
	inline ProductMetadata_t3417118930 ** get_address_of_U3CmetadataU3Ek__BackingField_1() { return &___U3CmetadataU3Ek__BackingField_1; }
	inline void set_U3CmetadataU3Ek__BackingField_1(ProductMetadata_t3417118930 * value)
	{
		___U3CmetadataU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmetadataU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CavailableToPurchaseU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Product_t3244410059, ___U3CavailableToPurchaseU3Ek__BackingField_2)); }
	inline bool get_U3CavailableToPurchaseU3Ek__BackingField_2() const { return ___U3CavailableToPurchaseU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CavailableToPurchaseU3Ek__BackingField_2() { return &___U3CavailableToPurchaseU3Ek__BackingField_2; }
	inline void set_U3CavailableToPurchaseU3Ek__BackingField_2(bool value)
	{
		___U3CavailableToPurchaseU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Product_t3244410059, ___U3CtransactionIDU3Ek__BackingField_3)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_3() const { return ___U3CtransactionIDU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_3() { return &___U3CtransactionIDU3Ek__BackingField_3; }
	inline void set_U3CtransactionIDU3Ek__BackingField_3(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Product_t3244410059, ___U3CreceiptU3Ek__BackingField_4)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_4() const { return ___U3CreceiptU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_4() { return &___U3CreceiptU3Ek__BackingField_4; }
	inline void set_U3CreceiptU3Ek__BackingField_4(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCT_T3244410059_H
#ifndef PRODUCTCOLLECTION_T2671956229_H
#define PRODUCTCOLLECTION_T2671956229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductCollection
struct  ProductCollection_t2671956229  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_IdToProduct
	Dictionary_2_t3029666358 * ___m_IdToProduct_0;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_StoreSpecificIdToProduct
	Dictionary_2_t3029666358 * ___m_StoreSpecificIdToProduct_1;
	// UnityEngine.Purchasing.Product[] UnityEngine.Purchasing.ProductCollection::m_Products
	ProductU5BU5D_t2942947242* ___m_Products_2;
	// System.Collections.Generic.HashSet`1<UnityEngine.Purchasing.Product> UnityEngine.Purchasing.ProductCollection::m_ProductSet
	HashSet_1_t1809359533 * ___m_ProductSet_3;

public:
	inline static int32_t get_offset_of_m_IdToProduct_0() { return static_cast<int32_t>(offsetof(ProductCollection_t2671956229, ___m_IdToProduct_0)); }
	inline Dictionary_2_t3029666358 * get_m_IdToProduct_0() const { return ___m_IdToProduct_0; }
	inline Dictionary_2_t3029666358 ** get_address_of_m_IdToProduct_0() { return &___m_IdToProduct_0; }
	inline void set_m_IdToProduct_0(Dictionary_2_t3029666358 * value)
	{
		___m_IdToProduct_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_IdToProduct_0), value);
	}

	inline static int32_t get_offset_of_m_StoreSpecificIdToProduct_1() { return static_cast<int32_t>(offsetof(ProductCollection_t2671956229, ___m_StoreSpecificIdToProduct_1)); }
	inline Dictionary_2_t3029666358 * get_m_StoreSpecificIdToProduct_1() const { return ___m_StoreSpecificIdToProduct_1; }
	inline Dictionary_2_t3029666358 ** get_address_of_m_StoreSpecificIdToProduct_1() { return &___m_StoreSpecificIdToProduct_1; }
	inline void set_m_StoreSpecificIdToProduct_1(Dictionary_2_t3029666358 * value)
	{
		___m_StoreSpecificIdToProduct_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreSpecificIdToProduct_1), value);
	}

	inline static int32_t get_offset_of_m_Products_2() { return static_cast<int32_t>(offsetof(ProductCollection_t2671956229, ___m_Products_2)); }
	inline ProductU5BU5D_t2942947242* get_m_Products_2() const { return ___m_Products_2; }
	inline ProductU5BU5D_t2942947242** get_address_of_m_Products_2() { return &___m_Products_2; }
	inline void set_m_Products_2(ProductU5BU5D_t2942947242* value)
	{
		___m_Products_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Products_2), value);
	}

	inline static int32_t get_offset_of_m_ProductSet_3() { return static_cast<int32_t>(offsetof(ProductCollection_t2671956229, ___m_ProductSet_3)); }
	inline HashSet_1_t1809359533 * get_m_ProductSet_3() const { return ___m_ProductSet_3; }
	inline HashSet_1_t1809359533 ** get_address_of_m_ProductSet_3() { return &___m_ProductSet_3; }
	inline void set_m_ProductSet_3(HashSet_1_t1809359533 * value)
	{
		___m_ProductSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ProductSet_3), value);
	}
};

struct ProductCollection_t2671956229_StaticFields
{
public:
	// System.Func`2<UnityEngine.Purchasing.Product,System.String> UnityEngine.Purchasing.ProductCollection::<>f__am$cache0
	Func_2_t1313482816 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<UnityEngine.Purchasing.Product,System.String> UnityEngine.Purchasing.ProductCollection::<>f__am$cache1
	Func_2_t1313482816 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(ProductCollection_t2671956229_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_t1313482816 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_t1313482816 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_t1313482816 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(ProductCollection_t2671956229_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t1313482816 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t1313482816 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t1313482816 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTCOLLECTION_T2671956229_H
#ifndef PURCHASEEVENTARGS_T3033159582_H
#define PURCHASEEVENTARGS_T3033159582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchaseEventArgs
struct  PurchaseEventArgs_t3033159582  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.Product UnityEngine.Purchasing.PurchaseEventArgs::<purchasedProduct>k__BackingField
	Product_t3244410059 * ___U3CpurchasedProductU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CpurchasedProductU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PurchaseEventArgs_t3033159582, ___U3CpurchasedProductU3Ek__BackingField_0)); }
	inline Product_t3244410059 * get_U3CpurchasedProductU3Ek__BackingField_0() const { return ___U3CpurchasedProductU3Ek__BackingField_0; }
	inline Product_t3244410059 ** get_address_of_U3CpurchasedProductU3Ek__BackingField_0() { return &___U3CpurchasedProductU3Ek__BackingField_0; }
	inline void set_U3CpurchasedProductU3Ek__BackingField_0(Product_t3244410059 * value)
	{
		___U3CpurchasedProductU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpurchasedProductU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEEVENTARGS_T3033159582_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef DECIMAL_T2948259380_H
#define DECIMAL_T2948259380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2948259380 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t2948259380_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t2948259380  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2948259380  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2948259380  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t2948259380  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t2948259380  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinValue_0)); }
	inline Decimal_t2948259380  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t2948259380 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t2948259380  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValue_1)); }
	inline Decimal_t2948259380  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t2948259380 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t2948259380  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinusOne_2)); }
	inline Decimal_t2948259380  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t2948259380 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t2948259380  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___One_3)); }
	inline Decimal_t2948259380  get_One_3() const { return ___One_3; }
	inline Decimal_t2948259380 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t2948259380  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t2948259380  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t2948259380 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t2948259380  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2948259380_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef WAITFORSECONDS_T1699091251_H
#define WAITFORSECONDS_T1699091251_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t1699091251  : public YieldInstruction_t403091072
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t1699091251, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t1699091251_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T1699091251_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef STRINGCOMPARISON_T3657712135_H
#define STRINGCOMPARISON_T3657712135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.StringComparison
struct  StringComparison_t3657712135 
{
public:
	// System.Int32 System.StringComparison::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StringComparison_t3657712135, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGCOMPARISON_T3657712135_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef KEYCODE_T2599294277_H
#define KEYCODE_T2599294277_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2599294277 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2599294277, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2599294277_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef APPSTORE_T355301105_H
#define APPSTORE_T355301105_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AppStore
struct  AppStore_t355301105 
{
public:
	// System.Int32 UnityEngine.Purchasing.AppStore::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AppStore_t355301105, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPSTORE_T355301105_H
#ifndef FAKESTOREUIMODE_T680685637_H
#define FAKESTOREUIMODE_T680685637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeStoreUIMode
struct  FakeStoreUIMode_t680685637 
{
public:
	// System.Int32 UnityEngine.Purchasing.FakeStoreUIMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FakeStoreUIMode_t680685637, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESTOREUIMODE_T680685637_H
#ifndef INITIALIZATIONFAILUREREASON_T2740567704_H
#define INITIALIZATIONFAILUREREASON_T2740567704_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.InitializationFailureReason
struct  InitializationFailureReason_t2740567704 
{
public:
	// System.Int32 UnityEngine.Purchasing.InitializationFailureReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InitializationFailureReason_t2740567704, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIALIZATIONFAILUREREASON_T2740567704_H
#ifndef PRODUCTMETADATA_T3417118930_H
#define PRODUCTMETADATA_T3417118930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductMetadata
struct  ProductMetadata_t3417118930  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductMetadata::<localizedPriceString>k__BackingField
	String_t* ___U3ClocalizedPriceStringU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.ProductMetadata::<localizedTitle>k__BackingField
	String_t* ___U3ClocalizedTitleU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.ProductMetadata::<localizedDescription>k__BackingField
	String_t* ___U3ClocalizedDescriptionU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.ProductMetadata::<isoCurrencyCode>k__BackingField
	String_t* ___U3CisoCurrencyCodeU3Ek__BackingField_3;
	// System.Decimal UnityEngine.Purchasing.ProductMetadata::<localizedPrice>k__BackingField
	Decimal_t2948259380  ___U3ClocalizedPriceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3ClocalizedPriceStringU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductMetadata_t3417118930, ___U3ClocalizedPriceStringU3Ek__BackingField_0)); }
	inline String_t* get_U3ClocalizedPriceStringU3Ek__BackingField_0() const { return ___U3ClocalizedPriceStringU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3ClocalizedPriceStringU3Ek__BackingField_0() { return &___U3ClocalizedPriceStringU3Ek__BackingField_0; }
	inline void set_U3ClocalizedPriceStringU3Ek__BackingField_0(String_t* value)
	{
		___U3ClocalizedPriceStringU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalizedPriceStringU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3ClocalizedTitleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductMetadata_t3417118930, ___U3ClocalizedTitleU3Ek__BackingField_1)); }
	inline String_t* get_U3ClocalizedTitleU3Ek__BackingField_1() const { return ___U3ClocalizedTitleU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3ClocalizedTitleU3Ek__BackingField_1() { return &___U3ClocalizedTitleU3Ek__BackingField_1; }
	inline void set_U3ClocalizedTitleU3Ek__BackingField_1(String_t* value)
	{
		___U3ClocalizedTitleU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalizedTitleU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ClocalizedDescriptionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProductMetadata_t3417118930, ___U3ClocalizedDescriptionU3Ek__BackingField_2)); }
	inline String_t* get_U3ClocalizedDescriptionU3Ek__BackingField_2() const { return ___U3ClocalizedDescriptionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ClocalizedDescriptionU3Ek__BackingField_2() { return &___U3ClocalizedDescriptionU3Ek__BackingField_2; }
	inline void set_U3ClocalizedDescriptionU3Ek__BackingField_2(String_t* value)
	{
		___U3ClocalizedDescriptionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClocalizedDescriptionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CisoCurrencyCodeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProductMetadata_t3417118930, ___U3CisoCurrencyCodeU3Ek__BackingField_3)); }
	inline String_t* get_U3CisoCurrencyCodeU3Ek__BackingField_3() const { return ___U3CisoCurrencyCodeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CisoCurrencyCodeU3Ek__BackingField_3() { return &___U3CisoCurrencyCodeU3Ek__BackingField_3; }
	inline void set_U3CisoCurrencyCodeU3Ek__BackingField_3(String_t* value)
	{
		___U3CisoCurrencyCodeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CisoCurrencyCodeU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3ClocalizedPriceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProductMetadata_t3417118930, ___U3ClocalizedPriceU3Ek__BackingField_4)); }
	inline Decimal_t2948259380  get_U3ClocalizedPriceU3Ek__BackingField_4() const { return ___U3ClocalizedPriceU3Ek__BackingField_4; }
	inline Decimal_t2948259380 * get_address_of_U3ClocalizedPriceU3Ek__BackingField_4() { return &___U3ClocalizedPriceU3Ek__BackingField_4; }
	inline void set_U3ClocalizedPriceU3Ek__BackingField_4(Decimal_t2948259380  value)
	{
		___U3ClocalizedPriceU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTMETADATA_T3417118930_H
#ifndef PRODUCTTYPE_T1868976581_H
#define PRODUCTTYPE_T1868976581_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductType
struct  ProductType_t1868976581 
{
public:
	// System.Int32 UnityEngine.Purchasing.ProductType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ProductType_t1868976581, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTTYPE_T1868976581_H
#ifndef PURCHASEFAILUREREASON_T4243987912_H
#define PURCHASEFAILUREREASON_T4243987912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchaseFailureReason
struct  PurchaseFailureReason_t4243987912 
{
public:
	// System.Int32 UnityEngine.Purchasing.PurchaseFailureReason::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PurchaseFailureReason_t4243987912, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEFAILUREREASON_T4243987912_H
#ifndef PURCHASEPROCESSINGRESULT_T2219688332_H
#define PURCHASEPROCESSINGRESULT_T2219688332_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PurchaseProcessingResult
struct  PurchaseProcessingResult_t2219688332 
{
public:
	// System.Int32 UnityEngine.Purchasing.PurchaseProcessingResult::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PurchaseProcessingResult_t2219688332, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEPROCESSINGRESULT_T2219688332_H
#ifndef RUNTIMEPLATFORM_T4159857903_H
#define RUNTIMEPLATFORM_T4159857903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t4159857903 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t4159857903, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T4159857903_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef NOTIFICATIONTYPE_T693604564_H
#define NOTIFICATIONTYPE_T693604564_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.iOS.NotificationType
struct  NotificationType_t693604564 
{
public:
	// System.Int32 UnityEngine.iOS.NotificationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(NotificationType_t693604564, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONTYPE_T693604564_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef PRODUCTDEFINITION_T339727138_H
#define PRODUCTDEFINITION_T339727138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ProductDefinition
struct  ProductDefinition_t339727138  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.ProductDefinition::<id>k__BackingField
	String_t* ___U3CidU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.ProductDefinition::<storeSpecificId>k__BackingField
	String_t* ___U3CstoreSpecificIdU3Ek__BackingField_1;
	// UnityEngine.Purchasing.ProductType UnityEngine.Purchasing.ProductDefinition::<type>k__BackingField
	int32_t ___U3CtypeU3Ek__BackingField_2;
	// System.Boolean UnityEngine.Purchasing.ProductDefinition::<enabled>k__BackingField
	bool ___U3CenabledU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<UnityEngine.Purchasing.PayoutDefinition> UnityEngine.Purchasing.ProductDefinition::m_Payouts
	List_1_t748791510 * ___m_Payouts_4;

public:
	inline static int32_t get_offset_of_U3CidU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductDefinition_t339727138, ___U3CidU3Ek__BackingField_0)); }
	inline String_t* get_U3CidU3Ek__BackingField_0() const { return ___U3CidU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CidU3Ek__BackingField_0() { return &___U3CidU3Ek__BackingField_0; }
	inline void set_U3CidU3Ek__BackingField_0(String_t* value)
	{
		___U3CidU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CidU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CstoreSpecificIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductDefinition_t339727138, ___U3CstoreSpecificIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CstoreSpecificIdU3Ek__BackingField_1() const { return ___U3CstoreSpecificIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CstoreSpecificIdU3Ek__BackingField_1() { return &___U3CstoreSpecificIdU3Ek__BackingField_1; }
	inline void set_U3CstoreSpecificIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CstoreSpecificIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreSpecificIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ProductDefinition_t339727138, ___U3CtypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CtypeU3Ek__BackingField_2() const { return ___U3CtypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CtypeU3Ek__BackingField_2() { return &___U3CtypeU3Ek__BackingField_2; }
	inline void set_U3CtypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CtypeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CenabledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ProductDefinition_t339727138, ___U3CenabledU3Ek__BackingField_3)); }
	inline bool get_U3CenabledU3Ek__BackingField_3() const { return ___U3CenabledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CenabledU3Ek__BackingField_3() { return &___U3CenabledU3Ek__BackingField_3; }
	inline void set_U3CenabledU3Ek__BackingField_3(bool value)
	{
		___U3CenabledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_m_Payouts_4() { return static_cast<int32_t>(offsetof(ProductDefinition_t339727138, ___m_Payouts_4)); }
	inline List_1_t748791510 * get_m_Payouts_4() const { return ___m_Payouts_4; }
	inline List_1_t748791510 ** get_address_of_m_Payouts_4() { return &___m_Payouts_4; }
	inline void set_m_Payouts_4(List_1_t748791510 * value)
	{
		___m_Payouts_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Payouts_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTDEFINITION_T339727138_H
#ifndef STANDARDPURCHASINGMODULE_T2580735509_H
#define STANDARDPURCHASINGMODULE_T2580735509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.StandardPurchasingModule
struct  StandardPurchasingModule_t2580735509  : public AbstractPurchasingModule_t2882497868
{
public:
	// UnityEngine.Purchasing.AppStore UnityEngine.Purchasing.StandardPurchasingModule::m_AppStorePlatform
	int32_t ___m_AppStorePlatform_1;
	// UnityEngine.Purchasing.INativeStoreProvider UnityEngine.Purchasing.StandardPurchasingModule::m_NativeStoreProvider
	RuntimeObject* ___m_NativeStoreProvider_2;
	// UnityEngine.RuntimePlatform UnityEngine.Purchasing.StandardPurchasingModule::m_RuntimePlatform
	int32_t ___m_RuntimePlatform_3;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::m_UseCloudCatalog
	bool ___m_UseCloudCatalog_4;
	// Uniject.IUtil UnityEngine.Purchasing.StandardPurchasingModule::<util>k__BackingField
	RuntimeObject* ___U3CutilU3Ek__BackingField_6;
	// UnityEngine.ILogger UnityEngine.Purchasing.StandardPurchasingModule::<logger>k__BackingField
	RuntimeObject* ___U3CloggerU3Ek__BackingField_7;
	// UnityEngine.Purchasing.IAsyncWebUtil UnityEngine.Purchasing.StandardPurchasingModule::<webUtil>k__BackingField
	RuntimeObject* ___U3CwebUtilU3Ek__BackingField_8;
	// UnityEngine.Purchasing.StandardPurchasingModule/StoreInstance UnityEngine.Purchasing.StandardPurchasingModule::<storeInstance>k__BackingField
	StoreInstance_t2416643455 * ___U3CstoreInstanceU3Ek__BackingField_9;
	// UnityEngine.Purchasing.CloudCatalogImpl UnityEngine.Purchasing.StandardPurchasingModule::m_CloudCatalog
	CloudCatalogImpl_t1580312503 * ___m_CloudCatalog_11;
	// UnityEngine.Purchasing.FakeStoreUIMode UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreUIMode>k__BackingField
	int32_t ___U3CuseFakeStoreUIModeU3Ek__BackingField_12;
	// System.Boolean UnityEngine.Purchasing.StandardPurchasingModule::<useFakeStoreAlways>k__BackingField
	bool ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13;
	// UnityEngine.Purchasing.WinRTStore UnityEngine.Purchasing.StandardPurchasingModule::windowsStore
	WinRTStore_t2015085940 * ___windowsStore_14;

public:
	inline static int32_t get_offset_of_m_AppStorePlatform_1() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_AppStorePlatform_1)); }
	inline int32_t get_m_AppStorePlatform_1() const { return ___m_AppStorePlatform_1; }
	inline int32_t* get_address_of_m_AppStorePlatform_1() { return &___m_AppStorePlatform_1; }
	inline void set_m_AppStorePlatform_1(int32_t value)
	{
		___m_AppStorePlatform_1 = value;
	}

	inline static int32_t get_offset_of_m_NativeStoreProvider_2() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_NativeStoreProvider_2)); }
	inline RuntimeObject* get_m_NativeStoreProvider_2() const { return ___m_NativeStoreProvider_2; }
	inline RuntimeObject** get_address_of_m_NativeStoreProvider_2() { return &___m_NativeStoreProvider_2; }
	inline void set_m_NativeStoreProvider_2(RuntimeObject* value)
	{
		___m_NativeStoreProvider_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_NativeStoreProvider_2), value);
	}

	inline static int32_t get_offset_of_m_RuntimePlatform_3() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_RuntimePlatform_3)); }
	inline int32_t get_m_RuntimePlatform_3() const { return ___m_RuntimePlatform_3; }
	inline int32_t* get_address_of_m_RuntimePlatform_3() { return &___m_RuntimePlatform_3; }
	inline void set_m_RuntimePlatform_3(int32_t value)
	{
		___m_RuntimePlatform_3 = value;
	}

	inline static int32_t get_offset_of_m_UseCloudCatalog_4() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_UseCloudCatalog_4)); }
	inline bool get_m_UseCloudCatalog_4() const { return ___m_UseCloudCatalog_4; }
	inline bool* get_address_of_m_UseCloudCatalog_4() { return &___m_UseCloudCatalog_4; }
	inline void set_m_UseCloudCatalog_4(bool value)
	{
		___m_UseCloudCatalog_4 = value;
	}

	inline static int32_t get_offset_of_U3CutilU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CutilU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CutilU3Ek__BackingField_6() const { return ___U3CutilU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CutilU3Ek__BackingField_6() { return &___U3CutilU3Ek__BackingField_6; }
	inline void set_U3CutilU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CutilU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CutilU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CloggerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CloggerU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CloggerU3Ek__BackingField_7() const { return ___U3CloggerU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CloggerU3Ek__BackingField_7() { return &___U3CloggerU3Ek__BackingField_7; }
	inline void set_U3CloggerU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CloggerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CloggerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CwebUtilU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CwebUtilU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CwebUtilU3Ek__BackingField_8() const { return ___U3CwebUtilU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CwebUtilU3Ek__BackingField_8() { return &___U3CwebUtilU3Ek__BackingField_8; }
	inline void set_U3CwebUtilU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CwebUtilU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwebUtilU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CstoreInstanceU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CstoreInstanceU3Ek__BackingField_9)); }
	inline StoreInstance_t2416643455 * get_U3CstoreInstanceU3Ek__BackingField_9() const { return ___U3CstoreInstanceU3Ek__BackingField_9; }
	inline StoreInstance_t2416643455 ** get_address_of_U3CstoreInstanceU3Ek__BackingField_9() { return &___U3CstoreInstanceU3Ek__BackingField_9; }
	inline void set_U3CstoreInstanceU3Ek__BackingField_9(StoreInstance_t2416643455 * value)
	{
		___U3CstoreInstanceU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstoreInstanceU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_m_CloudCatalog_11() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___m_CloudCatalog_11)); }
	inline CloudCatalogImpl_t1580312503 * get_m_CloudCatalog_11() const { return ___m_CloudCatalog_11; }
	inline CloudCatalogImpl_t1580312503 ** get_address_of_m_CloudCatalog_11() { return &___m_CloudCatalog_11; }
	inline void set_m_CloudCatalog_11(CloudCatalogImpl_t1580312503 * value)
	{
		___m_CloudCatalog_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CloudCatalog_11), value);
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CuseFakeStoreUIModeU3Ek__BackingField_12)); }
	inline int32_t get_U3CuseFakeStoreUIModeU3Ek__BackingField_12() const { return ___U3CuseFakeStoreUIModeU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CuseFakeStoreUIModeU3Ek__BackingField_12() { return &___U3CuseFakeStoreUIModeU3Ek__BackingField_12; }
	inline void set_U3CuseFakeStoreUIModeU3Ek__BackingField_12(int32_t value)
	{
		___U3CuseFakeStoreUIModeU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13)); }
	inline bool get_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() const { return ___U3CuseFakeStoreAlwaysU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CuseFakeStoreAlwaysU3Ek__BackingField_13() { return &___U3CuseFakeStoreAlwaysU3Ek__BackingField_13; }
	inline void set_U3CuseFakeStoreAlwaysU3Ek__BackingField_13(bool value)
	{
		___U3CuseFakeStoreAlwaysU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_windowsStore_14() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509, ___windowsStore_14)); }
	inline WinRTStore_t2015085940 * get_windowsStore_14() const { return ___windowsStore_14; }
	inline WinRTStore_t2015085940 ** get_address_of_windowsStore_14() { return &___windowsStore_14; }
	inline void set_windowsStore_14(WinRTStore_t2015085940 * value)
	{
		___windowsStore_14 = value;
		Il2CppCodeGenWriteBarrier((&___windowsStore_14), value);
	}
};

struct StandardPurchasingModule_t2580735509_StaticFields
{
public:
	// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::ModuleInstance
	StandardPurchasingModule_t2580735509 * ___ModuleInstance_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Purchasing.AppStore,System.String> UnityEngine.Purchasing.StandardPurchasingModule::AndroidStoreNameMap
	Dictionary_2_t2070795836 * ___AndroidStoreNameMap_10;

public:
	inline static int32_t get_offset_of_ModuleInstance_5() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509_StaticFields, ___ModuleInstance_5)); }
	inline StandardPurchasingModule_t2580735509 * get_ModuleInstance_5() const { return ___ModuleInstance_5; }
	inline StandardPurchasingModule_t2580735509 ** get_address_of_ModuleInstance_5() { return &___ModuleInstance_5; }
	inline void set_ModuleInstance_5(StandardPurchasingModule_t2580735509 * value)
	{
		___ModuleInstance_5 = value;
		Il2CppCodeGenWriteBarrier((&___ModuleInstance_5), value);
	}

	inline static int32_t get_offset_of_AndroidStoreNameMap_10() { return static_cast<int32_t>(offsetof(StandardPurchasingModule_t2580735509_StaticFields, ___AndroidStoreNameMap_10)); }
	inline Dictionary_2_t2070795836 * get_AndroidStoreNameMap_10() const { return ___AndroidStoreNameMap_10; }
	inline Dictionary_2_t2070795836 ** get_address_of_AndroidStoreNameMap_10() { return &___AndroidStoreNameMap_10; }
	inline void set_AndroidStoreNameMap_10(Dictionary_2_t2070795836 * value)
	{
		___AndroidStoreNameMap_10 = value;
		Il2CppCodeGenWriteBarrier((&___AndroidStoreNameMap_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STANDARDPURCHASINGMODULE_T2580735509_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef AF_SAMPLE_BGSCRIPT_T4024703127_H
#define AF_SAMPLE_BGSCRIPT_T4024703127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AF_Sample_BGScript
struct  AF_Sample_BGScript_t4024703127  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject AF_Sample_BGScript::Button
	GameObject_t1113636619 * ___Button_4;
	// UnityEngine.GameObject AF_Sample_BGScript::ButtonText
	GameObject_t1113636619 * ___ButtonText_5;
	// UnityEngine.GameObject AF_Sample_BGScript::Check
	GameObject_t1113636619 * ___Check_6;
	// UnityEngine.GameObject AF_Sample_BGScript::text
	GameObject_t1113636619 * ___text_7;

public:
	inline static int32_t get_offset_of_Button_4() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t4024703127, ___Button_4)); }
	inline GameObject_t1113636619 * get_Button_4() const { return ___Button_4; }
	inline GameObject_t1113636619 ** get_address_of_Button_4() { return &___Button_4; }
	inline void set_Button_4(GameObject_t1113636619 * value)
	{
		___Button_4 = value;
		Il2CppCodeGenWriteBarrier((&___Button_4), value);
	}

	inline static int32_t get_offset_of_ButtonText_5() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t4024703127, ___ButtonText_5)); }
	inline GameObject_t1113636619 * get_ButtonText_5() const { return ___ButtonText_5; }
	inline GameObject_t1113636619 ** get_address_of_ButtonText_5() { return &___ButtonText_5; }
	inline void set_ButtonText_5(GameObject_t1113636619 * value)
	{
		___ButtonText_5 = value;
		Il2CppCodeGenWriteBarrier((&___ButtonText_5), value);
	}

	inline static int32_t get_offset_of_Check_6() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t4024703127, ___Check_6)); }
	inline GameObject_t1113636619 * get_Check_6() const { return ___Check_6; }
	inline GameObject_t1113636619 ** get_address_of_Check_6() { return &___Check_6; }
	inline void set_Check_6(GameObject_t1113636619 * value)
	{
		___Check_6 = value;
		Il2CppCodeGenWriteBarrier((&___Check_6), value);
	}

	inline static int32_t get_offset_of_text_7() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t4024703127, ___text_7)); }
	inline GameObject_t1113636619 * get_text_7() const { return ___text_7; }
	inline GameObject_t1113636619 ** get_address_of_text_7() { return &___text_7; }
	inline void set_text_7(GameObject_t1113636619 * value)
	{
		___text_7 = value;
		Il2CppCodeGenWriteBarrier((&___text_7), value);
	}
};

struct AF_Sample_BGScript_t4024703127_StaticFields
{
public:
	// System.Boolean AF_Sample_BGScript::isPressed
	bool ___isPressed_8;

public:
	inline static int32_t get_offset_of_isPressed_8() { return static_cast<int32_t>(offsetof(AF_Sample_BGScript_t4024703127_StaticFields, ___isPressed_8)); }
	inline bool get_isPressed_8() const { return ___isPressed_8; }
	inline bool* get_address_of_isPressed_8() { return &___isPressed_8; }
	inline void set_isPressed_8(bool value)
	{
		___isPressed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AF_SAMPLE_BGSCRIPT_T4024703127_H
#ifndef AFSCRIPTNEW_T3996606863_H
#define AFSCRIPTNEW_T3996606863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AfScriptNew
struct  AfScriptNew_t3996606863  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct AfScriptNew_t3996606863_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController AfScriptNew::m_StoreController
	RuntimeObject* ___m_StoreController_4;
	// UnityEngine.Purchasing.IExtensionProvider AfScriptNew::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_5;
	// System.String AfScriptNew::kProductIDConsumable
	String_t* ___kProductIDConsumable_6;

public:
	inline static int32_t get_offset_of_m_StoreController_4() { return static_cast<int32_t>(offsetof(AfScriptNew_t3996606863_StaticFields, ___m_StoreController_4)); }
	inline RuntimeObject* get_m_StoreController_4() const { return ___m_StoreController_4; }
	inline RuntimeObject** get_address_of_m_StoreController_4() { return &___m_StoreController_4; }
	inline void set_m_StoreController_4(RuntimeObject* value)
	{
		___m_StoreController_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreController_4), value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_5() { return static_cast<int32_t>(offsetof(AfScriptNew_t3996606863_StaticFields, ___m_StoreExtensionProvider_5)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_5() const { return ___m_StoreExtensionProvider_5; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_5() { return &___m_StoreExtensionProvider_5; }
	inline void set_m_StoreExtensionProvider_5(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreExtensionProvider_5), value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_6() { return static_cast<int32_t>(offsetof(AfScriptNew_t3996606863_StaticFields, ___kProductIDConsumable_6)); }
	inline String_t* get_kProductIDConsumable_6() const { return ___kProductIDConsumable_6; }
	inline String_t** get_address_of_kProductIDConsumable_6() { return &___kProductIDConsumable_6; }
	inline void set_kProductIDConsumable_6(String_t* value)
	{
		___kProductIDConsumable_6 = value;
		Il2CppCodeGenWriteBarrier((&___kProductIDConsumable_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFSCRIPTNEW_T3996606863_H
#ifndef STARTUP_T2202782244_H
#define STARTUP_T2202782244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StartUp
struct  StartUp_t2202782244  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject StartUp::text
	GameObject_t1113636619 * ___text_4;
	// System.Boolean StartUp::tokenSent
	bool ___tokenSent_5;

public:
	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(StartUp_t2202782244, ___text_4)); }
	inline GameObject_t1113636619 * get_text_4() const { return ___text_4; }
	inline GameObject_t1113636619 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(GameObject_t1113636619 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier((&___text_4), value);
	}

	inline static int32_t get_offset_of_tokenSent_5() { return static_cast<int32_t>(offsetof(StartUp_t2202782244, ___tokenSent_5)); }
	inline bool get_tokenSent_5() const { return ___tokenSent_5; }
	inline bool* get_address_of_tokenSent_5() { return &___tokenSent_5; }
	inline void set_tokenSent_5(bool value)
	{
		___tokenSent_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STARTUP_T2202782244_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef AFOBJECTSCRIPT_T980149766_H
#define AFOBJECTSCRIPT_T980149766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// afobjectscript
struct  afobjectscript_t980149766  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct afobjectscript_t980149766_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController afobjectscript::m_StoreController
	RuntimeObject* ___m_StoreController_4;
	// UnityEngine.Purchasing.IExtensionProvider afobjectscript::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_5;
	// System.String afobjectscript::kProductIDConsumable
	String_t* ___kProductIDConsumable_6;

public:
	inline static int32_t get_offset_of_m_StoreController_4() { return static_cast<int32_t>(offsetof(afobjectscript_t980149766_StaticFields, ___m_StoreController_4)); }
	inline RuntimeObject* get_m_StoreController_4() const { return ___m_StoreController_4; }
	inline RuntimeObject** get_address_of_m_StoreController_4() { return &___m_StoreController_4; }
	inline void set_m_StoreController_4(RuntimeObject* value)
	{
		___m_StoreController_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreController_4), value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_5() { return static_cast<int32_t>(offsetof(afobjectscript_t980149766_StaticFields, ___m_StoreExtensionProvider_5)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_5() const { return ___m_StoreExtensionProvider_5; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_5() { return &___m_StoreExtensionProvider_5; }
	inline void set_m_StoreExtensionProvider_5(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreExtensionProvider_5), value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_6() { return static_cast<int32_t>(offsetof(afobjectscript_t980149766_StaticFields, ___kProductIDConsumable_6)); }
	inline String_t* get_kProductIDConsumable_6() const { return ___kProductIDConsumable_6; }
	inline String_t** get_address_of_kProductIDConsumable_6() { return &___kProductIDConsumable_6; }
	inline void set_kProductIDConsumable_6(String_t* value)
	{
		___kProductIDConsumable_6 = value;
		Il2CppCodeGenWriteBarrier((&___kProductIDConsumable_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AFOBJECTSCRIPT_T980149766_H
#ifndef GRAPHIC_T1660335611_H
#define GRAPHIC_T1660335611_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t1660335611  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t340375123 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2555686324  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_8;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3704657025 * ___m_RectTransform_9;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_t2598313366 * ___m_CanvasRenderer_10;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_11;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_12;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t3245792599 * ___m_OnDirtyLayoutCallback_14;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t3245792599 * ___m_OnDirtyVertsCallback_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t3245792599 * ___m_OnDirtyMaterialCallback_16;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3055525458 * ___m_ColorTweenRunner_19;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Material_6)); }
	inline Material_t340375123 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t340375123 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t340375123 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_6), value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Color_7)); }
	inline Color_t2555686324  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t2555686324 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t2555686324  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_8() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RaycastTarget_8)); }
	inline bool get_m_RaycastTarget_8() const { return ___m_RaycastTarget_8; }
	inline bool* get_address_of_m_RaycastTarget_8() { return &___m_RaycastTarget_8; }
	inline void set_m_RaycastTarget_8(bool value)
	{
		___m_RaycastTarget_8 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_9() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_RectTransform_9)); }
	inline RectTransform_t3704657025 * get_m_RectTransform_9() const { return ___m_RectTransform_9; }
	inline RectTransform_t3704657025 ** get_address_of_m_RectTransform_9() { return &___m_RectTransform_9; }
	inline void set_m_RectTransform_9(RectTransform_t3704657025 * value)
	{
		___m_RectTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_9), value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_10() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_CanvasRenderer_10)); }
	inline CanvasRenderer_t2598313366 * get_m_CanvasRenderer_10() const { return ___m_CanvasRenderer_10; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CanvasRenderer_10() { return &___m_CanvasRenderer_10; }
	inline void set_m_CanvasRenderer_10(CanvasRenderer_t2598313366 * value)
	{
		___m_CanvasRenderer_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRenderer_10), value);
	}

	inline static int32_t get_offset_of_m_Canvas_11() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_Canvas_11)); }
	inline Canvas_t3310196443 * get_m_Canvas_11() const { return ___m_Canvas_11; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_11() { return &___m_Canvas_11; }
	inline void set_m_Canvas_11(Canvas_t3310196443 * value)
	{
		___m_Canvas_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_11), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_12() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_VertsDirty_12)); }
	inline bool get_m_VertsDirty_12() const { return ___m_VertsDirty_12; }
	inline bool* get_address_of_m_VertsDirty_12() { return &___m_VertsDirty_12; }
	inline void set_m_VertsDirty_12(bool value)
	{
		___m_VertsDirty_12 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_13() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_MaterialDirty_13)); }
	inline bool get_m_MaterialDirty_13() const { return ___m_MaterialDirty_13; }
	inline bool* get_address_of_m_MaterialDirty_13() { return &___m_MaterialDirty_13; }
	inline void set_m_MaterialDirty_13(bool value)
	{
		___m_MaterialDirty_13 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyLayoutCallback_14)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyLayoutCallback_14() const { return ___m_OnDirtyLayoutCallback_14; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyLayoutCallback_14() { return &___m_OnDirtyLayoutCallback_14; }
	inline void set_m_OnDirtyLayoutCallback_14(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyLayoutCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_14), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_15() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyVertsCallback_15)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyVertsCallback_15() const { return ___m_OnDirtyVertsCallback_15; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyVertsCallback_15() { return &___m_OnDirtyVertsCallback_15; }
	inline void set_m_OnDirtyVertsCallback_15(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyVertsCallback_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_15), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_16() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_OnDirtyMaterialCallback_16)); }
	inline UnityAction_t3245792599 * get_m_OnDirtyMaterialCallback_16() const { return ___m_OnDirtyMaterialCallback_16; }
	inline UnityAction_t3245792599 ** get_address_of_m_OnDirtyMaterialCallback_16() { return &___m_OnDirtyMaterialCallback_16; }
	inline void set_m_OnDirtyMaterialCallback_16(UnityAction_t3245792599 * value)
	{
		___m_OnDirtyMaterialCallback_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_16), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_19() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___m_ColorTweenRunner_19)); }
	inline TweenRunner_1_t3055525458 * get_m_ColorTweenRunner_19() const { return ___m_ColorTweenRunner_19; }
	inline TweenRunner_1_t3055525458 ** get_address_of_m_ColorTweenRunner_19() { return &___m_ColorTweenRunner_19; }
	inline void set_m_ColorTweenRunner_19(TweenRunner_1_t3055525458 * value)
	{
		___m_ColorTweenRunner_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_19), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(Graphic_t1660335611, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_20() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_20; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_20(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_20 = value;
	}
};

struct Graphic_t1660335611_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t340375123 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3648964284 * ___s_Mesh_17;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t2453304189 * ___s_VertexHelper_18;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t340375123 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t340375123 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t340375123 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_4), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_5), value);
	}

	inline static int32_t get_offset_of_s_Mesh_17() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_Mesh_17)); }
	inline Mesh_t3648964284 * get_s_Mesh_17() const { return ___s_Mesh_17; }
	inline Mesh_t3648964284 ** get_address_of_s_Mesh_17() { return &___s_Mesh_17; }
	inline void set_s_Mesh_17(Mesh_t3648964284 * value)
	{
		___s_Mesh_17 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_17), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_18() { return static_cast<int32_t>(offsetof(Graphic_t1660335611_StaticFields, ___s_VertexHelper_18)); }
	inline VertexHelper_t2453304189 * get_s_VertexHelper_18() const { return ___s_VertexHelper_18; }
	inline VertexHelper_t2453304189 ** get_address_of_s_VertexHelper_18() { return &___s_VertexHelper_18; }
	inline void set_s_VertexHelper_18(VertexHelper_t2453304189 * value)
	{
		___s_VertexHelper_18 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T1660335611_H
#ifndef MASKABLEGRAPHIC_T3839221559_H
#define MASKABLEGRAPHIC_T3839221559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t3839221559  : public Graphic_t1660335611
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_21;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t340375123 * ___m_MaskMaterial_22;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t3474889437 * ___m_ParentMask_23;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_25;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3661388177 * ___m_OnCullStateChanged_26;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_27;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_28;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1718750761* ___m_Corners_29;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculateStencil_21)); }
	inline bool get_m_ShouldRecalculateStencil_21() const { return ___m_ShouldRecalculateStencil_21; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_21() { return &___m_ShouldRecalculateStencil_21; }
	inline void set_m_ShouldRecalculateStencil_21(bool value)
	{
		___m_ShouldRecalculateStencil_21 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_MaskMaterial_22)); }
	inline Material_t340375123 * get_m_MaskMaterial_22() const { return ___m_MaskMaterial_22; }
	inline Material_t340375123 ** get_address_of_m_MaskMaterial_22() { return &___m_MaskMaterial_22; }
	inline void set_m_MaskMaterial_22(Material_t340375123 * value)
	{
		___m_MaskMaterial_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_22), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ParentMask_23)); }
	inline RectMask2D_t3474889437 * get_m_ParentMask_23() const { return ___m_ParentMask_23; }
	inline RectMask2D_t3474889437 ** get_address_of_m_ParentMask_23() { return &___m_ParentMask_23; }
	inline void set_m_ParentMask_23(RectMask2D_t3474889437 * value)
	{
		___m_ParentMask_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_23), value);
	}

	inline static int32_t get_offset_of_m_Maskable_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Maskable_24)); }
	inline bool get_m_Maskable_24() const { return ___m_Maskable_24; }
	inline bool* get_address_of_m_Maskable_24() { return &___m_Maskable_24; }
	inline void set_m_Maskable_24(bool value)
	{
		___m_Maskable_24 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_IncludeForMasking_25)); }
	inline bool get_m_IncludeForMasking_25() const { return ___m_IncludeForMasking_25; }
	inline bool* get_address_of_m_IncludeForMasking_25() { return &___m_IncludeForMasking_25; }
	inline void set_m_IncludeForMasking_25(bool value)
	{
		___m_IncludeForMasking_25 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_OnCullStateChanged_26)); }
	inline CullStateChangedEvent_t3661388177 * get_m_OnCullStateChanged_26() const { return ___m_OnCullStateChanged_26; }
	inline CullStateChangedEvent_t3661388177 ** get_address_of_m_OnCullStateChanged_26() { return &___m_OnCullStateChanged_26; }
	inline void set_m_OnCullStateChanged_26(CullStateChangedEvent_t3661388177 * value)
	{
		___m_OnCullStateChanged_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_26), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_ShouldRecalculate_27)); }
	inline bool get_m_ShouldRecalculate_27() const { return ___m_ShouldRecalculate_27; }
	inline bool* get_address_of_m_ShouldRecalculate_27() { return &___m_ShouldRecalculate_27; }
	inline void set_m_ShouldRecalculate_27(bool value)
	{
		___m_ShouldRecalculate_27 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_StencilValue_28)); }
	inline int32_t get_m_StencilValue_28() const { return ___m_StencilValue_28; }
	inline int32_t* get_address_of_m_StencilValue_28() { return &___m_StencilValue_28; }
	inline void set_m_StencilValue_28(int32_t value)
	{
		___m_StencilValue_28 = value;
	}

	inline static int32_t get_offset_of_m_Corners_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t3839221559, ___m_Corners_29)); }
	inline Vector3U5BU5D_t1718750761* get_m_Corners_29() const { return ___m_Corners_29; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_Corners_29() { return &___m_Corners_29; }
	inline void set_m_Corners_29(Vector3U5BU5D_t1718750761* value)
	{
		___m_Corners_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T3839221559_H
#ifndef TEXT_T1901882714_H
#define TEXT_T1901882714_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Text
struct  Text_t1901882714  : public MaskableGraphic_t3839221559
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t746620069 * ___m_FontData_30;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_31;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t3211863866 * ___m_TextCache_32;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t3211863866 * ___m_TextCacheForLayout_33;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_35;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_t1981460040* ___m_TempVerts_36;

public:
	inline static int32_t get_offset_of_m_FontData_30() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_FontData_30)); }
	inline FontData_t746620069 * get_m_FontData_30() const { return ___m_FontData_30; }
	inline FontData_t746620069 ** get_address_of_m_FontData_30() { return &___m_FontData_30; }
	inline void set_m_FontData_30(FontData_t746620069 * value)
	{
		___m_FontData_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontData_30), value);
	}

	inline static int32_t get_offset_of_m_Text_31() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_Text_31)); }
	inline String_t* get_m_Text_31() const { return ___m_Text_31; }
	inline String_t** get_address_of_m_Text_31() { return &___m_Text_31; }
	inline void set_m_Text_31(String_t* value)
	{
		___m_Text_31 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_31), value);
	}

	inline static int32_t get_offset_of_m_TextCache_32() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCache_32)); }
	inline TextGenerator_t3211863866 * get_m_TextCache_32() const { return ___m_TextCache_32; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCache_32() { return &___m_TextCache_32; }
	inline void set_m_TextCache_32(TextGenerator_t3211863866 * value)
	{
		___m_TextCache_32 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCache_32), value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_33() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TextCacheForLayout_33)); }
	inline TextGenerator_t3211863866 * get_m_TextCacheForLayout_33() const { return ___m_TextCacheForLayout_33; }
	inline TextGenerator_t3211863866 ** get_address_of_m_TextCacheForLayout_33() { return &___m_TextCacheForLayout_33; }
	inline void set_m_TextCacheForLayout_33(TextGenerator_t3211863866 * value)
	{
		___m_TextCacheForLayout_33 = value;
		Il2CppCodeGenWriteBarrier((&___m_TextCacheForLayout_33), value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_35() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_DisableFontTextureRebuiltCallback_35)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_35() const { return ___m_DisableFontTextureRebuiltCallback_35; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_35() { return &___m_DisableFontTextureRebuiltCallback_35; }
	inline void set_m_DisableFontTextureRebuiltCallback_35(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_35 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_36() { return static_cast<int32_t>(offsetof(Text_t1901882714, ___m_TempVerts_36)); }
	inline UIVertexU5BU5D_t1981460040* get_m_TempVerts_36() const { return ___m_TempVerts_36; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_TempVerts_36() { return &___m_TempVerts_36; }
	inline void set_m_TempVerts_36(UIVertexU5BU5D_t1981460040* value)
	{
		___m_TempVerts_36 = value;
		Il2CppCodeGenWriteBarrier((&___m_TempVerts_36), value);
	}
};

struct Text_t1901882714_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t340375123 * ___s_DefaultText_34;

public:
	inline static int32_t get_offset_of_s_DefaultText_34() { return static_cast<int32_t>(offsetof(Text_t1901882714_StaticFields, ___s_DefaultText_34)); }
	inline Material_t340375123 * get_s_DefaultText_34() const { return ___s_DefaultText_34; }
	inline Material_t340375123 ** get_address_of_s_DefaultText_34() { return &___s_DefaultText_34; }
	inline void set_s_DefaultText_34(Material_t340375123 * value)
	{
		___s_DefaultText_34 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultText_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXT_T1901882714_H
// UnityEngine.Purchasing.Extension.IPurchasingModule[]
struct IPurchasingModuleU5BU5D_t3784316456  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(!0)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Dictionary_2_get_Item_m2714930061_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m2387223709_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method);
// System.Collections.IEnumerator AF_Sample_BGScript::PressAnim()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* AF_Sample_BGScript_PressAnim_m1380001828 (AF_Sample_BGScript_t4024703127 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CPressAnimU3Ec__Iterator0__ctor_m1605106169 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_t1901882714 * GameObject_GetComponent_TisText_t1901882714_m565625081 (GameObject_t1113636619 * __this, const RuntimeMethod* method)
{
	return ((  Text_t1901882714 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2049753423_gshared)(__this, method);
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m2199082655 (WaitForSeconds_t1699091251 * __this, float p0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void AfScriptNew::appsFlyerInit()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_appsFlyerInit_m713040632 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method);
// System.Void AfScriptNew::InitializePurchasing()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_InitializePurchasing_m1487909055 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method);
// System.Boolean AfScriptNew::IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool AfScriptNew_IsInitialized_m2350561814 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method);
// UnityEngine.Purchasing.StandardPurchasingModule UnityEngine.Purchasing.StandardPurchasingModule::Instance()
extern "C" IL2CPP_METHOD_ATTR StandardPurchasingModule_t2580735509 * StandardPurchasingModule_Instance_m2991111855 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Purchasing.ConfigurationBuilder UnityEngine.Purchasing.ConfigurationBuilder::Instance(UnityEngine.Purchasing.Extension.IPurchasingModule,UnityEngine.Purchasing.Extension.IPurchasingModule[])
extern "C" IL2CPP_METHOD_ATTR ConfigurationBuilder_t1618671084 * ConfigurationBuilder_Instance_m2204111312 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, IPurchasingModuleU5BU5D_t3784316456* p1, const RuntimeMethod* method);
// UnityEngine.Purchasing.ConfigurationBuilder UnityEngine.Purchasing.ConfigurationBuilder::AddProduct(System.String,UnityEngine.Purchasing.ProductType)
extern "C" IL2CPP_METHOD_ATTR ConfigurationBuilder_t1618671084 * ConfigurationBuilder_AddProduct_m788979654 (ConfigurationBuilder_t1618671084 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method);
// System.Void UnityEngine.Purchasing.UnityPurchasing::Initialize(UnityEngine.Purchasing.IStoreListener,UnityEngine.Purchasing.ConfigurationBuilder)
extern "C" IL2CPP_METHOD_ATTR void UnityPurchasing_Initialize_m1836262307 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, ConfigurationBuilder_t1618671084 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour_print_m330341231 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void AfScriptNew::BuyProductID(System.String)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_BuyProductID_m2762636159 (AfScriptNew_t3996606863 * __this, String_t* ___productId0, const RuntimeMethod* method);
// UnityEngine.Purchasing.Product UnityEngine.Purchasing.ProductCollection::WithID(System.String)
extern "C" IL2CPP_METHOD_ATTR Product_t3244410059 * ProductCollection_WithID_m2597694943 (ProductCollection_t2671956229 * __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Purchasing.Product::get_availableToPurchase()
extern "C" IL2CPP_METHOD_ATTR bool Product_get_availableToPurchase_m3282912434 (Product_t3244410059 * __this, const RuntimeMethod* method);
// UnityEngine.Purchasing.ProductDefinition UnityEngine.Purchasing.Product::get_definition()
extern "C" IL2CPP_METHOD_ATTR ProductDefinition_t339727138 * Product_get_definition_m3366103520 (Product_t3244410059 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Purchasing.ProductDefinition::get_id()
extern "C" IL2CPP_METHOD_ATTR String_t* ProductDefinition_get_id_m1593385231 (ProductDefinition_t339727138 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C" IL2CPP_METHOD_ATTR void Debug_Log_m4051431634 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void AfScriptNew::BuyConsumable()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_BuyConsumable_m148322322 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// UnityEngine.Purchasing.Product UnityEngine.Purchasing.PurchaseEventArgs::get_purchasedProduct()
extern "C" IL2CPP_METHOD_ATTR Product_t3244410059 * PurchaseEventArgs_get_purchasedProduct_m3472521060 (PurchaseEventArgs_t3033159582 * __this, const RuntimeMethod* method);
// UnityEngine.Purchasing.ProductMetadata UnityEngine.Purchasing.Product::get_metadata()
extern "C" IL2CPP_METHOD_ATTR ProductMetadata_t3417118930 * Product_get_metadata_m3202120155 (Product_t3244410059 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Purchasing.ProductMetadata::get_localizedPriceString()
extern "C" IL2CPP_METHOD_ATTR String_t* ProductMetadata_get_localizedPriceString_m3339862584 (ProductMetadata_t3417118930 * __this, const RuntimeMethod* method);
// System.String System.Text.RegularExpressions.Regex::Replace(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* Regex_Replace_m2667570911 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.String UnityEngine.Purchasing.ProductMetadata::get_isoCurrencyCode()
extern "C" IL2CPP_METHOD_ATTR String_t* ProductMetadata_get_isoCurrencyCode_m1144927692 (ProductMetadata_t3417118930 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::.ctor()
inline void Dictionary_2__ctor_m3302800229 (Dictionary_2_t1632706988 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1632706988 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.String UnityEngine.Purchasing.Product::get_receipt()
extern "C" IL2CPP_METHOD_ATTR String_t* Product_get_receipt_m117487645 (Product_t3244410059 * __this, const RuntimeMethod* method);
// System.Object UnityEngine.Purchasing.MiniJson::JsonDecode(System.String)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * MiniJson_JsonDecode_m1204021123 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0)
inline RuntimeObject * Dictionary_2_get_Item_m1539906286 (Dictionary_2_t2865362463 * __this, String_t* p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Dictionary_2_t2865362463 *, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m2714930061_gshared)(__this, p0, method);
}
// System.Boolean System.String::Equals(System.String,System.String,System.StringComparison)
extern "C" IL2CPP_METHOD_ATTR bool String_Equals_m2359609904 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, int32_t p2, const RuntimeMethod* method);
// System.Void AfScriptNew::afiosValidate(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_afiosValidate_m178811455 (AfScriptNew_t3996606863 * __this, String_t* ___prodID0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionID3, Dictionary_2_t1632706988 * ___extraParams4, const RuntimeMethod* method);
// System.String System.String::Concat(System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m1809518182 (RuntimeObject * __this /* static, unused */, StringU5BU5D_t1281789340* p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setIsSandbox(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setIsSandbox_m2398892449 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Void AppsFlyer::validateReceipt(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_validateReceipt_m1909103642 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, Dictionary_2_t1632706988 * p4, const RuntimeMethod* method);
// System.String UnityEngine.Purchasing.ProductDefinition::get_storeSpecificId()
extern "C" IL2CPP_METHOD_ATTR String_t* ProductDefinition_get_storeSpecificId_m2520532185 (ProductDefinition_t339727138 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void AppsFlyer::setAppsFlyerKey(System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setAppsFlyerKey_m2232093163 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setIsDebug(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setIsDebug_m775783760 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setAppID(System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setAppID_m125861847 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void AppsFlyer::getConversionData()
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_getConversionData_m3717632094 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void AppsFlyer::trackAppLaunch()
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_trackAppLaunch_m1307190503 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Application_set_runInBackground_m2169704730 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C" IL2CPP_METHOD_ATTR void Screen_set_orientation_m3561207030 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR void Object_DontDestroyOnLoad_m166252750 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setHost(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setHost_m812428899 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void AppsFlyer::setMinTimeBetweenSessions(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setMinTimeBetweenSessions_m1747056130 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1)
inline void Dictionary_2_Add_m3097518446 (Dictionary_2_t1632706988 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t1632706988 *, String_t*, String_t*, const RuntimeMethod*))Dictionary_2_Add_m2387223709_gshared)(__this, p0, p1, method);
}
// System.Void AppsFlyer::setAdditionalData(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setAdditionalData_m1491833587 (RuntimeObject * __this /* static, unused */, Dictionary_2_t1632706988 * p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setCustomerUserID(System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setCustomerUserID_m3283082404 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.String AppsFlyer::getAppsFlyerId()
extern "C" IL2CPP_METHOD_ATTR String_t* AppsFlyer_getAppsFlyerId_m2086277239 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void AppsFlyer::trackRichEvent(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_trackRichEvent_m3155636984 (RuntimeObject * __this /* static, unused */, String_t* p0, Dictionary_2_t1632706988 * p1, const RuntimeMethod* method);
// System.Void AppsFlyer::stopTracking(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_stopTracking_m2527952211 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Void AppsFlyer::trackCrossPromoteImpression(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_trackCrossPromoteImpression_m12540899 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void UnityEngine.iOS.NotificationServices::RegisterForNotifications(UnityEngine.iOS.NotificationType)
extern "C" IL2CPP_METHOD_ATTR void NotificationServices_RegisterForNotifications_m3159498804 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C" IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m17791917 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Byte[] UnityEngine.iOS.NotificationServices::get_deviceToken()
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* NotificationServices_get_deviceToken_m4135060131 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void AppsFlyer::registerUninstall(System.Byte[])
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_registerUninstall_m1283358625 (RuntimeObject * __this /* static, unused */, ByteU5BU5D_t4116647657* p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setCurrencyCode(System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setCurrencyCode_m656834413 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void AppsFlyer::setAppInviteOneLinkID(System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_setAppInviteOneLinkID_m3680476995 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void AppsFlyer::generateUserInviteLink(System.Collections.Generic.Dictionary`2<System.String,System.String>,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void AppsFlyer_generateUserInviteLink_m3446747668 (RuntimeObject * __this /* static, unused */, Dictionary_2_t1632706988 * p0, String_t* p1, String_t* p2, String_t* p3, const RuntimeMethod* method);
// System.Void AF_Sample_BGScript::pressed()
extern "C" IL2CPP_METHOD_ATTR void AF_Sample_BGScript_pressed_m3047931247 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.String::Contains(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_Contains_m1147431944 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Void afobjectscript::appsFlyerInit()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_appsFlyerInit_m1783334442 (afobjectscript_t980149766 * __this, const RuntimeMethod* method);
// System.Void afobjectscript::InitializePurchasing()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_InitializePurchasing_m2132555723 (afobjectscript_t980149766 * __this, const RuntimeMethod* method);
// System.Boolean afobjectscript::IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool afobjectscript_IsInitialized_m2000466453 (afobjectscript_t980149766 * __this, const RuntimeMethod* method);
// System.Void afobjectscript::BuyProductID(System.String)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_BuyProductID_m652833323 (afobjectscript_t980149766 * __this, String_t* ___productId0, const RuntimeMethod* method);
// System.Void afobjectscript::BuyConsumable()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_BuyConsumable_m2485974033 (afobjectscript_t980149766 * __this, const RuntimeMethod* method);
// System.Void afobjectscript::afiosValidate(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_afiosValidate_m391050797 (afobjectscript_t980149766 * __this, String_t* ___prodID0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionID3, Dictionary_2_t1632706988 * ___extraParams4, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AFInAppEvents::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AFInAppEvents__ctor_m1946470285 (AFInAppEvents_t2088576796 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AF_Sample_BGScript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AF_Sample_BGScript__ctor_m585555093 (AF_Sample_BGScript_t4024703127 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AF_Sample_BGScript::Start()
extern "C" IL2CPP_METHOD_ATTR void AF_Sample_BGScript_Start_m1693817813 (AF_Sample_BGScript_t4024703127 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1113636619 * L_0 = __this->get_Check_6();
		NullCheck(L_0);
		GameObject_SetActive_m796801857(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AF_Sample_BGScript::Update()
extern "C" IL2CPP_METHOD_ATTR void AF_Sample_BGScript_Update_m2621362297 (AF_Sample_BGScript_t4024703127 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript_Update_m2621362297_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var);
		bool L_0 = ((AF_Sample_BGScript_t4024703127_StaticFields*)il2cpp_codegen_static_fields_for(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var))->get_isPressed_8();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		RuntimeObject* L_1 = AF_Sample_BGScript_PressAnim_m1380001828(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var);
		((AF_Sample_BGScript_t4024703127_StaticFields*)il2cpp_codegen_static_fields_for(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var))->set_isPressed_8((bool)0);
	}

IL_001d:
	{
		return;
	}
}
// System.Void AF_Sample_BGScript::pressed()
extern "C" IL2CPP_METHOD_ATTR void AF_Sample_BGScript_pressed_m3047931247 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript_pressed_m3047931247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var);
		((AF_Sample_BGScript_t4024703127_StaticFields*)il2cpp_codegen_static_fields_for(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var))->set_isPressed_8((bool)1);
		return;
	}
}
// System.Collections.IEnumerator AF_Sample_BGScript::PressAnim()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* AF_Sample_BGScript_PressAnim_m1380001828 (AF_Sample_BGScript_t4024703127 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AF_Sample_BGScript_PressAnim_m1380001828_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CPressAnimU3Ec__Iterator0_t2687738030 * V_0 = NULL;
	{
		U3CPressAnimU3Ec__Iterator0_t2687738030 * L_0 = (U3CPressAnimU3Ec__Iterator0_t2687738030 *)il2cpp_codegen_object_new(U3CPressAnimU3Ec__Iterator0_t2687738030_il2cpp_TypeInfo_var);
		U3CPressAnimU3Ec__Iterator0__ctor_m1605106169(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CPressAnimU3Ec__Iterator0_t2687738030 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_0(__this);
		U3CPressAnimU3Ec__Iterator0_t2687738030 * L_2 = V_0;
		return L_2;
	}
}
// System.Void AF_Sample_BGScript::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AF_Sample_BGScript__cctor_m3288264768 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CPressAnimU3Ec__Iterator0__ctor_m1605106169 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AF_Sample_BGScript/<PressAnim>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CPressAnimU3Ec__Iterator0_MoveNext_m3825884020 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPressAnimU3Ec__Iterator0_MoveNext_m3825884020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0081;
			}
		}
	}
	{
		goto IL_00c4;
	}

IL_0021:
	{
		AF_Sample_BGScript_t4024703127 * L_2 = __this->get_U24this_0();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = L_2->get_Check_6();
		NullCheck(L_3);
		GameObject_SetActive_m796801857(L_3, (bool)1, /*hidden argument*/NULL);
		AF_Sample_BGScript_t4024703127 * L_4 = __this->get_U24this_0();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = L_4->get_ButtonText_5();
		NullCheck(L_5);
		Text_t1901882714 * L_6 = GameObject_GetComponent_TisText_t1901882714_m565625081(L_5, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m565625081_RuntimeMethod_var);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral2863737031);
		AF_Sample_BGScript_t4024703127 * L_7 = __this->get_U24this_0();
		NullCheck(L_7);
		GameObject_t1113636619 * L_8 = L_7->get_Button_4();
		NullCheck(L_8);
		GameObject_SetActive_m796801857(L_8, (bool)0, /*hidden argument*/NULL);
		WaitForSeconds_t1699091251 * L_9 = (WaitForSeconds_t1699091251 *)il2cpp_codegen_object_new(WaitForSeconds_t1699091251_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m2199082655(L_9, (3.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_9);
		bool L_10 = __this->get_U24disposing_2();
		if (L_10)
		{
			goto IL_007c;
		}
	}
	{
		__this->set_U24PC_3(1);
	}

IL_007c:
	{
		goto IL_00c6;
	}

IL_0081:
	{
		AF_Sample_BGScript_t4024703127 * L_11 = __this->get_U24this_0();
		NullCheck(L_11);
		GameObject_t1113636619 * L_12 = L_11->get_Check_6();
		NullCheck(L_12);
		GameObject_SetActive_m796801857(L_12, (bool)0, /*hidden argument*/NULL);
		AF_Sample_BGScript_t4024703127 * L_13 = __this->get_U24this_0();
		NullCheck(L_13);
		GameObject_t1113636619 * L_14 = L_13->get_ButtonText_5();
		NullCheck(L_14);
		Text_t1901882714 * L_15 = GameObject_GetComponent_TisText_t1901882714_m565625081(L_14, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m565625081_RuntimeMethod_var);
		NullCheck(L_15);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_15, _stringLiteral3385962031);
		AF_Sample_BGScript_t4024703127 * L_16 = __this->get_U24this_0();
		NullCheck(L_16);
		GameObject_t1113636619 * L_17 = L_16->get_Button_4();
		NullCheck(L_17);
		GameObject_SetActive_m796801857(L_17, (bool)1, /*hidden argument*/NULL);
		__this->set_U24PC_3((-1));
	}

IL_00c4:
	{
		return (bool)0;
	}

IL_00c6:
	{
		return (bool)1;
	}
}
// System.Object AF_Sample_BGScript/<PressAnim>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CPressAnimU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m835405681 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object AF_Sample_BGScript/<PressAnim>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CPressAnimU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2820856387 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CPressAnimU3Ec__Iterator0_Dispose_m2525715905 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_2((bool)1);
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void AF_Sample_BGScript/<PressAnim>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CPressAnimU3Ec__Iterator0_Reset_m1696407584 (U3CPressAnimU3Ec__Iterator0_t2687738030 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CPressAnimU3Ec__Iterator0_Reset_m1696407584_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CPressAnimU3Ec__Iterator0_Reset_m1696407584_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AfScriptNew::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew__ctor_m803890474 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AfScriptNew::Start()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_Start_m2271372884 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_Start_m2271372884_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AfScriptNew_appsFlyerInit_m713040632(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		AfScriptNew_InitializePurchasing_m1487909055(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void AfScriptNew::InitializePurchasing()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_InitializePurchasing_m1487909055 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_InitializePurchasing_m1487909055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConfigurationBuilder_t1618671084 * V_0 = NULL;
	{
		bool L_0 = AfScriptNew_IsInitialized_m2350561814(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t2580735509_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t2580735509 * L_1 = StandardPurchasingModule_Instance_m2991111855(NULL /*static, unused*/, /*hidden argument*/NULL);
		IPurchasingModuleU5BU5D_t3784316456* L_2 = (IPurchasingModuleU5BU5D_t3784316456*)SZArrayNew(IPurchasingModuleU5BU5D_t3784316456_il2cpp_TypeInfo_var, (uint32_t)0);
		ConfigurationBuilder_t1618671084 * L_3 = ConfigurationBuilder_Instance_m2204111312(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		ConfigurationBuilder_t1618671084 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		String_t* L_5 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_kProductIDConsumable_6();
		NullCheck(L_4);
		ConfigurationBuilder_AddProduct_m788979654(L_4, L_5, 0, /*hidden argument*/NULL);
		ConfigurationBuilder_t1618671084 * L_6 = V_0;
		UnityPurchasing_Initialize_m1836262307(NULL /*static, unused*/, __this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AfScriptNew::IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool AfScriptNew_IsInitialized_m2350561814 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_IsInitialized_m2350561814_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_m_StoreExtensionProvider_5();
		G_B3_0 = ((((int32_t)((((RuntimeObject*)(RuntimeObject*)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Void AfScriptNew::BuyConsumable()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_BuyConsumable_m148322322 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_BuyConsumable_m148322322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral23435790, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		String_t* L_0 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_kProductIDConsumable_6();
		AfScriptNew_BuyProductID_m2762636159(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AfScriptNew::BuyProductID(System.String)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_BuyProductID_m2762636159 (AfScriptNew_t3996606863 * __this, String_t* ___productId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_BuyProductID_m2762636159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t3244410059 * V_0 = NULL;
	{
		bool L_0 = AfScriptNew_IsInitialized_m2350561814(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		NullCheck(L_1);
		ProductCollection_t2671956229 * L_2 = InterfaceFuncInvoker0< ProductCollection_t2671956229 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t2579314702_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___productId0;
		NullCheck(L_2);
		Product_t3244410059 * L_4 = ProductCollection_WithID_m2597694943(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Product_t3244410059 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		Product_t3244410059 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Product_get_availableToPurchase_m3282912434(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		Product_t3244410059 * L_8 = V_0;
		NullCheck(L_8);
		ProductDefinition_t339727138 * L_9 = Product_get_definition_m3366103520(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_m1593385231(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral923836985, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		RuntimeObject* L_12 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		Product_t3244410059 * L_13 = V_0;
		NullCheck(L_12);
		InterfaceActionInvoker1< Product_t3244410059 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product) */, IStoreController_t2579314702_il2cpp_TypeInfo_var, L_12, L_13);
		goto IL_0061;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3239286485, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0070;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral310096932, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void AfScriptNew::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_OnInitialized_m2338278659 (AfScriptNew_t3996606863 * __this, RuntimeObject* ___controller0, RuntimeObject* ___extensions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_OnInitialized_m2338278659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3898871526, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___controller0;
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->set_m_StoreController_4(L_0);
		RuntimeObject* L_1 = ___extensions1;
		((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->set_m_StoreExtensionProvider_5(L_1);
		return;
	}
}
// System.Void AfScriptNew::ConsTaskOnClick()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_ConsTaskOnClick_m1294378824 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_ConsTaskOnClick_m1294378824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral279557124, /*hidden argument*/NULL);
		AfScriptNew_BuyConsumable_m148322322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AfScriptNew::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_OnInitializeFailed_m420728356 (AfScriptNew_t3996606863 * __this, int32_t ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_OnInitializeFailed_m420728356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___error0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(InitializationFailureReason_t2740567704_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3073017110, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult AfScriptNew::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C" IL2CPP_METHOD_ATTR int32_t AfScriptNew_ProcessPurchase_m1130738387 (AfScriptNew_t3996606863 * __this, PurchaseEventArgs_t3033159582 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_ProcessPurchase_m1130738387_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	Dictionary_2_t1632706988 * V_4 = NULL;
	String_t* V_5 = NULL;
	Dictionary_2_t2865362463 * V_6 = NULL;
	String_t* V_7 = NULL;
	{
		PurchaseEventArgs_t3033159582 * L_0 = ___args0;
		NullCheck(L_0);
		Product_t3244410059 * L_1 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ProductDefinition_t339727138 * L_2 = Product_get_definition_m3366103520(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_m1593385231(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PurchaseEventArgs_t3033159582 * L_4 = ___args0;
		NullCheck(L_4);
		Product_t3244410059 * L_5 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		ProductMetadata_t3417118930 * L_6 = Product_get_metadata_m3202120155(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ProductMetadata_get_localizedPriceString_m3339862584(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3657309853_il2cpp_TypeInfo_var);
		String_t* L_10 = Regex_Replace_m2667570911(NULL /*static, unused*/, L_8, _stringLiteral356456174, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		PurchaseEventArgs_t3033159582 * L_11 = ___args0;
		NullCheck(L_11);
		Product_t3244410059 * L_12 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		ProductMetadata_t3417118930 * L_13 = Product_get_metadata_m3202120155(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = ProductMetadata_get_isoCurrencyCode_m1144927692(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Dictionary_2_t1632706988 * L_15 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_15, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_4 = L_15;
		PurchaseEventArgs_t3033159582 * L_16 = ___args0;
		NullCheck(L_16);
		Product_t3244410059 * L_17 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Product_get_receipt_m117487645(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		PurchaseEventArgs_t3033159582 * L_19 = ___args0;
		NullCheck(L_19);
		Product_t3244410059 * L_20 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = Product_get_receipt_m117487645(L_20, /*hidden argument*/NULL);
		RuntimeObject * L_22 = MiniJson_JsonDecode_m1204021123(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_6 = ((Dictionary_2_t2865362463 *)CastclassClass((RuntimeObject*)L_22, Dictionary_2_t2865362463_il2cpp_TypeInfo_var));
		Dictionary_2_t2865362463 * L_23 = V_6;
		NullCheck(L_23);
		RuntimeObject * L_24 = Dictionary_2_get_Item_m1539906286(L_23, _stringLiteral1632276064, /*hidden argument*/Dictionary_2_get_Item_m1539906286_RuntimeMethod_var);
		V_7 = ((String_t*)CastclassSealed((RuntimeObject*)L_24, String_t_il2cpp_TypeInfo_var));
		PurchaseEventArgs_t3033159582 * L_25 = ___args0;
		NullCheck(L_25);
		Product_t3244410059 * L_26 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		ProductDefinition_t339727138 * L_27 = Product_get_definition_m3366103520(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_28 = ProductDefinition_get_id_m1593385231(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AfScriptNew_t3996606863_il2cpp_TypeInfo_var);
		String_t* L_29 = ((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->get_kProductIDConsumable_6();
		bool L_30 = String_Equals_m2359609904(NULL /*static, unused*/, L_28, L_29, 4, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00d3;
		}
	}
	{
		PurchaseEventArgs_t3033159582 * L_31 = ___args0;
		NullCheck(L_31);
		Product_t3244410059 * L_32 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		ProductDefinition_t339727138 * L_33 = Product_get_definition_m3366103520(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		String_t* L_34 = ProductDefinition_get_id_m1593385231(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral4051270574, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		String_t* L_36 = V_0;
		String_t* L_37 = V_2;
		String_t* L_38 = V_3;
		String_t* L_39 = V_7;
		Dictionary_2_t1632706988 * L_40 = V_4;
		AfScriptNew_afiosValidate_m178811455(__this, L_36, L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00d3:
	{
		PurchaseEventArgs_t3033159582 * L_41 = ___args0;
		NullCheck(L_41);
		Product_t3244410059 * L_42 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		ProductDefinition_t339727138 * L_43 = Product_get_definition_m3366103520(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44 = ProductDefinition_get_id_m1593385231(L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3885527639, L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return (int32_t)(0);
	}
}
// System.Void AfScriptNew::afiosValidate(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_afiosValidate_m178811455 (AfScriptNew_t3996606863 * __this, String_t* ___prodID0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionID3, Dictionary_2_t1632706988 * ___extraParams4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_afiosValidate_m178811455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)8);
		StringU5BU5D_t1281789340* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1928966392);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1928966392);
		StringU5BU5D_t1281789340* L_2 = L_1;
		String_t* L_3 = ___prodID0;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1281789340* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral2418339336);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2418339336);
		StringU5BU5D_t1281789340* L_5 = L_4;
		String_t* L_6 = ___transactionID3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_6);
		StringU5BU5D_t1281789340* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3836975195);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3836975195);
		StringU5BU5D_t1281789340* L_8 = L_7;
		String_t* L_9 = ___price1;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_9);
		StringU5BU5D_t1281789340* L_10 = L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3678466557);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3678466557);
		StringU5BU5D_t1281789340* L_11 = L_10;
		String_t* L_12 = ___currency2;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		AppsFlyer_setIsSandbox_m2398892449(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		String_t* L_14 = ___prodID0;
		String_t* L_15 = ___price1;
		String_t* L_16 = ___currency2;
		String_t* L_17 = ___transactionID3;
		Dictionary_2_t1632706988 * L_18 = ___extraParams4;
		AppsFlyer_validateReceipt_m1909103642(NULL /*static, unused*/, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AfScriptNew::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_OnPurchaseFailed_m2320970959 (AfScriptNew_t3996606863 * __this, Product_t3244410059 * ___product0, int32_t ___failureReason1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_OnPurchaseFailed_m2320970959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t3244410059 * L_0 = ___product0;
		NullCheck(L_0);
		ProductDefinition_t339727138 * L_1 = Product_get_definition_m3366103520(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_storeSpecificId_m2520532185(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___failureReason1;
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(PurchaseFailureReason_t4243987912_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral508901929, L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AfScriptNew::appsFlyerInit()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew_appsFlyerInit_m713040632 (AfScriptNew_t3996606863 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew_appsFlyerInit_m713040632_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AppsFlyer_setAppsFlyerKey_m2232093163(NULL /*static, unused*/, _stringLiteral531322540, /*hidden argument*/NULL);
		AppsFlyer_setIsDebug_m775783760(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AppsFlyer_setAppID_m125861847(NULL /*static, unused*/, _stringLiteral1670686406, /*hidden argument*/NULL);
		AppsFlyer_setIsSandbox_m2398892449(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AppsFlyer_getConversionData_m3717632094(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppsFlyer_trackAppLaunch_m1307190503(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AfScriptNew::.cctor()
extern "C" IL2CPP_METHOD_ATTR void AfScriptNew__cctor_m3116246755 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AfScriptNew__cctor_m3116246755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AfScriptNew_t3996606863_StaticFields*)il2cpp_codegen_static_fields_for(AfScriptNew_t3996606863_il2cpp_TypeInfo_var))->set_kProductIDConsumable_6(_stringLiteral4289976629);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void StartUp::.ctor()
extern "C" IL2CPP_METHOD_ATTR void StartUp__ctor_m869510197 (StartUp_t2202782244 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUp::Start()
extern "C" IL2CPP_METHOD_ATTR void StartUp_Start_m453063797 (StartUp_t2202782244 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_Start_m453063797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t1632706988 * V_0 = NULL;
	Dictionary_2_t1632706988 * V_1 = NULL;
	{
		Application_set_runInBackground_m2169704730(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Screen_set_orientation_m3561207030(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m166252750(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		AppsFlyer_setIsDebug_m775783760(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AppsFlyer_setHost_m812428899(NULL /*static, unused*/, _stringLiteral83672638, _stringLiteral1504355976, /*hidden argument*/NULL);
		AppsFlyer_setMinTimeBetweenSessions_m1747056130(NULL /*static, unused*/, ((int32_t)10), /*hidden argument*/NULL);
		AppsFlyer_trackAppLaunch_m1307190503(NULL /*static, unused*/, /*hidden argument*/NULL);
		Dictionary_2_t1632706988 * L_0 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_0, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_0 = L_0;
		Dictionary_2_t1632706988 * L_1 = V_0;
		NullCheck(L_1);
		Dictionary_2_Add_m3097518446(L_1, _stringLiteral3050893988, _stringLiteral4002445229, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_2 = V_0;
		AppsFlyer_setAdditionalData_m1491833587(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		AppsFlyer_setAppsFlyerKey_m2232093163(NULL /*static, unused*/, _stringLiteral531322540, /*hidden argument*/NULL);
		AppsFlyer_setAppID_m125861847(NULL /*static, unused*/, _stringLiteral1670686406, /*hidden argument*/NULL);
		AppsFlyer_setIsDebug_m775783760(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AppsFlyer_getConversionData_m3717632094(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppsFlyer_setCustomerUserID_m3283082404(NULL /*static, unused*/, _stringLiteral21130133, /*hidden argument*/NULL);
		AppsFlyer_getAppsFlyerId_m2086277239(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_3 = AppsFlyer_getAppsFlyerId_m2086277239(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral415387938, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		String_t* L_5 = AppsFlyer_getAppsFlyerId_m2086277239(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral415387938, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Dictionary_2_t1632706988 * L_7 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_7, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_1 = L_7;
		Dictionary_2_t1632706988 * L_8 = V_1;
		NullCheck(L_8);
		Dictionary_2_Add_m3097518446(L_8, _stringLiteral4000924501, _stringLiteral1315542119, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_9 = V_1;
		AppsFlyer_trackRichEvent_m3155636984(NULL /*static, unused*/, _stringLiteral425026648, L_9, /*hidden argument*/NULL);
		AppsFlyer_trackAppLaunch_m1307190503(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppsFlyer_stopTracking_m2527952211(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		AppsFlyer_trackCrossPromoteImpression_m12540899(NULL /*static, unused*/, _stringLiteral1002553885, _stringLiteral1056623483, /*hidden argument*/NULL);
		NotificationServices_RegisterForNotifications_m3159498804(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		Screen_set_orientation_m3561207030(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUp::Update()
extern "C" IL2CPP_METHOD_ATTR void StartUp_Update_m423706476 (StartUp_t2202782244 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_Update_m423706476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_t4116647657* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m17791917(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}

IL_000c:
	{
		bool L_1 = __this->get_tokenSent_5();
		if (L_1)
		{
			goto IL_0030;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_2 = NotificationServices_get_deviceToken_m4135060131(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteU5BU5D_t4116647657* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		ByteU5BU5D_t4116647657* L_4 = V_0;
		AppsFlyer_registerUninstall_m1283358625(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_tokenSent_5((bool)1);
	}

IL_0030:
	{
		return;
	}
}
// System.Void StartUp::Purchase()
extern "C" IL2CPP_METHOD_ATTR void StartUp_Purchase_m2962335323 (StartUp_t2202782244 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_Purchase_m2962335323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t1632706988 * V_0 = NULL;
	Dictionary_2_t1632706988 * V_1 = NULL;
	Dictionary_2_t1632706988 * V_2 = NULL;
	Dictionary_2_t1632706988 * V_3 = NULL;
	Dictionary_2_t1632706988 * V_4 = NULL;
	Dictionary_2_t1632706988 * V_5 = NULL;
	{
		Dictionary_2_t1632706988 * L_0 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_0, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_0 = L_0;
		Dictionary_2_t1632706988 * L_1 = V_0;
		NullCheck(L_1);
		Dictionary_2_Add_m3097518446(L_1, _stringLiteral748725514, _stringLiteral3074643751, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_2 = V_0;
		NullCheck(L_2);
		Dictionary_2_Add_m3097518446(L_2, _stringLiteral533775410, _stringLiteral3833001788, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_Add_m3097518446(L_3, _stringLiteral1727939182, _stringLiteral421963658, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_4 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_4, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_1 = L_4;
		Dictionary_2_t1632706988 * L_5 = V_1;
		NullCheck(L_5);
		Dictionary_2_Add_m3097518446(L_5, _stringLiteral3050893988, _stringLiteral4002445229, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_6 = V_1;
		AppsFlyer_setAdditionalData_m1491833587(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		AppsFlyer_setCurrencyCode_m656834413(NULL /*static, unused*/, _stringLiteral3099958611, /*hidden argument*/NULL);
		AppsFlyer_setAppInviteOneLinkID_m3680476995(NULL /*static, unused*/, _stringLiteral623525981, /*hidden argument*/NULL);
		Dictionary_2_t1632706988 * L_7 = V_0;
		AppsFlyer_trackRichEvent_m3155636984(NULL /*static, unused*/, _stringLiteral2795224173, L_7, /*hidden argument*/NULL);
		Dictionary_2_t1632706988 * L_8 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_8, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_2 = L_8;
		Dictionary_2_t1632706988 * L_9 = V_2;
		NullCheck(L_9);
		Dictionary_2_Add_m3097518446(L_9, _stringLiteral748725514, _stringLiteral3451565963, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_10 = V_2;
		NullCheck(L_10);
		Dictionary_2_Add_m3097518446(L_10, _stringLiteral533775410, _stringLiteral457746925, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_11 = V_2;
		NullCheck(L_11);
		Dictionary_2_Add_m3097518446(L_11, _stringLiteral1727939182, _stringLiteral261945041, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_12 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_12, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_3 = L_12;
		Dictionary_2_t1632706988 * L_13 = V_3;
		NullCheck(L_13);
		Dictionary_2_Add_m3097518446(L_13, _stringLiteral1232190289, _stringLiteral4002445229, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_14 = V_3;
		AppsFlyer_validateReceipt_m1909103642(NULL /*static, unused*/, _stringLiteral3924207445, _stringLiteral3451565967, _stringLiteral1844033921, _stringLiteral1849190365, L_14, /*hidden argument*/NULL);
		Dictionary_2_t1632706988 * L_15 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_15, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_4 = L_15;
		Dictionary_2_t1632706988 * L_16 = V_4;
		NullCheck(L_16);
		Dictionary_2_Add_m3097518446(L_16, _stringLiteral470911652, _stringLiteral4101302744, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_17 = V_4;
		NullCheck(L_17);
		Dictionary_2_Add_m3097518446(L_17, _stringLiteral116130005, _stringLiteral3655604716, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_18 = V_4;
		NullCheck(L_18);
		Dictionary_2_Add_m3097518446(L_18, _stringLiteral2972012286, _stringLiteral1731952304, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_19 = V_4;
		NullCheck(L_19);
		Dictionary_2_Add_m3097518446(L_19, _stringLiteral1344052996, _stringLiteral3329093674, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_20 = V_4;
		NullCheck(L_20);
		Dictionary_2_Add_m3097518446(L_20, _stringLiteral940768469, _stringLiteral3329093677, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_21 = V_4;
		AppsFlyer_generateUserInviteLink_m3446747668(NULL /*static, unused*/, L_21, _stringLiteral552165993, _stringLiteral1002265957, _stringLiteral209837549, /*hidden argument*/NULL);
		AppsFlyer_setIsSandbox_m2398892449(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		Dictionary_2_t1632706988 * L_22 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_22, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_5 = L_22;
		Dictionary_2_t1632706988 * L_23 = V_5;
		NullCheck(L_23);
		Dictionary_2_Add_m3097518446(L_23, _stringLiteral470911652, _stringLiteral232548031, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_24 = V_5;
		NullCheck(L_24);
		Dictionary_2_Add_m3097518446(L_24, _stringLiteral116130005, _stringLiteral1867591191, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		Dictionary_2_t1632706988 * L_25 = V_5;
		NullCheck(L_25);
		Dictionary_2_Add_m3097518446(L_25, _stringLiteral2972012286, _stringLiteral1805404713, /*hidden argument*/Dictionary_2_Add_m3097518446_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(AF_Sample_BGScript_t4024703127_il2cpp_TypeInfo_var);
		AF_Sample_BGScript_pressed_m3047931247(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUp::didReceiveConversionData(System.String)
extern "C" IL2CPP_METHOD_ATTR void StartUp_didReceiveConversionData_m2499638998 (StartUp_t2202782244 * __this, String_t* ___conversionData0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_didReceiveConversionData_m2499638998_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___conversionData0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral181065742, L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___conversionData0;
		NullCheck(L_2);
		bool L_3 = String_Contains_m1147431944(L_2, _stringLiteral4072640822, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		GameObject_t1113636619 * L_4 = __this->get_text_4();
		NullCheck(L_4);
		Text_t1901882714 * L_5 = GameObject_GetComponent_TisText_t1901882714_m565625081(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m565625081_RuntimeMethod_var);
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral3514087433);
		goto IL_004f;
	}

IL_003a:
	{
		GameObject_t1113636619 * L_6 = __this->get_text_4();
		NullCheck(L_6);
		Text_t1901882714 * L_7 = GameObject_GetComponent_TisText_t1901882714_m565625081(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t1901882714_m565625081_RuntimeMethod_var);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(73 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, _stringLiteral2777653202);
	}

IL_004f:
	{
		return;
	}
}
// System.Void StartUp::didReceiveConversionDataWithError(System.String)
extern "C" IL2CPP_METHOD_ATTR void StartUp_didReceiveConversionDataWithError_m2011039941 (StartUp_t2202782244 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_didReceiveConversionDataWithError_m2011039941_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1929732520, L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUp::onAppOpenAttribution(System.String)
extern "C" IL2CPP_METHOD_ATTR void StartUp_onAppOpenAttribution_m3708816227 (StartUp_t2202782244 * __this, String_t* ___validateResult0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_onAppOpenAttribution_m3708816227_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___validateResult0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3054649102, L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void StartUp::onAppOpenAttributionFailure(System.String)
extern "C" IL2CPP_METHOD_ATTR void StartUp_onAppOpenAttributionFailure_m1278686197 (StartUp_t2202782244 * __this, String_t* ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StartUp_onAppOpenAttributionFailure_m1278686197_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___error0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral1008257438, L_0, /*hidden argument*/NULL);
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void afobjectscript::.ctor()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript__ctor_m2814794479 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void afobjectscript::Start()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_Start_m1357431867 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_Start_m1357431867_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		afobjectscript_appsFlyerInit_m1783334442(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		afobjectscript_InitializePurchasing_m2132555723(__this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void afobjectscript::InitializePurchasing()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_InitializePurchasing_m2132555723 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_InitializePurchasing_m2132555723_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ConfigurationBuilder_t1618671084 * V_0 = NULL;
	{
		bool L_0 = afobjectscript_IsInitialized_m2000466453(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(StandardPurchasingModule_t2580735509_il2cpp_TypeInfo_var);
		StandardPurchasingModule_t2580735509 * L_1 = StandardPurchasingModule_Instance_m2991111855(NULL /*static, unused*/, /*hidden argument*/NULL);
		IPurchasingModuleU5BU5D_t3784316456* L_2 = (IPurchasingModuleU5BU5D_t3784316456*)SZArrayNew(IPurchasingModuleU5BU5D_t3784316456_il2cpp_TypeInfo_var, (uint32_t)0);
		ConfigurationBuilder_t1618671084 * L_3 = ConfigurationBuilder_Instance_m2204111312(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		ConfigurationBuilder_t1618671084 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		String_t* L_5 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_kProductIDConsumable_6();
		NullCheck(L_4);
		ConfigurationBuilder_AddProduct_m788979654(L_4, L_5, 0, /*hidden argument*/NULL);
		ConfigurationBuilder_t1618671084 * L_6 = V_0;
		UnityPurchasing_Initialize_m1836262307(NULL /*static, unused*/, __this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean afobjectscript::IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool afobjectscript_IsInitialized_m2000466453 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_IsInitialized_m2000466453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_m_StoreExtensionProvider_5();
		G_B3_0 = ((((int32_t)((((RuntimeObject*)(RuntimeObject*)L_1) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
	}

IL_0018:
	{
		return (bool)G_B3_0;
	}
}
// System.Void afobjectscript::BuyConsumable()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_BuyConsumable_m2485974033 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_BuyConsumable_m2485974033_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		MonoBehaviour_print_m330341231(NULL /*static, unused*/, _stringLiteral23435790, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		String_t* L_0 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_kProductIDConsumable_6();
		afobjectscript_BuyProductID_m652833323(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void afobjectscript::BuyProductID(System.String)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_BuyProductID_m652833323 (afobjectscript_t980149766 * __this, String_t* ___productId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_BuyProductID_m652833323_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Product_t3244410059 * V_0 = NULL;
	{
		bool L_0 = afobjectscript_IsInitialized_m2000466453(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		RuntimeObject* L_1 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		NullCheck(L_1);
		ProductCollection_t2671956229 * L_2 = InterfaceFuncInvoker0< ProductCollection_t2671956229 * >::Invoke(0 /* UnityEngine.Purchasing.ProductCollection UnityEngine.Purchasing.IStoreController::get_products() */, IStoreController_t2579314702_il2cpp_TypeInfo_var, L_1);
		String_t* L_3 = ___productId0;
		NullCheck(L_2);
		Product_t3244410059 * L_4 = ProductCollection_WithID_m2597694943(L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Product_t3244410059 * L_5 = V_0;
		if (!L_5)
		{
			goto IL_0057;
		}
	}
	{
		Product_t3244410059 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Product_get_availableToPurchase_m3282912434(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0057;
		}
	}
	{
		Product_t3244410059 * L_8 = V_0;
		NullCheck(L_8);
		ProductDefinition_t339727138 * L_9 = Product_get_definition_m3366103520(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = ProductDefinition_get_id_m1593385231(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral923836985, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		RuntimeObject* L_12 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_m_StoreController_4();
		Product_t3244410059 * L_13 = V_0;
		NullCheck(L_12);
		InterfaceActionInvoker1< Product_t3244410059 * >::Invoke(2 /* System.Void UnityEngine.Purchasing.IStoreController::InitiatePurchase(UnityEngine.Purchasing.Product) */, IStoreController_t2579314702_il2cpp_TypeInfo_var, L_12, L_13);
		goto IL_0061;
	}

IL_0057:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3239286485, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0070;
	}

IL_0066:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral310096932, /*hidden argument*/NULL);
	}

IL_0070:
	{
		return;
	}
}
// System.Void afobjectscript::OnInitialized(UnityEngine.Purchasing.IStoreController,UnityEngine.Purchasing.IExtensionProvider)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_OnInitialized_m3062782771 (afobjectscript_t980149766 * __this, RuntimeObject* ___controller0, RuntimeObject* ___extensions1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_OnInitialized_m3062782771_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral3898871526, /*hidden argument*/NULL);
		RuntimeObject* L_0 = ___controller0;
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->set_m_StoreController_4(L_0);
		RuntimeObject* L_1 = ___extensions1;
		((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->set_m_StoreExtensionProvider_5(L_1);
		return;
	}
}
// System.Void afobjectscript::ConsTaskOnClick()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_ConsTaskOnClick_m1203192837 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_ConsTaskOnClick_m1203192837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, _stringLiteral279557124, /*hidden argument*/NULL);
		afobjectscript_BuyConsumable_m2485974033(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void afobjectscript::OnInitializeFailed(UnityEngine.Purchasing.InitializationFailureReason)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_OnInitializeFailed_m2008076800 (afobjectscript_t980149766 * __this, int32_t ___error0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_OnInitializeFailed_m2008076800_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___error0;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(InitializationFailureReason_t2740567704_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral3073017110, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Purchasing.PurchaseProcessingResult afobjectscript::ProcessPurchase(UnityEngine.Purchasing.PurchaseEventArgs)
extern "C" IL2CPP_METHOD_ATTR int32_t afobjectscript_ProcessPurchase_m4169781192 (afobjectscript_t980149766 * __this, PurchaseEventArgs_t3033159582 * ___args0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_ProcessPurchase_m4169781192_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* V_1 = NULL;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	Dictionary_2_t1632706988 * V_4 = NULL;
	String_t* V_5 = NULL;
	Dictionary_2_t2865362463 * V_6 = NULL;
	String_t* V_7 = NULL;
	{
		PurchaseEventArgs_t3033159582 * L_0 = ___args0;
		NullCheck(L_0);
		Product_t3244410059 * L_1 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ProductDefinition_t339727138 * L_2 = Product_get_definition_m3366103520(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ProductDefinition_get_id_m1593385231(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		PurchaseEventArgs_t3033159582 * L_4 = ___args0;
		NullCheck(L_4);
		Product_t3244410059 * L_5 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		ProductMetadata_t3417118930 * L_6 = Product_get_metadata_m3202120155(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ProductMetadata_get_localizedPriceString_m3339862584(L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		String_t* L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		IL2CPP_RUNTIME_CLASS_INIT(Regex_t3657309853_il2cpp_TypeInfo_var);
		String_t* L_10 = Regex_Replace_m2667570911(NULL /*static, unused*/, L_8, _stringLiteral356456174, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		PurchaseEventArgs_t3033159582 * L_11 = ___args0;
		NullCheck(L_11);
		Product_t3244410059 * L_12 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		ProductMetadata_t3417118930 * L_13 = Product_get_metadata_m3202120155(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		String_t* L_14 = ProductMetadata_get_isoCurrencyCode_m1144927692(L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Dictionary_2_t1632706988 * L_15 = (Dictionary_2_t1632706988 *)il2cpp_codegen_object_new(Dictionary_2_t1632706988_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3302800229(L_15, /*hidden argument*/Dictionary_2__ctor_m3302800229_RuntimeMethod_var);
		V_4 = L_15;
		PurchaseEventArgs_t3033159582 * L_16 = ___args0;
		NullCheck(L_16);
		Product_t3244410059 * L_17 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_18 = Product_get_receipt_m117487645(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		PurchaseEventArgs_t3033159582 * L_19 = ___args0;
		NullCheck(L_19);
		Product_t3244410059 * L_20 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = Product_get_receipt_m117487645(L_20, /*hidden argument*/NULL);
		RuntimeObject * L_22 = MiniJson_JsonDecode_m1204021123(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_6 = ((Dictionary_2_t2865362463 *)CastclassClass((RuntimeObject*)L_22, Dictionary_2_t2865362463_il2cpp_TypeInfo_var));
		Dictionary_2_t2865362463 * L_23 = V_6;
		NullCheck(L_23);
		RuntimeObject * L_24 = Dictionary_2_get_Item_m1539906286(L_23, _stringLiteral1632276064, /*hidden argument*/Dictionary_2_get_Item_m1539906286_RuntimeMethod_var);
		V_7 = ((String_t*)CastclassSealed((RuntimeObject*)L_24, String_t_il2cpp_TypeInfo_var));
		PurchaseEventArgs_t3033159582 * L_25 = ___args0;
		NullCheck(L_25);
		Product_t3244410059 * L_26 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_25, /*hidden argument*/NULL);
		NullCheck(L_26);
		ProductDefinition_t339727138 * L_27 = Product_get_definition_m3366103520(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		String_t* L_28 = ProductDefinition_get_id_m1593385231(L_27, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(afobjectscript_t980149766_il2cpp_TypeInfo_var);
		String_t* L_29 = ((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->get_kProductIDConsumable_6();
		bool L_30 = String_Equals_m2359609904(NULL /*static, unused*/, L_28, L_29, 4, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00d3;
		}
	}
	{
		PurchaseEventArgs_t3033159582 * L_31 = ___args0;
		NullCheck(L_31);
		Product_t3244410059 * L_32 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_31, /*hidden argument*/NULL);
		NullCheck(L_32);
		ProductDefinition_t339727138 * L_33 = Product_get_definition_m3366103520(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		String_t* L_34 = ProductDefinition_get_id_m1593385231(L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral4051270574, L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_35, /*hidden argument*/NULL);
		String_t* L_36 = V_0;
		String_t* L_37 = V_2;
		String_t* L_38 = V_3;
		String_t* L_39 = V_7;
		Dictionary_2_t1632706988 * L_40 = V_4;
		afobjectscript_afiosValidate_m391050797(__this, L_36, L_37, L_38, L_39, L_40, /*hidden argument*/NULL);
		goto IL_00f2;
	}

IL_00d3:
	{
		PurchaseEventArgs_t3033159582 * L_41 = ___args0;
		NullCheck(L_41);
		Product_t3244410059 * L_42 = PurchaseEventArgs_get_purchasedProduct_m3472521060(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		ProductDefinition_t339727138 * L_43 = Product_get_definition_m3366103520(L_42, /*hidden argument*/NULL);
		NullCheck(L_43);
		String_t* L_44 = ProductDefinition_get_id_m1593385231(L_43, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral3885527639, L_44, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
	}

IL_00f2:
	{
		return (int32_t)(0);
	}
}
// System.Void afobjectscript::afiosValidate(System.String,System.String,System.String,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_afiosValidate_m391050797 (afobjectscript_t980149766 * __this, String_t* ___prodID0, String_t* ___price1, String_t* ___currency2, String_t* ___transactionID3, Dictionary_2_t1632706988 * ___extraParams4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_afiosValidate_m391050797_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)8);
		StringU5BU5D_t1281789340* L_1 = L_0;
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, _stringLiteral1928966392);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1928966392);
		StringU5BU5D_t1281789340* L_2 = L_1;
		String_t* L_3 = ___prodID0;
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_3);
		StringU5BU5D_t1281789340* L_4 = L_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteral2418339336);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral2418339336);
		StringU5BU5D_t1281789340* L_5 = L_4;
		String_t* L_6 = ___transactionID3;
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_6);
		StringU5BU5D_t1281789340* L_7 = L_5;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral3836975195);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3836975195);
		StringU5BU5D_t1281789340* L_8 = L_7;
		String_t* L_9 = ___price1;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_9);
		StringU5BU5D_t1281789340* L_10 = L_8;
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, _stringLiteral3678466557);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(6), (String_t*)_stringLiteral3678466557);
		StringU5BU5D_t1281789340* L_11 = L_10;
		String_t* L_12 = ___currency2;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(7), (String_t*)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		AppsFlyer_setIsSandbox_m2398892449(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		String_t* L_14 = ___prodID0;
		String_t* L_15 = ___price1;
		String_t* L_16 = ___currency2;
		String_t* L_17 = ___transactionID3;
		Dictionary_2_t1632706988 * L_18 = ___extraParams4;
		AppsFlyer_validateReceipt_m1909103642(NULL /*static, unused*/, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void afobjectscript::OnPurchaseFailed(UnityEngine.Purchasing.Product,UnityEngine.Purchasing.PurchaseFailureReason)
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_OnPurchaseFailed_m3942370016 (afobjectscript_t980149766 * __this, Product_t3244410059 * ___product0, int32_t ___failureReason1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_OnPurchaseFailed_m3942370016_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Product_t3244410059 * L_0 = ___product0;
		NullCheck(L_0);
		ProductDefinition_t339727138 * L_1 = Product_get_definition_m3366103520(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = ProductDefinition_get_storeSpecificId_m2520532185(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___failureReason1;
		int32_t L_4 = L_3;
		RuntimeObject * L_5 = Box(PurchaseFailureReason_t4243987912_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral508901929, L_2, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_Log_m4051431634(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void afobjectscript::appsFlyerInit()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript_appsFlyerInit_m1783334442 (afobjectscript_t980149766 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript_appsFlyerInit_m1783334442_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AppsFlyer_setAppsFlyerKey_m2232093163(NULL /*static, unused*/, _stringLiteral531322540, /*hidden argument*/NULL);
		AppsFlyer_setIsDebug_m775783760(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AppsFlyer_setAppID_m125861847(NULL /*static, unused*/, _stringLiteral1670686406, /*hidden argument*/NULL);
		AppsFlyer_setIsSandbox_m2398892449(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		AppsFlyer_getConversionData_m3717632094(NULL /*static, unused*/, /*hidden argument*/NULL);
		AppsFlyer_trackAppLaunch_m1307190503(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void afobjectscript::.cctor()
extern "C" IL2CPP_METHOD_ATTR void afobjectscript__cctor_m869613066 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (afobjectscript__cctor_m869613066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((afobjectscript_t980149766_StaticFields*)il2cpp_codegen_static_fields_for(afobjectscript_t980149766_il2cpp_TypeInfo_var))->set_kProductIDConsumable_6(_stringLiteral4289976629);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
