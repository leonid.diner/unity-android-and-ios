﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState>
struct Action_1_t1412811613;
// System.Action`2<System.Boolean,System.String>
struct Action_2_t1290832230;
// System.Action`2<System.String,System.String>
struct Action_2_t3970170674;
// System.Action`3<System.String,System.String,System.String>
struct Action_3_t2617928178;
// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String>
struct Action_3_t1784145519;
// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String>
struct Action_3_t3666713281;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.RectTransform>
struct List_1_t881764471;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single>
struct Func_2_t235587086;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.Predicate`1<UnityEngine.Component>
struct Predicate_1_t2748928575;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// Uniject.IUtil
struct IUtil_t1069285358;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t4131667876;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Events.UnityAction`1<UnityEngine.Component>
struct UnityAction_1_t2508470592;
// UnityEngine.Purchasing.Extension.IStoreCallback
struct IStoreCallback_t764570979;
// UnityEngine.Purchasing.IUnityCallback
struct IUnityCallback_t3880085028;
// UnityEngine.Purchasing.MoolahStoreImpl
struct MoolahStoreImpl_t4045980644;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>
struct ObjectPool_1_t240936516;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct GetRayIntersectionAllCallback_t3913627115;
// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct GetRayIntersectionAllNonAllocCallback_t2311174851;
// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct GetRaycastNonAllocCallback_t3841783507;
// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct Raycast2DCallback_t768590915;
// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct Raycast3DCallback_t701940803;
// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct RaycastAllCallback_t1884415901;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;




#ifndef U3CMODULEU3E_T692745554_H
#define U3CMODULEU3E_T692745554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745554 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745554_H
#ifndef U3CMODULEU3E_T692745555_H
#define U3CMODULEU3E_T692745555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745555 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745555_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef ANDROIDJAVAPROXY_T2835824643_H
#define ANDROIDJAVAPROXY_T2835824643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AndroidJavaProxy
struct  AndroidJavaProxy_t2835824643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVAPROXY_T2835824643_H
#ifndef AMAZONAPPSTORESTOREEXTENSIONS_T4153153958_H
#define AMAZONAPPSTORESTOREEXTENSIONS_T4153153958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AmazonAppStoreStoreExtensions
struct  AmazonAppStoreStoreExtensions_t4153153958  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AmazonAppStoreStoreExtensions::android
	AndroidJavaObject_t4131667876 * ___android_0;

public:
	inline static int32_t get_offset_of_android_0() { return static_cast<int32_t>(offsetof(AmazonAppStoreStoreExtensions_t4153153958, ___android_0)); }
	inline AndroidJavaObject_t4131667876 * get_android_0() const { return ___android_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_android_0() { return &___android_0; }
	inline void set_android_0(AndroidJavaObject_t4131667876 * value)
	{
		___android_0 = value;
		Il2CppCodeGenWriteBarrier((&___android_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AMAZONAPPSTORESTOREEXTENSIONS_T4153153958_H
#ifndef ANDROIDJAVASTORE_T1912360766_H
#define ANDROIDJAVASTORE_T1912360766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.AndroidJavaStore
struct  AndroidJavaStore_t1912360766  : public RuntimeObject
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.AndroidJavaStore::m_Store
	AndroidJavaObject_t4131667876 * ___m_Store_0;

public:
	inline static int32_t get_offset_of_m_Store_0() { return static_cast<int32_t>(offsetof(AndroidJavaStore_t1912360766, ___m_Store_0)); }
	inline AndroidJavaObject_t4131667876 * get_m_Store_0() const { return ___m_Store_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_Store_0() { return &___m_Store_0; }
	inline void set_m_Store_0(AndroidJavaObject_t4131667876 * value)
	{
		___m_Store_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Store_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDJAVASTORE_T1912360766_H
#ifndef FACTORY_T3975828591_H
#define FACTORY_T3975828591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Default.Factory
struct  Factory_t3975828591  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORY_T3975828591_H
#ifndef FAKEAMAZONEXTENSIONS_T2007213879_H
#define FAKEAMAZONEXTENSIONS_T2007213879_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeAmazonExtensions
struct  FakeAmazonExtensions_t2007213879  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEAMAZONEXTENSIONS_T2007213879_H
#ifndef FAKEGOOGLEPLAYCONFIGURATION_T3630139311_H
#define FAKEGOOGLEPLAYCONFIGURATION_T3630139311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeGooglePlayConfiguration
struct  FakeGooglePlayConfiguration_t3630139311  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGOOGLEPLAYCONFIGURATION_T3630139311_H
#ifndef FAKEGOOGLEPLAYSTOREEXTENSIONS_T2625986117_H
#define FAKEGOOGLEPLAYSTOREEXTENSIONS_T2625986117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeGooglePlayStoreExtensions
struct  FakeGooglePlayStoreExtensions_t2625986117  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEGOOGLEPLAYSTOREEXTENSIONS_T2625986117_H
#ifndef FAKEMOOLAHCONFIGURATION_T983287713_H
#define FAKEMOOLAHCONFIGURATION_T983287713_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeMoolahConfiguration
struct  FakeMoolahConfiguration_t983287713  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.FakeMoolahConfiguration::m_appKey
	String_t* ___m_appKey_0;
	// System.String UnityEngine.Purchasing.FakeMoolahConfiguration::m_hashKey
	String_t* ___m_hashKey_1;

public:
	inline static int32_t get_offset_of_m_appKey_0() { return static_cast<int32_t>(offsetof(FakeMoolahConfiguration_t983287713, ___m_appKey_0)); }
	inline String_t* get_m_appKey_0() const { return ___m_appKey_0; }
	inline String_t** get_address_of_m_appKey_0() { return &___m_appKey_0; }
	inline void set_m_appKey_0(String_t* value)
	{
		___m_appKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_appKey_0), value);
	}

	inline static int32_t get_offset_of_m_hashKey_1() { return static_cast<int32_t>(offsetof(FakeMoolahConfiguration_t983287713, ___m_hashKey_1)); }
	inline String_t* get_m_hashKey_1() const { return ___m_hashKey_1; }
	inline String_t** get_address_of_m_hashKey_1() { return &___m_hashKey_1; }
	inline void set_m_hashKey_1(String_t* value)
	{
		___m_hashKey_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashKey_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMOOLAHCONFIGURATION_T983287713_H
#ifndef FAKEMOOLAHEXTENSIONS_T4194088837_H
#define FAKEMOOLAHEXTENSIONS_T4194088837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeMoolahExtensions
struct  FakeMoolahExtensions_t4194088837  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEMOOLAHEXTENSIONS_T4194088837_H
#ifndef FAKESAMSUNGAPPSEXTENSIONS_T2880024647_H
#define FAKESAMSUNGAPPSEXTENSIONS_T2880024647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeSamsungAppsExtensions
struct  FakeSamsungAppsExtensions_t2880024647  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKESAMSUNGAPPSEXTENSIONS_T2880024647_H
#ifndef FAKEUNITYCHANNELCONFIGURATION_T2647072263_H
#define FAKEUNITYCHANNELCONFIGURATION_T2647072263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUnityChannelConfiguration
struct  FakeUnityChannelConfiguration_t2647072263  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Purchasing.FakeUnityChannelConfiguration::<fetchReceiptPayloadOnPurchase>k__BackingField
	bool ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FakeUnityChannelConfiguration_t2647072263, ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0)); }
	inline bool get_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() const { return ___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0() { return &___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0; }
	inline void set_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(bool value)
	{
		___U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUNITYCHANNELCONFIGURATION_T2647072263_H
#ifndef FAKEUNITYCHANNELEXTENSIONS_T3596584646_H
#define FAKEUNITYCHANNELEXTENSIONS_T3596584646_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.FakeUnityChannelExtensions
struct  FakeUnityChannelExtensions_t3596584646  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FAKEUNITYCHANNELEXTENSIONS_T3596584646_H
#ifndef JSONSERIALIZER_T4060786200_H
#define JSONSERIALIZER_T4060786200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JSONSerializer
struct  JSONSerializer_t4060786200  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T4060786200_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T3759937156_H
#define U3CU3EC__DISPLAYCLASS18_0_T3759937156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t3759937156  : public RuntimeObject
{
public:
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0::purchaseSucceed
	Action_3_t2617928178 * ___purchaseSucceed_0;
	// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0::purchaseFailed
	Action_3_t1784145519 * ___purchaseFailed_1;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<>c__DisplayClass18_0::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_purchaseSucceed_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t3759937156, ___purchaseSucceed_0)); }
	inline Action_3_t2617928178 * get_purchaseSucceed_0() const { return ___purchaseSucceed_0; }
	inline Action_3_t2617928178 ** get_address_of_purchaseSucceed_0() { return &___purchaseSucceed_0; }
	inline void set_purchaseSucceed_0(Action_3_t2617928178 * value)
	{
		___purchaseSucceed_0 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseSucceed_0), value);
	}

	inline static int32_t get_offset_of_purchaseFailed_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t3759937156, ___purchaseFailed_1)); }
	inline Action_3_t1784145519 * get_purchaseFailed_1() const { return ___purchaseFailed_1; }
	inline Action_3_t1784145519 ** get_address_of_purchaseFailed_1() { return &___purchaseFailed_1; }
	inline void set_purchaseFailed_1(Action_3_t1784145519 * value)
	{
		___purchaseFailed_1 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseFailed_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t3759937156, ___U3CU3E4__this_2)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T3759937156_H
#ifndef U3CREQUESTAUTHCODEU3ED__22_T4220939193_H
#define U3CREQUESTAUTHCODEU3ED__22_T4220939193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22
struct  U3CRequestAuthCodeU3Ed__22_t4220939193  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::wf
	WWWForm_t4064702195 * ___wf_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::productID
	String_t* ___productID_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::transactionId
	String_t* ___transactionId_4;
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::succeed
	Action_3_t2617928178 * ___succeed_5;
	// System.Action`2<System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::failed
	Action_2_t3970170674 * ___failed_6;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_7;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<w>5__1
	WWW_t3688466362 * ___U3CwU3E5__1_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<authCodeResult>5__2
	Dictionary_2_t2865362463 * ___U3CauthCodeResultU3E5__2_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<authCodeValues>5__3
	Dictionary_2_t2865362463 * ___U3CauthCodeValuesU3E5__3_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<authCode>5__4
	String_t* ___U3CauthCodeU3E5__4_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RequestAuthCode>d__22::<paymentURL>5__5
	String_t* ___U3CpaymentURLU3E5__5_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_wf_2() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___wf_2)); }
	inline WWWForm_t4064702195 * get_wf_2() const { return ___wf_2; }
	inline WWWForm_t4064702195 ** get_address_of_wf_2() { return &___wf_2; }
	inline void set_wf_2(WWWForm_t4064702195 * value)
	{
		___wf_2 = value;
		Il2CppCodeGenWriteBarrier((&___wf_2), value);
	}

	inline static int32_t get_offset_of_productID_3() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___productID_3)); }
	inline String_t* get_productID_3() const { return ___productID_3; }
	inline String_t** get_address_of_productID_3() { return &___productID_3; }
	inline void set_productID_3(String_t* value)
	{
		___productID_3 = value;
		Il2CppCodeGenWriteBarrier((&___productID_3), value);
	}

	inline static int32_t get_offset_of_transactionId_4() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___transactionId_4)); }
	inline String_t* get_transactionId_4() const { return ___transactionId_4; }
	inline String_t** get_address_of_transactionId_4() { return &___transactionId_4; }
	inline void set_transactionId_4(String_t* value)
	{
		___transactionId_4 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_4), value);
	}

	inline static int32_t get_offset_of_succeed_5() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___succeed_5)); }
	inline Action_3_t2617928178 * get_succeed_5() const { return ___succeed_5; }
	inline Action_3_t2617928178 ** get_address_of_succeed_5() { return &___succeed_5; }
	inline void set_succeed_5(Action_3_t2617928178 * value)
	{
		___succeed_5 = value;
		Il2CppCodeGenWriteBarrier((&___succeed_5), value);
	}

	inline static int32_t get_offset_of_failed_6() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___failed_6)); }
	inline Action_2_t3970170674 * get_failed_6() const { return ___failed_6; }
	inline Action_2_t3970170674 ** get_address_of_failed_6() { return &___failed_6; }
	inline void set_failed_6(Action_2_t3970170674 * value)
	{
		___failed_6 = value;
		Il2CppCodeGenWriteBarrier((&___failed_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_7() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CU3E4__this_7)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_7() const { return ___U3CU3E4__this_7; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_7() { return &___U3CU3E4__this_7; }
	inline void set_U3CU3E4__this_7(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_7), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__1_8() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CwU3E5__1_8)); }
	inline WWW_t3688466362 * get_U3CwU3E5__1_8() const { return ___U3CwU3E5__1_8; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E5__1_8() { return &___U3CwU3E5__1_8; }
	inline void set_U3CwU3E5__1_8(WWW_t3688466362 * value)
	{
		___U3CwU3E5__1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__1_8), value);
	}

	inline static int32_t get_offset_of_U3CauthCodeResultU3E5__2_9() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CauthCodeResultU3E5__2_9)); }
	inline Dictionary_2_t2865362463 * get_U3CauthCodeResultU3E5__2_9() const { return ___U3CauthCodeResultU3E5__2_9; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CauthCodeResultU3E5__2_9() { return &___U3CauthCodeResultU3E5__2_9; }
	inline void set_U3CauthCodeResultU3E5__2_9(Dictionary_2_t2865362463 * value)
	{
		___U3CauthCodeResultU3E5__2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthCodeResultU3E5__2_9), value);
	}

	inline static int32_t get_offset_of_U3CauthCodeValuesU3E5__3_10() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CauthCodeValuesU3E5__3_10)); }
	inline Dictionary_2_t2865362463 * get_U3CauthCodeValuesU3E5__3_10() const { return ___U3CauthCodeValuesU3E5__3_10; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CauthCodeValuesU3E5__3_10() { return &___U3CauthCodeValuesU3E5__3_10; }
	inline void set_U3CauthCodeValuesU3E5__3_10(Dictionary_2_t2865362463 * value)
	{
		___U3CauthCodeValuesU3E5__3_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthCodeValuesU3E5__3_10), value);
	}

	inline static int32_t get_offset_of_U3CauthCodeU3E5__4_11() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CauthCodeU3E5__4_11)); }
	inline String_t* get_U3CauthCodeU3E5__4_11() const { return ___U3CauthCodeU3E5__4_11; }
	inline String_t** get_address_of_U3CauthCodeU3E5__4_11() { return &___U3CauthCodeU3E5__4_11; }
	inline void set_U3CauthCodeU3E5__4_11(String_t* value)
	{
		___U3CauthCodeU3E5__4_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CauthCodeU3E5__4_11), value);
	}

	inline static int32_t get_offset_of_U3CpaymentURLU3E5__5_12() { return static_cast<int32_t>(offsetof(U3CRequestAuthCodeU3Ed__22_t4220939193, ___U3CpaymentURLU3E5__5_12)); }
	inline String_t* get_U3CpaymentURLU3E5__5_12() const { return ___U3CpaymentURLU3E5__5_12; }
	inline String_t** get_address_of_U3CpaymentURLU3E5__5_12() { return &___U3CpaymentURLU3E5__5_12; }
	inline void set_U3CpaymentURLU3E5__5_12(String_t* value)
	{
		___U3CpaymentURLU3E5__5_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpaymentURLU3E5__5_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREQUESTAUTHCODEU3ED__22_T4220939193_H
#ifndef U3CSTARTPURCHASEPOLLINGU3ED__23_T3243852457_H
#define U3CSTARTPURCHASEPOLLINGU3ED__23_T3243852457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23
struct  U3CStartPurchasePollingU3Ed__23_t3243852457  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::authGlobal
	String_t* ___authGlobal_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::transactionId
	String_t* ___transactionId_3;
	// System.Action`3<System.String,System.String,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::purchaseSucceed
	Action_3_t2617928178 * ___purchaseSucceed_4;
	// System.Action`3<System.String,UnityEngine.Purchasing.PurchaseFailureReason,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::purchaseFailed
	Action_3_t1784145519 * ___purchaseFailed_5;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<orderSuccess>5__1
	String_t* ___U3CorderSuccessU3E5__1_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<signstr>5__2
	String_t* ___U3CsignstrU3E5__2_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<sign>5__3
	String_t* ___U3CsignU3E5__3_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<param>5__4
	String_t* ___U3CparamU3E5__4_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<url>5__5
	String_t* ___U3CurlU3E5__5_11;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<pollingstr>5__6
	WWW_t3688466362 * ___U3CpollingstrU3E5__6_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<jsonPollingObjects>5__7
	Dictionary_2_t2865362463 * ___U3CjsonPollingObjectsU3E5__7_13;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<code>5__8
	String_t* ___U3CcodeU3E5__8_14;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<pollingValues>5__9
	Dictionary_2_t2865362463 * ___U3CpollingValuesU3E5__9_15;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<tradeSeq>5__10
	String_t* ___U3CtradeSeqU3E5__10_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<tradeState>5__11
	String_t* ___U3CtradeStateU3E5__11_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<productId>5__12
	String_t* ___U3CproductIdU3E5__12_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<Msg>5__13
	String_t* ___U3CMsgU3E5__13_19;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<StartPurchasePolling>d__23::<receipt>5__14
	String_t* ___U3CreceiptU3E5__14_20;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_authGlobal_2() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___authGlobal_2)); }
	inline String_t* get_authGlobal_2() const { return ___authGlobal_2; }
	inline String_t** get_address_of_authGlobal_2() { return &___authGlobal_2; }
	inline void set_authGlobal_2(String_t* value)
	{
		___authGlobal_2 = value;
		Il2CppCodeGenWriteBarrier((&___authGlobal_2), value);
	}

	inline static int32_t get_offset_of_transactionId_3() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___transactionId_3)); }
	inline String_t* get_transactionId_3() const { return ___transactionId_3; }
	inline String_t** get_address_of_transactionId_3() { return &___transactionId_3; }
	inline void set_transactionId_3(String_t* value)
	{
		___transactionId_3 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_3), value);
	}

	inline static int32_t get_offset_of_purchaseSucceed_4() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___purchaseSucceed_4)); }
	inline Action_3_t2617928178 * get_purchaseSucceed_4() const { return ___purchaseSucceed_4; }
	inline Action_3_t2617928178 ** get_address_of_purchaseSucceed_4() { return &___purchaseSucceed_4; }
	inline void set_purchaseSucceed_4(Action_3_t2617928178 * value)
	{
		___purchaseSucceed_4 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseSucceed_4), value);
	}

	inline static int32_t get_offset_of_purchaseFailed_5() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___purchaseFailed_5)); }
	inline Action_3_t1784145519 * get_purchaseFailed_5() const { return ___purchaseFailed_5; }
	inline Action_3_t1784145519 ** get_address_of_purchaseFailed_5() { return &___purchaseFailed_5; }
	inline void set_purchaseFailed_5(Action_3_t1784145519 * value)
	{
		___purchaseFailed_5 = value;
		Il2CppCodeGenWriteBarrier((&___purchaseFailed_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_6() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CU3E4__this_6)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_6() const { return ___U3CU3E4__this_6; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_6() { return &___U3CU3E4__this_6; }
	inline void set_U3CU3E4__this_6(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_6), value);
	}

	inline static int32_t get_offset_of_U3CorderSuccessU3E5__1_7() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CorderSuccessU3E5__1_7)); }
	inline String_t* get_U3CorderSuccessU3E5__1_7() const { return ___U3CorderSuccessU3E5__1_7; }
	inline String_t** get_address_of_U3CorderSuccessU3E5__1_7() { return &___U3CorderSuccessU3E5__1_7; }
	inline void set_U3CorderSuccessU3E5__1_7(String_t* value)
	{
		___U3CorderSuccessU3E5__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CorderSuccessU3E5__1_7), value);
	}

	inline static int32_t get_offset_of_U3CsignstrU3E5__2_8() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CsignstrU3E5__2_8)); }
	inline String_t* get_U3CsignstrU3E5__2_8() const { return ___U3CsignstrU3E5__2_8; }
	inline String_t** get_address_of_U3CsignstrU3E5__2_8() { return &___U3CsignstrU3E5__2_8; }
	inline void set_U3CsignstrU3E5__2_8(String_t* value)
	{
		___U3CsignstrU3E5__2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignstrU3E5__2_8), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_9() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CsignU3E5__3_9)); }
	inline String_t* get_U3CsignU3E5__3_9() const { return ___U3CsignU3E5__3_9; }
	inline String_t** get_address_of_U3CsignU3E5__3_9() { return &___U3CsignU3E5__3_9; }
	inline void set_U3CsignU3E5__3_9(String_t* value)
	{
		___U3CsignU3E5__3_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__3_9), value);
	}

	inline static int32_t get_offset_of_U3CparamU3E5__4_10() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CparamU3E5__4_10)); }
	inline String_t* get_U3CparamU3E5__4_10() const { return ___U3CparamU3E5__4_10; }
	inline String_t** get_address_of_U3CparamU3E5__4_10() { return &___U3CparamU3E5__4_10; }
	inline void set_U3CparamU3E5__4_10(String_t* value)
	{
		___U3CparamU3E5__4_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparamU3E5__4_10), value);
	}

	inline static int32_t get_offset_of_U3CurlU3E5__5_11() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CurlU3E5__5_11)); }
	inline String_t* get_U3CurlU3E5__5_11() const { return ___U3CurlU3E5__5_11; }
	inline String_t** get_address_of_U3CurlU3E5__5_11() { return &___U3CurlU3E5__5_11; }
	inline void set_U3CurlU3E5__5_11(String_t* value)
	{
		___U3CurlU3E5__5_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E5__5_11), value);
	}

	inline static int32_t get_offset_of_U3CpollingstrU3E5__6_12() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CpollingstrU3E5__6_12)); }
	inline WWW_t3688466362 * get_U3CpollingstrU3E5__6_12() const { return ___U3CpollingstrU3E5__6_12; }
	inline WWW_t3688466362 ** get_address_of_U3CpollingstrU3E5__6_12() { return &___U3CpollingstrU3E5__6_12; }
	inline void set_U3CpollingstrU3E5__6_12(WWW_t3688466362 * value)
	{
		___U3CpollingstrU3E5__6_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpollingstrU3E5__6_12), value);
	}

	inline static int32_t get_offset_of_U3CjsonPollingObjectsU3E5__7_13() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CjsonPollingObjectsU3E5__7_13)); }
	inline Dictionary_2_t2865362463 * get_U3CjsonPollingObjectsU3E5__7_13() const { return ___U3CjsonPollingObjectsU3E5__7_13; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CjsonPollingObjectsU3E5__7_13() { return &___U3CjsonPollingObjectsU3E5__7_13; }
	inline void set_U3CjsonPollingObjectsU3E5__7_13(Dictionary_2_t2865362463 * value)
	{
		___U3CjsonPollingObjectsU3E5__7_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonPollingObjectsU3E5__7_13), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__8_14() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CcodeU3E5__8_14)); }
	inline String_t* get_U3CcodeU3E5__8_14() const { return ___U3CcodeU3E5__8_14; }
	inline String_t** get_address_of_U3CcodeU3E5__8_14() { return &___U3CcodeU3E5__8_14; }
	inline void set_U3CcodeU3E5__8_14(String_t* value)
	{
		___U3CcodeU3E5__8_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__8_14), value);
	}

	inline static int32_t get_offset_of_U3CpollingValuesU3E5__9_15() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CpollingValuesU3E5__9_15)); }
	inline Dictionary_2_t2865362463 * get_U3CpollingValuesU3E5__9_15() const { return ___U3CpollingValuesU3E5__9_15; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CpollingValuesU3E5__9_15() { return &___U3CpollingValuesU3E5__9_15; }
	inline void set_U3CpollingValuesU3E5__9_15(Dictionary_2_t2865362463 * value)
	{
		___U3CpollingValuesU3E5__9_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpollingValuesU3E5__9_15), value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__10_16() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CtradeSeqU3E5__10_16)); }
	inline String_t* get_U3CtradeSeqU3E5__10_16() const { return ___U3CtradeSeqU3E5__10_16; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__10_16() { return &___U3CtradeSeqU3E5__10_16; }
	inline void set_U3CtradeSeqU3E5__10_16(String_t* value)
	{
		___U3CtradeSeqU3E5__10_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeSeqU3E5__10_16), value);
	}

	inline static int32_t get_offset_of_U3CtradeStateU3E5__11_17() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CtradeStateU3E5__11_17)); }
	inline String_t* get_U3CtradeStateU3E5__11_17() const { return ___U3CtradeStateU3E5__11_17; }
	inline String_t** get_address_of_U3CtradeStateU3E5__11_17() { return &___U3CtradeStateU3E5__11_17; }
	inline void set_U3CtradeStateU3E5__11_17(String_t* value)
	{
		___U3CtradeStateU3E5__11_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeStateU3E5__11_17), value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__12_18() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CproductIdU3E5__12_18)); }
	inline String_t* get_U3CproductIdU3E5__12_18() const { return ___U3CproductIdU3E5__12_18; }
	inline String_t** get_address_of_U3CproductIdU3E5__12_18() { return &___U3CproductIdU3E5__12_18; }
	inline void set_U3CproductIdU3E5__12_18(String_t* value)
	{
		___U3CproductIdU3E5__12_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIdU3E5__12_18), value);
	}

	inline static int32_t get_offset_of_U3CMsgU3E5__13_19() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CMsgU3E5__13_19)); }
	inline String_t* get_U3CMsgU3E5__13_19() const { return ___U3CMsgU3E5__13_19; }
	inline String_t** get_address_of_U3CMsgU3E5__13_19() { return &___U3CMsgU3E5__13_19; }
	inline void set_U3CMsgU3E5__13_19(String_t* value)
	{
		___U3CMsgU3E5__13_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMsgU3E5__13_19), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__14_20() { return static_cast<int32_t>(offsetof(U3CStartPurchasePollingU3Ed__23_t3243852457, ___U3CreceiptU3E5__14_20)); }
	inline String_t* get_U3CreceiptU3E5__14_20() const { return ___U3CreceiptU3E5__14_20; }
	inline String_t** get_address_of_U3CreceiptU3E5__14_20() { return &___U3CreceiptU3E5__14_20; }
	inline void set_U3CreceiptU3E5__14_20(String_t* value)
	{
		___U3CreceiptU3E5__14_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3E5__14_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTPURCHASEPOLLINGU3ED__23_T3243852457_H
#ifndef U3CVAILDATEPRODUCTU3ED__13_T3921323125_H
#define U3CVAILDATEPRODUCTU3ED__13_T3921323125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13
struct  U3CVaildateProductU3Ed__13_t3921323125  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::appkey
	String_t* ___appkey_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::productInfo
	String_t* ___productInfo_3;
	// System.Action`2<System.Boolean,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::result
	Action_2_t1290832230 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_5;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<sign>5__1
	String_t* ___U3CsignU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<wf>5__2
	WWWForm_t4064702195 * ___U3CwfU3E5__2_7;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<VaildateProduct>d__13::<w>5__3
	WWW_t3688466362 * ___U3CwU3E5__3_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_appkey_2() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___appkey_2)); }
	inline String_t* get_appkey_2() const { return ___appkey_2; }
	inline String_t** get_address_of_appkey_2() { return &___appkey_2; }
	inline void set_appkey_2(String_t* value)
	{
		___appkey_2 = value;
		Il2CppCodeGenWriteBarrier((&___appkey_2), value);
	}

	inline static int32_t get_offset_of_productInfo_3() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___productInfo_3)); }
	inline String_t* get_productInfo_3() const { return ___productInfo_3; }
	inline String_t** get_address_of_productInfo_3() { return &___productInfo_3; }
	inline void set_productInfo_3(String_t* value)
	{
		___productInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___productInfo_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___result_4)); }
	inline Action_2_t1290832230 * get_result_4() const { return ___result_4; }
	inline Action_2_t1290832230 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_2_t1290832230 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___U3CsignU3E5__1_6)); }
	inline String_t* get_U3CsignU3E5__1_6() const { return ___U3CsignU3E5__1_6; }
	inline String_t** get_address_of_U3CsignU3E5__1_6() { return &___U3CsignU3E5__1_6; }
	inline void set_U3CsignU3E5__1_6(String_t* value)
	{
		___U3CsignU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t4064702195 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t4064702195 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CVaildateProductU3Ed__13_t3921323125, ___U3CwU3E5__3_8)); }
	inline WWW_t3688466362 * get_U3CwU3E5__3_8() const { return ___U3CwU3E5__3_8; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E5__3_8() { return &___U3CwU3E5__3_8; }
	inline void set_U3CwU3E5__3_8(WWW_t3688466362 * value)
	{
		___U3CwU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__3_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVAILDATEPRODUCTU3ED__13_T3921323125_H
#ifndef U3CVALIDATERECEIPTPROCESSU3ED__47_T372105418_H
#define U3CVALIDATERECEIPTPROCESSU3ED__47_T372105418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47
struct  U3CValidateReceiptProcessU3Ed__47_t372105418  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::transactionId
	String_t* ___transactionId_2;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::receipt
	String_t* ___receipt_3;
	// System.Action`3<System.String,UnityEngine.Purchasing.ValidateReceiptState,System.String> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::result
	Action_3_t3666713281 * ___result_4;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<tempJson>5__1
	Dictionary_2_t2865362463 * ___U3CtempJsonU3E5__1_6;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<wf>5__2
	WWWForm_t4064702195 * ___U3CwfU3E5__2_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<sign>5__3
	String_t* ___U3CsignU3E5__3_8;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<w>5__4
	WWW_t3688466362 * ___U3CwU3E5__4_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<jsonObjects>5__5
	Dictionary_2_t2865362463 * ___U3CjsonObjectsU3E5__5_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<code>5__6
	String_t* ___U3CcodeU3E5__6_11;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<ValidateReceiptProcess>d__47::<msg>5__7
	String_t* ___U3CmsgU3E5__7_12;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_transactionId_2() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___transactionId_2)); }
	inline String_t* get_transactionId_2() const { return ___transactionId_2; }
	inline String_t** get_address_of_transactionId_2() { return &___transactionId_2; }
	inline void set_transactionId_2(String_t* value)
	{
		___transactionId_2 = value;
		Il2CppCodeGenWriteBarrier((&___transactionId_2), value);
	}

	inline static int32_t get_offset_of_receipt_3() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___receipt_3)); }
	inline String_t* get_receipt_3() const { return ___receipt_3; }
	inline String_t** get_address_of_receipt_3() { return &___receipt_3; }
	inline void set_receipt_3(String_t* value)
	{
		___receipt_3 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_3), value);
	}

	inline static int32_t get_offset_of_result_4() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___result_4)); }
	inline Action_3_t3666713281 * get_result_4() const { return ___result_4; }
	inline Action_3_t3666713281 ** get_address_of_result_4() { return &___result_4; }
	inline void set_result_4(Action_3_t3666713281 * value)
	{
		___result_4 = value;
		Il2CppCodeGenWriteBarrier((&___result_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CU3E4__this_5)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CtempJsonU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CtempJsonU3E5__1_6)); }
	inline Dictionary_2_t2865362463 * get_U3CtempJsonU3E5__1_6() const { return ___U3CtempJsonU3E5__1_6; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CtempJsonU3E5__1_6() { return &___U3CtempJsonU3E5__1_6; }
	inline void set_U3CtempJsonU3E5__1_6(Dictionary_2_t2865362463 * value)
	{
		___U3CtempJsonU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtempJsonU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CwfU3E5__2_7)); }
	inline WWWForm_t4064702195 * get_U3CwfU3E5__2_7() const { return ___U3CwfU3E5__2_7; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwfU3E5__2_7() { return &___U3CwfU3E5__2_7; }
	inline void set_U3CwfU3E5__2_7(WWWForm_t4064702195 * value)
	{
		___U3CwfU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CsignU3E5__3_8)); }
	inline String_t* get_U3CsignU3E5__3_8() const { return ___U3CsignU3E5__3_8; }
	inline String_t** get_address_of_U3CsignU3E5__3_8() { return &___U3CsignU3E5__3_8; }
	inline void set_U3CsignU3E5__3_8(String_t* value)
	{
		___U3CsignU3E5__3_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__3_8), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__4_9() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CwU3E5__4_9)); }
	inline WWW_t3688466362 * get_U3CwU3E5__4_9() const { return ___U3CwU3E5__4_9; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E5__4_9() { return &___U3CwU3E5__4_9; }
	inline void set_U3CwU3E5__4_9(WWW_t3688466362 * value)
	{
		___U3CwU3E5__4_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__4_9), value);
	}

	inline static int32_t get_offset_of_U3CjsonObjectsU3E5__5_10() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CjsonObjectsU3E5__5_10)); }
	inline Dictionary_2_t2865362463 * get_U3CjsonObjectsU3E5__5_10() const { return ___U3CjsonObjectsU3E5__5_10; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CjsonObjectsU3E5__5_10() { return &___U3CjsonObjectsU3E5__5_10; }
	inline void set_U3CjsonObjectsU3E5__5_10(Dictionary_2_t2865362463 * value)
	{
		___U3CjsonObjectsU3E5__5_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonObjectsU3E5__5_10), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__6_11() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CcodeU3E5__6_11)); }
	inline String_t* get_U3CcodeU3E5__6_11() const { return ___U3CcodeU3E5__6_11; }
	inline String_t** get_address_of_U3CcodeU3E5__6_11() { return &___U3CcodeU3E5__6_11; }
	inline void set_U3CcodeU3E5__6_11(String_t* value)
	{
		___U3CcodeU3E5__6_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__6_11), value);
	}

	inline static int32_t get_offset_of_U3CmsgU3E5__7_12() { return static_cast<int32_t>(offsetof(U3CValidateReceiptProcessU3Ed__47_t372105418, ___U3CmsgU3E5__7_12)); }
	inline String_t* get_U3CmsgU3E5__7_12() const { return ___U3CmsgU3E5__7_12; }
	inline String_t** get_address_of_U3CmsgU3E5__7_12() { return &___U3CmsgU3E5__7_12; }
	inline void set_U3CmsgU3E5__7_12(String_t* value)
	{
		___U3CmsgU3E5__7_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmsgU3E5__7_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CVALIDATERECEIPTPROCESSU3ED__47_T372105418_H
#ifndef PAYMETHOD_T328949237_H
#define PAYMETHOD_T328949237_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.PayMethod
struct  PayMethod_t328949237  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYMETHOD_T328949237_H
#ifndef SCRIPTINGUNITYCALLBACK_T4035349519_H
#define SCRIPTINGUNITYCALLBACK_T4035349519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ScriptingUnityCallback
struct  ScriptingUnityCallback_t4035349519  : public RuntimeObject
{
public:
	// UnityEngine.Purchasing.IUnityCallback UnityEngine.Purchasing.ScriptingUnityCallback::forwardTo
	RuntimeObject* ___forwardTo_0;
	// Uniject.IUtil UnityEngine.Purchasing.ScriptingUnityCallback::util
	RuntimeObject* ___util_1;

public:
	inline static int32_t get_offset_of_forwardTo_0() { return static_cast<int32_t>(offsetof(ScriptingUnityCallback_t4035349519, ___forwardTo_0)); }
	inline RuntimeObject* get_forwardTo_0() const { return ___forwardTo_0; }
	inline RuntimeObject** get_address_of_forwardTo_0() { return &___forwardTo_0; }
	inline void set_forwardTo_0(RuntimeObject* value)
	{
		___forwardTo_0 = value;
		Il2CppCodeGenWriteBarrier((&___forwardTo_0), value);
	}

	inline static int32_t get_offset_of_util_1() { return static_cast<int32_t>(offsetof(ScriptingUnityCallback_t4035349519, ___util_1)); }
	inline RuntimeObject* get_util_1() const { return ___util_1; }
	inline RuntimeObject** get_address_of_util_1() { return &___util_1; }
	inline void set_util_1(RuntimeObject* value)
	{
		___util_1 = value;
		Il2CppCodeGenWriteBarrier((&___util_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGUNITYCALLBACK_T4035349519_H
#ifndef SERIALIZATIONEXTENSIONS_T4283915465_H
#define SERIALIZATIONEXTENSIONS_T4283915465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SerializationExtensions
struct  SerializationExtensions_t4283915465  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONEXTENSIONS_T4283915465_H
#ifndef BASEVERTEXEFFECT_T2675891272_H
#define BASEVERTEXEFFECT_T2675891272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseVertexEffect
struct  BaseVertexEffect_t2675891272  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVERTEXEFFECT_T2675891272_H
#ifndef U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#define U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0
struct  U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::rectTransform
	RectTransform_t3704657025 * ___rectTransform_0;
	// System.Object UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 UnityEngine.UI.LayoutGroup/<DelayedSetDirty>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_rectTransform_0() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___rectTransform_0)); }
	inline RectTransform_t3704657025 * get_rectTransform_0() const { return ___rectTransform_0; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_0() { return &___rectTransform_0; }
	inline void set_rectTransform_0(RectTransform_t3704657025 * value)
	{
		___rectTransform_0 = value;
		Il2CppCodeGenWriteBarrier((&___rectTransform_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEDSETDIRTYU3EC__ITERATOR0_T3170500204_H
#ifndef LAYOUTREBUILDER_T541313304_H
#define LAYOUTREBUILDER_T541313304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutRebuilder
struct  LayoutRebuilder_t541313304  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.LayoutRebuilder::m_ToRebuild
	RectTransform_t3704657025 * ___m_ToRebuild_0;
	// System.Int32 UnityEngine.UI.LayoutRebuilder::m_CachedHashFromTransform
	int32_t ___m_CachedHashFromTransform_1;

public:
	inline static int32_t get_offset_of_m_ToRebuild_0() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_ToRebuild_0)); }
	inline RectTransform_t3704657025 * get_m_ToRebuild_0() const { return ___m_ToRebuild_0; }
	inline RectTransform_t3704657025 ** get_address_of_m_ToRebuild_0() { return &___m_ToRebuild_0; }
	inline void set_m_ToRebuild_0(RectTransform_t3704657025 * value)
	{
		___m_ToRebuild_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_ToRebuild_0), value);
	}

	inline static int32_t get_offset_of_m_CachedHashFromTransform_1() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304, ___m_CachedHashFromTransform_1)); }
	inline int32_t get_m_CachedHashFromTransform_1() const { return ___m_CachedHashFromTransform_1; }
	inline int32_t* get_address_of_m_CachedHashFromTransform_1() { return &___m_CachedHashFromTransform_1; }
	inline void set_m_CachedHashFromTransform_1(int32_t value)
	{
		___m_CachedHashFromTransform_1 = value;
	}
};

struct LayoutRebuilder_t541313304_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder> UnityEngine.UI.LayoutRebuilder::s_Rebuilders
	ObjectPool_1_t240936516 * ___s_Rebuilders_2;
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.UI.LayoutRebuilder::<>f__mg$cache0
	ReapplyDrivenProperties_t1258266594 * ___U3CU3Ef__mgU24cache0_3;
	// System.Predicate`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache0
	Predicate_1_t2748928575 * ___U3CU3Ef__amU24cache0_4;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache1
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache1_5;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache2
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache2_6;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache3
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache3_7;
	// UnityEngine.Events.UnityAction`1<UnityEngine.Component> UnityEngine.UI.LayoutRebuilder::<>f__am$cache4
	UnityAction_1_t2508470592 * ___U3CU3Ef__amU24cache4_8;

public:
	inline static int32_t get_offset_of_s_Rebuilders_2() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___s_Rebuilders_2)); }
	inline ObjectPool_1_t240936516 * get_s_Rebuilders_2() const { return ___s_Rebuilders_2; }
	inline ObjectPool_1_t240936516 ** get_address_of_s_Rebuilders_2() { return &___s_Rebuilders_2; }
	inline void set_s_Rebuilders_2(ObjectPool_1_t240936516 * value)
	{
		___s_Rebuilders_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_Rebuilders_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_3() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__mgU24cache0_3)); }
	inline ReapplyDrivenProperties_t1258266594 * get_U3CU3Ef__mgU24cache0_3() const { return ___U3CU3Ef__mgU24cache0_3; }
	inline ReapplyDrivenProperties_t1258266594 ** get_address_of_U3CU3Ef__mgU24cache0_3() { return &___U3CU3Ef__mgU24cache0_3; }
	inline void set_U3CU3Ef__mgU24cache0_3(ReapplyDrivenProperties_t1258266594 * value)
	{
		___U3CU3Ef__mgU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Predicate_1_t2748928575 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Predicate_1_t2748928575 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Predicate_1_t2748928575 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_8() { return static_cast<int32_t>(offsetof(LayoutRebuilder_t541313304_StaticFields, ___U3CU3Ef__amU24cache4_8)); }
	inline UnityAction_1_t2508470592 * get_U3CU3Ef__amU24cache4_8() const { return ___U3CU3Ef__amU24cache4_8; }
	inline UnityAction_1_t2508470592 ** get_address_of_U3CU3Ef__amU24cache4_8() { return &___U3CU3Ef__amU24cache4_8; }
	inline void set_U3CU3Ef__amU24cache4_8(UnityAction_1_t2508470592 * value)
	{
		___U3CU3Ef__amU24cache4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTREBUILDER_T541313304_H
#ifndef LAYOUTUTILITY_T2745813735_H
#define LAYOUTUTILITY_T2745813735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutUtility
struct  LayoutUtility_t2745813735  : public RuntimeObject
{
public:

public:
};

struct LayoutUtility_t2745813735_StaticFields
{
public:
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache0
	Func_2_t235587086 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache1
	Func_2_t235587086 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache2
	Func_2_t235587086 * ___U3CU3Ef__amU24cache2_2;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache3
	Func_2_t235587086 * ___U3CU3Ef__amU24cache3_3;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache4
	Func_2_t235587086 * ___U3CU3Ef__amU24cache4_4;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache5
	Func_2_t235587086 * ___U3CU3Ef__amU24cache5_5;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache6
	Func_2_t235587086 * ___U3CU3Ef__amU24cache6_6;
	// System.Func`2<UnityEngine.UI.ILayoutElement,System.Single> UnityEngine.UI.LayoutUtility::<>f__am$cache7
	Func_2_t235587086 * ___U3CU3Ef__amU24cache7_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(LayoutUtility_t2745813735_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline Func_2_t235587086 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline Func_2_t235587086 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(Func_2_t235587086 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTUTILITY_T2745813735_H
#ifndef RECTANGULARVERTEXCLIPPER_T626611136_H
#define RECTANGULARVERTEXCLIPPER_T626611136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RectangularVertexClipper
struct  RectangularVertexClipper_t626611136  : public RuntimeObject
{
public:
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_WorldCorners
	Vector3U5BU5D_t1718750761* ___m_WorldCorners_0;
	// UnityEngine.Vector3[] UnityEngine.UI.RectangularVertexClipper::m_CanvasCorners
	Vector3U5BU5D_t1718750761* ___m_CanvasCorners_1;

public:
	inline static int32_t get_offset_of_m_WorldCorners_0() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t626611136, ___m_WorldCorners_0)); }
	inline Vector3U5BU5D_t1718750761* get_m_WorldCorners_0() const { return ___m_WorldCorners_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_WorldCorners_0() { return &___m_WorldCorners_0; }
	inline void set_m_WorldCorners_0(Vector3U5BU5D_t1718750761* value)
	{
		___m_WorldCorners_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_WorldCorners_0), value);
	}

	inline static int32_t get_offset_of_m_CanvasCorners_1() { return static_cast<int32_t>(offsetof(RectangularVertexClipper_t626611136, ___m_CanvasCorners_1)); }
	inline Vector3U5BU5D_t1718750761* get_m_CanvasCorners_1() const { return ___m_CanvasCorners_1; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_CanvasCorners_1() { return &___m_CanvasCorners_1; }
	inline void set_m_CanvasCorners_1(Vector3U5BU5D_t1718750761* value)
	{
		___m_CanvasCorners_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasCorners_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTANGULARVERTEXCLIPPER_T626611136_H
#ifndef REFLECTIONMETHODSCACHE_T2103211062_H
#define REFLECTIONMETHODSCACHE_T2103211062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache
struct  ReflectionMethodsCache_t2103211062  : public RuntimeObject
{
public:
	// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback UnityEngine.UI.ReflectionMethodsCache::raycast3D
	Raycast3DCallback_t701940803 * ___raycast3D_0;
	// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback UnityEngine.UI.ReflectionMethodsCache::raycast3DAll
	RaycastAllCallback_t1884415901 * ___raycast3DAll_1;
	// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback UnityEngine.UI.ReflectionMethodsCache::raycast2D
	Raycast2DCallback_t768590915 * ___raycast2D_2;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAll
	GetRayIntersectionAllCallback_t3913627115 * ___getRayIntersectionAll_3;
	// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRayIntersectionAllNonAlloc
	GetRayIntersectionAllNonAllocCallback_t2311174851 * ___getRayIntersectionAllNonAlloc_4;
	// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback UnityEngine.UI.ReflectionMethodsCache::getRaycastNonAlloc
	GetRaycastNonAllocCallback_t3841783507 * ___getRaycastNonAlloc_5;

public:
	inline static int32_t get_offset_of_raycast3D_0() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3D_0)); }
	inline Raycast3DCallback_t701940803 * get_raycast3D_0() const { return ___raycast3D_0; }
	inline Raycast3DCallback_t701940803 ** get_address_of_raycast3D_0() { return &___raycast3D_0; }
	inline void set_raycast3D_0(Raycast3DCallback_t701940803 * value)
	{
		___raycast3D_0 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3D_0), value);
	}

	inline static int32_t get_offset_of_raycast3DAll_1() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast3DAll_1)); }
	inline RaycastAllCallback_t1884415901 * get_raycast3DAll_1() const { return ___raycast3DAll_1; }
	inline RaycastAllCallback_t1884415901 ** get_address_of_raycast3DAll_1() { return &___raycast3DAll_1; }
	inline void set_raycast3DAll_1(RaycastAllCallback_t1884415901 * value)
	{
		___raycast3DAll_1 = value;
		Il2CppCodeGenWriteBarrier((&___raycast3DAll_1), value);
	}

	inline static int32_t get_offset_of_raycast2D_2() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___raycast2D_2)); }
	inline Raycast2DCallback_t768590915 * get_raycast2D_2() const { return ___raycast2D_2; }
	inline Raycast2DCallback_t768590915 ** get_address_of_raycast2D_2() { return &___raycast2D_2; }
	inline void set_raycast2D_2(Raycast2DCallback_t768590915 * value)
	{
		___raycast2D_2 = value;
		Il2CppCodeGenWriteBarrier((&___raycast2D_2), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAll_3() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAll_3)); }
	inline GetRayIntersectionAllCallback_t3913627115 * get_getRayIntersectionAll_3() const { return ___getRayIntersectionAll_3; }
	inline GetRayIntersectionAllCallback_t3913627115 ** get_address_of_getRayIntersectionAll_3() { return &___getRayIntersectionAll_3; }
	inline void set_getRayIntersectionAll_3(GetRayIntersectionAllCallback_t3913627115 * value)
	{
		___getRayIntersectionAll_3 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAll_3), value);
	}

	inline static int32_t get_offset_of_getRayIntersectionAllNonAlloc_4() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRayIntersectionAllNonAlloc_4)); }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 * get_getRayIntersectionAllNonAlloc_4() const { return ___getRayIntersectionAllNonAlloc_4; }
	inline GetRayIntersectionAllNonAllocCallback_t2311174851 ** get_address_of_getRayIntersectionAllNonAlloc_4() { return &___getRayIntersectionAllNonAlloc_4; }
	inline void set_getRayIntersectionAllNonAlloc_4(GetRayIntersectionAllNonAllocCallback_t2311174851 * value)
	{
		___getRayIntersectionAllNonAlloc_4 = value;
		Il2CppCodeGenWriteBarrier((&___getRayIntersectionAllNonAlloc_4), value);
	}

	inline static int32_t get_offset_of_getRaycastNonAlloc_5() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062, ___getRaycastNonAlloc_5)); }
	inline GetRaycastNonAllocCallback_t3841783507 * get_getRaycastNonAlloc_5() const { return ___getRaycastNonAlloc_5; }
	inline GetRaycastNonAllocCallback_t3841783507 ** get_address_of_getRaycastNonAlloc_5() { return &___getRaycastNonAlloc_5; }
	inline void set_getRaycastNonAlloc_5(GetRaycastNonAllocCallback_t3841783507 * value)
	{
		___getRaycastNonAlloc_5 = value;
		Il2CppCodeGenWriteBarrier((&___getRaycastNonAlloc_5), value);
	}
};

struct ReflectionMethodsCache_t2103211062_StaticFields
{
public:
	// UnityEngine.UI.ReflectionMethodsCache UnityEngine.UI.ReflectionMethodsCache::s_ReflectionMethodsCache
	ReflectionMethodsCache_t2103211062 * ___s_ReflectionMethodsCache_6;

public:
	inline static int32_t get_offset_of_s_ReflectionMethodsCache_6() { return static_cast<int32_t>(offsetof(ReflectionMethodsCache_t2103211062_StaticFields, ___s_ReflectionMethodsCache_6)); }
	inline ReflectionMethodsCache_t2103211062 * get_s_ReflectionMethodsCache_6() const { return ___s_ReflectionMethodsCache_6; }
	inline ReflectionMethodsCache_t2103211062 ** get_address_of_s_ReflectionMethodsCache_6() { return &___s_ReflectionMethodsCache_6; }
	inline void set_s_ReflectionMethodsCache_6(ReflectionMethodsCache_t2103211062 * value)
	{
		___s_ReflectionMethodsCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ReflectionMethodsCache_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONMETHODSCACHE_T2103211062_H
#ifndef U24ARRAYTYPEU3D12_T2488454196_H
#define U24ARRAYTYPEU3D12_T2488454196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454196 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454196__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454196_H
#ifndef ENUMERATOR_T2146457487_H
#define ENUMERATOR_T2146457487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct  Enumerator_t2146457487 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t257213610 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___l_0)); }
	inline List_1_t257213610 * get_l_0() const { return ___l_0; }
	inline List_1_t257213610 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t257213610 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t2146457487, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T2146457487_H
#ifndef DECIMAL_T2948259380_H
#define DECIMAL_T2948259380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t2948259380 
{
public:
	// System.UInt32 System.Decimal::flags
	uint32_t ___flags_5;
	// System.UInt32 System.Decimal::hi
	uint32_t ___hi_6;
	// System.UInt32 System.Decimal::lo
	uint32_t ___lo_7;
	// System.UInt32 System.Decimal::mid
	uint32_t ___mid_8;

public:
	inline static int32_t get_offset_of_flags_5() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___flags_5)); }
	inline uint32_t get_flags_5() const { return ___flags_5; }
	inline uint32_t* get_address_of_flags_5() { return &___flags_5; }
	inline void set_flags_5(uint32_t value)
	{
		___flags_5 = value;
	}

	inline static int32_t get_offset_of_hi_6() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___hi_6)); }
	inline uint32_t get_hi_6() const { return ___hi_6; }
	inline uint32_t* get_address_of_hi_6() { return &___hi_6; }
	inline void set_hi_6(uint32_t value)
	{
		___hi_6 = value;
	}

	inline static int32_t get_offset_of_lo_7() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___lo_7)); }
	inline uint32_t get_lo_7() const { return ___lo_7; }
	inline uint32_t* get_address_of_lo_7() { return &___lo_7; }
	inline void set_lo_7(uint32_t value)
	{
		___lo_7 = value;
	}

	inline static int32_t get_offset_of_mid_8() { return static_cast<int32_t>(offsetof(Decimal_t2948259380, ___mid_8)); }
	inline uint32_t get_mid_8() const { return ___mid_8; }
	inline uint32_t* get_address_of_mid_8() { return &___mid_8; }
	inline void set_mid_8(uint32_t value)
	{
		___mid_8 = value;
	}
};

struct Decimal_t2948259380_StaticFields
{
public:
	// System.Decimal System.Decimal::MinValue
	Decimal_t2948259380  ___MinValue_0;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t2948259380  ___MaxValue_1;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t2948259380  ___MinusOne_2;
	// System.Decimal System.Decimal::One
	Decimal_t2948259380  ___One_3;
	// System.Decimal System.Decimal::MaxValueDiv10
	Decimal_t2948259380  ___MaxValueDiv10_4;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinValue_0)); }
	inline Decimal_t2948259380  get_MinValue_0() const { return ___MinValue_0; }
	inline Decimal_t2948259380 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(Decimal_t2948259380  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValue_1)); }
	inline Decimal_t2948259380  get_MaxValue_1() const { return ___MaxValue_1; }
	inline Decimal_t2948259380 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(Decimal_t2948259380  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinusOne_2() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MinusOne_2)); }
	inline Decimal_t2948259380  get_MinusOne_2() const { return ___MinusOne_2; }
	inline Decimal_t2948259380 * get_address_of_MinusOne_2() { return &___MinusOne_2; }
	inline void set_MinusOne_2(Decimal_t2948259380  value)
	{
		___MinusOne_2 = value;
	}

	inline static int32_t get_offset_of_One_3() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___One_3)); }
	inline Decimal_t2948259380  get_One_3() const { return ___One_3; }
	inline Decimal_t2948259380 * get_address_of_One_3() { return &___One_3; }
	inline void set_One_3(Decimal_t2948259380  value)
	{
		___One_3 = value;
	}

	inline static int32_t get_offset_of_MaxValueDiv10_4() { return static_cast<int32_t>(offsetof(Decimal_t2948259380_StaticFields, ___MaxValueDiv10_4)); }
	inline Decimal_t2948259380  get_MaxValueDiv10_4() const { return ___MaxValueDiv10_4; }
	inline Decimal_t2948259380 * get_address_of_MaxValueDiv10_4() { return &___MaxValueDiv10_4; }
	inline void set_MaxValueDiv10_4(Decimal_t2948259380  value)
	{
		___MaxValueDiv10_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T2948259380_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_0;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_1;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_2;

public:
	inline static int32_t get_offset_of_MaxValue_0() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_0)); }
	inline TimeSpan_t881159249  get_MaxValue_0() const { return ___MaxValue_0; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_0() { return &___MaxValue_0; }
	inline void set_MaxValue_0(TimeSpan_t881159249  value)
	{
		___MaxValue_0 = value;
	}

	inline static int32_t get_offset_of_MinValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_1)); }
	inline TimeSpan_t881159249  get_MinValue_1() const { return ___MinValue_1; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_1() { return &___MinValue_1; }
	inline void set_MinValue_1(TimeSpan_t881159249  value)
	{
		___MinValue_1 = value;
	}

	inline static int32_t get_offset_of_Zero_2() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_2)); }
	inline TimeSpan_t881159249  get_Zero_2() const { return ___Zero_2; }
	inline TimeSpan_t881159249 * get_address_of_Zero_2() { return &___Zero_2; }
	inline void set_Zero_2(TimeSpan_t881159249  value)
	{
		___Zero_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#define DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t2562230146 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t2562230146__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRIVENRECTTRANSFORMTRACKER_T2562230146_H
#ifndef GOOGLEPLAYANDROIDJAVASTORE_T3169878644_H
#define GOOGLEPLAYANDROIDJAVASTORE_T3169878644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayAndroidJavaStore
struct  GooglePlayAndroidJavaStore_t3169878644  : public AndroidJavaStore_t1912360766
{
public:
	// Uniject.IUtil UnityEngine.Purchasing.GooglePlayAndroidJavaStore::m_Util
	RuntimeObject* ___m_Util_1;

public:
	inline static int32_t get_offset_of_m_Util_1() { return static_cast<int32_t>(offsetof(GooglePlayAndroidJavaStore_t3169878644, ___m_Util_1)); }
	inline RuntimeObject* get_m_Util_1() const { return ___m_Util_1; }
	inline RuntimeObject** get_address_of_m_Util_1() { return &___m_Util_1; }
	inline void set_m_Util_1(RuntimeObject* value)
	{
		___m_Util_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Util_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYANDROIDJAVASTORE_T3169878644_H
#ifndef GOOGLEPLAYSTORECALLBACK_T1082514000_H
#define GOOGLEPLAYSTORECALLBACK_T1082514000_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayStoreCallback
struct  GooglePlayStoreCallback_t1082514000  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.GooglePlayStoreCallback::callback
	Action_1_t269755560 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(GooglePlayStoreCallback_t1082514000, ___callback_0)); }
	inline Action_1_t269755560 * get_callback_0() const { return ___callback_0; }
	inline Action_1_t269755560 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Action_1_t269755560 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYSTORECALLBACK_T1082514000_H
#ifndef GOOGLEPLAYSTOREEXTENSIONS_T904424962_H
#define GOOGLEPLAYSTOREEXTENSIONS_T904424962_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.GooglePlayStoreExtensions
struct  GooglePlayStoreExtensions_t904424962  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.GooglePlayStoreExtensions::m_Java
	AndroidJavaObject_t4131667876 * ___m_Java_0;

public:
	inline static int32_t get_offset_of_m_Java_0() { return static_cast<int32_t>(offsetof(GooglePlayStoreExtensions_t904424962, ___m_Java_0)); }
	inline AndroidJavaObject_t4131667876 * get_m_Java_0() const { return ___m_Java_0; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_Java_0() { return &___m_Java_0; }
	inline void set_m_Java_0(AndroidJavaObject_t4131667876 * value)
	{
		___m_Java_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Java_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GOOGLEPLAYSTOREEXTENSIONS_T904424962_H
#ifndef JAVABRIDGE_T3489274022_H
#define JAVABRIDGE_T3489274022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.JavaBridge
struct  JavaBridge_t3489274022  : public AndroidJavaProxy_t2835824643
{
public:
	// UnityEngine.Purchasing.IUnityCallback UnityEngine.Purchasing.JavaBridge::forwardTo
	RuntimeObject* ___forwardTo_0;

public:
	inline static int32_t get_offset_of_forwardTo_0() { return static_cast<int32_t>(offsetof(JavaBridge_t3489274022, ___forwardTo_0)); }
	inline RuntimeObject* get_forwardTo_0() const { return ___forwardTo_0; }
	inline RuntimeObject** get_address_of_forwardTo_0() { return &___forwardTo_0; }
	inline void set_forwardTo_0(RuntimeObject* value)
	{
		___forwardTo_0 = value;
		Il2CppCodeGenWriteBarrier((&___forwardTo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVABRIDGE_T3489274022_H
#ifndef SAMSUNGAPPSSTOREEXTENSIONS_T3605433812_H
#define SAMSUNGAPPSSTOREEXTENSIONS_T3605433812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SamsungAppsStoreExtensions
struct  SamsungAppsStoreExtensions_t3605433812  : public AndroidJavaProxy_t2835824643
{
public:
	// System.Action`1<System.Boolean> UnityEngine.Purchasing.SamsungAppsStoreExtensions::m_RestoreCallback
	Action_1_t269755560 * ___m_RestoreCallback_0;
	// UnityEngine.AndroidJavaObject UnityEngine.Purchasing.SamsungAppsStoreExtensions::m_Java
	AndroidJavaObject_t4131667876 * ___m_Java_1;

public:
	inline static int32_t get_offset_of_m_RestoreCallback_0() { return static_cast<int32_t>(offsetof(SamsungAppsStoreExtensions_t3605433812, ___m_RestoreCallback_0)); }
	inline Action_1_t269755560 * get_m_RestoreCallback_0() const { return ___m_RestoreCallback_0; }
	inline Action_1_t269755560 ** get_address_of_m_RestoreCallback_0() { return &___m_RestoreCallback_0; }
	inline void set_m_RestoreCallback_0(Action_1_t269755560 * value)
	{
		___m_RestoreCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_RestoreCallback_0), value);
	}

	inline static int32_t get_offset_of_m_Java_1() { return static_cast<int32_t>(offsetof(SamsungAppsStoreExtensions_t3605433812, ___m_Java_1)); }
	inline AndroidJavaObject_t4131667876 * get_m_Java_1() const { return ___m_Java_1; }
	inline AndroidJavaObject_t4131667876 ** get_address_of_m_Java_1() { return &___m_Java_1; }
	inline void set_m_Java_1(AndroidJavaObject_t4131667876 * value)
	{
		___m_Java_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Java_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMSUNGAPPSSTOREEXTENSIONS_T3605433812_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_5)); }
	inline Vector3_t3722313464  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t3722313464  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_6)); }
	inline Vector3_t3722313464  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t3722313464 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t3722313464  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_7)); }
	inline Vector3_t3722313464  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t3722313464 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t3722313464  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_8)); }
	inline Vector3_t3722313464  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t3722313464 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t3722313464  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_9)); }
	inline Vector3_t3722313464  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t3722313464 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t3722313464  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_10)); }
	inline Vector3_t3722313464  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t3722313464 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t3722313464  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_11)); }
	inline Vector3_t3722313464  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t3722313464  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_12)); }
	inline Vector3_t3722313464  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t3722313464 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t3722313464  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46
	U24ArrayTypeU3D12_t2488454196  ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0)); }
	inline U24ArrayTypeU3D12_t2488454196  get_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() const { return ___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline U24ArrayTypeU3D12_t2488454196 * get_address_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0() { return &___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0; }
	inline void set_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(U24ArrayTypeU3D12_t2488454196  value)
	{
		___U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CLOUDMOOLAHMODE_T3412107490_H
#define CLOUDMOOLAHMODE_T3412107490_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.CloudMoolahMode
struct  CloudMoolahMode_t3412107490 
{
public:
	// System.Int32 UnityEngine.Purchasing.CloudMoolahMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CloudMoolahMode_t3412107490, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDMOOLAHMODE_T3412107490_H
#ifndef WINPRODUCTDESCRIPTION_T2080881907_H
#define WINPRODUCTDESCRIPTION_T2080881907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Default.WinProductDescription
struct  WinProductDescription_t2080881907  : public RuntimeObject
{
public:
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<platformSpecificID>k__BackingField
	String_t* ___U3CplatformSpecificIDU3Ek__BackingField_0;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<price>k__BackingField
	String_t* ___U3CpriceU3Ek__BackingField_1;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<title>k__BackingField
	String_t* ___U3CtitleU3Ek__BackingField_2;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_3;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<ISOCurrencyCode>k__BackingField
	String_t* ___U3CISOCurrencyCodeU3Ek__BackingField_4;
	// System.Decimal UnityEngine.Purchasing.Default.WinProductDescription::<priceDecimal>k__BackingField
	Decimal_t2948259380  ___U3CpriceDecimalU3Ek__BackingField_5;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<receipt>k__BackingField
	String_t* ___U3CreceiptU3Ek__BackingField_6;
	// System.String UnityEngine.Purchasing.Default.WinProductDescription::<transactionID>k__BackingField
	String_t* ___U3CtransactionIDU3Ek__BackingField_7;
	// System.Boolean UnityEngine.Purchasing.Default.WinProductDescription::<consumable>k__BackingField
	bool ___U3CconsumableU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CplatformSpecificIDU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CplatformSpecificIDU3Ek__BackingField_0)); }
	inline String_t* get_U3CplatformSpecificIDU3Ek__BackingField_0() const { return ___U3CplatformSpecificIDU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CplatformSpecificIDU3Ek__BackingField_0() { return &___U3CplatformSpecificIDU3Ek__BackingField_0; }
	inline void set_U3CplatformSpecificIDU3Ek__BackingField_0(String_t* value)
	{
		___U3CplatformSpecificIDU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplatformSpecificIDU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CpriceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CpriceU3Ek__BackingField_1)); }
	inline String_t* get_U3CpriceU3Ek__BackingField_1() const { return ___U3CpriceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CpriceU3Ek__BackingField_1() { return &___U3CpriceU3Ek__BackingField_1; }
	inline void set_U3CpriceU3Ek__BackingField_1(String_t* value)
	{
		___U3CpriceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpriceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CtitleU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CtitleU3Ek__BackingField_2)); }
	inline String_t* get_U3CtitleU3Ek__BackingField_2() const { return ___U3CtitleU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CtitleU3Ek__BackingField_2() { return &___U3CtitleU3Ek__BackingField_2; }
	inline void set_U3CtitleU3Ek__BackingField_2(String_t* value)
	{
		___U3CtitleU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtitleU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CdescriptionU3Ek__BackingField_3)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_3() const { return ___U3CdescriptionU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_3() { return &___U3CdescriptionU3Ek__BackingField_3; }
	inline void set_U3CdescriptionU3Ek__BackingField_3(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescriptionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CISOCurrencyCodeU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CISOCurrencyCodeU3Ek__BackingField_4)); }
	inline String_t* get_U3CISOCurrencyCodeU3Ek__BackingField_4() const { return ___U3CISOCurrencyCodeU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CISOCurrencyCodeU3Ek__BackingField_4() { return &___U3CISOCurrencyCodeU3Ek__BackingField_4; }
	inline void set_U3CISOCurrencyCodeU3Ek__BackingField_4(String_t* value)
	{
		___U3CISOCurrencyCodeU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CISOCurrencyCodeU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CpriceDecimalU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CpriceDecimalU3Ek__BackingField_5)); }
	inline Decimal_t2948259380  get_U3CpriceDecimalU3Ek__BackingField_5() const { return ___U3CpriceDecimalU3Ek__BackingField_5; }
	inline Decimal_t2948259380 * get_address_of_U3CpriceDecimalU3Ek__BackingField_5() { return &___U3CpriceDecimalU3Ek__BackingField_5; }
	inline void set_U3CpriceDecimalU3Ek__BackingField_5(Decimal_t2948259380  value)
	{
		___U3CpriceDecimalU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CreceiptU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CreceiptU3Ek__BackingField_6)); }
	inline String_t* get_U3CreceiptU3Ek__BackingField_6() const { return ___U3CreceiptU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CreceiptU3Ek__BackingField_6() { return &___U3CreceiptU3Ek__BackingField_6; }
	inline void set_U3CreceiptU3Ek__BackingField_6(String_t* value)
	{
		___U3CreceiptU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CtransactionIDU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CtransactionIDU3Ek__BackingField_7)); }
	inline String_t* get_U3CtransactionIDU3Ek__BackingField_7() const { return ___U3CtransactionIDU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CtransactionIDU3Ek__BackingField_7() { return &___U3CtransactionIDU3Ek__BackingField_7; }
	inline void set_U3CtransactionIDU3Ek__BackingField_7(String_t* value)
	{
		___U3CtransactionIDU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtransactionIDU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CconsumableU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(WinProductDescription_t2080881907, ___U3CconsumableU3Ek__BackingField_8)); }
	inline bool get_U3CconsumableU3Ek__BackingField_8() const { return ___U3CconsumableU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CconsumableU3Ek__BackingField_8() { return &___U3CconsumableU3Ek__BackingField_8; }
	inline void set_U3CconsumableU3Ek__BackingField_8(bool value)
	{
		___U3CconsumableU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WINPRODUCTDESCRIPTION_T2080881907_H
#ifndef RESTORETRANSACTIONIDSTATE_T1240344018_H
#define RESTORETRANSACTIONIDSTATE_T1240344018_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.RestoreTransactionIDState
struct  RestoreTransactionIDState_t1240344018 
{
public:
	// System.Int32 UnityEngine.Purchasing.RestoreTransactionIDState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RestoreTransactionIDState_t1240344018, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTORETRANSACTIONIDSTATE_T1240344018_H
#ifndef SAMSUNGAPPSMODE_T3466750770_H
#define SAMSUNGAPPSMODE_T3466750770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.SamsungAppsMode
struct  SamsungAppsMode_t3466750770 
{
public:
	// System.Int32 UnityEngine.Purchasing.SamsungAppsMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SamsungAppsMode_t3466750770, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMSUNGAPPSMODE_T3466750770_H
#ifndef TRADESEQSTATE_T912436049_H
#define TRADESEQSTATE_T912436049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.TradeSeqState
struct  TradeSeqState_t912436049 
{
public:
	// System.Int32 UnityEngine.Purchasing.TradeSeqState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TradeSeqState_t912436049, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRADESEQSTATE_T912436049_H
#ifndef VALIDATERECEIPTSTATE_T3921901150_H
#define VALIDATERECEIPTSTATE_T3921901150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.ValidateReceiptState
struct  ValidateReceiptState_t3921901150 
{
public:
	// System.Int32 UnityEngine.Purchasing.ValidateReceiptState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ValidateReceiptState_t3921901150, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATERECEIPTSTATE_T3921901150_H
#ifndef RAY_T3785851493_H
#define RAY_T3785851493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray
struct  Ray_t3785851493 
{
public:
	// UnityEngine.Vector3 UnityEngine.Ray::m_Origin
	Vector3_t3722313464  ___m_Origin_0;
	// UnityEngine.Vector3 UnityEngine.Ray::m_Direction
	Vector3_t3722313464  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Origin_0)); }
	inline Vector3_t3722313464  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector3_t3722313464 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector3_t3722313464  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray_t3785851493, ___m_Direction_1)); }
	inline Vector3_t3722313464  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector3_t3722313464 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector3_t3722313464  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAY_T3785851493_H
#ifndef RAYCASTHIT_T1056001966_H
#define RAYCASTHIT_T1056001966_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t1056001966 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t3722313464  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t3722313464  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_t2156229523  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Point_0)); }
	inline Vector3_t3722313464  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t3722313464 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t3722313464  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Normal_1)); }
	inline Vector3_t3722313464  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t3722313464 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t3722313464  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_UV_4)); }
	inline Vector2_t2156229523  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_t2156229523 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_t2156229523  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t1056001966, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T1056001966_H
#ifndef RAYCASTHIT2D_T2279581989_H
#define RAYCASTHIT2D_T2279581989_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t2279581989 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t2156229523  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t2156229523  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t2156229523  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Centroid_0)); }
	inline Vector2_t2156229523  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t2156229523 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t2156229523  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Point_1)); }
	inline Vector2_t2156229523  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t2156229523 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t2156229523  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Normal_2)); }
	inline Vector2_t2156229523  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t2156229523 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t2156229523  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t2279581989, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT2D_T2279581989_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef ASPECTMODE_T3417192999_H
#define ASPECTMODE_T3417192999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter/AspectMode
struct  AspectMode_t3417192999 
{
public:
	// System.Int32 UnityEngine.UI.AspectRatioFitter/AspectMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AspectMode_t3417192999, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTMODE_T3417192999_H
#ifndef SCALEMODE_T2604066427_H
#define SCALEMODE_T2604066427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScaleMode
struct  ScaleMode_t2604066427 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScaleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScaleMode_t2604066427, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEMODE_T2604066427_H
#ifndef SCREENMATCHMODE_T3675272090_H
#define SCREENMATCHMODE_T3675272090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/ScreenMatchMode
struct  ScreenMatchMode_t3675272090 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/ScreenMatchMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenMatchMode_t3675272090, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMATCHMODE_T3675272090_H
#ifndef UNIT_T2218508340_H
#define UNIT_T2218508340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler/Unit
struct  Unit_t2218508340 
{
public:
	// System.Int32 UnityEngine.UI.CanvasScaler/Unit::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Unit_t2218508340, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIT_T2218508340_H
#ifndef FITMODE_T3267881214_H
#define FITMODE_T3267881214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter/FitMode
struct  FitMode_t3267881214 
{
public:
	// System.Int32 UnityEngine.UI.ContentSizeFitter/FitMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FitMode_t3267881214, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FITMODE_T3267881214_H
#ifndef AXIS_T3613393006_H
#define AXIS_T3613393006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Axis
struct  Axis_t3613393006 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Axis::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Axis_t3613393006, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXIS_T3613393006_H
#ifndef CONSTRAINT_T814224393_H
#define CONSTRAINT_T814224393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Constraint
struct  Constraint_t814224393 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Constraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Constraint_t814224393, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRAINT_T814224393_H
#ifndef CORNER_T1493259673_H
#define CORNER_T1493259673_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup/Corner
struct  Corner_t1493259673 
{
public:
	// System.Int32 UnityEngine.UI.GridLayoutGroup/Corner::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Corner_t1493259673, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CORNER_T1493259673_H
#ifndef VERTEXHELPER_T2453304189_H
#define VERTEXHELPER_T2453304189_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VertexHelper
struct  VertexHelper_t2453304189  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t899420910 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_t4072576034 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t3628304265 * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t3628304265 * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t3628304265 * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t3628304265 * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t899420910 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t496136383 * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t128053199 * ___m_Indices_8;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Positions_0)); }
	inline List_1_t899420910 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t899420910 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t899420910 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Positions_0), value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Colors_1)); }
	inline List_1_t4072576034 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_t4072576034 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_t4072576034 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Colors_1), value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv0S_2)); }
	inline List_1_t3628304265 * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t3628304265 ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t3628304265 * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv0S_2), value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv1S_3)); }
	inline List_1_t3628304265 * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t3628304265 ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t3628304265 * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv1S_3), value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv2S_4)); }
	inline List_1_t3628304265 * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t3628304265 ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t3628304265 * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv2S_4), value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Uv3S_5)); }
	inline List_1_t3628304265 * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t3628304265 ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t3628304265 * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Uv3S_5), value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Normals_6)); }
	inline List_1_t899420910 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t899420910 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t899420910 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normals_6), value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Tangents_7)); }
	inline List_1_t496136383 * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t496136383 ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t496136383 * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tangents_7), value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189, ___m_Indices_8)); }
	inline List_1_t128053199 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t128053199 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t128053199 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Indices_8), value);
	}
};

struct VertexHelper_t2453304189_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_t3319028937  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t3722313464  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t3319028937  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t3319028937 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t3319028937  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_t2453304189_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t3722313464  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t3722313464 * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t3722313464  value)
	{
		___s_DefaultNormal_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXHELPER_T2453304189_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T3017501136_H
#define U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T3017501136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45
struct  U3CRestoreTransactionIDProcessU3Ed__45_t3017501136  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Action`1<UnityEngine.Purchasing.RestoreTransactionIDState> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::result
	Action_1_t1412811613 * ___result_2;
	// UnityEngine.Purchasing.MoolahStoreImpl UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>4__this
	MoolahStoreImpl_t4045980644 * ___U3CU3E4__this_3;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<customID>5__1
	String_t* ___U3CcustomIDU3E5__1_4;
	// UnityEngine.WWWForm UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<wf>5__2
	WWWForm_t4064702195 * ___U3CwfU3E5__2_5;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<now>5__3
	DateTime_t3738529785  ___U3CnowU3E5__3_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<endDate>5__4
	String_t* ___U3CendDateU3E5__4_7;
	// System.DateTime UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<upperWeek>5__5
	DateTime_t3738529785  ___U3CupperWeekU3E5__5_8;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<startDate>5__6
	String_t* ___U3CstartDateU3E5__6_9;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<sign>5__7
	String_t* ___U3CsignU3E5__7_10;
	// UnityEngine.WWW UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<w>5__8
	WWW_t3688466362 * ___U3CwU3E5__8_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreObjects>5__9
	Dictionary_2_t2865362463 * ___U3CrestoreObjectsU3E5__9_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<code>5__10
	String_t* ___U3CcodeU3E5__10_13;
	// System.Collections.Generic.List`1<System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreValues>5__11
	List_1_t257213610 * ___U3CrestoreValuesU3E5__11_14;
	// System.Collections.Generic.List`1/Enumerator<System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<>s__12
	Enumerator_t2146457487  ___U3CU3Es__12_15;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<restoreObjectElem>5__13
	Dictionary_2_t2865362463 * ___U3CrestoreObjectElemU3E5__13_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<productId>5__14
	String_t* ___U3CproductIdU3E5__14_17;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<tradeSeq>5__15
	String_t* ___U3CtradeSeqU3E5__15_18;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl/<RestoreTransactionIDProcess>d__45::<receipt>5__16
	String_t* ___U3CreceiptU3E5__16_19;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_result_2() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___result_2)); }
	inline Action_1_t1412811613 * get_result_2() const { return ___result_2; }
	inline Action_1_t1412811613 ** get_address_of_result_2() { return &___result_2; }
	inline void set_result_2(Action_1_t1412811613 * value)
	{
		___result_2 = value;
		Il2CppCodeGenWriteBarrier((&___result_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3E4__this_3)); }
	inline MoolahStoreImpl_t4045980644 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline MoolahStoreImpl_t4045980644 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(MoolahStoreImpl_t4045980644 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CcustomIDU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CcustomIDU3E5__1_4)); }
	inline String_t* get_U3CcustomIDU3E5__1_4() const { return ___U3CcustomIDU3E5__1_4; }
	inline String_t** get_address_of_U3CcustomIDU3E5__1_4() { return &___U3CcustomIDU3E5__1_4; }
	inline void set_U3CcustomIDU3E5__1_4(String_t* value)
	{
		___U3CcustomIDU3E5__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcustomIDU3E5__1_4), value);
	}

	inline static int32_t get_offset_of_U3CwfU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CwfU3E5__2_5)); }
	inline WWWForm_t4064702195 * get_U3CwfU3E5__2_5() const { return ___U3CwfU3E5__2_5; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwfU3E5__2_5() { return &___U3CwfU3E5__2_5; }
	inline void set_U3CwfU3E5__2_5(WWWForm_t4064702195 * value)
	{
		___U3CwfU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwfU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CnowU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CnowU3E5__3_6)); }
	inline DateTime_t3738529785  get_U3CnowU3E5__3_6() const { return ___U3CnowU3E5__3_6; }
	inline DateTime_t3738529785 * get_address_of_U3CnowU3E5__3_6() { return &___U3CnowU3E5__3_6; }
	inline void set_U3CnowU3E5__3_6(DateTime_t3738529785  value)
	{
		___U3CnowU3E5__3_6 = value;
	}

	inline static int32_t get_offset_of_U3CendDateU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CendDateU3E5__4_7)); }
	inline String_t* get_U3CendDateU3E5__4_7() const { return ___U3CendDateU3E5__4_7; }
	inline String_t** get_address_of_U3CendDateU3E5__4_7() { return &___U3CendDateU3E5__4_7; }
	inline void set_U3CendDateU3E5__4_7(String_t* value)
	{
		___U3CendDateU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CendDateU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CupperWeekU3E5__5_8() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CupperWeekU3E5__5_8)); }
	inline DateTime_t3738529785  get_U3CupperWeekU3E5__5_8() const { return ___U3CupperWeekU3E5__5_8; }
	inline DateTime_t3738529785 * get_address_of_U3CupperWeekU3E5__5_8() { return &___U3CupperWeekU3E5__5_8; }
	inline void set_U3CupperWeekU3E5__5_8(DateTime_t3738529785  value)
	{
		___U3CupperWeekU3E5__5_8 = value;
	}

	inline static int32_t get_offset_of_U3CstartDateU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CstartDateU3E5__6_9)); }
	inline String_t* get_U3CstartDateU3E5__6_9() const { return ___U3CstartDateU3E5__6_9; }
	inline String_t** get_address_of_U3CstartDateU3E5__6_9() { return &___U3CstartDateU3E5__6_9; }
	inline void set_U3CstartDateU3E5__6_9(String_t* value)
	{
		___U3CstartDateU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstartDateU3E5__6_9), value);
	}

	inline static int32_t get_offset_of_U3CsignU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CsignU3E5__7_10)); }
	inline String_t* get_U3CsignU3E5__7_10() const { return ___U3CsignU3E5__7_10; }
	inline String_t** get_address_of_U3CsignU3E5__7_10() { return &___U3CsignU3E5__7_10; }
	inline void set_U3CsignU3E5__7_10(String_t* value)
	{
		___U3CsignU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsignU3E5__7_10), value);
	}

	inline static int32_t get_offset_of_U3CwU3E5__8_11() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CwU3E5__8_11)); }
	inline WWW_t3688466362 * get_U3CwU3E5__8_11() const { return ___U3CwU3E5__8_11; }
	inline WWW_t3688466362 ** get_address_of_U3CwU3E5__8_11() { return &___U3CwU3E5__8_11; }
	inline void set_U3CwU3E5__8_11(WWW_t3688466362 * value)
	{
		___U3CwU3E5__8_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwU3E5__8_11), value);
	}

	inline static int32_t get_offset_of_U3CrestoreObjectsU3E5__9_12() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CrestoreObjectsU3E5__9_12)); }
	inline Dictionary_2_t2865362463 * get_U3CrestoreObjectsU3E5__9_12() const { return ___U3CrestoreObjectsU3E5__9_12; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CrestoreObjectsU3E5__9_12() { return &___U3CrestoreObjectsU3E5__9_12; }
	inline void set_U3CrestoreObjectsU3E5__9_12(Dictionary_2_t2865362463 * value)
	{
		___U3CrestoreObjectsU3E5__9_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreObjectsU3E5__9_12), value);
	}

	inline static int32_t get_offset_of_U3CcodeU3E5__10_13() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CcodeU3E5__10_13)); }
	inline String_t* get_U3CcodeU3E5__10_13() const { return ___U3CcodeU3E5__10_13; }
	inline String_t** get_address_of_U3CcodeU3E5__10_13() { return &___U3CcodeU3E5__10_13; }
	inline void set_U3CcodeU3E5__10_13(String_t* value)
	{
		___U3CcodeU3E5__10_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcodeU3E5__10_13), value);
	}

	inline static int32_t get_offset_of_U3CrestoreValuesU3E5__11_14() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CrestoreValuesU3E5__11_14)); }
	inline List_1_t257213610 * get_U3CrestoreValuesU3E5__11_14() const { return ___U3CrestoreValuesU3E5__11_14; }
	inline List_1_t257213610 ** get_address_of_U3CrestoreValuesU3E5__11_14() { return &___U3CrestoreValuesU3E5__11_14; }
	inline void set_U3CrestoreValuesU3E5__11_14(List_1_t257213610 * value)
	{
		___U3CrestoreValuesU3E5__11_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreValuesU3E5__11_14), value);
	}

	inline static int32_t get_offset_of_U3CU3Es__12_15() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CU3Es__12_15)); }
	inline Enumerator_t2146457487  get_U3CU3Es__12_15() const { return ___U3CU3Es__12_15; }
	inline Enumerator_t2146457487 * get_address_of_U3CU3Es__12_15() { return &___U3CU3Es__12_15; }
	inline void set_U3CU3Es__12_15(Enumerator_t2146457487  value)
	{
		___U3CU3Es__12_15 = value;
	}

	inline static int32_t get_offset_of_U3CrestoreObjectElemU3E5__13_16() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CrestoreObjectElemU3E5__13_16)); }
	inline Dictionary_2_t2865362463 * get_U3CrestoreObjectElemU3E5__13_16() const { return ___U3CrestoreObjectElemU3E5__13_16; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CrestoreObjectElemU3E5__13_16() { return &___U3CrestoreObjectElemU3E5__13_16; }
	inline void set_U3CrestoreObjectElemU3E5__13_16(Dictionary_2_t2865362463 * value)
	{
		___U3CrestoreObjectElemU3E5__13_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrestoreObjectElemU3E5__13_16), value);
	}

	inline static int32_t get_offset_of_U3CproductIdU3E5__14_17() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CproductIdU3E5__14_17)); }
	inline String_t* get_U3CproductIdU3E5__14_17() const { return ___U3CproductIdU3E5__14_17; }
	inline String_t** get_address_of_U3CproductIdU3E5__14_17() { return &___U3CproductIdU3E5__14_17; }
	inline void set_U3CproductIdU3E5__14_17(String_t* value)
	{
		___U3CproductIdU3E5__14_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CproductIdU3E5__14_17), value);
	}

	inline static int32_t get_offset_of_U3CtradeSeqU3E5__15_18() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CtradeSeqU3E5__15_18)); }
	inline String_t* get_U3CtradeSeqU3E5__15_18() const { return ___U3CtradeSeqU3E5__15_18; }
	inline String_t** get_address_of_U3CtradeSeqU3E5__15_18() { return &___U3CtradeSeqU3E5__15_18; }
	inline void set_U3CtradeSeqU3E5__15_18(String_t* value)
	{
		___U3CtradeSeqU3E5__15_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtradeSeqU3E5__15_18), value);
	}

	inline static int32_t get_offset_of_U3CreceiptU3E5__16_19() { return static_cast<int32_t>(offsetof(U3CRestoreTransactionIDProcessU3Ed__45_t3017501136, ___U3CreceiptU3E5__16_19)); }
	inline String_t* get_U3CreceiptU3E5__16_19() const { return ___U3CreceiptU3E5__16_19; }
	inline String_t** get_address_of_U3CreceiptU3E5__16_19() { return &___U3CreceiptU3E5__16_19; }
	inline void set_U3CreceiptU3E5__16_19(String_t* value)
	{
		___U3CreceiptU3E5__16_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CreceiptU3E5__16_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTORETRANSACTIONIDPROCESSU3ED__45_T3017501136_H
#ifndef GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#define GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllCallback
struct  GetRayIntersectionAllCallback_t3913627115  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLCALLBACK_T3913627115_H
#ifndef GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#define GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRayIntersectionAllNonAllocCallback
struct  GetRayIntersectionAllNonAllocCallback_t2311174851  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYINTERSECTIONALLNONALLOCCALLBACK_T2311174851_H
#ifndef GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#define GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/GetRaycastNonAllocCallback
struct  GetRaycastNonAllocCallback_t3841783507  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETRAYCASTNONALLOCCALLBACK_T3841783507_H
#ifndef RAYCAST2DCALLBACK_T768590915_H
#define RAYCAST2DCALLBACK_T768590915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast2DCallback
struct  Raycast2DCallback_t768590915  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST2DCALLBACK_T768590915_H
#ifndef RAYCAST3DCALLBACK_T701940803_H
#define RAYCAST3DCALLBACK_T701940803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/Raycast3DCallback
struct  Raycast3DCallback_t701940803  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCAST3DCALLBACK_T701940803_H
#ifndef RAYCASTALLCALLBACK_T1884415901_H
#define RAYCASTALLCALLBACK_T1884415901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ReflectionMethodsCache/RaycastAllCallback
struct  RaycastAllCallback_t1884415901  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTALLCALLBACK_T1884415901_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef UIBEHAVIOUR_T3495933518_H
#define UIBEHAVIOUR_T3495933518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3495933518  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3495933518_H
#ifndef MOOLAHSTOREIMPL_T4045980644_H
#define MOOLAHSTOREIMPL_T4045980644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.MoolahStoreImpl
struct  MoolahStoreImpl_t4045980644  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Purchasing.Extension.IStoreCallback UnityEngine.Purchasing.MoolahStoreImpl::m_callback
	RuntimeObject* ___m_callback_9;
	// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl::isNeedPolling
	bool ___isNeedPolling_10;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_CurrentStoreProductID
	String_t* ___m_CurrentStoreProductID_11;
	// System.Boolean UnityEngine.Purchasing.MoolahStoreImpl::isRequestAuthCodeing
	bool ___isRequestAuthCodeing_12;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_appKey
	String_t* ___m_appKey_13;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_hashKey
	String_t* ___m_hashKey_14;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_notificationURL
	String_t* ___m_notificationURL_15;
	// UnityEngine.Purchasing.CloudMoolahMode UnityEngine.Purchasing.MoolahStoreImpl::m_mode
	int32_t ___m_mode_16;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::m_CustomerID
	String_t* ___m_CustomerID_17;

public:
	inline static int32_t get_offset_of_m_callback_9() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_callback_9)); }
	inline RuntimeObject* get_m_callback_9() const { return ___m_callback_9; }
	inline RuntimeObject** get_address_of_m_callback_9() { return &___m_callback_9; }
	inline void set_m_callback_9(RuntimeObject* value)
	{
		___m_callback_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_callback_9), value);
	}

	inline static int32_t get_offset_of_isNeedPolling_10() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___isNeedPolling_10)); }
	inline bool get_isNeedPolling_10() const { return ___isNeedPolling_10; }
	inline bool* get_address_of_isNeedPolling_10() { return &___isNeedPolling_10; }
	inline void set_isNeedPolling_10(bool value)
	{
		___isNeedPolling_10 = value;
	}

	inline static int32_t get_offset_of_m_CurrentStoreProductID_11() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_CurrentStoreProductID_11)); }
	inline String_t* get_m_CurrentStoreProductID_11() const { return ___m_CurrentStoreProductID_11; }
	inline String_t** get_address_of_m_CurrentStoreProductID_11() { return &___m_CurrentStoreProductID_11; }
	inline void set_m_CurrentStoreProductID_11(String_t* value)
	{
		___m_CurrentStoreProductID_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentStoreProductID_11), value);
	}

	inline static int32_t get_offset_of_isRequestAuthCodeing_12() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___isRequestAuthCodeing_12)); }
	inline bool get_isRequestAuthCodeing_12() const { return ___isRequestAuthCodeing_12; }
	inline bool* get_address_of_isRequestAuthCodeing_12() { return &___isRequestAuthCodeing_12; }
	inline void set_isRequestAuthCodeing_12(bool value)
	{
		___isRequestAuthCodeing_12 = value;
	}

	inline static int32_t get_offset_of_m_appKey_13() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_appKey_13)); }
	inline String_t* get_m_appKey_13() const { return ___m_appKey_13; }
	inline String_t** get_address_of_m_appKey_13() { return &___m_appKey_13; }
	inline void set_m_appKey_13(String_t* value)
	{
		___m_appKey_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_appKey_13), value);
	}

	inline static int32_t get_offset_of_m_hashKey_14() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_hashKey_14)); }
	inline String_t* get_m_hashKey_14() const { return ___m_hashKey_14; }
	inline String_t** get_address_of_m_hashKey_14() { return &___m_hashKey_14; }
	inline void set_m_hashKey_14(String_t* value)
	{
		___m_hashKey_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_hashKey_14), value);
	}

	inline static int32_t get_offset_of_m_notificationURL_15() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_notificationURL_15)); }
	inline String_t* get_m_notificationURL_15() const { return ___m_notificationURL_15; }
	inline String_t** get_address_of_m_notificationURL_15() { return &___m_notificationURL_15; }
	inline void set_m_notificationURL_15(String_t* value)
	{
		___m_notificationURL_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_notificationURL_15), value);
	}

	inline static int32_t get_offset_of_m_mode_16() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_mode_16)); }
	inline int32_t get_m_mode_16() const { return ___m_mode_16; }
	inline int32_t* get_address_of_m_mode_16() { return &___m_mode_16; }
	inline void set_m_mode_16(int32_t value)
	{
		___m_mode_16 = value;
	}

	inline static int32_t get_offset_of_m_CustomerID_17() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644, ___m_CustomerID_17)); }
	inline String_t* get_m_CustomerID_17() const { return ___m_CustomerID_17; }
	inline String_t** get_address_of_m_CustomerID_17() { return &___m_CustomerID_17; }
	inline void set_m_CustomerID_17(String_t* value)
	{
		___m_CustomerID_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomerID_17), value);
	}
};

struct MoolahStoreImpl_t4045980644_StaticFields
{
public:
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::pollingPath
	String_t* ___pollingPath_4;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestAuthCodePath
	String_t* ___requestAuthCodePath_5;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestRestoreTransactionUrl
	String_t* ___requestRestoreTransactionUrl_6;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestValidateReceiptUrl
	String_t* ___requestValidateReceiptUrl_7;
	// System.String UnityEngine.Purchasing.MoolahStoreImpl::requestProductValidateUrl
	String_t* ___requestProductValidateUrl_8;

public:
	inline static int32_t get_offset_of_pollingPath_4() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644_StaticFields, ___pollingPath_4)); }
	inline String_t* get_pollingPath_4() const { return ___pollingPath_4; }
	inline String_t** get_address_of_pollingPath_4() { return &___pollingPath_4; }
	inline void set_pollingPath_4(String_t* value)
	{
		___pollingPath_4 = value;
		Il2CppCodeGenWriteBarrier((&___pollingPath_4), value);
	}

	inline static int32_t get_offset_of_requestAuthCodePath_5() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644_StaticFields, ___requestAuthCodePath_5)); }
	inline String_t* get_requestAuthCodePath_5() const { return ___requestAuthCodePath_5; }
	inline String_t** get_address_of_requestAuthCodePath_5() { return &___requestAuthCodePath_5; }
	inline void set_requestAuthCodePath_5(String_t* value)
	{
		___requestAuthCodePath_5 = value;
		Il2CppCodeGenWriteBarrier((&___requestAuthCodePath_5), value);
	}

	inline static int32_t get_offset_of_requestRestoreTransactionUrl_6() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644_StaticFields, ___requestRestoreTransactionUrl_6)); }
	inline String_t* get_requestRestoreTransactionUrl_6() const { return ___requestRestoreTransactionUrl_6; }
	inline String_t** get_address_of_requestRestoreTransactionUrl_6() { return &___requestRestoreTransactionUrl_6; }
	inline void set_requestRestoreTransactionUrl_6(String_t* value)
	{
		___requestRestoreTransactionUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___requestRestoreTransactionUrl_6), value);
	}

	inline static int32_t get_offset_of_requestValidateReceiptUrl_7() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644_StaticFields, ___requestValidateReceiptUrl_7)); }
	inline String_t* get_requestValidateReceiptUrl_7() const { return ___requestValidateReceiptUrl_7; }
	inline String_t** get_address_of_requestValidateReceiptUrl_7() { return &___requestValidateReceiptUrl_7; }
	inline void set_requestValidateReceiptUrl_7(String_t* value)
	{
		___requestValidateReceiptUrl_7 = value;
		Il2CppCodeGenWriteBarrier((&___requestValidateReceiptUrl_7), value);
	}

	inline static int32_t get_offset_of_requestProductValidateUrl_8() { return static_cast<int32_t>(offsetof(MoolahStoreImpl_t4045980644_StaticFields, ___requestProductValidateUrl_8)); }
	inline String_t* get_requestProductValidateUrl_8() const { return ___requestProductValidateUrl_8; }
	inline String_t** get_address_of_requestProductValidateUrl_8() { return &___requestProductValidateUrl_8; }
	inline void set_requestProductValidateUrl_8(String_t* value)
	{
		___requestProductValidateUrl_8 = value;
		Il2CppCodeGenWriteBarrier((&___requestProductValidateUrl_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOOLAHSTOREIMPL_T4045980644_H
#ifndef ASPECTRATIOFITTER_T3312407083_H
#define ASPECTRATIOFITTER_T3312407083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.AspectRatioFitter
struct  AspectRatioFitter_t3312407083  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::m_AspectMode
	int32_t ___m_AspectMode_4;
	// System.Single UnityEngine.UI.AspectRatioFitter::m_AspectRatio
	float ___m_AspectRatio_5;
	// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::m_Rect
	RectTransform_t3704657025 * ___m_Rect_6;
	// System.Boolean UnityEngine.UI.AspectRatioFitter::m_DelayedSetDirty
	bool ___m_DelayedSetDirty_7;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.AspectRatioFitter::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_8;

public:
	inline static int32_t get_offset_of_m_AspectMode_4() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_AspectMode_4)); }
	inline int32_t get_m_AspectMode_4() const { return ___m_AspectMode_4; }
	inline int32_t* get_address_of_m_AspectMode_4() { return &___m_AspectMode_4; }
	inline void set_m_AspectMode_4(int32_t value)
	{
		___m_AspectMode_4 = value;
	}

	inline static int32_t get_offset_of_m_AspectRatio_5() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_AspectRatio_5)); }
	inline float get_m_AspectRatio_5() const { return ___m_AspectRatio_5; }
	inline float* get_address_of_m_AspectRatio_5() { return &___m_AspectRatio_5; }
	inline void set_m_AspectRatio_5(float value)
	{
		___m_AspectRatio_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_Rect_6)); }
	inline RectTransform_t3704657025 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t3704657025 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_DelayedSetDirty_7() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_DelayedSetDirty_7)); }
	inline bool get_m_DelayedSetDirty_7() const { return ___m_DelayedSetDirty_7; }
	inline bool* get_address_of_m_DelayedSetDirty_7() { return &___m_DelayedSetDirty_7; }
	inline void set_m_DelayedSetDirty_7(bool value)
	{
		___m_DelayedSetDirty_7 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_8() { return static_cast<int32_t>(offsetof(AspectRatioFitter_t3312407083, ___m_Tracker_8)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_8() const { return ___m_Tracker_8; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_8() { return &___m_Tracker_8; }
	inline void set_m_Tracker_8(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASPECTRATIOFITTER_T3312407083_H
#ifndef BASEMESHEFFECT_T2440176439_H
#define BASEMESHEFFECT_T2440176439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.BaseMeshEffect
struct  BaseMeshEffect_t2440176439  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::m_Graphic
	Graphic_t1660335611 * ___m_Graphic_4;

public:
	inline static int32_t get_offset_of_m_Graphic_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_t2440176439, ___m_Graphic_4)); }
	inline Graphic_t1660335611 * get_m_Graphic_4() const { return ___m_Graphic_4; }
	inline Graphic_t1660335611 ** get_address_of_m_Graphic_4() { return &___m_Graphic_4; }
	inline void set_m_Graphic_4(Graphic_t1660335611 * value)
	{
		___m_Graphic_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Graphic_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEMESHEFFECT_T2440176439_H
#ifndef CANVASSCALER_T2767979955_H
#define CANVASSCALER_T2767979955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.CanvasScaler
struct  CanvasScaler_t2767979955  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.CanvasScaler/ScaleMode UnityEngine.UI.CanvasScaler::m_UiScaleMode
	int32_t ___m_UiScaleMode_4;
	// System.Single UnityEngine.UI.CanvasScaler::m_ReferencePixelsPerUnit
	float ___m_ReferencePixelsPerUnit_5;
	// System.Single UnityEngine.UI.CanvasScaler::m_ScaleFactor
	float ___m_ScaleFactor_6;
	// UnityEngine.Vector2 UnityEngine.UI.CanvasScaler::m_ReferenceResolution
	Vector2_t2156229523  ___m_ReferenceResolution_7;
	// UnityEngine.UI.CanvasScaler/ScreenMatchMode UnityEngine.UI.CanvasScaler::m_ScreenMatchMode
	int32_t ___m_ScreenMatchMode_8;
	// System.Single UnityEngine.UI.CanvasScaler::m_MatchWidthOrHeight
	float ___m_MatchWidthOrHeight_9;
	// UnityEngine.UI.CanvasScaler/Unit UnityEngine.UI.CanvasScaler::m_PhysicalUnit
	int32_t ___m_PhysicalUnit_11;
	// System.Single UnityEngine.UI.CanvasScaler::m_FallbackScreenDPI
	float ___m_FallbackScreenDPI_12;
	// System.Single UnityEngine.UI.CanvasScaler::m_DefaultSpriteDPI
	float ___m_DefaultSpriteDPI_13;
	// System.Single UnityEngine.UI.CanvasScaler::m_DynamicPixelsPerUnit
	float ___m_DynamicPixelsPerUnit_14;
	// UnityEngine.Canvas UnityEngine.UI.CanvasScaler::m_Canvas
	Canvas_t3310196443 * ___m_Canvas_15;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevScaleFactor
	float ___m_PrevScaleFactor_16;
	// System.Single UnityEngine.UI.CanvasScaler::m_PrevReferencePixelsPerUnit
	float ___m_PrevReferencePixelsPerUnit_17;

public:
	inline static int32_t get_offset_of_m_UiScaleMode_4() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_UiScaleMode_4)); }
	inline int32_t get_m_UiScaleMode_4() const { return ___m_UiScaleMode_4; }
	inline int32_t* get_address_of_m_UiScaleMode_4() { return &___m_UiScaleMode_4; }
	inline void set_m_UiScaleMode_4(int32_t value)
	{
		___m_UiScaleMode_4 = value;
	}

	inline static int32_t get_offset_of_m_ReferencePixelsPerUnit_5() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ReferencePixelsPerUnit_5)); }
	inline float get_m_ReferencePixelsPerUnit_5() const { return ___m_ReferencePixelsPerUnit_5; }
	inline float* get_address_of_m_ReferencePixelsPerUnit_5() { return &___m_ReferencePixelsPerUnit_5; }
	inline void set_m_ReferencePixelsPerUnit_5(float value)
	{
		___m_ReferencePixelsPerUnit_5 = value;
	}

	inline static int32_t get_offset_of_m_ScaleFactor_6() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ScaleFactor_6)); }
	inline float get_m_ScaleFactor_6() const { return ___m_ScaleFactor_6; }
	inline float* get_address_of_m_ScaleFactor_6() { return &___m_ScaleFactor_6; }
	inline void set_m_ScaleFactor_6(float value)
	{
		___m_ScaleFactor_6 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceResolution_7() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ReferenceResolution_7)); }
	inline Vector2_t2156229523  get_m_ReferenceResolution_7() const { return ___m_ReferenceResolution_7; }
	inline Vector2_t2156229523 * get_address_of_m_ReferenceResolution_7() { return &___m_ReferenceResolution_7; }
	inline void set_m_ReferenceResolution_7(Vector2_t2156229523  value)
	{
		___m_ReferenceResolution_7 = value;
	}

	inline static int32_t get_offset_of_m_ScreenMatchMode_8() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_ScreenMatchMode_8)); }
	inline int32_t get_m_ScreenMatchMode_8() const { return ___m_ScreenMatchMode_8; }
	inline int32_t* get_address_of_m_ScreenMatchMode_8() { return &___m_ScreenMatchMode_8; }
	inline void set_m_ScreenMatchMode_8(int32_t value)
	{
		___m_ScreenMatchMode_8 = value;
	}

	inline static int32_t get_offset_of_m_MatchWidthOrHeight_9() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_MatchWidthOrHeight_9)); }
	inline float get_m_MatchWidthOrHeight_9() const { return ___m_MatchWidthOrHeight_9; }
	inline float* get_address_of_m_MatchWidthOrHeight_9() { return &___m_MatchWidthOrHeight_9; }
	inline void set_m_MatchWidthOrHeight_9(float value)
	{
		___m_MatchWidthOrHeight_9 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalUnit_11() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_PhysicalUnit_11)); }
	inline int32_t get_m_PhysicalUnit_11() const { return ___m_PhysicalUnit_11; }
	inline int32_t* get_address_of_m_PhysicalUnit_11() { return &___m_PhysicalUnit_11; }
	inline void set_m_PhysicalUnit_11(int32_t value)
	{
		___m_PhysicalUnit_11 = value;
	}

	inline static int32_t get_offset_of_m_FallbackScreenDPI_12() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_FallbackScreenDPI_12)); }
	inline float get_m_FallbackScreenDPI_12() const { return ___m_FallbackScreenDPI_12; }
	inline float* get_address_of_m_FallbackScreenDPI_12() { return &___m_FallbackScreenDPI_12; }
	inline void set_m_FallbackScreenDPI_12(float value)
	{
		___m_FallbackScreenDPI_12 = value;
	}

	inline static int32_t get_offset_of_m_DefaultSpriteDPI_13() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_DefaultSpriteDPI_13)); }
	inline float get_m_DefaultSpriteDPI_13() const { return ___m_DefaultSpriteDPI_13; }
	inline float* get_address_of_m_DefaultSpriteDPI_13() { return &___m_DefaultSpriteDPI_13; }
	inline void set_m_DefaultSpriteDPI_13(float value)
	{
		___m_DefaultSpriteDPI_13 = value;
	}

	inline static int32_t get_offset_of_m_DynamicPixelsPerUnit_14() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_DynamicPixelsPerUnit_14)); }
	inline float get_m_DynamicPixelsPerUnit_14() const { return ___m_DynamicPixelsPerUnit_14; }
	inline float* get_address_of_m_DynamicPixelsPerUnit_14() { return &___m_DynamicPixelsPerUnit_14; }
	inline void set_m_DynamicPixelsPerUnit_14(float value)
	{
		___m_DynamicPixelsPerUnit_14 = value;
	}

	inline static int32_t get_offset_of_m_Canvas_15() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_Canvas_15)); }
	inline Canvas_t3310196443 * get_m_Canvas_15() const { return ___m_Canvas_15; }
	inline Canvas_t3310196443 ** get_address_of_m_Canvas_15() { return &___m_Canvas_15; }
	inline void set_m_Canvas_15(Canvas_t3310196443 * value)
	{
		___m_Canvas_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_15), value);
	}

	inline static int32_t get_offset_of_m_PrevScaleFactor_16() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_PrevScaleFactor_16)); }
	inline float get_m_PrevScaleFactor_16() const { return ___m_PrevScaleFactor_16; }
	inline float* get_address_of_m_PrevScaleFactor_16() { return &___m_PrevScaleFactor_16; }
	inline void set_m_PrevScaleFactor_16(float value)
	{
		___m_PrevScaleFactor_16 = value;
	}

	inline static int32_t get_offset_of_m_PrevReferencePixelsPerUnit_17() { return static_cast<int32_t>(offsetof(CanvasScaler_t2767979955, ___m_PrevReferencePixelsPerUnit_17)); }
	inline float get_m_PrevReferencePixelsPerUnit_17() const { return ___m_PrevReferencePixelsPerUnit_17; }
	inline float* get_address_of_m_PrevReferencePixelsPerUnit_17() { return &___m_PrevReferencePixelsPerUnit_17; }
	inline void set_m_PrevReferencePixelsPerUnit_17(float value)
	{
		___m_PrevReferencePixelsPerUnit_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASSCALER_T2767979955_H
#ifndef CONTENTSIZEFITTER_T3850442145_H
#define CONTENTSIZEFITTER_T3850442145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.ContentSizeFitter
struct  ContentSizeFitter_t3850442145  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_HorizontalFit
	int32_t ___m_HorizontalFit_4;
	// UnityEngine.UI.ContentSizeFitter/FitMode UnityEngine.UI.ContentSizeFitter::m_VerticalFit
	int32_t ___m_VerticalFit_5;
	// UnityEngine.RectTransform UnityEngine.UI.ContentSizeFitter::m_Rect
	RectTransform_t3704657025 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.ContentSizeFitter::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_7;

public:
	inline static int32_t get_offset_of_m_HorizontalFit_4() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_HorizontalFit_4)); }
	inline int32_t get_m_HorizontalFit_4() const { return ___m_HorizontalFit_4; }
	inline int32_t* get_address_of_m_HorizontalFit_4() { return &___m_HorizontalFit_4; }
	inline void set_m_HorizontalFit_4(int32_t value)
	{
		___m_HorizontalFit_4 = value;
	}

	inline static int32_t get_offset_of_m_VerticalFit_5() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_VerticalFit_5)); }
	inline int32_t get_m_VerticalFit_5() const { return ___m_VerticalFit_5; }
	inline int32_t* get_address_of_m_VerticalFit_5() { return &___m_VerticalFit_5; }
	inline void set_m_VerticalFit_5(int32_t value)
	{
		___m_VerticalFit_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_Rect_6)); }
	inline RectTransform_t3704657025 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t3704657025 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(ContentSizeFitter_t3850442145, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTSIZEFITTER_T3850442145_H
#ifndef LAYOUTELEMENT_T1785403678_H
#define LAYOUTELEMENT_T1785403678_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutElement
struct  LayoutElement_t1785403678  : public UIBehaviour_t3495933518
{
public:
	// System.Boolean UnityEngine.UI.LayoutElement::m_IgnoreLayout
	bool ___m_IgnoreLayout_4;
	// System.Single UnityEngine.UI.LayoutElement::m_MinWidth
	float ___m_MinWidth_5;
	// System.Single UnityEngine.UI.LayoutElement::m_MinHeight
	float ___m_MinHeight_6;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredWidth
	float ___m_PreferredWidth_7;
	// System.Single UnityEngine.UI.LayoutElement::m_PreferredHeight
	float ___m_PreferredHeight_8;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleWidth
	float ___m_FlexibleWidth_9;
	// System.Single UnityEngine.UI.LayoutElement::m_FlexibleHeight
	float ___m_FlexibleHeight_10;
	// System.Int32 UnityEngine.UI.LayoutElement::m_LayoutPriority
	int32_t ___m_LayoutPriority_11;

public:
	inline static int32_t get_offset_of_m_IgnoreLayout_4() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_IgnoreLayout_4)); }
	inline bool get_m_IgnoreLayout_4() const { return ___m_IgnoreLayout_4; }
	inline bool* get_address_of_m_IgnoreLayout_4() { return &___m_IgnoreLayout_4; }
	inline void set_m_IgnoreLayout_4(bool value)
	{
		___m_IgnoreLayout_4 = value;
	}

	inline static int32_t get_offset_of_m_MinWidth_5() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_MinWidth_5)); }
	inline float get_m_MinWidth_5() const { return ___m_MinWidth_5; }
	inline float* get_address_of_m_MinWidth_5() { return &___m_MinWidth_5; }
	inline void set_m_MinWidth_5(float value)
	{
		___m_MinWidth_5 = value;
	}

	inline static int32_t get_offset_of_m_MinHeight_6() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_MinHeight_6)); }
	inline float get_m_MinHeight_6() const { return ___m_MinHeight_6; }
	inline float* get_address_of_m_MinHeight_6() { return &___m_MinHeight_6; }
	inline void set_m_MinHeight_6(float value)
	{
		___m_MinHeight_6 = value;
	}

	inline static int32_t get_offset_of_m_PreferredWidth_7() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_PreferredWidth_7)); }
	inline float get_m_PreferredWidth_7() const { return ___m_PreferredWidth_7; }
	inline float* get_address_of_m_PreferredWidth_7() { return &___m_PreferredWidth_7; }
	inline void set_m_PreferredWidth_7(float value)
	{
		___m_PreferredWidth_7 = value;
	}

	inline static int32_t get_offset_of_m_PreferredHeight_8() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_PreferredHeight_8)); }
	inline float get_m_PreferredHeight_8() const { return ___m_PreferredHeight_8; }
	inline float* get_address_of_m_PreferredHeight_8() { return &___m_PreferredHeight_8; }
	inline void set_m_PreferredHeight_8(float value)
	{
		___m_PreferredHeight_8 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleWidth_9() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_FlexibleWidth_9)); }
	inline float get_m_FlexibleWidth_9() const { return ___m_FlexibleWidth_9; }
	inline float* get_address_of_m_FlexibleWidth_9() { return &___m_FlexibleWidth_9; }
	inline void set_m_FlexibleWidth_9(float value)
	{
		___m_FlexibleWidth_9 = value;
	}

	inline static int32_t get_offset_of_m_FlexibleHeight_10() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_FlexibleHeight_10)); }
	inline float get_m_FlexibleHeight_10() const { return ___m_FlexibleHeight_10; }
	inline float* get_address_of_m_FlexibleHeight_10() { return &___m_FlexibleHeight_10; }
	inline void set_m_FlexibleHeight_10(float value)
	{
		___m_FlexibleHeight_10 = value;
	}

	inline static int32_t get_offset_of_m_LayoutPriority_11() { return static_cast<int32_t>(offsetof(LayoutElement_t1785403678, ___m_LayoutPriority_11)); }
	inline int32_t get_m_LayoutPriority_11() const { return ___m_LayoutPriority_11; }
	inline int32_t* get_address_of_m_LayoutPriority_11() { return &___m_LayoutPriority_11; }
	inline void set_m_LayoutPriority_11(int32_t value)
	{
		___m_LayoutPriority_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTELEMENT_T1785403678_H
#ifndef LAYOUTGROUP_T2436138090_H
#define LAYOUTGROUP_T2436138090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.LayoutGroup
struct  LayoutGroup_t2436138090  : public UIBehaviour_t3495933518
{
public:
	// UnityEngine.RectOffset UnityEngine.UI.LayoutGroup::m_Padding
	RectOffset_t1369453676 * ___m_Padding_4;
	// UnityEngine.TextAnchor UnityEngine.UI.LayoutGroup::m_ChildAlignment
	int32_t ___m_ChildAlignment_5;
	// UnityEngine.RectTransform UnityEngine.UI.LayoutGroup::m_Rect
	RectTransform_t3704657025 * ___m_Rect_6;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.LayoutGroup::m_Tracker
	DrivenRectTransformTracker_t2562230146  ___m_Tracker_7;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalMinSize
	Vector2_t2156229523  ___m_TotalMinSize_8;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalPreferredSize
	Vector2_t2156229523  ___m_TotalPreferredSize_9;
	// UnityEngine.Vector2 UnityEngine.UI.LayoutGroup::m_TotalFlexibleSize
	Vector2_t2156229523  ___m_TotalFlexibleSize_10;
	// System.Collections.Generic.List`1<UnityEngine.RectTransform> UnityEngine.UI.LayoutGroup::m_RectChildren
	List_1_t881764471 * ___m_RectChildren_11;

public:
	inline static int32_t get_offset_of_m_Padding_4() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Padding_4)); }
	inline RectOffset_t1369453676 * get_m_Padding_4() const { return ___m_Padding_4; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_4() { return &___m_Padding_4; }
	inline void set_m_Padding_4(RectOffset_t1369453676 * value)
	{
		___m_Padding_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_4), value);
	}

	inline static int32_t get_offset_of_m_ChildAlignment_5() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_ChildAlignment_5)); }
	inline int32_t get_m_ChildAlignment_5() const { return ___m_ChildAlignment_5; }
	inline int32_t* get_address_of_m_ChildAlignment_5() { return &___m_ChildAlignment_5; }
	inline void set_m_ChildAlignment_5(int32_t value)
	{
		___m_ChildAlignment_5 = value;
	}

	inline static int32_t get_offset_of_m_Rect_6() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Rect_6)); }
	inline RectTransform_t3704657025 * get_m_Rect_6() const { return ___m_Rect_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_Rect_6() { return &___m_Rect_6; }
	inline void set_m_Rect_6(RectTransform_t3704657025 * value)
	{
		___m_Rect_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rect_6), value);
	}

	inline static int32_t get_offset_of_m_Tracker_7() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_Tracker_7)); }
	inline DrivenRectTransformTracker_t2562230146  get_m_Tracker_7() const { return ___m_Tracker_7; }
	inline DrivenRectTransformTracker_t2562230146 * get_address_of_m_Tracker_7() { return &___m_Tracker_7; }
	inline void set_m_Tracker_7(DrivenRectTransformTracker_t2562230146  value)
	{
		___m_Tracker_7 = value;
	}

	inline static int32_t get_offset_of_m_TotalMinSize_8() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalMinSize_8)); }
	inline Vector2_t2156229523  get_m_TotalMinSize_8() const { return ___m_TotalMinSize_8; }
	inline Vector2_t2156229523 * get_address_of_m_TotalMinSize_8() { return &___m_TotalMinSize_8; }
	inline void set_m_TotalMinSize_8(Vector2_t2156229523  value)
	{
		___m_TotalMinSize_8 = value;
	}

	inline static int32_t get_offset_of_m_TotalPreferredSize_9() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalPreferredSize_9)); }
	inline Vector2_t2156229523  get_m_TotalPreferredSize_9() const { return ___m_TotalPreferredSize_9; }
	inline Vector2_t2156229523 * get_address_of_m_TotalPreferredSize_9() { return &___m_TotalPreferredSize_9; }
	inline void set_m_TotalPreferredSize_9(Vector2_t2156229523  value)
	{
		___m_TotalPreferredSize_9 = value;
	}

	inline static int32_t get_offset_of_m_TotalFlexibleSize_10() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_TotalFlexibleSize_10)); }
	inline Vector2_t2156229523  get_m_TotalFlexibleSize_10() const { return ___m_TotalFlexibleSize_10; }
	inline Vector2_t2156229523 * get_address_of_m_TotalFlexibleSize_10() { return &___m_TotalFlexibleSize_10; }
	inline void set_m_TotalFlexibleSize_10(Vector2_t2156229523  value)
	{
		___m_TotalFlexibleSize_10 = value;
	}

	inline static int32_t get_offset_of_m_RectChildren_11() { return static_cast<int32_t>(offsetof(LayoutGroup_t2436138090, ___m_RectChildren_11)); }
	inline List_1_t881764471 * get_m_RectChildren_11() const { return ___m_RectChildren_11; }
	inline List_1_t881764471 ** get_address_of_m_RectChildren_11() { return &___m_RectChildren_11; }
	inline void set_m_RectChildren_11(List_1_t881764471 * value)
	{
		___m_RectChildren_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectChildren_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYOUTGROUP_T2436138090_H
#ifndef GRIDLAYOUTGROUP_T3046220461_H
#define GRIDLAYOUTGROUP_T3046220461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.GridLayoutGroup
struct  GridLayoutGroup_t3046220461  : public LayoutGroup_t2436138090
{
public:
	// UnityEngine.UI.GridLayoutGroup/Corner UnityEngine.UI.GridLayoutGroup::m_StartCorner
	int32_t ___m_StartCorner_12;
	// UnityEngine.UI.GridLayoutGroup/Axis UnityEngine.UI.GridLayoutGroup::m_StartAxis
	int32_t ___m_StartAxis_13;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_CellSize
	Vector2_t2156229523  ___m_CellSize_14;
	// UnityEngine.Vector2 UnityEngine.UI.GridLayoutGroup::m_Spacing
	Vector2_t2156229523  ___m_Spacing_15;
	// UnityEngine.UI.GridLayoutGroup/Constraint UnityEngine.UI.GridLayoutGroup::m_Constraint
	int32_t ___m_Constraint_16;
	// System.Int32 UnityEngine.UI.GridLayoutGroup::m_ConstraintCount
	int32_t ___m_ConstraintCount_17;

public:
	inline static int32_t get_offset_of_m_StartCorner_12() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartCorner_12)); }
	inline int32_t get_m_StartCorner_12() const { return ___m_StartCorner_12; }
	inline int32_t* get_address_of_m_StartCorner_12() { return &___m_StartCorner_12; }
	inline void set_m_StartCorner_12(int32_t value)
	{
		___m_StartCorner_12 = value;
	}

	inline static int32_t get_offset_of_m_StartAxis_13() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_StartAxis_13)); }
	inline int32_t get_m_StartAxis_13() const { return ___m_StartAxis_13; }
	inline int32_t* get_address_of_m_StartAxis_13() { return &___m_StartAxis_13; }
	inline void set_m_StartAxis_13(int32_t value)
	{
		___m_StartAxis_13 = value;
	}

	inline static int32_t get_offset_of_m_CellSize_14() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_CellSize_14)); }
	inline Vector2_t2156229523  get_m_CellSize_14() const { return ___m_CellSize_14; }
	inline Vector2_t2156229523 * get_address_of_m_CellSize_14() { return &___m_CellSize_14; }
	inline void set_m_CellSize_14(Vector2_t2156229523  value)
	{
		___m_CellSize_14 = value;
	}

	inline static int32_t get_offset_of_m_Spacing_15() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Spacing_15)); }
	inline Vector2_t2156229523  get_m_Spacing_15() const { return ___m_Spacing_15; }
	inline Vector2_t2156229523 * get_address_of_m_Spacing_15() { return &___m_Spacing_15; }
	inline void set_m_Spacing_15(Vector2_t2156229523  value)
	{
		___m_Spacing_15 = value;
	}

	inline static int32_t get_offset_of_m_Constraint_16() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_Constraint_16)); }
	inline int32_t get_m_Constraint_16() const { return ___m_Constraint_16; }
	inline int32_t* get_address_of_m_Constraint_16() { return &___m_Constraint_16; }
	inline void set_m_Constraint_16(int32_t value)
	{
		___m_Constraint_16 = value;
	}

	inline static int32_t get_offset_of_m_ConstraintCount_17() { return static_cast<int32_t>(offsetof(GridLayoutGroup_t3046220461, ___m_ConstraintCount_17)); }
	inline int32_t get_m_ConstraintCount_17() const { return ___m_ConstraintCount_17; }
	inline int32_t* get_address_of_m_ConstraintCount_17() { return &___m_ConstraintCount_17; }
	inline void set_m_ConstraintCount_17(int32_t value)
	{
		___m_ConstraintCount_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRIDLAYOUTGROUP_T3046220461_H
#ifndef HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#define HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct  HorizontalOrVerticalLayoutGroup_t729725570  : public LayoutGroup_t2436138090
{
public:
	// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_Spacing
	float ___m_Spacing_12;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandWidth
	bool ___m_ChildForceExpandWidth_13;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildForceExpandHeight
	bool ___m_ChildForceExpandHeight_14;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlWidth
	bool ___m_ChildControlWidth_15;
	// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::m_ChildControlHeight
	bool ___m_ChildControlHeight_16;

public:
	inline static int32_t get_offset_of_m_Spacing_12() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_Spacing_12)); }
	inline float get_m_Spacing_12() const { return ___m_Spacing_12; }
	inline float* get_address_of_m_Spacing_12() { return &___m_Spacing_12; }
	inline void set_m_Spacing_12(float value)
	{
		___m_Spacing_12 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandWidth_13() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandWidth_13)); }
	inline bool get_m_ChildForceExpandWidth_13() const { return ___m_ChildForceExpandWidth_13; }
	inline bool* get_address_of_m_ChildForceExpandWidth_13() { return &___m_ChildForceExpandWidth_13; }
	inline void set_m_ChildForceExpandWidth_13(bool value)
	{
		___m_ChildForceExpandWidth_13 = value;
	}

	inline static int32_t get_offset_of_m_ChildForceExpandHeight_14() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildForceExpandHeight_14)); }
	inline bool get_m_ChildForceExpandHeight_14() const { return ___m_ChildForceExpandHeight_14; }
	inline bool* get_address_of_m_ChildForceExpandHeight_14() { return &___m_ChildForceExpandHeight_14; }
	inline void set_m_ChildForceExpandHeight_14(bool value)
	{
		___m_ChildForceExpandHeight_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlWidth_15() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlWidth_15)); }
	inline bool get_m_ChildControlWidth_15() const { return ___m_ChildControlWidth_15; }
	inline bool* get_address_of_m_ChildControlWidth_15() { return &___m_ChildControlWidth_15; }
	inline void set_m_ChildControlWidth_15(bool value)
	{
		___m_ChildControlWidth_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildControlHeight_16() { return static_cast<int32_t>(offsetof(HorizontalOrVerticalLayoutGroup_t729725570, ___m_ChildControlHeight_16)); }
	inline bool get_m_ChildControlHeight_16() const { return ___m_ChildControlHeight_16; }
	inline bool* get_address_of_m_ChildControlHeight_16() { return &___m_ChildControlHeight_16; }
	inline void set_m_ChildControlHeight_16(bool value)
	{
		___m_ChildControlHeight_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALORVERTICALLAYOUTGROUP_T729725570_H
#ifndef POSITIONASUV1_T3991086357_H
#define POSITIONASUV1_T3991086357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.PositionAsUV1
struct  PositionAsUV1_t3991086357  : public BaseMeshEffect_t2440176439
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONASUV1_T3991086357_H
#ifndef SHADOW_T773074319_H
#define SHADOW_T773074319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Shadow
struct  Shadow_t773074319  : public BaseMeshEffect_t2440176439
{
public:
	// UnityEngine.Color UnityEngine.UI.Shadow::m_EffectColor
	Color_t2555686324  ___m_EffectColor_5;
	// UnityEngine.Vector2 UnityEngine.UI.Shadow::m_EffectDistance
	Vector2_t2156229523  ___m_EffectDistance_6;
	// System.Boolean UnityEngine.UI.Shadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_7;

public:
	inline static int32_t get_offset_of_m_EffectColor_5() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectColor_5)); }
	inline Color_t2555686324  get_m_EffectColor_5() const { return ___m_EffectColor_5; }
	inline Color_t2555686324 * get_address_of_m_EffectColor_5() { return &___m_EffectColor_5; }
	inline void set_m_EffectColor_5(Color_t2555686324  value)
	{
		___m_EffectColor_5 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_6() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_EffectDistance_6)); }
	inline Vector2_t2156229523  get_m_EffectDistance_6() const { return ___m_EffectDistance_6; }
	inline Vector2_t2156229523 * get_address_of_m_EffectDistance_6() { return &___m_EffectDistance_6; }
	inline void set_m_EffectDistance_6(Vector2_t2156229523  value)
	{
		___m_EffectDistance_6 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_7() { return static_cast<int32_t>(offsetof(Shadow_t773074319, ___m_UseGraphicAlpha_7)); }
	inline bool get_m_UseGraphicAlpha_7() const { return ___m_UseGraphicAlpha_7; }
	inline bool* get_address_of_m_UseGraphicAlpha_7() { return &___m_UseGraphicAlpha_7; }
	inline void set_m_UseGraphicAlpha_7(bool value)
	{
		___m_UseGraphicAlpha_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOW_T773074319_H
#ifndef HORIZONTALLAYOUTGROUP_T2586782146_H
#define HORIZONTALLAYOUTGROUP_T2586782146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.HorizontalLayoutGroup
struct  HorizontalLayoutGroup_t2586782146  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALLAYOUTGROUP_T2586782146_H
#ifndef OUTLINE_T2536100125_H
#define OUTLINE_T2536100125_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Outline
struct  Outline_t2536100125  : public Shadow_t773074319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OUTLINE_T2536100125_H
#ifndef VERTICALLAYOUTGROUP_T923838031_H
#define VERTICALLAYOUTGROUP_T923838031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.VerticalLayoutGroup
struct  VerticalLayoutGroup_t923838031  : public HorizontalOrVerticalLayoutGroup_t729725570
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALLAYOUTGROUP_T923838031_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { sizeof (RectangularVertexClipper_t626611136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2202[2] = 
{
	RectangularVertexClipper_t626611136::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t626611136::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { sizeof (AspectRatioFitter_t3312407083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[5] = 
{
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectMode_4(),
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectRatio_5(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Rect_6(),
	AspectRatioFitter_t3312407083::get_offset_of_m_DelayedSetDirty_7(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Tracker_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { sizeof (AspectMode_t3417192999)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2204[6] = 
{
	AspectMode_t3417192999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { sizeof (CanvasScaler_t2767979955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[14] = 
{
	CanvasScaler_t2767979955::get_offset_of_m_UiScaleMode_4(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferencePixelsPerUnit_5(),
	CanvasScaler_t2767979955::get_offset_of_m_ScaleFactor_6(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferenceResolution_7(),
	CanvasScaler_t2767979955::get_offset_of_m_ScreenMatchMode_8(),
	CanvasScaler_t2767979955::get_offset_of_m_MatchWidthOrHeight_9(),
	0,
	CanvasScaler_t2767979955::get_offset_of_m_PhysicalUnit_11(),
	CanvasScaler_t2767979955::get_offset_of_m_FallbackScreenDPI_12(),
	CanvasScaler_t2767979955::get_offset_of_m_DefaultSpriteDPI_13(),
	CanvasScaler_t2767979955::get_offset_of_m_DynamicPixelsPerUnit_14(),
	CanvasScaler_t2767979955::get_offset_of_m_Canvas_15(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevScaleFactor_16(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevReferencePixelsPerUnit_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { sizeof (ScaleMode_t2604066427)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2206[4] = 
{
	ScaleMode_t2604066427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (ScreenMatchMode_t3675272090)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2207[4] = 
{
	ScreenMatchMode_t3675272090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Unit_t2218508340)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2208[6] = 
{
	Unit_t2218508340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { sizeof (ContentSizeFitter_t3850442145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2209[4] = 
{
	ContentSizeFitter_t3850442145::get_offset_of_m_HorizontalFit_4(),
	ContentSizeFitter_t3850442145::get_offset_of_m_VerticalFit_5(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Rect_6(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Tracker_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { sizeof (FitMode_t3267881214)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2210[4] = 
{
	FitMode_t3267881214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (GridLayoutGroup_t3046220461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2211[6] = 
{
	GridLayoutGroup_t3046220461::get_offset_of_m_StartCorner_12(),
	GridLayoutGroup_t3046220461::get_offset_of_m_StartAxis_13(),
	GridLayoutGroup_t3046220461::get_offset_of_m_CellSize_14(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Spacing_15(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Constraint_16(),
	GridLayoutGroup_t3046220461::get_offset_of_m_ConstraintCount_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (Corner_t1493259673)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2212[5] = 
{
	Corner_t1493259673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (Axis_t3613393006)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2213[3] = 
{
	Axis_t3613393006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (Constraint_t814224393)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2214[4] = 
{
	Constraint_t814224393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (HorizontalLayoutGroup_t2586782146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { sizeof (HorizontalOrVerticalLayoutGroup_t729725570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2216[5] = 
{
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_Spacing_12(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandWidth_13(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandHeight_14(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlWidth_15(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlHeight_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (LayoutElement_t1785403678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[8] = 
{
	LayoutElement_t1785403678::get_offset_of_m_IgnoreLayout_4(),
	LayoutElement_t1785403678::get_offset_of_m_MinWidth_5(),
	LayoutElement_t1785403678::get_offset_of_m_MinHeight_6(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredWidth_7(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredHeight_8(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleWidth_9(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleHeight_10(),
	LayoutElement_t1785403678::get_offset_of_m_LayoutPriority_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_4(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_5(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_6(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_9(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_10(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2224[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2225[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2226[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2232[7] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAllNonAlloc_4(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRaycastNonAlloc_5(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (GetRayIntersectionAllNonAllocCallback_t2311174851), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (GetRaycastNonAllocCallback_t3841783507), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2239[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2241[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_5(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_6(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_7(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2247[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (U3CModuleU3E_t692745554), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (Factory_t3975828591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (WinProductDescription_t2080881907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2253[9] = 
{
	WinProductDescription_t2080881907::get_offset_of_U3CplatformSpecificIDU3Ek__BackingField_0(),
	WinProductDescription_t2080881907::get_offset_of_U3CpriceU3Ek__BackingField_1(),
	WinProductDescription_t2080881907::get_offset_of_U3CtitleU3Ek__BackingField_2(),
	WinProductDescription_t2080881907::get_offset_of_U3CdescriptionU3Ek__BackingField_3(),
	WinProductDescription_t2080881907::get_offset_of_U3CISOCurrencyCodeU3Ek__BackingField_4(),
	WinProductDescription_t2080881907::get_offset_of_U3CpriceDecimalU3Ek__BackingField_5(),
	WinProductDescription_t2080881907::get_offset_of_U3CreceiptU3Ek__BackingField_6(),
	WinProductDescription_t2080881907::get_offset_of_U3CtransactionIDU3Ek__BackingField_7(),
	WinProductDescription_t2080881907::get_offset_of_U3CconsumableU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (U3CModuleU3E_t692745555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (AndroidJavaStore_t1912360766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2256[1] = 
{
	AndroidJavaStore_t1912360766::get_offset_of_m_Store_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (JavaBridge_t3489274022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	JavaBridge_t3489274022::get_offset_of_forwardTo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (SerializationExtensions_t4283915465), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2261 = { sizeof (JSONSerializer_t4060786200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2262 = { sizeof (ScriptingUnityCallback_t4035349519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2262[2] = 
{
	ScriptingUnityCallback_t4035349519::get_offset_of_forwardTo_0(),
	ScriptingUnityCallback_t4035349519::get_offset_of_util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2263 = { sizeof (AmazonAppStoreStoreExtensions_t4153153958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2263[1] = 
{
	AmazonAppStoreStoreExtensions_t4153153958::get_offset_of_android_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2264 = { sizeof (FakeAmazonExtensions_t2007213879), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2265 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2266 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2267 = { sizeof (FakeMoolahConfiguration_t983287713), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2267[2] = 
{
	FakeMoolahConfiguration_t983287713::get_offset_of_m_appKey_0(),
	FakeMoolahConfiguration_t983287713::get_offset_of_m_hashKey_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2268 = { sizeof (FakeMoolahExtensions_t4194088837), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2269 = { sizeof (CloudMoolahMode_t3412107490)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2269[4] = 
{
	CloudMoolahMode_t3412107490::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2270 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2271 = { sizeof (ValidateReceiptState_t3921901150)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2271[4] = 
{
	ValidateReceiptState_t3921901150::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2272 = { sizeof (RestoreTransactionIDState_t1240344018)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2272[5] = 
{
	RestoreTransactionIDState_t1240344018::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2273 = { sizeof (TradeSeqState_t912436049)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2273[7] = 
{
	TradeSeqState_t912436049::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2274 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2275 = { sizeof (MoolahStoreImpl_t4045980644), -1, sizeof(MoolahStoreImpl_t4045980644_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2275[14] = 
{
	MoolahStoreImpl_t4045980644_StaticFields::get_offset_of_pollingPath_4(),
	MoolahStoreImpl_t4045980644_StaticFields::get_offset_of_requestAuthCodePath_5(),
	MoolahStoreImpl_t4045980644_StaticFields::get_offset_of_requestRestoreTransactionUrl_6(),
	MoolahStoreImpl_t4045980644_StaticFields::get_offset_of_requestValidateReceiptUrl_7(),
	MoolahStoreImpl_t4045980644_StaticFields::get_offset_of_requestProductValidateUrl_8(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_callback_9(),
	MoolahStoreImpl_t4045980644::get_offset_of_isNeedPolling_10(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_CurrentStoreProductID_11(),
	MoolahStoreImpl_t4045980644::get_offset_of_isRequestAuthCodeing_12(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_appKey_13(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_hashKey_14(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_notificationURL_15(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_mode_16(),
	MoolahStoreImpl_t4045980644::get_offset_of_m_CustomerID_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2276 = { sizeof (U3CVaildateProductU3Ed__13_t3921323125), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2276[9] = 
{
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_U3CU3E1__state_0(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_U3CU3E2__current_1(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_appkey_2(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_productInfo_3(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_result_4(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_U3CU3E4__this_5(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_U3CsignU3E5__1_6(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_U3CwfU3E5__2_7(),
	U3CVaildateProductU3Ed__13_t3921323125::get_offset_of_U3CwU3E5__3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2277 = { sizeof (U3CU3Ec__DisplayClass18_0_t3759937156), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2277[3] = 
{
	U3CU3Ec__DisplayClass18_0_t3759937156::get_offset_of_purchaseSucceed_0(),
	U3CU3Ec__DisplayClass18_0_t3759937156::get_offset_of_purchaseFailed_1(),
	U3CU3Ec__DisplayClass18_0_t3759937156::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2278 = { sizeof (U3CRequestAuthCodeU3Ed__22_t4220939193), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2278[13] = 
{
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CU3E1__state_0(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CU3E2__current_1(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_wf_2(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_productID_3(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_transactionId_4(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_succeed_5(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_failed_6(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CU3E4__this_7(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CwU3E5__1_8(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CauthCodeResultU3E5__2_9(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CauthCodeValuesU3E5__3_10(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CauthCodeU3E5__4_11(),
	U3CRequestAuthCodeU3Ed__22_t4220939193::get_offset_of_U3CpaymentURLU3E5__5_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2279 = { sizeof (U3CStartPurchasePollingU3Ed__23_t3243852457), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2279[21] = 
{
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CU3E1__state_0(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CU3E2__current_1(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_authGlobal_2(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_transactionId_3(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_purchaseSucceed_4(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_purchaseFailed_5(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CU3E4__this_6(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CorderSuccessU3E5__1_7(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CsignstrU3E5__2_8(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CsignU3E5__3_9(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CparamU3E5__4_10(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CurlU3E5__5_11(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CpollingstrU3E5__6_12(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CjsonPollingObjectsU3E5__7_13(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CcodeU3E5__8_14(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CpollingValuesU3E5__9_15(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CtradeSeqU3E5__10_16(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CtradeStateU3E5__11_17(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CproductIdU3E5__12_18(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CMsgU3E5__13_19(),
	U3CStartPurchasePollingU3Ed__23_t3243852457::get_offset_of_U3CreceiptU3E5__14_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2280 = { sizeof (U3CRestoreTransactionIDProcessU3Ed__45_t3017501136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2280[20] = 
{
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3E1__state_0(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3E2__current_1(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_result_2(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3E4__this_3(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CcustomIDU3E5__1_4(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CwfU3E5__2_5(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CnowU3E5__3_6(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CendDateU3E5__4_7(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CupperWeekU3E5__5_8(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CstartDateU3E5__6_9(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CsignU3E5__7_10(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CwU3E5__8_11(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CrestoreObjectsU3E5__9_12(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CcodeU3E5__10_13(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CrestoreValuesU3E5__11_14(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CU3Es__12_15(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CrestoreObjectElemU3E5__13_16(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CproductIdU3E5__14_17(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CtradeSeqU3E5__15_18(),
	U3CRestoreTransactionIDProcessU3Ed__45_t3017501136::get_offset_of_U3CreceiptU3E5__16_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2281 = { sizeof (U3CValidateReceiptProcessU3Ed__47_t372105418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2281[13] = 
{
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CU3E1__state_0(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CU3E2__current_1(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_transactionId_2(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_receipt_3(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_result_4(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CU3E4__this_5(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CtempJsonU3E5__1_6(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CwfU3E5__2_7(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CsignU3E5__3_8(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CwU3E5__4_9(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CjsonObjectsU3E5__5_10(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CcodeU3E5__6_11(),
	U3CValidateReceiptProcessU3Ed__47_t372105418::get_offset_of_U3CmsgU3E5__7_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2282 = { sizeof (PayMethod_t328949237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2283 = { sizeof (FakeGooglePlayStoreExtensions_t2625986117), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2284 = { sizeof (GooglePlayAndroidJavaStore_t3169878644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2284[1] = 
{
	GooglePlayAndroidJavaStore_t3169878644::get_offset_of_m_Util_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2285 = { sizeof (FakeGooglePlayConfiguration_t3630139311), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2286 = { sizeof (GooglePlayStoreCallback_t1082514000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2286[1] = 
{
	GooglePlayStoreCallback_t1082514000::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2287 = { sizeof (GooglePlayStoreExtensions_t904424962), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2287[1] = 
{
	GooglePlayStoreExtensions_t904424962::get_offset_of_m_Java_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2288 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2289 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2290 = { sizeof (FakeSamsungAppsExtensions_t2880024647), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2291 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2292 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2293 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2294 = { sizeof (SamsungAppsMode_t3466750770)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2294[4] = 
{
	SamsungAppsMode_t3466750770::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2295 = { sizeof (SamsungAppsStoreExtensions_t3605433812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2295[2] = 
{
	SamsungAppsStoreExtensions_t3605433812::get_offset_of_m_RestoreCallback_0(),
	SamsungAppsStoreExtensions_t3605433812::get_offset_of_m_Java_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2296 = { sizeof (FakeUnityChannelConfiguration_t2647072263), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2296[1] = 
{
	FakeUnityChannelConfiguration_t2647072263::get_offset_of_U3CfetchReceiptPayloadOnPurchaseU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2297 = { sizeof (FakeUnityChannelExtensions_t3596584646), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2298 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2299 = { 0, -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
